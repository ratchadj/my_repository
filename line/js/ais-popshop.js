Number.prototype.formatMoney = function(c, d, t) {
    var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = (d == undefined ? "." : d),
            t = (t == undefined ? "," : t),
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

;
(function() {
    $.fn.customRadioCheck = function() {

        return this.each(function() {

            var $this = jQuery(this);
            var $span = jQuery('<span/>');

            $span.addClass('custom-' + ($this.is(':checkbox') ? 'check' : 'radio'));
            $this.is(':checked') && $span.addClass('checked'); // init
            $span.insertAfter($this);

            $this.parent('label').addClass('custom-label')
                    .attr('onclick', ''); // Fix clicking label in iOS
            // hide by shifting left
            $this.css({position: 'absolute', left: '-9999px'});

            // Events
            $this.on({
                change: function() {
                    if ($this.is(':radio')) {
                        $this.parent().siblings('label')
                                .find('.custom-radio').removeClass('checked');
                    }
                    $span.toggleClass('checked', $this.is(':checked'));
                },
                focus: function() {
                    $span.addClass('focus');
                },
                blur: function() {
                    $span.removeClass('focus');
                }
            });
        });
    };
}());

jQuery(document).ready(function() {

    jQuery('.product-tab').jQueryTab({
        initialTab: 1,
        tabInTransition: 'fadeIn',
        tabOutTransition: 'fadeOut',
        cookieName: 'active-product-tab'

    });

    jQuery('.thumbs img').click(function() {
        var thmb = this;
        var src = this.src;
        jQuery('.bottlesWrapper img').fadeOut(400, function() {
            thmb.src = this.src;
            jQuery(this).fadeIn(400)[0].src = src;
        });
    });

    jQuery('input[name="billing[payment]"]').click(function() {
        jQuery('div[id^="payment-"]').hide();
        jQuery('div[id^="payment-' + jQuery(this).val() + '""]').show();
    });

    overlayPosition();
    shippingAddress();


    jQuery(".province").change(function() {
        var region_id = jQuery(this).val();
        var isBilling = (this.id.indexOf('billing_') != -1) ? true : false;

        var obj = (isBilling) ? "#billing_postcode" : "#shipping_postcode";
        jQuery(obj).val('');

        var obj = (isBilling) ? "#billing_subdistrict" : "#billing_subdistrict";
        jQuery(obj).find('option').each(function() {
            if (jQuery(this).val() !== '') {
                jQuery(this).remove();
            }
        });
        ;

        obj = (isBilling) ? "#billing_district" : "#shipping_district";

        jQuery(obj).find('option').each(function() {
            if (jQuery(this).val() !== '') {
                jQuery(this).remove();
            }
        });

        for (var i in districts) {
            var district = districts[i];
            if (district.region_id == region_id) {
                jQuery(obj).append(jQuery("<option>", {
                    value: district.district_id,
                    text: district.district_name
                }));
            }
        }

        //jQuery(".district").heapbox("update");
    });

    jQuery(".district").change(function() {
        var district_id = jQuery(this).val();
        var isBilling = (this.id.indexOf('billing_') != -1) ? true : false;

        var obj = (isBilling) ? "#billing_postcode" : "#shipping_postcode";
        jQuery(obj).val('');

        var obj = (isBilling) ? "#billing_subdistrict" : "#shipping_subdistrict";

        jQuery(obj).find('option').each(function() {
            if (jQuery(this).val() !== '') {
                jQuery(this).remove();
            }
        });

        for (var i in subdistricts) {
            var subdistrict = subdistricts[i];
            if (subdistrict.district_id == district_id) {
                jQuery(obj).append(jQuery("<option>", {
                    value: subdistrict.subdistrict_id,
                    text: subdistrict.subdistrict_name
                }));
            }
        }

        //jQuery(".subdistrict").heapbox("update");
    });

    jQuery(".subdistrict").change(function() {
        var subdistrict_id = jQuery(this).val();
        var isBilling = (this.id.indexOf('billing_') != -1) ? true : false;
        var obj = (isBilling) ? "#billing_postcode" : "#shipping_postcode";
        jQuery(obj).val('');

        for (var i in subdistricts) {
            var subdistrict = subdistricts[i];
            if (subdistrict.subdistrict_id == subdistrict_id) {
                jQuery(obj).val(subdistrict.postcode);
            }
        }
    });

    jQuery('input[type=checkbox], input[type=radio]').customRadioCheck();
});

jQuery(window).resize(function() {
    overlayPosition();
});

var overlayPosition = function() {

    var top = (jQuery('#errorBox').height()/2) * (-1);
    jQuery("#errorBox").css("margin-top", top + "px");

    //top = (jQuery('#checkoutBox').height()/2) * (-1);
    jQuery("#checkoutBox").css("margin-top", "0px");
    jQuery("#checkoutBox").css("top", "10px");

    top = (jQuery('#aisPopup').height()/2) * (-1);
    jQuery("#aisPopup").css("margin-top", top + "px");

    top = (jQuery('#thankyouBox').height()/2) * (-1);
    jQuery("#thankyouBox").css("margin-top", top + "px");
};

var overlayClose = function(name) {
    jQuery('#overlayBlack').hide();
    jQuery('#close-bt').hide();
    jQuery(name).hide();
};

var overlayBt = function(name) {

    var url = 'http://store.ais.co.th/line/line.php?param=inventory';
    jQuery.ajax({
        url: url,
        type: 'get',
        dataType: 'jsonp',
        data: {'p': curProductId},
        jsonpCallback: 'getInventoryResult'
    });
};



var requestOtp = function() {

    jQuery("#opt-request").validate({
        rules: {
            telephone: {
                required: true,
                digits: true,
                maxlength: 10,
                minlength: 10
            }
        },
        debug: true
    });

    var valid = jQuery("#opt-request").valid();
    if (valid) {
        jQuery('#otpRequest').hide();
        jQuery('#otpLoading').show();
        jQuery("#overlayBlack").show();
        jQuery('#otpVerifyFail').hide();
        var phone = jQuery('#telephone').val();
        sendOptData(phone);
    } else {
        jQuery("#otp-request").after(jQuery("#telephone-error"));
    }
};

var requestOtp1 = function() {

    jQuery("#opt-verifyfail").validate({
        rules: {
            telephone1: {
                required: true,
                digits: true,
                maxlength: 10,
                minlength: 10
            }
        },
        debug: true
    });

    var valid = jQuery("#opt-verifyfail").valid();
    if (valid) {
        jQuery('#otpRequest').hide();
        jQuery('#otpLoading').show();
        jQuery("#overlayBlack").show();
        jQuery('#otpVerifyFail').hide();
        var phone = jQuery('#telephone1').val();
        sendOptData(phone);
    } else {
        jQuery("#otp-request1").after(jQuery("#telephone1-error"));
    }
};

var requestVerify = function() {

    jQuery("#opt-verify").validate({
        rules: {
            otp: {
                required: true,
                digits: true,
                maxlength: 4,
                minlength: 4
            }
        },
        debug: true
    });

    var valid = jQuery("#opt-verify").valid();
    if (valid) {
        jQuery('#otpVerify').hide();
        jQuery('#otpLoadingVerify').show();
        jQuery("#overlayBlack").show();
        sendOtpVerify();
    } else {
        jQuery("#send-otp").after(jQuery("#otp-error"));
    }

};
var otpSuccess = function() {
    jQuery('#otpLoadingVerify').hide();
    jQuery('#otpVerifySuccessful').show();
    jQuery("#overlayBlack").show();
    setTimeout(function() {
        jQuery('#aisPopup').hide();
        jQuery('#checkoutBox').show();
        jQuery("#overlayBlack").show();
        addProduct();
    }, 3000);
};

var otpFail = function() {
    jQuery('#otpLoadingVerify').hide();
    jQuery('#otpVerifyFail').show();
    jQuery("#overlayBlack").show();
};

var gotoCheckout = function() {
    jQuery('#aisPopup').hide();
    jQuery('#checkoutBox').show();
    jQuery("#overlayBlack").show();
    addProduct();
};

var closeCheckout = function() {
    jQuery('#overlayBlack').hide();
    jQuery('#checkoutBox').hide();
    jQuery("#placeorder-btn").removeAttr('disabled');
};

var shippingAddress = function() {
    jQuery('.check-address').change(function() {
        if (jQuery(this).is(':checked'))
        {
            jQuery('.shippingAddress').hide();
            copyAddress();
        } else {
            jQuery('.shippingAddress').show();
            jQuery("#overlayBlack").show();
        }

    });
};

var headertext = [],
        headers = document.querySelectorAll("#reviewOrder th"),
        tablerows = document.querySelectorAll("#reviewOrder th"),
        tablebody = document.querySelectorAll("#reviewOrder tbody");

for (var i = 0; i < headers.length; i++) {
    var current = headers[i];
    headertext.push(current.textContent.replace(/\r?\n|\r/, ""));
}

//alert(tablebody.length);

//for (var i = 0, row; row = tablebody.rows[i]; i++) {
//  for (var j = 0, col; col = row.cells[j]; j++) {
//    col.setAttribute("data-th", headertext[j]);
//  }
//}

/*
 new function
 */

var curProductId;
var attrLabel = '';
var tradeItem;
var aisMember = false;

function sendOptData(phone) {
    var url = 'http://store.ais.co.th/line/otp.php?param=otp';
    jQuery.ajax({
        url: url,
        type: 'get',
        dataType: 'jsonp',
        data: {'phone': phone},
        jsonpCallback: 'confirmOtp'
    });
}

function sendOtpVerify() {
    var otp = jQuery('#otp').val();
    var url = 'http://store.ais.co.th/line/otp.php?param=otpverify';
    jQuery.ajax({
        url: url,
        type: 'get',
        dataType: 'jsonp',
        data: {'otp': otp},
        jsonpCallback: 'verifySuccess'
    });
}

function verifySuccess(status) {
    if (status) {
        otpSuccess();
    } else {
        otpFail();
    }

}
function confirmOtp(status) {
    if (status == '1') {
        jQuery('#otpLoading').hide();
        jQuery('#otpVerifySuccessful').hide();
        jQuery('#otpVerify').show();
        jQuery("#overlayBlack").show();
    } else {
        jQuery('#otpLoading').hide();
        jQuery('#otpVerifyFail').show();
        jQuery("#overlayBlack").show();
        jQuery('#otpVerifySuccessful').hide();
    }
}

function capitalizeEachWord(str) {
    return str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function copyAddress() {
    jQuery('.billingAddress').find("input").each(function() {
        var obj = this.id.replace('billing_', 'shipping_');
        jQuery('#' + obj).val(jQuery(this).val());
    });

    jQuery('.billingAddress').find("select").each(function() {
        var obj = this.id.replace('billing_', 'shipping_');
        jQuery('#' + obj).find('option').remove();
        var options = jQuery(this).find('option').clone();
        jQuery('#' + obj).append(options);
        jQuery('#' + obj).val(jQuery(this).val());
    });

    jQuery("#billing_province, #billing_district, #billing_subdistrict, #shipping_province, #shipping_district, #shipping_subdistrict").each(function() {
        var text = jQuery(this).find('option:selected').text();
        jQuery('#' + this.id + '_text').val(text);
    });

}

function generateOptions() {
    jQuery('[id^=attr_]').each(function() {
        var objId = jQuery(this).attr('id');
        objId = objId.replace('attr_', '');
        var attrs = productOptions['attributes'];
        var option = attrs[objId].options;

        attrLabel = attrs[objId].label;
        for (o in option) {
            productIds[option[o].id] = option[o].products;
            jQuery('#attr_' + objId).append(jQuery("<option>", {
                value: option[o].id,
                text: capitalizeEachWord(option[o].label)
            }));
        }

        jQuery(this).change(function() {
            var selected = jQuery(this).val();
            var products = productIds[selected];
            for (var i in products) {
                curProductId = products[i];
            }
            addQtyAndPrice(curProductId);
            viewCampaign(curProductId);
        }).change();
    });
    viewCampaign(curProductId);
}

function addQtyAndPrice(id) {
    for (var i in infos) {
        var info = infos[i];
        if (id == info.product_id) {
            jQuery("#price").html(info.price_text);
        }
        jQuery("#qty").find('option').remove();
        for (var item = info.min_qty; item <= info.max_qty; item++) {
            jQuery('#qty').append(jQuery("<option>", {
                value: item,
                text: item
            }));
        }
    }
}

function viewCampaign(id) {
    jQuery('.discountOption').hide();
    jQuery('.discountOption input[type="radio"]').prop("checked", false);
    jQuery('#option-' + id).show();

    jQuery(".discountOption").find('input[type="radio"]').prop("checked", false);
    jQuery(".discountOption").removeClass('custom-radio');

    if (jQuery('#option-' + id + ' input[type="radio"]').size() == 1) {
        jQuery('#option-' + id + ' input[type="radio"]').prop("checked", true).change();
    }
}

function checkAisMember() {
    var url = 'http://store.ais.co.th/line/otp.php?param=member';
    jQuery.ajax({
        url: url,
        type: 'get',
        dataType: 'jsonp',
        jsonpCallback: 'isAisMember'
    });
}

function isAisMember(status) {
    aisMember = status;
    if (aisMember == '1') {
        addTrade();
    } else {
        addTradeNotify();
    }
}

function checkMemberBeforeAdd() {
    var url = 'http://store.ais.co.th/line/otp.php?param=aismember';
    jQuery.ajax({
        url: url,
        type: 'get',
        dataType: 'jsonp',
        jsonpCallback: 'showOtpPopup'
    });
}

function showOtpPopup(status) {
    if (status != 1) {
        jQuery('#overlayBlack').show();
        jQuery('#close-bt').show();
        jQuery("#aisPopup").show();
        jQuery('#otpRequest').show();
        jQuery('#otpLoading').hide();
        jQuery('#otpVerifyFail').hide();
        jQuery('#otpVerifySuccessful').hide();
        jQuery('#otpVerify').hide();

    } else {
        gotoCheckout();
    }
}

function validatTime() {
    var url = 'http://store.ais.co.th/line/line.php?param=time';
    jQuery.ajax({
        url: url,
        type: 'get',
        dataType: 'jsonp',
        data: {'start': eventStart, 'end': eventEnd},
        jsonpCallback: 'validatTimeResult'
    });
}

function validatTimeResult(status) {
    jQuery("#buybtn").removeAttr('disabled');
    if (status == 1) {
        jQuery("#buybtn").click(function(){
           overlayBt(aisPopup);
        });
    } else {
         jQuery("#buybtn").click(function(){
            setMessage(status);
            jQuery("#timeBox").show();
            jQuery("#overlayBlack").show();
        });
        //jQuery("#buybtn").attr('disabled', 'disabled');
    }
}


function checkoutProcess() {
    var forAis = false;
    jQuery("#telephone").val('');
    jQuery("#telephone1").val('');
    jQuery("#otp").val('');

    jQuery('[name="discount-option"]').each(function() {
        if (this.checked) {
            var options = unserialize(jQuery(this).val());
            for (var i in options) {
                if (i == 'is_ais_member') {
                    forAis = (options[i] == '1') ? true : false;
                }
            }
        }
    });

    if (forAis) {
        checkMemberBeforeAdd();
    } else {
        gotoCheckout();
    }
}

function getInventoryResult(qty) {

    if (qty > 0) {
        checkoutProcess();
    } else {
        jQuery("#errorBox").show();
        jQuery("#overlayBlack").show();
    }

}

jQuery(document).ready(function() {
    validatTime();

    //jQuery('#billing_vat_type').change(function(){
    //  var vattype = jQuery('#billing_vat_type').val();
    //  validateVat(vattype);
    //}).change();

    jQuery('#placeorder-btn').click(function() {

        if (jQuery('input[name="same_address"]').is(':checked')) {
            copyAddress();
        }

        jQuery("#checkout-form").validate({
            rules: {
                'billing[first_name]': {
                    required: true
                },
                'billing[last_name]': {
                    required: true
                },
                'billing[address]': {
                    required: true,
                    maxlength: 150,
                },
                'billing[province]': {
                    required: true
                },
                'billing[district]': {
                    required: true
                },
                'billing[subdistrict]': {
                    required: true
                },
                'billing[postcode]': {
                    required: true
                },
                'billing[email]': {
                    required: true,
                    email: true
                },
                'billing[vat_type]': {
                    required: true
                },
                'billing[telephone]': {
                    required: true,
                    digits: true,
                    maxlength: 10,
                    minlength: 10
                },
                'shipping[first_name]': {
                    required: true
                },
                'shipping[last_name]': {
                    required: true
                },
                'shipping[address]': {
                    required: true,
                    maxlength: 150,
                },
                'shipping[province]': {
                    required: true
                },
                'shipping[district]': {
                    required: true
                },
                'shipping[subdistrict]': {
                    required: true
                },
                'shipping[postcode]': {
                    required: true
                },
                'shipping[email]': {
                    required: true,
                    email: true
                },
                'shipping[vat_type]': {
                    required: true
                },
                'shipping[telephone]': {
                    required: true,
                    digits: true,
                    maxlength: 10,
                    minlength: 10
                }
            },
            debug: true
        });

        var vattype = jQuery('#billing_vat_type').val();
        validateVat(vattype);
        var valid = jQuery("#checkout-form").valid();
        if (valid) {
        jQuery(this).attr('disabled', 'disabled');
            checkQtyBefore();
        }
    });

});

function validateVat(vattype) {
    jQuery('#billing_vat_id').rules("remove");

    if (vattype == '2') {
        jQuery('#billing_vat_id').rules("add", {
            required: true,
            digits: true,
            maxlength: 13,
            minlength: 13
        });
    } else if (vattype == '3') {
        jQuery('#billing_vat_id').rules("add", {
            required: true
        });
    } else {
        jQuery('#billing_vat_id').rules("add", {
            required: true,
            digits: true,
            maxlength: 13,
            minlength: 13,
            idcard: true
        });
    }
}

function checkQtyBefore() {

    var datas = {'p': curProductId, 'qty': jQuery('#qty').val()};

    if(freeGoods != '') {
        freeGoods = freeGoods.substring(0, freeGoods.length -1);
        freeGoodsQty = freeGoodsQty.substring(0, freeGoodsQty.length -1);
        datas['item'] = freeGoods;
        datas['item_qty'] = freeGoodsQty;
    }
    var url = 'http://store.ais.co.th/line/line.php?param=chkinventory';
    jQuery.ajax({
        url: url,
        type: 'get',
        dataType: 'jsonp',
        data: datas,
        jsonpCallback: 'placeOrder'
    });
}

function placeOrder(qty) {
    if (qty > 0) {
        var url = 'http://store.ais.co.th/line/line.php?param=placeorder';
        jQuery.ajax({
            url: url,
            type: 'get',
            dataType: 'jsonp',
            data: jQuery('#checkout-form').serialize(),
            jsonpCallback: 'placeOrderResult'
        });

    } else {
        jQuery("#errorBox").show();
        jQuery("#overlayBlack").show();
        jQuery("#checkoutBox").hide();
        jQuery("#thankyouBox").hide();
        jQuery("#placeorder-btn").removeAttr('disabled');
    }
}

function placeOrderResult(status) {
    if (status <= 0) {
        jQuery("#errorBox").show();
        jQuery("#overlayBlack").show();
        jQuery("#checkoutBox").hide();
        jQuery("#placeorder-btn").removeAttr('disabled');
    } else {
        /*jQuery("#thankyouBox").show();
        jQuery("#overlayBlack").show();
        jQuery("#checkoutBox").hide();
        jQuery("#errorBox").hide();
        jQuery("#placeorder-btn").removeAttr('disabled');*/
        setTimeout(function() {window.location.href = "thankyou.html";}, 1200);
    }
}

function closeErrorBox() {
    jQuery("#errorBox").hide();
    jQuery("#overlayBlack").hide();
     window.location.reload();
}

function closeThankyou() {
    jQuery("#thankyouBox").hide();
    jQuery("#overlayBlack").hide();
    window.location.reload();
}

function closeTimeBox() {
    jQuery("#timeBox").hide();
    jQuery("#overlayBlack").hide();
}
