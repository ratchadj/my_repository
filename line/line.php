<?php

error_reporting(-1);
ini_set('display_errors', 1);

class line
{
    protected $_host = array();
    protected $_username = array();
    protected $_password = array();
    protected $_dbname = array();
    protected $_connection = array();
    protected $_isClosed = false;
    protected $_qty = 293;

    /**
     * Class constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->getConString();
    }

    public function getParam($param, $defaultValue = null)
    {
        return (isset($_GET[$param])) ? $_GET[$param] : $defaultValue;
    }

    public function closeConnection()
    {
        $this->connect()->close();
        $this->_isClosed = true;
    }

    protected function getConString()
    {
        $doc = new DOMDocument();
        $xml = simplexml_load_file('../app/etc/local.xml');

        $config = $xml->global->resources->default_setup->connection;
        $this->_host['read'] = $config->host;
        $this->_username['read'] = $config->username;
        $this->_password['read'] = $config->password;
        $this->_dbname['read'] = $config->dbname;

        $config = $xml->global->resources->default_setup->connection;
        $this->_host['write'] = $config->host;
        $this->_username['write'] = $config->username;
        $this->_password['write'] = $config->password;
        $this->_dbname['write'] = $config->dbname;
    }

    /**
     * Return Mysql Connection
     *
     * @return resource
     */
    public function connect($type = 'read')
    {
        if (isset($this->_connection[$type]) && !$this->_isClosed) {
            return $this->_connection[$type];
        } else {
            $this->_connection[$type] = new mysqli($this->_host[$type], $this->_username[$type],
                                                   $this->_password[$type], $this->_dbname[$type]);

            $this->_connection[$type]->set_charset("utf8");
            $this->_isClosed = false;
            return $this->connect($type);
        }
    }

    /**
     * Result of request
     *
     * @return string
     */
    public function result()
    {
        $param = $this->getParam('param');

        switch ($param) {
            case 'time':
                return $this->_validateTime();
                break;
            case 'inventory':
                return $this->_getInventoryQty();
                break;
            case 'chkinventory':
                return $this->_getInventoryQtyBefore();
                break;
            case 'placeorder':
                return $this->_addNewOrder();
                break;

            default:
                return "Don't cheat me";
        }
    }

    protected function removeCookies()
    {
        setcookie("ais_otp", "", time() - 3600);
        setcookie("ais_member", "", time() - 3600);
    }

    protected function _addNewOrder()
    {
        $billing = $this->getParam('billing', false);
        $orderQty = (int) $billing['qty'];
        if (isset($billing['product_id'])) {
            $productId = $billing['product_id'];
            $qty = $this->_getInventory($productId);

            $option = $billing['option'];
            $option = unserialize($option);
            $item_qty = 0;

            if(isset($option["free_goods"]) && !is_null($option["free_goods"]))  {
                $freeGoods = $option["free_goods"];
                foreach ($freeGoods as $key => $value) {
                    $goods_id = $this->_getIdBySku($key);
                    $item_qty = $this->_getInventory($goods_id);
                    $item_qty = $item_qty - $value;
                    if($item_qty < 0) {
                        break;
                    }
                }
            }

            if (($qty - $orderQty) >= 0 && ($item_qty >= 0)) {
                try {
                    $id = $this->_placeOrder();
                    $this->removeCookies();
                    return 'placeOrderResult(' . $id . ');';
                } catch (Exception $e) {
                    return 'placeOrderResult(0);';
                }
            } else {
                return 'placeOrderResult(-1);';
            }
        }
    }

    public function _subtracInventory($id, $qty)
    {

        //$id = $this->getParam('p');
        $sql = "UPDATE `catalog_product_entity_int` SET `value` = `value` - ? WHERE entity_id = ?
                AND attribute_id = ? and store_id = 0";

        $statement = $this->connect()->stmt_init();
        if ($statement->prepare($sql)) {
            $statement->bind_param("iii", $qty, $id, $this->_qty);
            $statement->execute();
            $statement->close();
            $this->closeConnection();
        }
    }

    protected function _getIdBySku($sku) {
        $sql = "SELECT entity_id FROM catalog_product_entity WHERE sku = ?";

        $product_id = 0;
        $statement = $this->connect()->stmt_init();
        if ($statement->prepare($sql)) {
            $statement->bind_param("s", $sku);
            $statement->execute();
            $statement->bind_result($product_id);
            $statement->fetch();
            $statement->close();
            $this->closeConnection();
        }
        return $product_id;
    }

    protected function _placeOrder()
    {
        $billing = $this->getParam('billing', false);
        $shipping = $this->getParam('shipping', false);
        $remember = $this->getParam('remember');

        $product_id = $this->_getFieldValue($billing, "product_id");
        $qty = $this->_getFieldValue($billing, "qty");
        $option = $this->_getFieldValue($billing, "option");

        $option = unserialize($option);

        if(isset($option["free_goods"]) && !is_null($option["free_goods"]))  {
            $freeGoods = $option["free_goods"];
            foreach ($freeGoods as $key => $value) {
                $goods_id = $this->_getIdBySku($key);
                $this->_subtracInventory($goods_id, $value);
            }
        }

        $this->_subtracInventory($product_id, $qty);
        $sql = "INSERT INTO temp_orders(billing_first_name,
                                       billing_last_name,
                                       billing_telephone,
                                       billing_email,
                                       billing_address,
                                       billing_province,
                                       billing_province_text,
                                       billing_district,
                                       billing_district_text,
                                       billing_subdistrict,
                                       billing_subdistrict_text,
                                       billing_postcode,
                                       billing_vat_type,
                                       billing_vat_id,
                                       billing_branch_no,
                                       product_id,
                                       qty,
                                       price,
                                       product_options,
                                       payment_method,
                                       shipping_first_name,
                                       shipping_last_name,
                                       shipping_telephone,
                                       shipping_email,
                                       shipping_address,
                                       shipping_province,
                                       shipping_province_text,
                                       shipping_district,
                                       shipping_district_text,
                                       shipping_subdistrict,
                                       shipping_subdistrict_text,
                                       shipping_postcode,
                                       shipping_amount,
                                       shipping_method,
                                       created_at)
                                        VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                                               ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                                               ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,
                                               ?, ?, ?, ?, now())";

        if (is_array($billing) && is_array($shipping)) {
            $statement = $this->connect()->stmt_init();
            //$statement->prepare($sql);
            //echo $this->connect()->error;

            if ($statement->prepare($sql)) {
                $statement->bind_param("sssssisisississiddsssssssisisissds", $billing_first_name, $billing_last_name,
                                       $billing_telephone, $billing_email, $billing_address, $billing_province,
                                       $billing_province_text, $billing_district, $billing_district_text,
                                       $billing_subdistrict, $billing_subdistrict_text, $billing_postcode,
                                       $billing_vat_type, $billing_vat_id, $billing_branch_no, $product_id, $qty,
                                       $price, $option, $payment_method, $shipping_first_name, $shipping_last_name,
                                       $shipping_telephone, $shipping_email, $shipping_address, $shipping_province,
                                       $shipping_province_text, $shipping_district, $shipping_district_text,
                                       $shipping_subdistrict, $shipping_subdistrict_text, $shipping_postcode,
                                       $shipping_amount, $shipping_method);


                $billing_first_name = $this->_getFieldValue($billing, "first_name");
                $billing_last_name = $this->_getFieldValue($billing, "last_name");
                $billing_telephone = $this->_getFieldValue($billing, "telephone");
                $billing_email = $this->_getFieldValue($billing, "email");
                $billing_address = $this->_getFieldValue($billing, "address");
                $billing_province = $this->_getFieldValue($billing, "province");
                $billing_province_text = $this->_getFieldValue($billing, "province_text");
                $billing_district = $this->_getFieldValue($billing, "district");
                $billing_district_text = $this->_getFieldValue($billing, "district_text");
                $billing_subdistrict = $this->_getFieldValue($billing, "subdistrict");
                $billing_subdistrict_text = $this->_getFieldValue($billing, "subdistrict_text");
                $billing_postcode = $this->_getFieldValue($billing, "postcode");
                $billing_vat_type = $this->_getFieldValue($billing, "vat_type");
                $billing_vat_id = $this->_getFieldValue($billing, "vat_id");
                $billing_branch_no = $this->_getFieldValue($billing, "branch_no");

                $product_id = $this->_getFieldValue($billing, "product_id");
                $qty = $this->_getFieldValue($billing, "qty");
                $price = $this->_getFieldValue($billing, "price");
                $option = $this->_getFieldValue($billing, "option");
                $payment_method = $this->_getFieldValue($billing, "payment");

                $shipping_first_name = $this->_getFieldValue($shipping, "first_name");
                $shipping_last_name = $this->_getFieldValue($shipping, "last_name");
                $shipping_telephone = $this->_getFieldValue($shipping, "telephone");
                $shipping_email = $this->_getFieldValue($shipping, "email");
                $shipping_address = $this->_getFieldValue($shipping, "address");
                $shipping_province = $this->_getFieldValue($shipping, "province");
                $shipping_province_text = $this->_getFieldValue($shipping, "province_text");
                $shipping_district = $this->_getFieldValue($shipping, "district");
                $shipping_district_text = $this->_getFieldValue($shipping, "district_text");
                $shipping_subdistrict = $this->_getFieldValue($shipping, "subdistrict");
                $shipping_subdistrict_text = $this->_getFieldValue($shipping, "subdistrict_text");
                $shipping_postcode = $this->_getFieldValue($shipping, "postcode");
                $shipping_amount = $this->_getFieldValue($shipping, "amount");
                $shipping_method = $this->_getFieldValue($shipping, "method");

                $statement->execute();


                $lastId = $this->connect()->insert_id;
                $statement->close();
                $this->closeConnection();
                return $lastId;
            }
        }
        return 0;
    }

    protected function _getFieldValue($object, $field)
    {
        return (isset($object[$field])) ? $object[$field] : "";
    }

    protected function _getInventoryQtyBefore()
    {
        $id = $this->getParam('p');
        $orderQty = $this->getParam('qty');

        $items = $this->getParam('item', false);
        $itemqty = $this->getParam('item_qty', 1);

        if($items !== false) {
            $items = explode(',', $items);
            $itemqty = explode(',', $itemqty);
            if(count($items) > 0) {
                foreach($items as $key => $item) {
                     $temp =   $this->_getInventory($item);
                     $temp = $temp - $itemqty[$key];
                     if($temp < 0) {
                        $itemqty = $temp;
                        break;
                     }
                }
            }
        }

        $qty = $this->_getInventory($id);

        if (($qty - $orderQty) >= 0 && $itemqty >= 0) {
            return "placeOrder(" . $qty . ");";
        } else {
            return "placeOrder(0);";
        }
    }

    protected function _validateTime()
    {
        $currentTime = strtotime($this->_getTime());
        $startTime = strtotime($this->getParam('start'));
        $endTime = strtotime($this->getParam('end'));
        $result = (int) (($startTime <= $currentTime) && ($endTime >= $currentTime));

        if(!$result) {
            if(($startTime > $currentTime)) {
                return "validatTimeResult(-1)";
            } elseif(($endTime < $currentTime)) {
                return "validatTimeResult(-2)";
            }
        }
        return "validatTimeResult(" . $result . ")";
    }

    protected function _getInventoryQty()
    {
        $id = $this->getParam('p');
        $qty = $this->_getInventory($id);
        //$qty = 0;
        return "getInventoryResult(" . $qty . ");";
    }

    public function _getInventory($id)
    {

        //$id = $this->getParam('p');
        $sql = "SELECT `value` AS `qty` FROM `catalog_product_entity_int` WHERE entity_id = ?
                AND attribute_id = ? and store_id = 0";

        $qty = 0;
        $statement = $this->connect()->stmt_init();
        if ($statement->prepare($sql)) {
            $statement->bind_param("ii", $id, $this->_qty);
            $statement->execute();
            $statement->bind_result($qty);
            $statement->fetch();
            $statement->close();
            $this->closeConnection();
        }
        return $qty;
    }

    protected function _getTime()
    {
        $timeZone = "Asia/Bangkok";
        date_default_timezone_set($timeZone);
        return date("Y-m-d H:i:s");
    }
}

$line = new line();
echo $line->result();