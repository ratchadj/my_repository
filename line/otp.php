<?php

error_reporting(-1);
ini_set('display_errors', 1);

class otp
{

    /**
     * Result of request
     *
     * @return string
     */
    public function result()
    {
        $param = $this->getParam('param');

        switch ($param) {
            case 'otp':
                return $this->_sendOtp();
                break;
            case 'otpverify':
                return $this->_verifyOtp();
                break;
            case 'aismember':
                return $this->_verifyAisMemberBefore();
                break;
            case 'member':
                return $this->_isAisMember();
                break;
            default:
                return "Don't cheat me";
        }
    }

    protected function _verifyOtp()
    {

        $otp = $this->getParam('otp');
        $aisOtp = '';
        if (isset($_COOKIE["ais_otp"])) {
            $aisOtp = $_COOKIE["ais_otp"];
        }

        if ($otp == $aisOtp) {
            setcookie("ais_member", 1, time() + (60 * 60));
            return "verifySuccess(1)";
        } else {
            return "verifySuccess(0)";
        }
    }

    protected function _isAisMember()
    {
        $isAisMember = 0;
        if (isset($_COOKIE["ais_member"])) {
            $isAisMember = $_COOKIE["ais_member"];
        }

        if (isset($_SERVER['HTTP_X_MSISDN'])) {
            $isAisMember = 1;
        }

        return "isAisMember(" . $isAisMember . ")";
    }

    protected function _verifyAisMemberBefore()
    {
        $isAisMember = 0;
        if (isset($_COOKIE["ais_member"])) {
            $isAisMember = $_COOKIE["ais_member"];
        }

        if (isset($_SERVER['HTTP_X_MSISDN'])) {
            $isAisMember = 1;
        }

        return "showOtpPopup(" . $isAisMember . ")";
    }

    protected function _checkAisMember($phone)
    {
        $url = "http://aisapi.acommercedev.com/api/customer_service/profile/";

        $arr = array('phone' => $phone);
        $isAisMember = false;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($arr));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = trim(curl_exec($ch));

        if (!curl_errno($ch)) {
            $obj = json_decode($content);
            if (isset($obj->response)) {
                $isAisMember = $obj->response;
            }
        }
        curl_close($ch);
        return $isAisMember;
    }

    protected function _sendOtp()
    {
        $url = "http://aisapi.acommercedev.com/api/sms_service/send_message";
        $timeout = 900;
        $message = "Your OTP is {{otp}}. Please do not disclose it to anyone by any means, in order to prevent fraud. AIS does not have the policy of asking for your password.";

        $phone = $this->getParam('phone');
        $isAisMember = false;
        $sendStatus = 1;

        if (strlen($phone) == 10) {
            $otp = $this->_randomOpt($phone, 4);
            $phone = '66' . ltrim($phone, '0');
        }

        if (!in_array($phone, array('66817724182', '66866339007', '66867728406', '66866132807'))) {
            $isAisMember = $this->_checkAisMember($phone);
        } else {
            $isAisMember = true;
        }

        //$otp = '1234';
        if ($isAisMember) {
            $arr = array("phone" => $phone, "message" => str_replace('{{otp}}', $otp, $message));
            $data_string = json_encode($arr);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $content = curl_exec($ch);

            $result = array();
            if (!curl_errno($ch)) {
                $obj = json_decode($content);
                if (isset($obj->error)) {
                    $sendStatus = 0;
                } elseif (isset($obj->response)) {
                    if ($obj->response) {
                        setcookie("ais_otp", $otp, time() + $timeout);
                    } else {
                        $sendStatus = 0;
                    }
                }
            } else {
                $sendStatus = 0;
            }
        } else {
            $sendStatus = 0;
        }

        setcookie("ais_otp", $otp, time() + $timeout);
        return 'confirmOtp(' . $sendStatus . ');';
    }

    protected function _getTime()
    {
        $timeZone = "Asia/Bangkok";
        date_default_timezone_set($timeZone);
        return date("Y-m-d H:i:s");
    }

    protected function _randomOpt($str, $len)
    {
        $otp = '';
        for ($i = 0; $i < $len; $i++) {
            $ran = rand(0, 9);
            $otp .= substr($str, $ran, 1);
        }
        return $otp;
    }

    /**
     * Get Request param
     *
     * @param string $param        parameter name
     * @param string $defaultValue if parameter not set
     *
     * @return string
     */
    public function getParam($param, $defaultValue = null)
    {
        return (isset($_GET[$param])) ? $_GET[$param] : $defaultValue;
    }
}

$otp = new otp();
echo $otp->result();