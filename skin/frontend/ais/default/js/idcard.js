Validation.add('validate-idcard', 'Your national id card is invalid', function(v) {
    if(Validation.get('IsEmpty').test(v)) {
        return false;
    }
    
    if(v.length != 13) {
        return false;
    }
    
    for(i=0, sum=0; i < 12; i++) {
        sum += parseFloat(v.charAt(i))*(13-i); 
    }
    
    if((11-sum%11)%10!=parseFloat(v.charAt(12))) {
       return false; 
    }
    return true;
})