/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE_AFL.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    design
 * @package     default_default
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 */

// =============================================
// Primary Break Points
// =============================================

// These should be used with the bp (max-width, xx) mixin
// where a min-width is used, remember to +1 to break correctly
// If these are changed, they must also be updated in _var.scss

var bp = {
    xsmall: 479,
    small: 599,
    medium: 770,
    large: 979,
    xlarge: 1199
}

// ==============================================
// Search
// ==============================================

Varien.searchForm.prototype.initialize = function (form, field, emptyText) {
    this.form = $(form);
    this.field = $(field);
    this.emptyText = emptyText;

    this.validator = new Validation(this.form);

    Event.observe(this.form, 'submit', this.submit.bind(this));
    Event.observe(this.field, 'focus', this.focus.bind(this));
    Event.observe(this.field, 'blur', this.blur.bind(this));
    this.blur();
}

Varien.searchForm.prototype.submit = function (event) {
    if (!this.validator || !this.validator.validate()) {
        Event.stop(event);
        return false;
    }
    return true;
}

// ==============================================
// jQuery Init
// ==============================================

// Avoid PrototypeJS conflicts, assign jQuery to $j instead of $
var $j = jQuery.noConflict();

// Use $j(document).ready() because Magento executes Prototype inline
$j(document).ready(function () {

    // ==============================================
    // Shared Vars
    // ==============================================

    // Document
    var w = $j(window);
    var d = $j(document);
    var body = $j('body');

    // =============================================
    // Skip Links
    // =============================================

    var skipContents = $j('.skip-content');
    var skipLinks = $j('.skip-link');

    skipLinks.on('click', function (e) {
        e.preventDefault();

        var self = $j(this);
        var target = self.attr('href');

        // Get target element
        var elem = $j(target);

        // Check if stub is open
        var isSkipContentOpen = elem.hasClass('skip-active') ? 1 : 0;

        // Hide all stubs
        skipLinks.removeClass('skip-active');
        skipContents.removeClass('skip-active');

        // Toggle stubs
        if (isSkipContentOpen) {
            self.removeClass('skip-active');
        } else {
            if($j('div.popTrack').is(':visible')){
                $j('div.popTrack').slideToggle('slow');
            }
            self.addClass('skip-active');
            elem.addClass('skip-active');
        }
    });

    $j('#header-cart').on('click', '.skip-link-close', function(e) {
        var parent = $j(this).parents('.skip-content');
        var link = parent.siblings('.skip-link');

        parent.removeClass('skip-active');
        link.removeClass('skip-active');
        e.preventDefault();
    });


    // ==============================================
    // Header Menus
    // ==============================================

    var nav = $j('#nav');

    // ----------------------------------------------
    // Top Menus

    var MenuManagerState = {
        TOUCH_SCROLL_THRESHOLD: 20,

        touchStartPosition: null,

        shouldCancelTouch: function() {
            return true; //ranai
            if(!this.touchStartPosition) {
                return false;
            }

            var scroll = $j(window).scrollTop() - this.touchStartPosition;
            return Math.abs(scroll) > this.TOUCH_SCROLL_THRESHOLD;
        }
    };

    var pointerEvent = 'touchend';
    // If device has implemented touch/click agnostic event, use it instead of "click"
    if (window.navigator.pointerEnabled) {
        pointerEvent = 'pointerdown';
    } else if (window.navigator.msPointerEnabled) {
        pointerEvent = 'MSPointerDown';
    }
    nav.find('a.has-children.level0').on(pointerEvent,function (event) {
        //scroll occurred, cancel event
        if(MenuManagerState.shouldCancelTouch()) {
            return;
        }

        // If mouse is being used on large viewport, use native hover state
        if (window.navigator.msPointerEnabled
            && event.originalEvent.pointerType == 'mouse'
            && Modernizr.mq("screen and (min-width:" + (bp.medium + 1) + "px)")
        ) {
            $j(this).data('has-touch', false);
            return;
        }
        $j(this).data('has-touch', true);
        var elem = $j(this).parent();

        var alreadyExpanded = elem.hasClass('menu-active');

        nav.find('li.level0').removeClass('menu-active');

        // Collapse all active sub-menus
        nav.find('.sub-menu-active').removeClass('sub-menu-active');

        if (!alreadyExpanded) {
            elem.addClass('menu-active');
        }

        event.preventDefault();
    }).on('click', function (event) {
        var elem = $j(this);
        if (elem.data('has-touch')) {
            elem.data('has-touch', false);
            event.preventDefault();
            return;
        }

        if(Modernizr.mq("screen and (max-width:" + bp.medium + "px)")) {
            var elem = $j(this).parent();

            var alreadyExpanded = elem.hasClass('menu-active');

            nav.find('li.level0').removeClass('menu-active');

            // Collapse all active sub-menus
            nav.find('.sub-menu-active').removeClass('sub-menu-active');

            if (!alreadyExpanded) {
                elem.addClass('menu-active');
            }

            event.preventDefault();
        }
    }).on('touchstart', function(event) {
        $j(this).data('has-touch');
        MenuManagerState.touchStartPosition = $j(window).scrollTop();
    });

    // ----------------------------------------------
    // Sub Menus

    nav.find('li.level1 a.has-children').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        var elem = $j(this).parent();

        // Check if sub-menu is open
        var isSubMenuActive = elem.hasClass('sub-menu-active') ? 1 : 0;

        // On smaller screens, allow multiple sibling sub-menus to show at once,
        // but this is a large touch device, avoid multiple sub-menus showing at once.
        if (Modernizr.mq("screen and (min-width:" + (bp.medium + 1) + "px)")) {
            elem
                .siblings('.sub-menu-active')
                .removeClass('sub-menu-active')
                .find('.sub-menu-active')
                .removeClass('sub-menu-active');
        }
        if (isSubMenuActive) {
            elem.removeClass('sub-menu-active');
        } else {
            elem.addClass('sub-menu-active');
        }
    });

    // Prevent sub menus from spilling out of the window.
    function preventMenuSpill() {
        var windowWidth = $j(window).width();
        $j('ul.level0').each(function(){
            var ul = $j(this);
            //Show it long enough to get info, then hide it.
            ul.addClass('position-test');
            ul.removeClass('spill');
            var width = ul.outerWidth();
            var offset = ul.offset().left;
            ul.removeClass('position-test');
            //Add the spill class if it will spill off the page.
            if ((offset + width) > windowWidth) {
                ul.addClass('spill');
            }
        });
    }
    preventMenuSpill();
    $j(window).on('delayed-resize', preventMenuSpill);


    // ==============================================
    // Language Switcher
    // ==============================================

    // In order to display the language switcher next to the logo, we are moving the content at different viewports,
    // rather than having duplicate markup or changing the design
    enquire.register('(max-width: ' + bp.medium + 'px)', {
        match: function () {
            $j('.page-header-container .store-language-container').prepend($j('.form-language'));
        },
        unmatch: function () {
            $j('.header-language-container .store-language-container').prepend($j('.form-language'));
        }
    });

    // ==============================================
    // Enquire JS
    // ==============================================

    enquire.register('screen and (min-width: ' + (bp.medium + 1) + 'px)', {
        match: function () {
            $j('.menu-active').removeClass('menu-active');
            $j('.sub-menu-active').removeClass('sub-menu-active');
            $j('.skip-active').removeClass('skip-active');
        },
        unmatch: function () {
            $j('.menu-active').removeClass('menu-active');
            $j('.sub-menu-active').removeClass('sub-menu-active');
            $j('.skip-active').removeClass('skip-active');
        }
    });

    // ==============================================
    // UI Pattern - Media Switcher
    // ==============================================

    // Used to swap primary product photo from thumbnails.

    var mediaListLinks = $j('.media-list').find('a');
    var mediaPrimaryImage = $j('.primary-image').find('img');

    if (mediaListLinks.length) {
        mediaListLinks.on('click', function (e) {
            e.preventDefault();

            var self = $j(this);

            mediaPrimaryImage.attr('src', self.attr('href'));
        });
    }

    // ==============================================
    // UI Pattern - ToggleSingle
    // ==============================================

    // Use this plugin to toggle the visibility of content based on a toggle link/element.
    // This pattern differs from the accordion functionality in the Toggle pattern in that each toggle group acts
    // independently of the others. It is named so as not to be confused with the Toggle pattern below
    //
    // This plugin requires a specific markup structure. The plugin expects a set of elements that it
    // will use as the toggle link. It then hides all immediately following siblings and toggles the sibling's
    // visibility when the toggle link is clicked.
    //
    // Example markup:
    // <div class="block">
    //     <div class="block-title">Trigger</div>
    //     <div class="block-content">Content that should show when </div>
    // </div>
    //
    // JS: jQuery('.block-title').toggleSingle();
    //
    // Options:
    //     destruct: defaults to false, but if true, the plugin will remove itself, display content, and remove event handlers


    jQuery.fn.toggleSingle = function (options) {

        // passing destruct: true allows
        var settings = $j.extend({
            destruct: false
        }, options);

        return this.each(function () {
            if (!settings.destruct) {
                $j(this).on('click', function () {
                    $j(this)
                        .toggleClass('active')
                        .next()
                        .toggleClass('no-display');
                });
                // Hide the content
                $j(this).next().addClass('no-display');
            } else {
                // Remove event handler so that the toggle link can no longer be used
                $j(this).off('click');
                // Remove all classes that were added by this plugin
                $j(this)
                    .removeClass('active')
                    .next()
                    .removeClass('no-display');
            }

        });
    }

    // ==============================================
    // UI Pattern - Toggle Content (tabs and accordions in one setup)
    // ==============================================

    $j('.toggle-content').each(function () {
        var wrapper = jQuery(this);

        var hasTabs = wrapper.hasClass('tabs');
        var hasAccordion = wrapper.hasClass('accordion');
        var startOpen = wrapper.hasClass('open');

        var dl = wrapper.children('dl:first');
        var dts = dl.children('dt');
        var panes = dl.children('dd');
        var groups = new Array(dts, panes);

        //Create a ul for tabs if necessary.
        if (hasTabs) {
            var ul = jQuery('<ul class="toggle-tabs"></ul>');
            dts.each(function () {
                var dt = jQuery(this);
                var li = jQuery('<li></li>');
                li.html(dt.html());
                ul.append(li);
            });
            ul.insertBefore(dl);
            var lis = ul.children();
            groups.push(lis);
        }

        //Add "last" classes.
        var i;
        for (i = 0; i < groups.length; i++) {
            groups[i].filter(':last').addClass('last');
        }

        function toggleClasses(clickedItem, group) {
            var index = group.index(clickedItem);
            var i;
            for (i = 0; i < groups.length; i++) {
                groups[i].removeClass('current');
                groups[i].eq(index).addClass('current');
            }
        }

        //Toggle on tab (dt) click.
        dts.on('click', function (e) {
            //They clicked the current dt to close it. Restore the wrapper to unclicked state.
            if (jQuery(this).hasClass('current') && wrapper.hasClass('accordion-open')) {
                wrapper.removeClass('accordion-open');
            } else {
                //They're clicking something new. Reflect the explicit user interaction.
                wrapper.addClass('accordion-open');
            }
            toggleClasses(jQuery(this), dts);
        });

        //Toggle on tab (li) click.
        if (hasTabs) {
            lis.on('click', function (e) {
                toggleClasses(jQuery(this), lis);
            });
            //Open the first tab.
            lis.eq(0).trigger('click');
        }

        //Open the first accordion if desired.
        if (startOpen) {
            dts.eq(0).trigger('click');
        }

    });


    // ==============================================
    // Layered Navigation Block
    // ==============================================

    // On product list pages, we want to show the layered nav/category menu immediately above the product list.
    // While it would make more sense to just move the .block-layered-nav block rather than .col-left-first
    // (since other blocks can be inserted into left_first), it creates simpler code to move the entire
    // .col-left-first block, so that is the approach we're taking
    if ($j('.col-left-first > .block').length && $j('.category-products').length) {
        enquire.register('screen and (max-width: ' + bp.medium + 'px)', {
            match: function () {
                $j('.col-left-first').insertBefore($j('.category-products'))
            },
            unmatch: function () {
                // Move layered nav back to left column
                $j('.col-left-first').insertBefore($j('.col-main'))
            }
        });
    }

    // ==============================================
    // 3 column layout
    // ==============================================

    // On viewports smaller than 1000px, move the right column into the left column
    if ($j('.main-container.col3-layout').length > 0) {
        enquire.register('screen and (max-width: 1000px)', {
            match: function () {
                var rightColumn = $j('.col-right');
                var colWrapper = $j('.col-wrapper');

                rightColumn.appendTo(colWrapper);
            },
            unmatch: function () {
                var rightColumn = $j('.col-right');
                var main = $j('.main');

                rightColumn.appendTo(main);
            }
        });
    }


    // ==============================================
    // Block collapsing (on smaller viewports)
    // ==============================================

    // enquire.register('(max-width: ' + bp.medium + 'px)', {
    //     setup: function () {
    //         this.toggleElements = $j(
    //             // This selects the menu on the My Account and CMS pages
    //             '.col-left-first .block:not(.block-layered-nav) .block-title, ' +
    //                 '.col-left-first .block-layered-nav .block-subtitle--filter, ' +
    //                 '.sidebar:not(.col-left-first) .block .block-title'
    //         );
    //     },
    //     match: function () {
    //         this.toggleElements.toggleSingle();
    //     },
    //     unmatch: function () {
    //         this.toggleElements.toggleSingle({destruct: true});
    //     }
    // });


    // ==============================================
    // OPC - Progress Block
    // ==============================================

    if ($j('body.checkout-onepage-index').length) {
        enquire.register('(max-width: ' + bp.large + 'px)', {
            match: function () {
                $j('#checkout-step-review').prepend($j('#checkout-progress-wrapper'));
            },
            unmatch: function () {
                $j('.col-right').prepend($j('#checkout-progress-wrapper'));
            }
        });
    }


    // ==============================================
    // Checkout Cart - events
    // ==============================================

    if ($j('body.checkout-cart-index').length) {
        $j('input[name^="cart"]').focus(function () {
            $j(this).siblings('button').fadeIn();
        });
    }

    // ==============================================
    // Product Listing - Align action buttons/links
    // ==============================================

    // Since the number of columns per grid will vary based on the viewport size, the only way to align the action
    // buttons/links is via JS

    if ($j('.products-grid').length) {

        var alignProductGridActions = function () {
            var gridRows = []; // This will store an array per row
            var tempRow = [];
            productGridElements = $j('.products-grid > li');
            productGridElements.each(function (index) {
                // The JS ought to be agnostic of the specific CSS breakpoints, so we are dynamically checking to find
                // each row by grouping all cells (eg, li elements) up until we find an element that is cleared.
                // We are ignoring the first cell since it will always be cleared.
                if ($j(this).css('clear') != 'none' && index != 0) {
                    gridRows.push(tempRow); // Add the previous set of rows to the main array
                    tempRow = []; // Reset the array since we're on a new row
                }
                tempRow.push(this);

                // The last row will not contain any cells that clear that row, so we check to see if this is the last cell
                // in the grid, and if so, we add its row to the array
                if (productGridElements.length == index + 1) {
                    gridRows.push(tempRow);
                }
            });

            $j.each(gridRows, function () {
                var tallestHeight = 0;
                $j.each(this, function () {
                    // Since this function is called every time the page is resized, we need to remove the min-height
                    // so each cell can return to its natural size before being measured.
                    $j(this).find('.product-info').css('min-height', '');
                    // We are checking the height of .product-info (rather than the entire li), because the images
                    // will not be loaded when this JS is run.
                    elHeight = parseInt($j(this).find('.product-info').css('height'));
                    if (elHeight > tallestHeight) {
                        tallestHeight = elHeight;
                    }
                });
                // Set the height of all .product-info elements in a row to the tallest height
                //console.log('tallestHeight:   '+tallestHeight);
                //console.log('.promotionrec:   '+$j('.promotionrec').height());
                $j.each(this, function () {
                    if($j('.cms-home').length){
                        $j(this).find('.product-info').css('minHeight', tallestHeight-32);
                    }
                    else{
                        $j(this).find('.product-info').css('minHeight', tallestHeight);
                    }
                });
            });
        }
        alignProductGridActions();

        // Since the height of each cell and the number of columns per page may change when the page is resized, we are
        // going to run the alignment function each time the page is resized.
        $j(window).on('delayed-resize', function (e, resizeEvent) {
            alignProductGridActions();
        });
    }

    // ==============================================
    // Generic, efficient window resize handler
    // ==============================================

    // Using setTimeout since Web-Kit and some other browsers call the resize function constantly upon window resizing.
    var resizeTimer;
    $j(window).resize(function (e) {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {
            $j(window).trigger('delayed-resize', e);
        }, 250);
    });
});

// ==============================================
// PDP - image zoom - needs to be available outside document.ready scope
// ==============================================

var ProductMediaManager = {
    IMAGE_ZOOM_THRESHOLD: 20,
    zoomEnabled: Modernizr.mq("screen and (min-width:768px)"),
    imageWrapper: null,

    destroyZoom: function() {
        $j('.zoomContainer').remove();
        $j('.product-image-gallery .gallery-image').removeData('elevateZoom');
    },

    createZoom: function(image) {
        ProductMediaManager.destroyZoom();

        if(!ProductMediaManager.zoomEnabled) { //zoom not enabled
            return;
        }

        if(image.length <= 0) { //no image found
            return;
        }

        if(image[0].naturalWidth && image[0].naturalHeight) {
            var widthDiff = image[0].naturalWidth - image.width() - ProductMediaManager.IMAGE_ZOOM_THRESHOLD;
            var heightDiff = image[0].naturalHeight - image.height() - ProductMediaManager.IMAGE_ZOOM_THRESHOLD;

            if(widthDiff < 0 && heightDiff < 0) {
                //image not big enough

                image.parents('.product-image').removeClass('zoom-available');

                return;
            } else {
                image.parents('.product-image').addClass('zoom-available');
            }
        }

        image.elevateZoom();
    },

    swapImage: function(targetImage) {
        targetImage = $j(targetImage);
        targetImage.addClass('gallery-image');

        ProductMediaManager.destroyZoom();

        var imageGallery = $j('.product-image-gallery');

        if(targetImage[0].complete) { //image already loaded -- swap immediately

            imageGallery.find('.gallery-image').removeClass('visible');

            //move target image to correct place, in case it's necessary
            imageGallery.append(targetImage);

            //reveal new image
            targetImage.addClass('visible');

            //wire zoom on new image
            ProductMediaManager.createZoom(targetImage);

        } else { //need to wait for image to load

            //add spinner
            imageGallery.addClass('loading');

            //move target image to correct place, in case it's necessary
            imageGallery.append(targetImage);

            //wait until image is loaded
            imagesLoaded(targetImage, function() {
                //remove spinner
                imageGallery.removeClass('loading');

                //hide old image
                imageGallery.find('.gallery-image').removeClass('visible');

                //reveal new image
                targetImage.addClass('visible');

                //wire zoom on new image
                ProductMediaManager.createZoom(targetImage);
            });

        }
    },

    wireThumbnails: function() {
        //trigger image change event on thumbnail click
        $j('.product-image-thumbs .thumb-link').click(function(e) {
            e.preventDefault();
            var jlink = $j(this);
            var target = $j('#image-' + jlink.data('image-index'));

            ProductMediaManager.swapImage(target);
        });
    },

    initZoom: function() {
        ProductMediaManager.createZoom($j(".no-touch .gallery-image.visible")); //set zoom on first image
    },

    init: function() {
        ProductMediaManager.imageWrapper = $j('.product-img-box');

        enquire.register("screen and (min-width:768px)", {
            match : function() {
                ProductMediaManager.zoomEnabled = true;
                ProductMediaManager.initZoom();
            },
            unmatch : function() {
                ProductMediaManager.destroyZoom();
                ProductMediaManager.zoomEnabled = false;
            }
        });

        //resizing the window causes problems with zoom -- reinitialize
        $j(window).on('delayed-resize', function(e, resizeEvent) {
            ProductMediaManager.destroyZoom();
            ProductMediaManager.initZoom();
        });

        ProductMediaManager.wireThumbnails();

        $j(document).trigger('product-media-loaded', ProductMediaManager);
    }
};

$j(document).ready(function() {
    ProductMediaManager.init();
});

/*********** Functions for Mobile Responsive  ***********/
jQuery(document).ready(function() {
    /*=== set timeout for message  ===*/
    if(jQuery('ul.messages')){
        setTimeout( "jQuery('ul.messages').slideUp(300).fadeOut(600);",30000 );
    }

    //Search bar Control
    var deviceScreen_1 = getDeviceScreen(1024);
    var deviceScreen_2 = getDeviceScreen(1081); 

    searchExpand(); footerSameHeight();
    jQuery(window).resize(function(){
        searchExpand(); footerSameHeight();
    });  

    function searchExpand(){
        var theWidth = jQuery(window).width();
        if( deviceScreen_1 < theWidth < deviceScreen_2 ){
        jQuery('.navContainer #search').focus(function() {
            var theWidth = jQuery(window).width();
            jQuery('#header-search').addClass("expand");
        });
        jQuery('.navContainer #search').blur(function() {
            var theWidth = jQuery(window).width();
            jQuery('#header-search').removeClass("expand");
        });
        }
    }
    
    //Cart page member price label position changing
    if(jQuery('.checkout-cart-index').length > 0){
        memberPriceLabel();
        jQuery(window).resize(function(){
            memberPriceLabel();
        });
    }
    function memberPriceLabel(){
        if(jQuery('#shopping-cart-table .ais-price').length > 0){
            var theWidth = jQuery(window).width();
            var deviceScreen = getDeviceScreen(480);
            if( theWidth <= deviceScreen ){
                var aisPrice;
                jQuery('#shopping-cart-table tbody tr').each(function(){
                    aisPrice = jQuery(this).find('.product-cart-price .ais-price');
                    if((aisPrice.length > 0) && (jQuery(this).find('.product-cart-total .ais-price').length <= 0) ){                        
                       jQuery(this).find('.product-cart-total').append(aisPrice);
                    }
                });
            }else{
                jQuery('#shopping-cart-table tbody tr').each(function(){
                    aisPrice = jQuery(this).find('.product-cart-total .ais-price');
                    if((aisPrice.length > 0) && (jQuery(this).find('.product-cart-price .ais-price').length <= 0) ){                        
                       jQuery(this).find('.product-cart-price').append(aisPrice);
                    }
                });
            }
        }
    }

    //Footer Control
    jQuery('.footer .links .block-title').click(function(e){
        e.preventDefault();
        var theWidth = jQuery(window).width();
        var deviceScreen = getDeviceScreen(581);
        if( theWidth <= deviceScreen ){
            if(jQuery(this).next("ul.main").length > 0){
                if(jQuery(this).find("a")){
                    jQuery(this).find("a").unbind();
                }
                if(!jQuery(this).hasClass('active')){
                    jQuery('.footer .links .block-title').removeClass('active');
                }
                jQuery(this).toggleClass('active');
            }
        }
    });

    function footerSameHeight(){
        var highestBox = 0;
        var theWidth = jQuery(window).width();
        var deviceScreen_1 = getDeviceScreen(580);
        var deviceScreen_2 = getDeviceScreen(960);
        if(jQuery('.footer .links').length > 0){
            if( (theWidth >= deviceScreen_1) && (theWidth <= deviceScreen_2) ){
                jQuery('.footer .links').each(function(){  
                    if(jQuery(this).height() > highestBox){  
                        highestBox = jQuery(this).height();  
                    }
                })
                jQuery('.footer .links').height(highestBox);
            }
            else {
                jQuery('.footer .links').height('');
            }
        }
    }

    //Customer Account Page Menu Control
    jQuery('.customer-account .col-left .block-account .block-title').click(function(e){
        e.preventDefault();
        var theWidth = jQuery(window).width();
        var deviceScreen = getDeviceScreen(768);
        if( theWidth <= deviceScreen ){
            if(!jQuery(this).hasClass('active')){
                jQuery('.customer-account .col-left .block-account .block-title').removeClass('active');
            }
            jQuery(this).toggleClass('active');
        }
    });

    //Menu for order, invoice, and shipment label links in Customer Order View page
    jQuery('.title-buttons + .order-info li.current').click(function(e){
        e.preventDefault();
        var theWidth = jQuery(window).width();
        var deviceScreen = getDeviceScreen(480);
        if( theWidth <= deviceScreen ){
            if(!jQuery(this).hasClass("first last")){
                jQuery("ul#order-info-tabs").toggleClass('active');
            }
        }
    });

    //Click control for products comparison page
    jQuery('.data-table.compare-table tr.mobile-compare-th').click(function(e){
        e.preventDefault();
        var theWidth = jQuery(window).width();
        var deviceScreen = getDeviceScreen(768);
        if( theWidth <= deviceScreen ){
            if(!jQuery(this).hasClass('active')){
                jQuery('.data-table.compare-table tr.mobile-compare-th').removeClass('active');
            }
            jQuery(this).toggleClass('active');
        }
    });

    if(jQuery('.catalog-product-compare-index').length > 0){
        if(jQuery(".data-table.compare-table") > 0){
            var fixed_offset_top = jQuery(".data-table.compare-table").offset().top;
            comparePageHeader();
            jQuery(window).resize(function(){
                comparePageHeader();
            });
            jQuery(window).scroll(function(){  
                comparePageHeader();
            });
        }
    }

    function comparePageHeader(){
        var theWidth = jQuery(window).width();
        var thTopMargin = jQuery('.data-table.compare-table thead').height() 
                        + jQuery('.products-container').height();
        var deviceScreen = getDeviceScreen(768);
        if( theWidth <= deviceScreen ){
            var scroll = jQuery(window).scrollTop(); 
            if (scroll > fixed_offset_top) {                
                jQuery(".data-table.compare-table thead").addClass("fixed");
                jQuery(".products-container").addClass("fixed");
                jQuery(".products-container.fixed + .product-info").css("margin-top",thTopMargin);
            }else{
                jQuery(".data-table.compare-table .products-container.fixed + .product-info").css("margin-top","");
                jQuery(".data-table.compare-table thead").removeClass("fixed");
                jQuery(".data-table.compare-table .products-container").removeClass("fixed");
            }
        }else{
            jQuery(".data-table.compare-table .products-container.fixed + .product-info").css("margin-top","");
        }
    }

    jQuery('.data-table.compare-table .mobile-compare-th').click(function(e){ 
        var theWidth = jQuery(window).width();
        var deviceScreen = getDeviceScreen(768);
        if( theWidth <= deviceScreen ){
            var topHeight = jQuery('.data-table.compare-table thead').height() 
                        + jQuery('.products-container').height();
            goToByScroll(this, topHeight);
        }
    });

    function goToByScroll(element,offsetDiffer){          
        jQuery("html,body").animate({scrollTop: (jQuery(element).offset().top) - offsetDiffer },"slow"); 
    } 
    //End of Click control for products comparison page

    // Back to top button
    jQuery(document).ready(function () {
        ToggleScrollUp();

        jQuery(window).scroll(function () {
            ToggleScrollUp();
        });

        jQuery('#backtotop').click(function () {
            jQuery("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });
    });

    function ToggleScrollUp() {
        if (jQuery(".main").offset().top <= jQuery(window).scrollTop()) {
            jQuery('#backtotop').fadeIn();
        } else {
            jQuery('#backtotop').fadeOut();
        }
    }

    // Help Page Mobile control
    if(jQuery('.cms-help').length > 0){
        jQuery('.helpContent .accord-header').click(function(){
            var theWidth = jQuery(window).width();
            var deviceScreen = getDeviceScreen(768);
            if( theWidth <= deviceScreen ){
                if(jQuery(this).hasClass('current')){
                    jQuery("html,body").animate({scrollTop: (jQuery(this).offset().top)},"slow"); 
                }
            }
        });
    }
    if(jQuery(".cms-help").length > 0){
        jQuery(this).find('.page').append('<div id="backtotop"><a href="#top"></a></div>');
    }

    //Product List Page > Refine your search
    jQuery('.block-layered-nav p.block-subtitle-small').click(function() {
        var theWidth = jQuery(window).width();
        var deviceScreen = getDeviceScreen(960);
        if( theWidth <= deviceScreen ){
            jQuery(this).parent().toggleClass('active');
        }
    });
    jQuery('div.block-layered-nav dt').click(function(e){
        var theWidth = jQuery(window).width();
        var deviceScreen = getDeviceScreen(960);
        if( theWidth <= deviceScreen ){
            if(!jQuery(this).hasClass('active')){
                jQuery('div.block-layered-nav dt').removeClass('active');
            }
        }
        jQuery(this).toggleClass('active');
        // jQuery('div.block-layered-nav dd').slideUp();
        // jQuery(this).next('dd').slideDown();
    });

    // Product's name repositioning in product view page
    if(jQuery('.catalog-product-view').length > 0){        
        if(jQuery('.catalog-product-view .topDescription').length > 0){
            var deviceScreen = getDeviceScreen(480);
            productInfoMove(deviceScreen);
            jQuery(window).resize(function(){
                productInfoMove(deviceScreen);
            });
        }
    }
    function productInfoMove(deviceScreen){
        var theWidth = jQuery(window).width();            
        if( theWidth <= deviceScreen ) {
            jQuery('.catalog-product-view .topDescription').insertBefore('.product-img-box');
        }
        else{
            jQuery('.catalog-product-view .topDescription').insertAfter('.product-img-box');
        }
    }

    function getDeviceScreen(width){
        var deviceScreen;
        if(jQuery('html.touch').length > 0){
            deviceScreen = width;
        }else{
            deviceScreen = width-17;
        }
        return deviceScreen;
    }
    
});
