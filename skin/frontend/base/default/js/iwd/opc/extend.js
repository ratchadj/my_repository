;
//DUMMY FOR EE CHECKOUT
var checkout =  {
		steps : new Array("login", "billing", "shipping", "shipping_method", "payment", "review"),
		
		gotoSection: function(section){
			IWD.OPC.backToOpc();
		},
		accordion:{
			
		}
};

/** CHECK RESPONSE FROM AJAX AFTER SAVE ORDER **/
/** FUNCTION REWRITED FOR MAGENTO EE VERSION **/ 
IWD.OPC.prepareOrderResponse =  function(response){
	
	if (typeof(response.error) != "undefined" && response.error!=false){
		IWD.OPC.Checkout.hideLoader();
		IWD.OPC.Checkout.unlockPlaceOrder();

		IWD.OPC.saveOrderStatus = false;
		$j('.opc-message-container').html(response.error);
		$j('.opc-message-wrapper').show();
		IWD.OPC.Plugin.dispatch('error');
		return;
	}
	
	if (typeof(response.error_messages) != "undefined" && response.error_messages!=false){
		IWD.OPC.Checkout.hideLoader();
		IWD.OPC.Checkout.unlockPlaceOrder();				
				
		IWD.OPC.saveOrderStatus = false;
		$j('.opc-message-container').html(response.error_messages);
		$j('.opc-message-wrapper').show();
		IWD.OPC.Plugin.dispatch('error');
		return;
	}

	IWD.OPC.Plugin.dispatch('responseSaveOrderBefore', response);
	if (IWD.OPC.Sagepay.viewDialog==true || IWD.OPC.saveOrderStatus==false){
		return;
	}
	if (typeof(response.redirect) !="undefined"){
		if (response.redirect!==false){
			setLocation(response.redirect);
			return;
		}
	}

	if (typeof(response.update_section) != "undefined"){
		IWD.OPC.Checkout.hideLoader();
		IWD.OPC.Checkout.unlockPlaceOrder();

		//create catch for default logic  - for not spam errors to console
		try{
			$j('#checkout-' + response.update_section.name + '-load').html(response.update_section.html);
		}catch(e){
			
		}
		
		IWD.OPC.prepareExtendPaymentForm();
		$j('#payflow-advanced-iframe').show();
		$j('#payflow-link-iframe').show();
		$j('#hss-iframe').show();
		
	}
	IWD.OPC.Checkout.hideLoader();
	IWD.OPC.Checkout.unlockPlaceOrder();				
	
	IWD.OPC.Plugin.dispatch('responseSaveOrder', response);
	
};

IWD.OPC.prepareExtendPaymentForm =  function(){
	$j('.opc-col-left').hide();
	$j('.opc-col-center').hide();
	$j('.opc-col-right').addClass('full-page');
	$j('#checkout-review-table-wrapper').hide();
	$j('#checkout-review-submit').hide();
	$j('.opc-newsletter').hide();
	$j('.text-login').hide();
	
};

IWD.OPC.backToOpc =  function(){
	$j('.opc-col-left').show();
	$j('.opc-col-center').show();
	$j('.opc-col-right').removeClass('full-page');
	$j('#checkout-review-table-wrapper').show();
	$j('#checkout-review-submit').show();
	$j('#payflow-advanced-iframe').hide();
	$j('#payflow-link-iframe').hide();
	$j('#hss-iframe').hide();
	$j('.opc-newsletter').show();
	$j('.text-login').show();
	IWD.OPC.saveOrderStatus = false;
	
};



IWD.OPC.EE = {
		
		updatePayments: false,
		
		init: function(){
			this.initRemove();
			$j(document).on('click', '#use_reward_points', function(){
				IWD.OPC.validatePayment();
			});
			
			$j(document).on('click', '#use_customer_balance', function(){
				IWD.OPC.validatePayment();
			});
			
			
			$j(document).on('click','#checkout-shipping-method-load input', function(){
				try{
					payment.switchCustomerBalanceCheckbox();
				}catch(e){
					
				}
			});
		},
		
		initRemove: function(){
			$j(document).on('click','.opc-data-table .btn-remove', function(event){
				event.preventDefault();
				var linkUrl = $j(this).attr('href');
				var link = IWD.OPC.GiftCard.isGift(linkUrl);
				if (link!==false){
					IWD.OPC.GiftCard.remove(link);
					return;
				}
			});
		}
};


IWD.OPC.Plugin = {
		
		observer: {},
		
		
		dispatch: function(event, data){
			
			
			if (typeof(IWD.OPC.Plugin.observer[event]) !="undefined"){
				
				var callback = IWD.OPC.Plugin.observer[event];
				callback(data);
				
			}
		},
		
		event: function(eventName, callback){
			IWD.OPC.Plugin.observer[eventName] = callback;
		}
};

/** 3D Secure Credit Card Validation - CENTINEL **/
IWD.OPC.Centinel = {
	init: function(){
		IWD.OPC.Plugin.event('savePaymentAfter', IWD.OPC.Centinel.validate);
	},
	
	validate: function(){
		if (typeof(CentinelAuthenticateController) != "undefined"){
			$j('.opc-col-left').hide();
			$j('.opc-col-center').hide();
			$j('.opc-col-right').addClass('full-page');
		}
	},
	
	success: function(){
		if (typeof(CentinelAuthenticateController) != "undefined"){
			$j('.opc-col-right').removeClass('full-page');
			$j('.opc-col-left').show();
			$j('.opc-col-center').show();
		}
	}
	
};


/** SAGE PAY EBIZMARTS EXTENSION **/
IWD.OPC.Sagepay = {
		viewDialog: false, 
		originalUrl: null,
		
		init: function(){
			
			IWD.OPC.Plugin.event('saveOrder', IWD.OPC.Sagepay.validatePaymentMethod);
			IWD.OPC.Plugin.event('error', IWD.OPC.Sagepay.resetUrl);
			IWD.OPC.Plugin.event('responseSaveOrderBefore', IWD.OPC.Sagepay.responseSaveOrder);
			IWD.OPC.Plugin.event('responseSaveOrder', IWD.OPC.Sagepay.responseSaveOrder);
			
		}, 
		
		validatePaymentMethod: function(){
			
			var $payment = $j('.payment-block input:radio:checked:first');
			var name = $payment.val();
			
			if (name == 'sagepayform' || name == 'sagepaydirectpro' || name == 'sagepayserver'){
				
				this.originalSaveOrderUrl = IWD.OPC.Checkout.saveOrderUrl;
				
				IWD.OPC.Checkout.saveOrderUrl = SuiteConfig.getConfig('global', 'sgps_saveorder_url');
			}
			
		}, 
		
		resetUrl: function(){
			if (this.originalUrl!=null){
				IWD.OPC.Checkout.saveOrderUrl = this.originalUrl;
				this.originalUrl = null;
			}
		},
		
		responseSaveOrder: function(response){	
			
			if (response.success==false && response.response_status=='ERROR'){
				$j('.opc-message-container').html(response.response_status_detail);
				$j('.opc-message-wrapper').show();
				IWD.OPC.Checkout.hideLoader();
				IWD.OPC.saveOrderStatus = false;
				return;
			}
			if (payment.currentMethod=='sagepayserver'){				
				IWD.OPC.Checkout.hideLoader();
				IWD.OPC.Sagepay.viewDialog = true;
				IWD.OPC.saveOrderStatus = false;
				IWD.OPC.Sagepay.sagepayserver(response);
				return;
			}
			
			//console.log(payment.currentMethod);
			if (typeof(response.response_status) !="undefined" && response.response_status=='ERROR'){
				IWD.OPC.Checkout.hideLoader();
				IWD.OPC.saveOrderStatus = false;
				$j('.opc-message-container').html(response.response_status_detail);
				$j('.opc-message-wrapper').show();
				IWD.OPC.Plugin.dispatch('error');
				return;
			}
			
			if (payment.currentMethod=='sagepaydirectpro' && response.success && response.response_status == 'OK' && (typeof response.next_url == 'undefined')){
		        setLocation(SuiteConfig.getConfig('global','onepage_success_url'));
		        return;				
			}
			
		},
		
sagepayserver: function(response){
			
			$('sagepayserver-dummy-link').writeAttribute('href', response.redirect);

	        var rbButtons = $('review-buttons-container');

	        var lcont = new Element('div',{
	            className: 'lcontainer'
	        });
	        
	        var heit = parseInt(SuiteConfig.getConfig('server','iframe_height'));
	        
	        if(Prototype.Browser.IE){
	            heit = heit-65;
	        }

	        var wtype = SuiteConfig.getConfig('server','payment_iframe_position').toString();
	        
	        if(wtype == 'modal'){

	            var wm = new Control.Modal('sagepayserver-dummy-link',{
	                className: 'modal',
	                iframe: true,
	                closeOnClick: false,
	                insertRemoteContentAt: lcont,
	                height: SuiteConfig.getConfig('server','iframe_height'),
	                width: SuiteConfig.getConfig('server','iframe_width'),
	                fade: true,
	                afterOpen: function(){
	                    if(rbButtons){
	                        rbButtons.addClassName('disabled');
	                    }
	                },
	                afterClose: function(){
	                    if(rbButtons){
	                        rbButtons.removeClassName('disabled');
	                    }
	                }
	            });
	            wm.container.insert(lcont);
	            wm.container.down().setStyle({
	                'height':heit.toString() + 'px'
	                });
	            wm.container.down().insert(this.getServerSecuredImage());
	            wm.open();

	        }else if(wtype == 'incheckout') {

	            var iframeId = 'sagepaysuite-server-incheckout-iframe';
	            var paymentIframe = new Element('iframe', {
	                'src': response.redirect,
	                'id': iframeId
	            });

	            if(this.getConfig('osc')){
	                var placeBtn = $('onestepcheckout-place-order');

	                placeBtn.hide();

	                $(window._sagepayonepageFormId).insert( {
	                    after:paymentIframe
	                } );
	                $(iframeId).scrollTo();

	            }else{

	                if( (typeof $('checkout-review-submit')) == 'undefined' ){
	                    var btnsHtml  = $$('div.content.button-set').first();
	                }else{
	                    var btnsHtml  = $('checkout-review-submit');
	                }

	                btnsHtml.hide();
	                btnsHtml.insert( {
	                    after:paymentIframe
	                } );
	                IWD.OPC.prepareExtendPaymentForm ();		
	            }

	        }else if(wtype == 'full_redirect') {
	            setLocation(response.redirect);
	            return;
	        }
		},
		getServerSecuredImage: function(){
		    return new Element('img', {
		        'src':SuiteConfig.getConfig('server', 'secured_by_image'),
		        'style':'margin-bottom:5px'
		    });
		},
};



IWD.OPC.GiftCard = {
		init: function(){
			$j(document).on('submit', '#giftcard-form', function(e){
				e.preventDefault();
				IWD.OPC.GiftCard.submit();
			});
			
			
			$j(document).on('click','.giftcard h2', function(){
				if ($j(this).hasClass('open-block')){
					$j(this).removeClass('open-block');
					$j('#giftcard-form').hide();
				}else{
					$j(this).addClass('open-block');
					$j('#giftcard-form').show();
				}
			});
			
			/** COMMENT BLOCK **/
			$j(document).on('click','.comment-block h3', function(){
				if ($j(this).hasClass('open-block')){
					$j(this).removeClass('open-block');
					$j('.comment-block .discount').hide();
				}else{
					$j(this).addClass('open-block');
					$j('.comment-block .discount').show();
				}
			});
		},
		
		submit: function(){
			var giftcardForm = new VarienForm('giftcard-form');
			if (giftcardForm.validator && !giftcardForm.validator.validate()) {
				return false;
			}
			
			IWD.OPC.Checkout.showLoader();
			var form = $j('#giftcard-form').serializeArray();
			$j.post(IWD.OPC.Checkout.config.baseUrl + 'onepage/gift/add',form, IWD.OPC.GiftCard.addResponse,'json')
		},
		
		remove: function(code){
			IWD.OPC.Checkout.showLoader();
			var form = $j('#giftcard-form').serializeArray();
			form.push({"name":"code","value":code});
			$j.post(IWD.OPC.Checkout.config.baseUrl + 'onepage/gift/remove',form, IWD.OPC.GiftCard.addResponse,'json')
		},
		
		addResponse: function(response){
			IWD.OPC.Checkout.hideLoader();
			if (typeof(response.error) !="undefined"){
				if (response.error === false){
					IWD.OPC.Checkout.pullPayments();
				}
				
				$j('.opc-message-container').html(response.message);
				$j('.opc-message-wrapper').show();
				IWD.OPC.Checkout.hideLoader();
				
				
			}
			
		},
		
		checkGiftCardStatus: function(){
			var giftcardForm = new VarienForm('giftcard-form');
			if (giftcardForm.validator && !giftcardForm.validator.validate()) {
				return false;
			}
			
			new Ajax.Updater(
			'giftcard_balance_lookup',
			IWD.OPC.Checkout.config.baseUrl + 'giftcard/cart/quickCheck/',{
				onCreate: function() { $('gc-please-wait').show(); },
				onComplete: function() { $('gc-please-wait').hide(); },
				parameters : {giftcard_code : $('giftcard_code').value}
				}
			);
		},
		isGift: function(url){
			var txt=url
			var re1='.*?';	// Non-greedy match on filler
		      var re2='(giftcard)';	// Word 1
		      var re3='.*?';	// Non-greedy match on filler
		      var re4='(cart)';	// Word 2
		      var re5='.*?';	// Non-greedy match on filler
		      var re6='(remove)';	// Word 3

		      var p = new RegExp(re1+re2+re3+re4+re5+re6,["i"]);
		      var m = p.exec(txt);
		      if (m != null){
		          var pathArray = url.split( '/' );
		          var l = pathArray.length;
		          return pathArray[l-2];
		      }
		      return false;
		}
}

/** PAYPAL EXPRESS CHECKOUT LIGHTBOX **/
IWD.OPC.Lipp = {
		init: function(){
			if (IWD.OPC.Checkout.config.paypalLightBoxEnabled==true){
				IWD.OPC.Plugin.event('redirectPayment', IWD.OPC.Lipp.checkPaypalExpress);
			}
		},
		
		checkPaypalExpress:function(url){
			IWD.OPC.Checkout.showLoader();
			try{
				if (url.match(/paypal\/express\/start/i)){
					IWD.OPC.Checkout.xhr = true;
					IWD.OPC.Lipp.prepareToken();
				}
			
			}catch(e){
				IWD.OPC.Checkout.xhr = null;
			}
		},
		
		prepareToken: function(){
			$j.post(IWD.OPC.Checkout.config.baseUrl + 'onepage/express/start',{"redirect":'onepage'}, IWD.OPC.Lipp.prepareTokenResponse,'json');
		},
		
		prepareTokenResponse: function(response){
			if (typeof(response.error)!="undefined"){
				if (response.error==false){
					IWD.OPC.Checkout.hideLoader();
					PAYPAL.apps.Checkout.startFlow(IWD.OPC.Checkout.config.paypalexpress + response.token);
				}
				
				if (response.error==true){
					alert(response.message);
				}
			}
		}
		
		
}

function toggleContinueButton(){}//dummy

$j(document).ready(function(){
	IWD.OPC.EE.init(); 
	IWD.OPC.Sagepay.init();
	IWD.OPC.Centinel.init();
	IWD.OPC.GiftCard.init();
	IWD.OPC.Lipp.init(); 
});
