<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * New products block
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Block_Product_HomeNew extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Default value for products count that will be shown
     */
    const DEFAULT_PRODUCTS_COUNT = 10;

    /**
     * Products count
     *
     * @var null
     */
    protected $_productsCount;

    /**
     * Initialize block's cache
     */
    protected function _construct()
    {
        parent::_construct();

        $this->addColumnCountLayoutDepend('empty', 6)
            ->addColumnCountLayoutDepend('one_column', 5)
            ->addColumnCountLayoutDepend('two_columns_left', 4)
            ->addColumnCountLayoutDepend('two_columns_right', 4)
            ->addColumnCountLayoutDepend('three_columns', 3);

        $this->addData(array('cache_lifetime' => 86400));
        $this->addCacheTag(Mage_Catalog_Model_Product::CACHE_TAG);

    }

    /**
     * Get Key pieces for caching block content
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array(
            'CATALOG_PRODUCT_NEW',
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
            'template' => $this->getTemplate(),
            $this->getProductsCount()
        );
    }

    /**
     * Prepare and return product collection
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection|Object|Varien_Data_Collection
     */
    protected function _getProductCollection()
    {
        $todayStartOfDayDate  = Mage::app()->getLocale()->date()
            ->setTime('00:00:00')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $todayEndOfDayDate  = Mage::app()->getLocale()->date()
            ->setTime('23:59:59')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getResourceModel('catalog/product_collection')
                        //->setStoreId(Mage::app()->getStore()->getId())
        ;
        //$collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
        $attributes = Mage::getSingleton('catalog/config')
            ->getProductAttributes();

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addAttributeToSelect($attributes)
            //->addStoreFilter()
            //->addAttributeToFilter('ais_hotdeal', 1)
            ->addAttributeToSort('update_at', 'desc')
            //->setPageSize(13)
            //->setCurPage(1)
        ;
        //echo $collection->getSelectSql();

        //if ($this->hasData('category_id')) echo 'category_id: ' . $this->getData('category_id') . '<br/>';
        //if ($this->hasData('column_count')) echo 'column_count: ' . $this->getData('column_count') . '<br/>';
        //if ($this->hasData('products_count')) echo 'products_count: ' .  $this->getData('products_count');
        //if ($this->hasData('products_count')) $this->setProductsCount($this->getData('products_count'));
        if ($this->hasData('attr_code')) {
            $collection->addAttributeToFilter($this->getData('attr_code'), 1);
            //echo $this->getData('attr_code');
        }
        $this->setProductsCount($this->hasData('products_count') ? $this->getData('products_count') : 13);
        $collection->setPageSize($this->getProductsCount());
/*
        $category = Mage::getModel('catalog/category')->load($this->hasData('category_id') ? $this->getData('category_id') : 3);
        $collection = $category->getProductCollection();
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addAttributeToSort('product_id', 'desc')
            ->setPageSize($this->getProductsCount())
        ;
*/
        //echo $collection->getSize();
        //echo $collection->getPageSize();

        return $collection;
    }

    /**
     * Prepare collection with new products
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $this->setProductCollection($this->_getProductCollection());
        return parent::_beforeToHtml();
    }

    /**
     * Set how much product should be displayed at once.
     *
     * @param $count
     * @return Mage_Catalog_Block_Product_New
     */
    public function setProductsCount($count)
    {
        $this->_productsCount = $count;
        return $this;
    }

    /**
     * Get how much products should be displayed at once.
     *
     * @return int
     */
    public function getProductsCount()
    {
        if (null === $this->_productsCount) {
            $this->_productsCount = self::DEFAULT_PRODUCTS_COUNT;
        }
        return $this->_productsCount;
    }
}
