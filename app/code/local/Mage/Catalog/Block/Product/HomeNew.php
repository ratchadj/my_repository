<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * New products block
 *
 * @category   Mage
 * @package    Mage_Catalog
 * @author     Magento Core Team <core@magentocommerce.com>
 */
class Mage_Catalog_Block_Product_HomeNew extends Mage_Catalog_Block_Product_Abstract
{
    /**
     * Default value for products count that will be shown
     */
    const DEFAULT_PRODUCTS_COUNT = 10;

    /**
     * Products count
     *
     * @var null
     */
    protected $_productsCount;

    /**
     * Initialize block's cache
     */
    protected function _construct()
    {
        parent::_construct();

        $this->addColumnCountLayoutDepend('empty', 6)
            ->addColumnCountLayoutDepend('one_column', 5)
            ->addColumnCountLayoutDepend('two_columns_left', 4)
            ->addColumnCountLayoutDepend('two_columns_right', 4)
            ->addColumnCountLayoutDepend('three_columns', 3);

        $this->addData(array('cache_lifetime' => 86400));
        $this->addCacheTag(Mage_Catalog_Model_Product::CACHE_TAG);

    }

    /**
     * Get Key pieces for caching block content
     *
     * @return array
     */
    public function getCacheKeyInfo()
    {
        return array(
            'CATALOG_PRODUCT_NEW',
            Mage::app()->getStore()->getId(),
            Mage::getDesign()->getPackageName(),
            Mage::getDesign()->getTheme('template'),
            Mage::getSingleton('customer/session')->getCustomerGroupId(),
            'template' => $this->getTemplate(),
            $this->getProductsCount()
        );
    }

    /**
     * Prepare and return product collection
     *
     * @return Mage_Catalog_Model_Resource_Product_Collection|Object|Varien_Data_Collection
     */
    protected function _getProductCollection()
    {
        $todayStartOfDayDate  = Mage::app()->getLocale()->date()
            ->setTime('00:00:00')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        $todayEndOfDayDate  = Mage::app()->getLocale()->date()
            ->setTime('23:59:59')
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT);

        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection */
        $collection = Mage::getResourceModel('catalog/product_collection')
                        //->setStoreId(Mage::app()->getStore()->getId())
        ;
        //$collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
        $attributes = Mage::getSingleton('catalog/config')
            ->getProductAttributes();

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addAttributeToSelect($attributes)
            //->addAttributeToSelect("sort_order");
            //->addStoreFilter()
            //->addAttributeToFilter('ais_hotdeal', 1)
            ->addAttributeToSort('sort_order', 'asc')
            ->addAttributeToSort('update_at', 'desc')
            //->setPageSize(13)
            //->setCurPage(1)
        ;
        //echo $collection->getSelectSql();

        //if ($this->hasData('category_id')) echo 'category_id: ' . $this->getData('category_id') . '<br/>';
        //if ($this->hasData('column_count')) echo 'column_count: ' . $this->getData('column_count') . '<br/>';
        //if ($this->hasData('products_count')) echo 'products_count: ' .  $this->getData('products_count');
        //if ($this->hasData('products_count')) $this->setProductsCount($this->getData('products_count'));
        if ($this->hasData('attr_code')) {
            $collection->addAttributeToFilter($this->getData('attr_code'), 1);
            //echo $this->getData('attr_code');
        }
        $this->setProductsCount($this->hasData('products_count') ? $this->getData('products_count') : 13);
        $collection->setPageSize($this->getProductsCount());
/*
        $category = Mage::getModel('catalog/category')->load($this->hasData('category_id') ? $this->getData('category_id') : 3);
        $collection = $category->getProductCollection();
        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addAttributeToSort('product_id', 'desc')
            ->setPageSize($this->getProductsCount())
        ;
*/
        //echo $collection->getSize();
        //echo $collection->getPageSize();
        $collection->getSelect()->reset(Zend_Db_Select::ORDER);
        $collection->getSelect()->order('IFNULL(at_sort_order.value, 99)','ASC');
        $this->loadSwatchesToProductCollection($collection);
        return $collection;
    }

    /**
     * Prepare collection with new products
     *
     * @return Mage_Core_Block_Abstract
     */
    protected function _beforeToHtml()
    {
        $this->setProductCollection($this->_getProductCollection());
        return parent::_beforeToHtml();
    }

    /**
     * Set how much product should be displayed at once.
     *
     * @param $count
     * @return Mage_Catalog_Block_Product_New
     */
    public function setProductsCount($count)
    {
        $this->_productsCount = $count;
        return $this;
    }

    /**
     * Get how much products should be displayed at once.
     *
     * @return int
     */
    public function getProductsCount()
    {
        if (null === $this->_productsCount) {
            $this->_productsCount = self::DEFAULT_PRODUCTS_COUNT;
        }
        return $this->_productsCount;
    }


    protected $_ids = FALSE;

    /**
     * Getting current product ids list
     * @return array
     */
    public function getProductIds()
    {
        if ($this->_ids === FALSE) {
            $collection = $this->_getProductCollection();
            $this->_ids = $collection->getColumnValues('entity_id');
            //if(count($this->_ids) > 0) {
            //   array_shift($this->_ids);
            //}
        }
        return $this->_ids;
    }

    public function getSwatches()
    {
        $swatches = array();
        $collection = $this->_getProductCollection();
        $pId = '';
        foreach ($collection as $_product) {
            if ($_product->getSwatchesData()) {
                $swatches[$_product->getId()] = $_product->getSwatchesData();
                if(empty($pId)) {
                    $pId = $_product->getId();
                }
            }
        }

        //if(count($swatches) > 0) {
        //    unset($swatches[$pId]);
        //}

        return $swatches;
    }


    public function loadSwatchesToProductCollection($collection)
    {
        $helper = Mage::helper('belvgcolorswatch');
        if ($helper->isEnabled()) {
            $optionLabels     = array();
            $swatches         = array();
            $swatchesObjects  = array();
            $optionIds        = array();
            $attributeIds     = array();

            //$collection = $observer->getEvent()->getCollection();
            //$filters    = $collection->getLimitationFilters();

            //if (isset($filters['category_id']) && $filters['category_id']) {
                $setting = $helper->getListSettings();
                foreach ($collection AS $item) {
                    if ($item->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
                        $attributeCollection = $item->getTypeInstance()->getConfigurableAttributes();
                        foreach ($attributeCollection AS $attribute) {
                            $attributeId         = $attribute->getProductAttribute()->getAttributeId();
                            if (in_array($attributeId, $setting['category_attributes'])) {
                                $attributeCode   = $attribute->getProductAttribute()->getAttributeCode();
                                $attributeValues = $attribute->getProductAttribute()->getSource()->getAllOptions();
                                foreach ($attributeValues AS $attributeValue) {
                                    $optionLabels[$attributeValue['value']] = $attributeValue['label'];
                                }

                                $usedProducts = $item->getTypeInstance()->getUsedProducts();
                                foreach ($usedProducts AS $usedProduct) {
                                    $optionId = $usedProduct->getData($attributeCode);
                                    $optionIds[$optionId]    = $optionId;
                                    $attributeIds[$optionId] = $attributeId;

                                    if ($usedProduct->getSmallImage() && $usedProduct->getSmallImage() != 'no_selection') {
                                        $swatches[$item->getId()][$attributeId][$optionId]['img'] = Mage::helper('catalog/image')->init($usedProduct, 'small_image');

                                        $width  = (int) $helper->storeConfig('category/product_list_img_width');
                                        $height = (int) $helper->storeConfig('category/product_list_img_height');
                                        if ($width && $height) {
                                            $swatches[$item->getId()][$attributeId][$optionId]['img'] = (string) $swatches[$item->getId()][$attributeId][$optionId]['img']->resize($width, $height);
                                        } else {
                                            $swatches[$item->getId()][$attributeId][$optionId]['img'] = (string) $swatches[$item->getId()][$attributeId][$optionId]['img'];
                                        }
                                    } else {
                                        if (!isset($swatches[$item->getId()][$attributeId][$optionId]['img'])) {
                                            $swatches[$item->getId()][$attributeId][$optionId]['img'] = '';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (count($optionIds)) {
                    $swatchesAttribute = Mage::getModel('attricons/attribute')
                        ->getResourceCollection()
                        ->addFieldToFilter('option_id', array('in' => $optionIds));

                    $swatchesProduct = Mage::getModel('attricons/product')
                        ->getResourceCollection()
                        ->addFieldToFilter('value_index', array('in' => $optionIds));

                    foreach ($swatchesProduct AS $swatch) {
                        $swatchesObjects[$swatch->getProductId()][$attributeIds[$swatch->getValueIndex()]][$swatch->getValueIndex()] = $swatch;
                    }

                    foreach ($swatchesAttribute AS $swatch) {
                        if (!isset($swatchesObjects[0][$swatch->getAttributeId()][$swatch->getOptionId()])) {
                            $swatchesObjects[0][$swatch->getAttributeId()][$swatch->getOptionId()] = $swatch;
                        }
                    }

                    foreach ($swatches AS $productId => $attributes) {
                        foreach ($attributes AS $attributeId => $options) {
                            foreach ($options AS $optionId => $item) {
                                if (!isset($swatchesObjects[$productId][$attributeId][$optionId])) {
                                    if (isset($swatchesObjects[0][$attributeId][$optionId])) {
                                        $swatchesObjects[$productId][$attributeId][$optionId] = $swatchesObjects[0][$attributeId][$optionId];
                                    } else {
                                        $swatchesObjects[$productId][$attributeId][$optionId] = new Varien_Object;
                                    }
                                }

                                if (isset($optionLabels[$optionId])) {
                                    $swatches[$productId][$attributeId][$optionId]['icon'] = Mage::helper('attricons')->generateIcoCategoryHtml($swatchesObjects[$productId][$attributeId][$optionId], $optionLabels[$optionId], FALSE, FALSE, FALSE, $setting['catalog_icon_width'], $setting['catalog_icon_height']);
                                } else {
                                    unset($swatches[$productId][$attributeId][$optionId]);
                                }
                            }
                        }
                    }

                    foreach ($collection AS &$item) {
                        if (isset($swatches[$item->getId()])) {
                            $item->setData('swatches_data', $swatches[$item->getId()]);
                        }
                    }
                }
            //}
        }

        return $this;
    }
}
