<?php
//require 'app/code/core/Mage/CatalogSearch/controllers/ResultController.php';

require_once Mage::getModuleDir('controllers', 'Mage_CatalogSearch') . DS . 'ResultController.php';

class TM_AjaxLayeredNavigation_ResultController 
        extends Mage_CatalogSearch_ResultController
{
    /**
     * Display search result
     */
    public function indexAction()
    {
        $query = array();
        if (Mage::getStoreConfig('ajaxlayerednavigation/seo/enabled')
                && !Mage::helper('ajaxlayerednavigation')->isAdvancedSearchPage()) {
            $query = $this->getRequest()->getParam('filters');
            $categoryId = $this->getRequest()->getParam('id');
            $category = Mage::getModel('catalog/category')->load($categoryId);
            $currentUrl = Mage::helper('core/url')->getCurrentUrl();
            $seoSuffix = Mage::getStoreConfig('ajaxlayerednavigation/seo/suffix');
            $isSeoUrl = strpos($currentUrl, $seoSuffix);
            if (!$isSeoUrl) {
                $query = $this->getRequest()->getQuery();
                
                $seoSuffix = Mage::getStoreConfig('ajaxlayerednavigation/seo/suffix');
                $mageSuffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
                $model = Mage::getResourceModel('ajaxlayerednavigation/filters');
                $seoOptions = $model->getSeoOptionsValue();

                $urlPath = $urlPath = 'catalogsearch/result/'. $seoSuffix .'/';
                
                $model = Mage::getModel('core/url');
                $url = $model->getDirectUrl($urlPath);
                $url = substr($url, 0, -1);
                foreach($query as $key=>$value) {
                    if (null!==$value) {
                        if ('q' !== $key) {
                            $url .= '/'.$key .'/'.$seoOptions[$value];
                        } else {
                            $url .= '/'.$key .'/'.$value;
                        }
                    }
                }

                $url = str_replace('/index','',$url);

                $url .= $mageSuffix;
                header ('HTTP/1.1 301 Moved Permanently');
                header ('Location: ' . $url);
                exit;
            }
        } else {
            $query = $this->getRequest()->getQuery();
        }
        // $query = $this->getRequest()->getParam('filters');
        // if (null == $query) {
            // $query = $this->getRequest()->getQuery();
        // }

        Mage::register('query_request', $query);
        
        if (!$this->getRequest()->isXmlHttpRequest()) {
            return parent::indexAction();
        }
        $this->loadLayout();
        //$this->renderLayout();
        $layout = $this->getLayout();
        //$update = $layout->getUpdate();
        //$update->merge('catalogsearch_result_index');
        //$layout->generateXml();
        //$layout->generateBlocks();
        $blocks = array(
            'list'     => 'search.result',
            'filter'   => 'tm.catalogsearch.navigation',
            'brend'    => 'my.catalogsearch.top.brend'
        );
        $result = array();
        foreach ($blocks as $key => $block) {
            $result[$key] = $layout->getBlock($block)->toHtml();
        }
        
        $result['minRange'] = $this->getMinPrice();
        $result['maxRange'] = $this->getMaxPrice();
        
        $newQuery = Mage::helper('catalogsearch')->getQuery();
        $result['categoryName'] = '';
        
        $this->getResponse()->setBody(Zend_Json::encode($result));
        
    }

    public function getMaxPrice()
    {
        if ($this->getRequest()->getParam('price')) {
            $price = explode(',', $this->getRequest()->getParam('price'));
            return $price[1];
        }
        
        return (int)Mage::getModel('ajaxlayerednavigation/price')->getMaxRangeInt();
    }

    public function getMinPrice()
    {
        if ($this->getRequest()->getParam('price')) {
            $price = explode(',', $this->getRequest()->getParam('price'));
            return $price[0];
        }
        
        return (int)Mage::getModel('ajaxlayerednavigation/price')->getMinRangeInt();
    }
}
