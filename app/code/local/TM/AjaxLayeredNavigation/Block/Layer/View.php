<?php

class TM_AjaxLayeredNavigation_Block_Layer_View extends Mage_Catalog_Block_Layer_View
{
    /**
     * Get attribute filter block name
     *
     * @return string
     */
    protected function _getAttributeFilterBlockName()
    {
        if (!Mage::getStoreConfig('ajaxlayerednavigation/general/enabled')) {
            return parent::_getAttributeFilterBlockName();
        }
        return 'ajaxlayerednavigation/layer_filter_attribute';
    }

    /**
     * Prepare child blocks
     *
     * @return Mage_Catalog_Block_Layer_View
     */
    protected function _prepareLayout()
    {
        if (!Mage::getStoreConfig('ajaxlayerednavigation/general/enabled')) {
            return parent::_prepareLayout();
        }
        $stateBlock = $this->getLayout()->createBlock('ajaxlayerednavigation/layer_state')
            ->setLayer($this->getLayer());

        $categryBlock = $this->getLayout()->createBlock('ajaxlayerednavigation/layer_filter_category')
            ->setLayer($this->getLayer())
            ->init();

        $this->setChild('layer_state', $stateBlock);
        $this->setChild('ajaxlayerednavigation_filter', $categryBlock);

        $filterableAttributes = $this->_getFilterableAttributes();
        foreach ($filterableAttributes as $attribute) {
        	
            $filterBlockName = $this->_getAttributeFilterBlockName();

            if ($attribute->getAttributeCode() == 'price') {
                $filterBlockName = 'ajaxlayerednavigation/layer_filter_price';
            } elseif ($attribute->getBackendType() == 'decimal') {
                $filterBlockName = 'ajaxlayerednavigation/layer_filter_decimal';
            }

            $this->setChild($attribute->getAttributeCode().'_filter',
                $this->getLayout()->createBlock($filterBlockName)
                    ->setLayer($this->getLayer())
                    ->setAttributeModel($attribute)
                    ->init());
        }

        $this->getLayer()->apply();
        return $this;
    }

    /**
     * Get all fiterable attributes of current category
     *
     * @return array
     */
    protected function _getFilterableAttributes()
    {
        if (!Mage::getStoreConfig('ajaxlayerednavigation/general/enabled')) {
            return parent::_getFilterableAttributes();
        }
        $attributes = $this->getData('_filterable_attributes');
        if (is_null($attributes)) {
            $attributes = $this->getLayer()->getFilterableAttributes();
            $this->setData('_filterable_attributes', $attributes);
        }

        return $attributes;
    }

    /**
     * Get layered navigation state html
     *
     * @return string
     */
    public function getStateHtml()
    {
        return $this->getChildHtml('layer_state');
    }

    /**
     * Get all layer filters
     *
     * @return array
     */
    public function getFilters()
    {
        $filters = array();
        if ($categoryFilter = $this->_getCategoryFilter()) {
            $filters[] = $categoryFilter;
        }

        $filterableAttributes = $this->_getFilterableAttributes();

        foreach ($filterableAttributes as $attribute) {
            $filters[] = $this->getChild($attribute->getAttributeCode().'_filter');
        }

        return $filters;
    }

    /**
     * Get category filter block
     *
     * @return Mage_Catalog_Block_Layer_Filter_Category
     */
    protected function _getCategoryFilter()
    {
        if (!Mage::getStoreConfig('ajaxlayerednavigation/general/enabled')) {
            return parent::_getCategoryFilter();
        }
        return $this->getChild('ajaxlayerednavigation_filter');
    }

    /**
     * Check availability display layer options
     *
     * @return bool
     */
    public function canShowOptions()
    {
        foreach ($this->getFilters() as $filter) {
            if ($filter->getItemsCount()) {
                return true;
    }
        }
        return false;
    }

    /**
     * Check availability display layer block
     *
     * @return bool
     */
    public function canShowBlock()
    {
        return $this->canShowOptions() || count($this->getLayer()->getState()->getFilters());
    }

    /**
     * Retrieve Price Filter block
     *
     * @return Mage_Catalog_Block_Layer_Filter_Price
     */
    protected function _getPriceFilter()
    {
        return $this->getChild('_price_filter');
    }
    
}