<?php

class TM_AjaxLayeredNavigation_Model_Observer
{

    public function addToRegisterRootQuery($observer)
    {
        if($observer->getEvent()->getControllerAction()->getFullActionName() == 'ajaxlayerednavigation_layered_view')
        {
            $query = $observer->getControllerAction()->getRequest()->getQuery();
            Mage::register('root_request', $query);
        }
    }
}
