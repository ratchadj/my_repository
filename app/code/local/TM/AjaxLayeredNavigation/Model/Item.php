<?php

class TM_AjaxLayeredNavigation_Model_Item extends Mage_Catalog_Model_Layer_Filter_Item
{
    const DELIMITER = ',';
    
    public static function getDelimiter()
    {
        return self::DELIMITER;
    }
    
    /**
     * Get filter item url
     *
     * @return string
     */
    public function getUrl()
    {
        $query = $this->getFilter()->getUrlValue($this->getValue(), $this->getFilter()->getRequestVar());

        $currentCategory = Mage::registry('current_category');
        
        $currentCategoryId = $currentCategory->getId();
        $rootCategoryId = Mage::app()->getStore()->getRootCategoryId();
        $res = array();
        foreach($query as $key => $value) {
            $res[$key] = $value;
        }
        
        if ($this->isCatalogSearchPage()) {
            if (Mage::getStoreConfig('ajaxlayerednavigation/seo/enabled')) {
                $seoSuffix = Mage::getStoreConfig('ajaxlayerednavigation/seo/suffix');
                $mageSuffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
                
                $urlPath = 'catalogsearch/result/'. $seoSuffix .'/';

                $url = Mage::getUrl($urlPath, array(
                    '_nosid' => true
                ));
                $url = substr($url, 0, -1);
                foreach($res as $key=>$value) {
                    if (null!==$value) {
                        $url .= '/'.$key .'/'.$value; 
                    }
                }
                
                //$url = str_replace('/index','',$url);
                $url .= $mageSuffix;
                
            } else {
                $currentUrl = Mage::helper('core/url')->getCurrentUrl();
                $queryString = parse_url($currentUrl);
                
                $oldQuery = $this->convertUrlQuery($queryString['query']);
                $newQuery = array_merge($query, $oldQuery);
                $urlPath = 'catalogsearch/result/index';
                $url = Mage::getUrl($urlPath, array(
                    '_query' => $newQuery
                ));
            }
        } elseif ($this->isAdvancedSearchPage()) {
        	$newQuery = '';
            $currentUrl = Mage::helper('core/url')->getCurrentUrl();
            $queryString = parse_url($currentUrl);

            // $oldQuery = $queryString['query'];

            // foreach($query as $key => $value) {
            //     if ('p' == $key) {
            //         continue;
            //     }
            //     if ('price' == $key) {
            //     	$newQuery .= '&'.$key.'[from]='.$value['from'];
            //     	$newQuery .= '&'.$key.'[to]='.$value['to'];
            //     } else {
            //     	if (is_array($value)) {
            //     		foreach ($value as $item) {
            //     			$newQuery .= '&'.$key.'[]='.$item;
            //     		}
            //     	} else {
            //     		$newQuery .= '&'.$key.'[]='.$value;
            //     	}
            //     }
            // }

            $urlPath = 'catalogsearch/advanced/result';
            $url = Mage::getUrl($urlPath, array(
                '_query' => $query
            ));
			
        } elseif ((int)$currentCategoryId != (int)$rootCategoryId) {
            if (Mage::getStoreConfig('ajaxlayerednavigation/seo/enabled')) {
                $seoSuffix = Mage::getStoreConfig('ajaxlayerednavigation/seo/suffix');
                $mageSuffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
                
                $urlPath = str_replace($mageSuffix, '', $currentCategory->getUrlPath());
                
                $url = Mage::getUrl($urlPath, array(
                    '_nosid' => true
                ));

                $url = substr($url. $seoSuffix .'/', 0, -1);
                foreach($res as $key=>$value) {
                    if (null!==$value) {
                        $url .= '/'.$key .'/'.$value; 
                    }
                }
                
                //$url = str_replace('/index','',$url);
                $url .= $mageSuffix;
                
            } else {
                $urlPath = $currentCategory->getUrlPath();
                $url = Mage::getUrl($urlPath, array(
                    '_query' => $query,
                    '_current' => true,
                    '_use_rewrite' => true,
                ));
            }
        } else {
            if (Mage::getStoreConfig('ajaxlayerednavigation/seo/enabled')) {
                $seoSuffix = Mage::getStoreConfig('ajaxlayerednavigation/seo/suffix');
                $mageSuffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
                
                $urlPath = 'ajaxlayerednavigation/'.$seoSuffix .'/f/';
                $url = Mage::getUrl($urlPath, array(
                    '_nosid' => true
                ));
                $url = substr($url, 0, -1);
                foreach($res as $key=>$value) {
                    if (null!==$value) {
                        $url .= '/'.$key .'/'.str_replace($mageSuffix, '', $value); 
                    }
                }
                
                //$url = str_replace('/index','',$url);
                $url .= $mageSuffix;
            } else {
                $urlPath = 'ajaxlayerednavigation/layered/view';
                $url = Mage::getUrl($urlPath, array(
                    '_query' => $query
                ));
            }
        }
        
       $url = str_replace(array('/?', '.html/'), array('?', '.html'), $url);

        return utf8_decode(urldecode($url));
    }
    
    public function getRemoveUrl()
    {
        $currentValue = $this->getValue();
        $query = $this->getFilter()->getResetValue($currentValue, $this->getFilter()->getRequestVar());
        
        $currentCategory = Mage::registry('current_category');
        
        $currentCategoryId = $currentCategory->getId();
        $rootCategoryId = Mage::app()->getWebsite(true)->getDefaultStore()->getRootCategoryId();
        $res = array();
        foreach($query as $key => $value) {
            $res[$key] = $value;
        }
        
        if ($this->isCatalogSearchPage()) {
            if (Mage::getStoreConfig('ajaxlayerednavigation/seo/enabled')) {
                if (count($res) > 0) {
                    $seoSuffix = Mage::getStoreConfig('ajaxlayerednavigation/seo/suffix');
                    $mageSuffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
                    
                    $urlPath = 'catalogsearch/result/' . $seoSuffix .'/';
                    $url = Mage::getUrl($urlPath, array(
                        '_nosid' => true,
                    ));
                    //$url = str_replace('/index','',$url);
                    $url = substr($url, 0, -1);
                    foreach($res as $key=>$value) {
                        if (null!==$value) {
                            $url .= '/'.$key .'/'.str_replace($mageSuffix, '', $value); 
                        }
                    }
                    $url .= $mageSuffix;
                } else {
                    $url = $currentCategory->getUrl();
                }
            } else {
                $oldQuery = array();
                $currentUrl = Mage::helper('core/url')->getCurrentUrl();
                $queryString = parse_url($currentUrl);
                
                $oldQuery = $this->convertUrlQuery($queryString['query']);
                $newQuery = array_merge($query, $oldQuery);
                $urlPath = 'catalogsearch/result/index';
                $url = Mage::getUrl($urlPath, array(
                    '_query' => $newQuery
                ));
            }
        } elseif ($this->isAdvancedSearchPage()) {

            $urlPath = 'catalogsearch/advanced/result';
            $url = Mage::getUrl($urlPath, array(
                '_query' => $query
            ));
        } elseif ((int)$currentCategoryId != (int)$rootCategoryId) {
            if (Mage::getStoreConfig('ajaxlayerednavigation/seo/enabled')) {
                if (count($res) > 0) {
                    $seoSuffix = Mage::getStoreConfig('ajaxlayerednavigation/seo/suffix');
                    $mageSuffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
                    
                    $urlPath = str_replace($mageSuffix, '', $currentCategory->getUrlPath());
                    $url = Mage::getUrl($urlPath, array(
                        '_nosid' => true,
                    ));
                    //$url = str_replace('/index','',$url);
                    $url = substr($url. $seoSuffix .'/', 0, -1);
                    foreach($res as $key=>$value) {
                        if (null!==$value) {
                            $url .= '/'.$key .'/'.str_replace($mageSuffix, '', $value); 
                        }
                    }
                    $url .= $mageSuffix;
                } else {
                    $url = $currentCategory->getUrl();
                }
            } else {
                $urlPath = $currentCategory->getUrlPath();
                $url = Mage::getUrl($urlPath, array(
                    '_query' => $query,
                    '_current' => true,
                    '_use_rewrite' => true,
                ));
            }
        } else {
            if (Mage::getStoreConfig('ajaxlayerednavigation/seo/enabled')) {
                $activeFilter = false;
                foreach($res as $value) {
                    if (null !== $value) {
                        $activeFilter = true;
                    }
                }
                if ($activeFilter) {
                    $seoSuffix = Mage::getStoreConfig('ajaxlayerednavigation/seo/suffix');
                    $mageSuffix = Mage::getStoreConfig('catalog/seo/category_url_suffix');
                    
                    $urlPath = 'ajaxlayerednavigation/layered/view/';
                    $url = Mage::getUrl($urlPath, $res);
                    //$url = str_replace('/index','',$url);
                    $url = substr($url, 0, -1);
                    $url .= $mageSuffix;
                } else {
                    $url = Mage::getUrl('ajaxlayerednavigation/layered/view');
                }
            } else {
                $urlPath = 'ajaxlayerednavigation/layered/view';
                $url = Mage::getUrl($urlPath, array(
                    '_query' => $query
                ));
            }
        }
        
        $url = str_replace(array('/?', '.html/'), array('?', '.html'), $url); 

        return utf8_decode(urldecode($url));
    }
    
    public function isCatalogSearchPage()
    {
        return (Mage::app()->getFrontController()->getRequest()->getRouteName() == 'catalogsearch'
                && Mage::app()->getFrontController()->getRequest()->getControllerName() == 'result');
    }
    
    public function isAdvancedSearchPage()
    {
        return (Mage::app()->getFrontController()->getRequest()->getRouteName() == 'catalogsearch' 
                && Mage::app()->getFrontController()->getRequest()->getControllerName() == 'advanced');
    }
    
    public function convertUrlQuery($query) {
        $queryParts = explode('&', $query);
        
        $params = array();
        foreach ($queryParts as $param) {
            
            $item = explode('=', $param);
            if ($this->isAdvancedSearchPage()) {
                $params[$item[0]] = $item[1];
            } else {
                if ($item[0] == 'q' || $item[0] == 'x' || $item[0] == 'y') {
                    $params[$item[0]] = $item[1];
                }
            }
        }
       
        return $params;
    } 
}
