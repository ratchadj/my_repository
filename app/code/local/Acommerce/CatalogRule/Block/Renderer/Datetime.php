<?php

class Acommerce_CatalogRule_Block_Renderer_Datetime extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    /**
     * Date format string
     */
    protected static $_format = null;

    /**
     * Retrieve datetime format
     *
     * @return unknown
     */
    protected function _getFormat()
    {
        $format = $this->getColumn()->getFormat();
        if (!$format) {
            if (is_null(self::$_format)) {
                try {
                    self::$_format = Mage::app()->getLocale()->getDateTimeFormat(
                        Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM
                    );
                }
                catch (Exception $e) {
                    Mage::logException($e);
                }
            }
            $format = self::$_format;
        }
        return $format;
    }

    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $data = $this->_getValue($row);
        if ($data) {
            $format = $this->_getFormat();
            try {
                $data = strtotime($data);
                $data = new Zend_Date($data);
            }
            catch (Exception $e)
            {
                $data = strtotime($data);
                $data = new Zend_Date($data);
            }
            return $data;
        }
        return $this->getColumn()->getDefault();        
    }
}
