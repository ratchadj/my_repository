<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @copyright  Copyright (c) 2010 Acommerce Medien GmbH & Co. KG (http://www.phoenix-medien.de)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE `catalogrule`
	CHANGE COLUMN `from_date` `from_date` DATETIME NULL DEFAULT NULL COMMENT 'From Date' AFTER `description`,
	CHANGE COLUMN `to_date` `to_date` DATETIME NULL DEFAULT NULL COMMENT 'To Date' AFTER `from_date`;");

$installer->run("
ALTER TABLE `catalogrule_product_price`	
	CHANGE COLUMN `latest_start_date` `latest_start_date` DATETIME NULL DEFAULT NULL COMMENT 'Latest StartDate' AFTER `website_id`,
	CHANGE COLUMN `earliest_end_date` `earliest_end_date` DATETIME NULL DEFAULT NULL COMMENT 'Earliest EndDate' AFTER `latest_start_date`;");
    
    
$installer->endSetup();
