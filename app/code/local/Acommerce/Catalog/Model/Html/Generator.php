<?php
    class Acommerce_Catalog_Model_Html_Generator extends Mage_Core_Model_Abstract {

        public function generateProduct($productId) {

            Mage::getDesign()->setArea('frontend');
            Mage::getDesign()->setPackageName('ais');

            $product = Mage::getModel('catalog/product')->load($productId);
            $attributeData = $this->getAdditionalData($product);
            $campaigns = $this->getCampaigns($product);
            $regions = $this->getRegions();

            //var_dump($campaigns);
            $this->generateDestricts();
            $this->generateThankyou();
            $template = Mage::app()->getLayout()->createBlock('core/template')
                    ->setProduct($product)
                    ->setAdditionalData($attributeData)
                    ->setCampaigns($campaigns)
                    ->setRegions($regions)
                    ->setTemplate('catalog/product/generator/product.phtml')
                    ->toHtml();

            $this->_ftpFile($product->getUrlKey(), $template);
        }

        public function generateThankyou() {
            $template = Mage::app()->getLayout()->createBlock('core/template')
                    ->setTemplate('catalog/product/generator/thanks.phtml')
                    ->toHtml();
            $this->_ftpFile('thankyou', $template);
        }

        public function generateDestricts() {
            $districts = $this->getDistricts();
            $subDistricts = $this->getSubDistricts();

            $template = Mage::app()->getLayout()->createBlock('core/template')
                    ->setDistricts($districts)
                    ->setSubDistricts($subDistricts)
                    ->setTemplate('catalog/product/generator/districts.phtml')
                    ->toHtml();

            $this->_writeFile('districts.js', $template);

        }

        public function getSubDistricts() {
            $subDistricts = Mage::getResourceModel('adirectory/subdistrict_collection');
            $subDistricts->getSelect()->order('main_table.default_name ASC');
            $datas = array();
            foreach($subDistricts as $subDistrict) {
                $data = new stdClass();
                $data->district_id = $subDistrict->getDistrictId();
                $data->subdistrict_id = $subDistrict->getSubdistrictId();
                $data->subdistrict_name = $subDistrict->getDefaultName();
                $data->postcode = $subDistrict->getPostCode();
                $datas[] = $data;
            }
            return $datas;
        }

        public function getDistricts() {
            $districts = Mage::getResourceModel('adirectory/district_collection');
            $districts->getSelect()->order('main_table.default_name ASC');
            $datas = array();
            foreach($districts as $district) {
                $data = new stdClass();
                $data->region_id = $district->getRegionId();
                $data->district_id = $district->getDistrictId();
                $data->district_name = $district->getDefaultName();
                $datas[] = $data;
            }
            return $datas;
        }

        public function getRegions() {
            $countryId = Mage::getStoreConfig('general/country/default');

            $regions = Mage::getResourceModel('directory/region_collection');
            $regions->addFieldToFilter('main_table.country_id', array('eq' => $countryId));

            return $regions;
        }

        public function getAdditionalData($product, $excludeAttr = array())
        {
            $data = array();
            $attributes = $product->getAttributes();
            $attrSetId = $product->getAttributeSetId();

            $attributeSet = Mage::getModel('eav/entity_attribute_set')->load($attrSetId);

            $attributeSetName = $attributeSet->getAttributeSetName();
            $mobileSpecGroup = array(
                                'Processor & Operating System',
                                'Display & Camera',
                                'Memory & SIM Card',
                                'Connectivity',
                                'Style & Size',
                                'Battery',
                                //'In The Box'
                                );

            foreach ($attributes as $attribute) {    //
                if ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), $excludeAttr)) {
                    $value = $attribute->getFrontend()->getValue($product);

                    if (!$product->hasData($attribute->getAttributeCode())) {
                        $value = Mage::helper('catalog')->__('N/A');
                    } elseif ((string)$value == '') {
                        $value = Mage::helper('catalog')->__('No');
                    } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                        $value = Mage::app()->getStore()->convertPrice($value, true);
                    }

                    $group_id = $attribute->getData('attribute_set_info/' . $attrSetId . '/group_id');
                    $group = Mage::getModel('eav/entity_attribute_group')->load($group_id);

                    if($group->getData('attribute_group_name') == 'AIS')
                    {
                        continue;
                    }

                    if($attributeSetName == 'Mobile Phone' && in_array($group->getData('attribute_group_name'), $mobileSpecGroup)) {
                        if (is_string($value) && strlen($value)) {
                            $data[$group->getData('attribute_group_name')][] = array(
                                'label' => $attribute->getStoreLabel(),
                                'value' => $value,
                            );
                        }
                    } else {
                       if (is_string($value) && strlen($value)) {
                            $data[$group->getData('attribute_group_name')][] = array(
                                'label' => $attribute->getStoreLabel(),
                                'value' => $value,
                            );
                        }
                    }
                }
            }
            return $data;
        }


        public function getCampaigns($product) {

            $campaigns = array();

            if($product->getTypeId() == "configurable") {
                $productIds = $product->getTypeInstance()->getUsedProductIds();
            } else {
                $productIds = array($product->getId());
            }


            $products = array();
            foreach($productIds as $productId) {

                $finalPrice = Mage::getModel('catalog/product')->load($productId)->getFinalPrice();

                $discounts = Mage::getResourceModel('campaigns/discount_collection')
                                  ->getDiscountPrices($productId);

                $discountPrices = array();
                $freeGoods = array();
                if(count($discounts) > 0) {
                    foreach($discounts as $item) {
                        $key = $item->getCampaignId().'|'.$item->getIsAis();
                        if($item->getSalesPrice() > 0)
                        {
                            $discountPrices[$key] = (float)$item->getSalesPrice();
                        }

                        if($item->getDiscountType() == 'Percent') {
                            if(isset($discountPrices[$key])) {
                                $temp = $discountPrices[$key];
                                $discountPrices[$key] = ($temp - (($temp * $item->getDiscountValue()) / 100));
                            } else {
                                $discountPrices[$key] = ($finalPrice - (($finalPrice * $item->getDiscountValue()) / 100));
                            }
                        } else {
                            if(isset($discountPrices[$key])) {
                                $temp = $discountPrices[$key];
                                $discountPrices[$key] = $temp - $item->getDiscountValue();
                            } else {
                                $discountPrices[$key] = $finalPrice - $item->getDiscountValue();
                            }
                        }
                    }
                }

                $p = new stdClass();
                $p->productId = $productId;
                $p->discount = $discountPrices;
                $p->finalPrice = $finalPrice;


                $freegoods = Mage::getResourceModel('campaigns/freegoods_collection')
                                  ->getFreeGoodItems($productId);

                if(count($freegoods) > 0) {
                    foreach($freegoods as $item) {
                        $key = $item->getCampaignId().'|'.$item->getIsAis();
                        $freeGoods[$key] = unserialize($item->getFreeGoods());
                    }
                }
                $p->freeGoods = $freeGoods;
                $products[$productId] = $p;
            }
            return $products;
        }

        protected function _getDirExport()
        {
            $path = Mage::getBaseDir();
            if (!is_dir($path . DS . 'line')) {
                mkdir($path . DS . 'line', 0777, true);
            }

            if (!is_dir($path . DS . 'line'. DS . 'js')) {
                mkdir($path . DS . 'line'. DS . 'js', 0777, true);
            }

            return $path . DS . 'line'. DS . 'js';
        }

        protected function _writeFile($filename, $content)
        {
            $io = new Varien_Io_File();
            $path = $this->_getDirExport();

            $file = $path . DS . $filename;

            $io->setAllowCreateFolders(true);
            $io->open(array('path' => $path));
            $io->streamOpen($file, 'w+');
            $io->streamLock(true);
            $io->streamWrite($content);
            $io->streamUnlock();
            $io->streamClose();
            return $file;
        }

        protected function _ftpFile($filename, $content) {
            try {
                $usrname = Mage::getStoreConfig('line/export/usrname');
                $password = Mage::getStoreConfig('line/export/password');
                $cd = Mage::getStoreConfig('line/export/path');

                $host = Mage::getStoreConfig('line/export/host');
                $hosts = explode(',', $host);

                if(count($hosts) > 0) {
                    foreach($hosts as $host) {
                        if(trim($host) == '')
                        {
                            continue;
                        }

                        try {
                            $sftp = new Varien_Io_Sftp();
                            $sftp->open(
                                    array(
                                            'host'      => trim($host),
                                            'username'  => $usrname,
                                            'password'  => $password,
                                        )
                                    );

                            if($cd) {
                                $sftp->cd($cd);
                            }

                            $sftp->write($filename. '.html', $content);
                            $sftp->close();
                        } catch (Exception $e) {
                          Mage::log($e->getMessage(), null, 'line.log');
                        }
                    }
                }

            } catch (Exception $e) {
              Mage::log($e->getMessage(), null, 'line.log');
            }
        }
    }

?>