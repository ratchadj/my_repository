<?php

/**
 * Accommerce extension for Magento
 *
 * Add product edit attributes, tabs, removes some standart tabs.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Accommerce Product module to newer versions in the future.
 * If you wish to customize the Accommerce Product module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Accommerce
 * @package    Accommerce
 * @copyright  Copyright (C) 2011 Accommerce Web ltd (http://Accommerceweb.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Get available methods
 *
 * @category   Accommerce
 * @package    Accommerce
 * @subpackage Model
 * @author     Acommerce Team
 */
class Acommerce_Catalog_Model_Attributes_Shipping extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    protected $_options = array();

    /**
     * Get all options in array
     *
     * @param bool $inclAll include "All" option flag
     *
     * @return array
     */
    public function getAllOptions($inclAll = true)
    {
        if (!$this->_options) {
            $this->_options['free'] = array(
                        'value'    => 'free',
                        'label'    => Mage::helper('catalog')->__('Free'),
                        'selected' => 1 );
            $this->_options = array_merge($this->_options, $this->getMethodsArray());
            //$this->_options = $this->getMethodsArray();
        }
        return $this->_options;
    }

    /**
     * Get available methods array
     *
     * @return array
     */
    public function getMethodsArray()
    {
        $carriers = Mage::getSingleton('shipping/config')->getActiveCarriers();

        $methods = array();

        foreach (array_keys($carriers) as $carrierCode) {
            if ($carrierCode != 'googlecheckout' && $carrierCode != 'freeshipping') {
                $carrierName = Mage::getStoreConfig('carriers/' . $carrierCode . '/name');
                $methods[$carrierCode] = array(
                    'label' => $carrierName,
                    'value' => $carrierCode,
                );
            }
        }

        return $methods;
    }

    /**
     * Options compatible with grid
     *
     * @return array
     */
    public function getGridOptions()
    {
        $carriers = Mage::getSingleton('shipping/config')->getActiveCarriers();

        $methods = array();

        foreach (array_keys($carriers) as $carrierCode) {
            if ($carrierCode != 'googlecheckout' && $carrierCode != 'freeshipping') {
                $carrierName = Mage::getStoreConfig('carriers/' . $carrierCode . '/name');
                $methods[$carrierCode] = $carrierName;
            }
        }

        return $methods;
    }

    /**
     * Mock method for unittests
     *
     * @param string $value value
     *
     * @return string
     */
    public function getOptionId($value)
    {
        return $value;
    }

    /**
     * Option text mock
     *
     * @param string $value value
     *
     * @return string
     */
    public function getOptionText($value)
    {
        return $value;
    }

}
