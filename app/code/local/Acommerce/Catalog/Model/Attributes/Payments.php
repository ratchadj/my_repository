<?php

class Acommerce_Catalog_Model_Attributes_Payments extends Mage_Eav_Model_Entity_Attribute_Source_Table
{
    protected $_options = array();

    /**
     * Get all options in array
     *
     * @param bool $inclAll include "All" option flag
     *
     * @return array
     */
    public function getAllOptions($inclAll = true)
    {
        if (!$this->_options) {
            //$this->_options['all'] = array(
            //            'value'    => 'all',
            //            'label'    => Mage::helper('catalog')->__('All'),
            //            'selected' => 1 );
            //$this->_options = array_merge($this->_options, $this->getMethodsArray());
            $this->_options = $this->getMethodsArray();
        }
        return $this->_options;
    }

    /**
     * Get available methods array
     *
     * @return array
     */
    public function getMethodsArray()
    {
        $payments = Mage::getSingleton('payment/config')->getActiveMethods();

        $methods = array();

        foreach (array_keys($payments) as $paymentCode) {
            if ($paymentCode != 'customercredit' && $paymentCode != 'free' && $paymentCode != 'googlecheckout') {
                $paymentTitle = Mage::getStoreConfig('payment/' . $paymentCode . '/title');
                $methods[$paymentCode] = array(
                    'label'    => $paymentTitle,
                    'value'    => $paymentCode,
                    'selected' => 1,
                );
            }
        }

        return $methods;
    }

    /**
     * Options compatible with grid
     *
     * @return array
     */
    public function getGridOptions()
    {
        $payments = Mage::getSingleton('payment/config')->getActiveMethods();

        $methods = array();

        foreach (array_keys($payments) as $paymentCode) {
            $paymentTitle = Mage::getStoreConfig('payment/' . $paymentCode . '/title');
            $methods[$paymentCode] = $paymentTitle;
        }

        return $methods;
    }

    /**
     * Mock method for unittests
     *
     * @param string $value value
     *
     * @return string
     */
    public function getOptionId($value)
    {
        return $value;
    }

    /**
     * Option text mock
     *
     * @param string $value value
     *
     * @return string
     */
    public function getOptionText($value)
    {
        return $value;
    }

}