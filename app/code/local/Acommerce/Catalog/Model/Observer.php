<?php
	class Acommerce_Catalog_Model_Observer extends Mage_Core_Model_Abstract{
		
		const COMPARE_LIMIT = 3;
		
		public function limitProductCompare($event) {
			$helper = Mage::helper('catalog/product_compare');
			if ($helper->getItemCount()<self::COMPARE_LIMIT) return;

			$session = Mage::getSingleton('catalog/session');
			Mage::getSingleton('catalog/product_compare_list')->removeProduct($event->getProduct());

			$session->getMessages()->clear();
			$session->addNotice($helper->__('You have reached the limit of products to compare. Remove one and try again.'));
		} 
	}
?>