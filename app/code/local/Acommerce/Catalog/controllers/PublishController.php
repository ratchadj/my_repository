<?php
class Acommerce_Catalog_PublishController extends Mage_Core_Controller_Front_Action
{
    public function indexAction() {
        $productId = $this->getRequest()->getParam('id');
        Mage::getModel('acom_catalog/html_generator')->generateProduct($productId);
        echo "The product has been published.";
    }
}


