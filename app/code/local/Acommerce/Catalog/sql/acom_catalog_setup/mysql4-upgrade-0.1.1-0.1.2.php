<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product', 'line_title',
                         array(
    'group' => 'Line',
    'type' => 'varchar',
    'input' => 'text',
    'label' => Mage::helper('catalog')->__('Title'),
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'default_value' => 0,
    'visible_on_front' => 1,
    'used_in_product_listing' => 1,
));

$installer->updateAttribute('catalog_product', 'line_title', 'is_global',
                            Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE);

$installer->updateAttribute('catalog_product', 'line_title', 'used_in_product_listing', 1);
$installer->endSetup();