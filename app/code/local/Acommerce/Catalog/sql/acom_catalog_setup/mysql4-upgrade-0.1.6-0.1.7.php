<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'allowed_payments', array(
    'group' => 'Line',
    'type' => 'text',
    'input' => 'multiselect',
    'label' => 'Allowed Payments',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'default_value' => 0,
    'visible_on_front' => 1,
    'source' => 'acom_catalog/attributes_payments',
));

$installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'allowed_payments', 'backend_model', 'eav/entity_attribute_backend_array');
$installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'allowed_payments', 'used_in_product_listing', 1);
$installer->endSetup();