<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product', 'line_qty',
                         array(
    'group' => 'Line',
    'type' => 'int',
    'input' => 'text',
    'label' => Mage::helper('catalog')->__('Qty'),
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'default_value' => 0,
    'visible_on_front' => 1,
    'used_in_product_listing' => 1,
));

$installer->addAttribute('catalog_product', 'event_start', array(
    'group' => 'Line',
    'type' => 'datetime',
    'input' => 'datetime',
    'label' => Mage::helper('catalog')->__('Event Start'),
    'backend' => 'eav/entity_attribute_backend_datetime',
    'frontend' => 'eav/entity_attribute_frontend_datetime',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 0,
    'default_value' => 0,
    'visible_on_front' => 1,
));

$installer->addAttribute('catalog_product', 'event_end', array(
    'group' => 'Line',
    'type' => 'datetime',
    'input' => 'datetime',
    'label' => Mage::helper('catalog')->__('Event End'),
    'backend' => 'eav/entity_attribute_backend_datetime',
    'frontend' => 'eav/entity_attribute_frontend_datetime',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 0,
    'default_value' => 0,
    'visible_on_front' => 1,
));


$installer->updateAttribute('catalog_product', 'line_qty', 'is_global',
                            Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL);
$installer->updateAttribute('catalog_product', 'line_qty', 'used_in_product_listing', 1);

$installer->updateAttribute('catalog_product', 'event_start', 'is_global',
                            Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL);
$installer->updateAttribute('catalog_product', 'event_start', 'used_in_product_listing', 1);


$installer->updateAttribute('catalog_product', 'event_end', 'is_global',
                            Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL);
$installer->updateAttribute('catalog_product', 'event_end', 'used_in_product_listing', 1);

$installer->endSetup();