<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product', 'line_short_description',
                         array(
    'group' => 'Line',
    'type' => 'text',
    'input' => 'textarea',
    'label' => Mage::helper('catalog')->__('Short Desctiption'),
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'default_value' => 0,
    'visible_on_front' => 1,
    'used_in_product_listing' => 1,
));

$installer->updateAttribute('catalog_product', 'line_short_description', 'is_global',
                            Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE);

$installer->updateAttribute('catalog_product', 'line_short_description', 'used_in_product_listing', 1);
$installer->endSetup();