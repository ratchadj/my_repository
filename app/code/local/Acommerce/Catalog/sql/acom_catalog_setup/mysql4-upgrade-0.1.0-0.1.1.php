<?php

/**
 * Oggetto extension for Magento
 *
 * Add product edit attributes, tabs, removes some standart tabs.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Oggetto DealAttributes module to newer versions in the future.
 * If you wish to customize the Oggetto DealAttributes module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Oggetto
 * @package    Oggetto_DealAttributes
 * @copyright  Copyright (C) 2011 Oggetto Web ltd (http://oggettoweb.com/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product', 'sort_order',
                         array(
    'group' => 'AIS',
    'type' => 'int',
    'input' => 'text',
    'label' => 'Sort Order (Home page Only)',
    'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'default_value' => 99,
    'visible_on_front' => 1,
    'used_in_product_listing' => 1,
));

$installer->updateAttribute('catalog_product', 'sort_order', 'is_global',
                            Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL);

$installer->updateAttribute('catalog_product', 'sort_order', 'used_in_product_listing', 1);
$installer->updateAttribute('catalog_product', 'sort_order', 'default_value', 99);

$installer->endSetup();