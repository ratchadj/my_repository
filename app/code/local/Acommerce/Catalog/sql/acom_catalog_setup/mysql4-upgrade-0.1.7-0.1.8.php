<?php

/**
 * Petloft extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Acommerce Product module to newer versions in the future.
 * If you wish to customize the Acommerce Product module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Acommerce
 * @package    Petloft_Product
 * @copyright  Copyright (C) 2011 Acommerce (http://acommerce.asia/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$installer = $this;
$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'allowed_shipping_methods', array(
    'group' => 'Line',
    'type' => 'text',
    'input' => 'multiselect',
    'label' => 'Allowed Shipping Methods',
    'global' => 1,
    'visible' => 1,
    'required' => 0,
    'user_defined' => 1,
    'default_value' => 0,
    'visible_on_front' => 1,
    'source' => 'acom_catalog/attributes_shipping',
));

$installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'allowed_shipping_methods', 'backend_model', 'eav/entity_attribute_backend_array');
$installer->updateAttribute(Mage_Catalog_Model_Product::ENTITY, 'allowed_shipping_methods', 'used_in_product_listing', 1);

$installer->endSetup();



