<?php

$installer = $this;
$installer->startSetup();

$installer->updateAttribute('catalog_product', 'line_short_description', 'is_wysiwyg_enabled', 1);
$installer->updateAttribute('catalog_product', 'line_short_description', 'is_html_allowed_on_front', 1);

$installer->updateAttribute('catalog_product', 'line_description', 'is_wysiwyg_enabled', 1);
$installer->updateAttribute('catalog_product', 'line_description', 'is_html_allowed_on_front', 1);

$installer->endSetup();



