<?php

/**
 * Acommerce extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Acommerce AdminLog module to newer versions in the future.
 * If you wish to customize the Acommerce AdminLog module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @copyright  Copyright (C) 2011 Acommerce Web ltd (http://www.acommerce.asia/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Campaigns Controllers
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @subpackage Controller
 * @author     dev acommerce <dev@acommerce.asia>
 */
class Acommerce_Directory_AjaxController extends Mage_Core_Controller_Front_Action
{
    public function districtAction() {
        $regionId = $this->getRequest()->getParam('region_id');
        $collection = Mage::getModel('adirectory/district')->getCollection();
        if($regionId) {
            $collection->addFieldToFilter('main_table.region_id', array('eq' => $regionId));
        }
        
        $collection->getSelect()->order('main_table.default_name ASC');        
        $items = array();
        foreach($collection as $item) {
            $items[$item->getDefaultName()] = $item->getDistrictId();
        }        
        $this->getResponse()->setHeader('Content-type','application/json', true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($items));     
    }  

    public function subdistrictAction() {
        $regionId = $this->getRequest()->getParam('region_id');
        $collection = Mage::getModel('adirectory/subdistrict')->getCollection()
                        ->joinDistrict();
        
        if($regionId) {
            $collection->addFieldToFilter('main_table.district_id', array('eq' => $regionId));
        }
        $collection->getSelect()->order('main_table.default_name ASC');
        //echo get_class($collection);
        $items = array();        
        foreach($collection as $item) {
            $items[$item->getDefaultName()] = $item->getPostCode();
        }
        $this->getResponse()->setHeader('Content-type','application/json', true);
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($items));    
    } 
}