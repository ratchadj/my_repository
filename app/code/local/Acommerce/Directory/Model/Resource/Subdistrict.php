<?php

class Acommerce_Directory_Model_Resource_Subdistrict extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('adirectory/subdistrict', 'subdistrict_id');
    }
}