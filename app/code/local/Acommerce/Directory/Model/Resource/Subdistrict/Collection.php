<?php

class Acommerce_Directory_Model_Resource_Subdistrict_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('adirectory/subdistrict');
    }


    /**
     * Init collection select
     *
     * @return Mage_Core_Model_Resource_Db_Collection_Abstract
     */
    protected function _initSelect()
    {
        $locale = Mage::app()->getLocale()->getLocaleCode();
        $this->getSelect()->from(array('main_table' => $this->getMainTable()))
                          ->where('main_table.locale' . '=?', $locale);
        return $this;
    }


    /**
     * Init collection select
     *
     * @return Mage_Core_Model_Resource_Db_Collection_Abstract
     */
    public function joinDistrict()
    {
        $this->getSelect()
                ->join(
                        array('district' => 'directory_district'),
                        "(district.district_id = main_table.district_id) and (district.locale = main_table.locale)",
                        array('district' => 'district.default_name')
                )
                ->order('main_table.district_id');
        ;
        return $this;
    }
}