<?php

class Acommerce_Directory_Model_Resource_District_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('adirectory/district');
    }


    /**
     * Init collection select
     *
     * @return Mage_Core_Model_Resource_Db_Collection_Abstract
     */
    protected function _initSelect()
    {
        $locale = Mage::app()->getLocale()->getLocaleCode();
        $this->getSelect()->from(array('main_table' => $this->getMainTable()))
                          ->where('main_table.locale' . '=?', $locale);
        return $this;
    }
}