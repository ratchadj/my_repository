<?php

class Acommerce_Directory_Model_District extends Mage_Core_Model_Abstract
{
    public function _construct(){
        parent::_construct();
        $this->_init('adirectory/district');
    }

    public function getName() {
        return $this->getData('default_name');
    }
}