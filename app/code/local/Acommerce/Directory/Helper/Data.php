<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Directory
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Directory data helper
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Acommerce_Directory_Helper_Data extends Mage_Directory_Helper_Data
{
    /**
     * Get Regions for specific Countries
     * @param string $storeId
     * @return array|null
     */
    protected function _getRegions($storeId)
    {
        $countryIds = array();

        $countryCollection = $this->getCountryCollection()->loadByStore($storeId);
        foreach ($countryCollection as $country) {
            $countryIds[] = $country->getCountryId();
        }

        /** @var $regionModel Mage_Directory_Model_Region */
        $regionModel = $this->_factory->getModel('directory/region');
        /** @var $collection Mage_Directory_Model_Resource_Region_Collection */
        $collection = $regionModel->getResourceCollection()
            ->addCountryFilter($countryIds)
            ->load();

        $regions = array(
            'config' => array(
                'show_all_regions' => $this->getShowNonRequiredState(),
                'regions_required' => $this->getCountriesWithStatesRequired()
            )
        );
        foreach ($collection as $region) {
            if (!$region->getRegionId()) {
                continue;
            }
            $regionId = strval($region->getRegionId());
            $regions[$region->getCountryId()]["$regionId "] = array(
                'code' => $region->getCode(),
                'name' => $this->__($region->getName())
            );
        }        
        
        return $regions;
    }
    
    public function getDistrictJson() {
        $collection = Mage::getModel('adirectory/district')->getCollection();
        $collection->getSelect()->order('main_table.default_name ASC');
        //echo get_class($collection);
        $items = array();
        foreach($collection as $item) {
            //if(485 == $item->getRegionId()) {
                $items[$item->getRegionId()][$item->getDistrictId()] = array('district_id' => $item->getDistrictId(), 'name' => $item->getDefaultName());
            //}
        }
        //Mage::log(var_export($items, true), null, 'rr.log');
        return json_encode ($items);
    }


    public function getSubDistrictJson() {
        $collection = Mage::getModel('adirectory/subdistrict')->getCollection()
                        ->joinDistrict();

        $collection->getSelect()->order('main_table.default_name ASC');
        //echo get_class($collection);
        $items = array();
        $itemData = array();
        foreach($collection as $item) {
            //if(485 == $item->getRegionId()) {
                $c = new stdClass();
                $c->district_id = $item->getDistrictId();
                $c->district_name = $item->getDistrict();

                $itemDatas[$item->getDistrictId()] = $c;
                $items[$item->getDistrictId()][$item->getSubdistrictId()] = array('subdistrict_id' => $item->getSubdistrictId(), 'name' => $item->getDefaultName(), 'post_code' => $item->getPostCode());
            //}
        }

        foreach($itemDatas as $key => $value) {
            $c = $itemDatas[$key];
            $c->subdistrict = $items[$key];
            $itemDatas[$key] = $c;
        };

        return json_encode ($itemDatas);
    }
}
