<?php

$installer = $this;
$installer->startSetup();

$installer->run("
CREATE TABLE {$this->getTable('directory_district')} (
    `district_id` INT(8) NOT NULL auto_increment,
    `region_id` INT(8) NOT NULL,
    `locale` VARCHAR(8) NOT NULL,
    `default_name` VARCHAR(255),
    PRIMARY KEY (`district_id`),    
    INDEX `directory_district_region_id` (region_id),
    INDEX `directory_district_locale` (locale)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;");


$installer->run("
CREATE TABLE {$this->getTable('directory_subdistrict')} (
    `subdistrict_id` INT(8) NOT NULL auto_increment,
    `district_id` VARCHAR(4),
    `locale` VARCHAR(8) NOT NULL,
    `default_name` VARCHAR(255),
    `post_code` VARCHAR(10),
    PRIMARY KEY (`subdistrict_id`),
    INDEX `directory_subdistrict_district_id` (district_id),
    INDEX `directory_subdistrict_locale` (locale)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;");

$installer->endSetup();
