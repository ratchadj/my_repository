<?php
    /* @var $installer Mage_Customer_Model_Entity_Setup */
    $installer = $this;
    $installer->startSetup();

    $eavConfig = Mage::getSingleton('eav/config');

    $installer->addAttribute('customer_address', 'sub_district', array(
        'label'             => 'Sub District',
        'type'              => 'varchar',    //backend_type
        'input'             => 'text', //frontend_input
        'frontend_class'    => '',       
        'backend'           => '',
        'frontend'          => '',
        'sort_order'        => 1100,
        'is_required'       => 0,
    ));


    $attribute = $eavConfig->getAttribute('customer_address', 'sub_district');
    //$attribute->addData($data);
    $usedInForms = array(
        'adminhtml_customer_address',
        'customer_address_edit',
        'customer_register_address'
    );
    $attribute->setData('used_in_forms', $usedInForms);
    $attribute->save();

    $installer->run("
        ALTER TABLE {$this->getTable('sales_flat_quote_address')} ADD COLUMN `sub_district` VARCHAR(100) AFTER `street`;
        ALTER TABLE {$this->getTable('sales_flat_order_address')} ADD COLUMN `sub_district` VARCHAR(100) AFTER `street`;
    ");
    $installer->endSetup();