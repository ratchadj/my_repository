<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SalesExport_Block_Adminhtml_System_Config_Form_Field_Content extends Acommerce_SalesExport_Block_Adminhtml_System_Config_Form_Field_Abstract
{
    protected function _prepareLayout() {		
		$this->setGridId('salesexport_file_single_settings');
		$this->addColumn('type', array(
            'label' => Mage::helper('adminhtml')->__('Company'),
            'style' => 'width:90px',
			'type'	=> 'select',			
			'renderer' =>  $this->getLayout()->createBlock('salesexport/adminhtml_system_config_form_field_render_select'),
			'source'=>'salesexport/source_type',
        ));
		$this->addColumn('use_ship_pay', array(
            'label' => Mage::helper('adminhtml')->__('Include Shipping-COD Fee'),
            'style' => 'width:90px',
			'type'	=> 'select',			
			'renderer' =>  $this->getLayout()->createBlock('salesexport/adminhtml_system_config_form_field_render_select'),
			'source'=>'adminhtml/system_config_source_yesno',
        ));
        return parent::_prepareLayout();
	}
}
