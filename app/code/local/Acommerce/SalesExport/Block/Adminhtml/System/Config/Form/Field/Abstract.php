<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SalesExport_Block_Adminhtml_System_Config_Form_Field_Abstract extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
  public function __construct() {
		$this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add');
        parent::__construct();
		$this->setTemplate('salesexport/system/config/form/field/array.phtml');
    }
	public function addColumn($name, $params) {
        $this->_columns[$name] = array(
            'label'     => empty($params['label']) ? 'Column' : $params['label'],
            'size'      => empty($params['size'])  ? false    : $params['size'],
            'style'     => empty($params['style'])  ? null    : $params['style'],
            'class'     => empty($params['class'])  ? null    : $params['class'],
            'type'  	=> empty($params['type']) ? '' : $params['type'],
            'source'  	=> empty($params['source']) ? '' : $params['source'],
            'renderer'  => false,
        );
        if (isset($params['renderer']) && $params['renderer'] instanceof Mage_Core_Block_Abstract) {
            $this->_columns[$name]['renderer'] = $params['renderer'];
        }
    }
}
