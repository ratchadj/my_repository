<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SalesExport_Block_Adminhtml_System_Config_Form_Field_Ftp extends Acommerce_SalesExport_Block_Adminhtml_System_Config_Form_Field_Abstract
{
    protected function _prepareLayout() {
		$this->addColumn('type', array(
            'label' => Mage::helper('adminhtml')->__('Type'),
            'style' => 'width:57px',
			'type'	=> 'select',			
			'renderer' =>  $this->getLayout()->createBlock('salesexport/adminhtml_system_config_form_field_render_select'),
			'source'=>'salesexport/source_ftp',
        ));
        $this->addColumn('path', array(
            'label' => Mage::helper('adminhtml')->__('Path'),
            'style' => 'width:120px',
        ));
		$this->addColumn('host', array(
            'label' => Mage::helper('adminhtml')->__('Host[:port]'),
            'style' => 'width:120px',
        ));
		$this->addColumn('username', array(
            'label' => Mage::helper('adminhtml')->__('User Name'),
            'style' => 'width:120px',
        ));
		$this->addColumn('password', array(
            'label' => Mage::helper('adminhtml')->__('Password'),
            'style' => 'width:120px',
        ));
        return parent::_prepareLayout();
	}
}
