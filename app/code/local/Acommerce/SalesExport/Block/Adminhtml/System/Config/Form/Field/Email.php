<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SalesExport_Block_Adminhtml_System_Config_Form_Field_Email extends Acommerce_SalesExport_Block_Adminhtml_System_Config_Form_Field_Abstract
{
    protected function _prepareLayout() {
		$this->addColumn('email', array(
            'label' => Mage::helper('adminhtml')->__('Email'),
            'style' => 'width:90px',
        ));
		$this->addColumn('name', array(
            'label' => Mage::helper('adminhtml')->__('Name'),
            'style' => 'width:90px',
        ));
		$this->addColumn('from', array(
            'label' => Mage::helper('adminhtml')->__('Send From'),
            'style' => 'width:90px',
			'type'	=> 'select',			
			'renderer' =>  $this->getLayout()->createBlock('salesexport/adminhtml_system_config_form_field_render_select'),
			'source'=>'adminhtml/system_config_source_email_identity',
        ));
        $this->addColumn('title', array(
            'label' => Mage::helper('adminhtml')->__('Subject'),
            'style' => 'width:120px',
        ));
		$this->addColumn('content', array(
            'label' => Mage::helper('adminhtml')->__('Content'),
            'style' => 'width:120px',
			'renderer' =>  $this->getLayout()->createBlock('salesexport/adminhtml_system_config_form_field_render_textarea'),
			'source'=>'salesexport/source_ftp',
        ));
        return parent::_prepareLayout();
	}
}
