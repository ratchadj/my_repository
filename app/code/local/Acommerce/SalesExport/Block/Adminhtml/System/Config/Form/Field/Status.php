<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SalesExport_Block_Adminhtml_System_Config_Form_Field_Status extends Acommerce_SalesExport_Block_Adminhtml_System_Config_Form_Field_Abstract
{
    protected function _prepareLayout() {
		$this->addColumn('method', array(
            'label' => Mage::helper('adminhtml')->__('Method'),
            'style' => 'width:190px',
			'type'	=> 'select',			
			'renderer' =>  $this->getLayout()->createBlock('salesexport/adminhtml_system_config_form_field_render_select'),
			'source'=>'salesexport/source_allowedmethods',
        ));
		$this->addColumn('status', array(
            'label' => Mage::helper('adminhtml')->__('Status'),
            'style' => 'width:120px',
			'type'	=> 'select',			
			'renderer' =>  $this->getLayout()->createBlock('salesexport/adminhtml_system_config_form_field_render_select'),
			'source'=>'salesexport/source_order_status',
        ));
        return parent::_prepareLayout();
	}
}
