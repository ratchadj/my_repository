<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SalesExport_Model_Resource_Sales_Order_Collection extends Mage_Sales_Model_Resource_Order_Collection
{
    protected $_product_e;

    protected function _getProductEntity() {
        if(!$this->_product_e instanceof Mage_Eav_Model_Entity_Abstract) {
            $this->_product_e = Mage::getModel('eav/entity')->setType('catalog_product');
        }
        return $this->_product_e;
    }
    protected function _getProductAttribute($att) {
        return $this->_getProductEntity()->getAttribute($att);
    }
    /**
     * join sales order collection to payment
     *
     * @return order collection
     */
    public function addPaymentToSelect()
    {
        $this->addFilterToMap('payment_method', 'payment.method');
        $online = explode(',',Mage::helper('salesexport')->getGenaralConfig('online_pay'));
        foreach($online as $id=>$value){
            $online[$id] = "'".$value."'";
        }
        $online = implode(',',$online);
        $type = ($online)?'if(payment.method IN ('.$online.'),"CC","C1")':'if(payment.method,"C1","")';
        $this->join(
            array('payment'=>'sales/order_payment'),
            '(main_table.entity_id = payment.parent_id) AND ((payment.method IN ('.$online.') AND payment.cc_number_enc IS NOT NULL) OR (payment.method NOT IN('.$online.')))',
             array('pay_type' => $type,
            'credit_card_no'=>'payment.cc_number_enc')
        );
        $this->_quoteStatusToSql();
        return $this;
    }

    protected function _quoteStatusToSql() {
        $statuses = unserialize(Mage::helper('salesexport')->getGenaralConfig('export_status'));
        if(is_array($statuses) && !empty($statuses)) {
            $condition = array();
            foreach($statuses as $status) {
                if(isset($status['method']) && isset($status['status'])) {
                    $condition[] = "(main_table.status = '{$status['status']}' AND payment.method ='{$status['method']}')";
                }
            }
            $condition = implode('or',$condition);
            $this->getSelect()->where("{$condition}");
        }
        return $this;
    }


    public function addIgnoreDataInBatch() {

        $this->getSelect()->joinLeft(array('batch'=>'sales_order_export'), '(main_table.entity_id = batch.order_id)', NULL);
        $this->getSelect()->where("batch.batch_id IS NULL");
        return $this;
    }

    public function addOrderItems($productAttr = array()) {
        $itemAliasName = 'items_table';
        $simpleItem = 'simple_items';
        $joinTable = $this->getTable('sales/order_item');
        $this->addFilterToMap('item_qty', $itemAliasName . '.qty_ordered')
            ->addFilterToMap('product_id', $simpleItem . '.product_id')
            ->addFilterToMap('product_price', $itemAliasName . '.price_incl_tax');
        $this->getSelect()
            ->joinLeft(
                array($itemAliasName => $joinTable),
                "(main_table.entity_id = {$itemAliasName}.order_id
                AND {$itemAliasName}.parent_item_id IS NULL AND {$itemAliasName}.base_price > 0
                )",
                array(
                    'order_id'=>$itemAliasName . '.order_id',
                    'item_id'=>$itemAliasName . '.item_id',
                    //'product_id'=>$itemAliasName . '.product_id',
                    'qty'=>$itemAliasName . '.qty_ordered',
                    'trade_no'=>$itemAliasName . '.trade_no',
                    'ais_ussd_code'=>$itemAliasName . '.ussd_code',
                    'access_code'=>$itemAliasName . '.access_code',
                    'installment_term'=>$itemAliasName . '.installment_term',
                    'installment_rate'=>$itemAliasName . '.installment_rate',
                    //'trade_name'=>$itemAliasName . '.trade_name',
                    'free_goods'=>$itemAliasName . '.free_goods',
                    'row_total1'=>$itemAliasName . '.base_row_total',
                    'row_total'=>'('.$itemAliasName . '.original_price)*'.$itemAliasName . '.qty_ordered',
                    'name'=>$itemAliasName . '.name',
                    'project_type'=>$itemAliasName . '.project_type',
                    'is_cashback'=>$itemAliasName . '.is_cashback',

                    /*'mobile_no'=>$itemAliasName . '.customer_mobile_phone',*/

                    //'item_type'=>$itemAliasName . '.product_type',
                    //'discount_amount'=>$itemAliasName . '.discount_amount',
                )
            )
            ->from(false,array('discount_amount'=> new Zend_Db_Expr('('.$itemAliasName . '.original_price - '.$itemAliasName . '.price)*'.$itemAliasName . '.qty_ordered')))
            ->from(false,array('shipping_cost'=> new Zend_Db_Expr('0')))
            ->joinLeft(
                array($simpleItem => $joinTable),
                "({$itemAliasName}.order_id = {$simpleItem}.order_id
                AND {$simpleItem}.parent_item_id = {$itemAliasName}.item_id
                )",
                array('product_id'=>"ifnull({$simpleItem}.product_id,{$itemAliasName}.product_id)")
            );
        if(!empty($productAttr)) {
            $this->joinProductAttributes('product_id',$productAttr);
        }
        Mage::getResourceHelper('core')->prepareColumnsList($this->getSelect());
        return $this;
    }
    public function joinProductAttributes($attrField,$attr,$fieldAlias=null,$getValue=true) {
        if(is_string($attr)) {
            $attr = $this->_getProductAttribute($attr);
        }
        elseif(is_array($attr)) {
            foreach($attr as $value=>$_attr) {
                if($value === 'option_id') {
                    $this->joinProductAttributes($attrField,$_attr,$fieldAlias,false);
                }
                else {
                    $this->joinProductAttributes($attrField,$_attr,$fieldAlias);
                }
            }
            return $this;
        }
        if(!($attr instanceof Mage_Eav_Model_Entity_Attribute_Abstract)) {
            throw Mage::exception('Mage_Eav', Mage::helper('salesexport')->__('Invalid attribute %s',$attr));
        }
        if(!$fieldAlias) {
            $fieldAlias = $attr->getAttributeCode();
        }
        $tableAlias = 'att_'.$fieldAlias;
        $tableOptsAlias = 'att_'.$fieldAlias.'_opt';
        $tableOptsValAlias = 'att_'.$fieldAlias.'_opt_val';
        $adapter = $this->getConnection();
        $condition = $this->getConnection()->quoteInto(
                        $adapter->quoteColumnAs("$tableAlias.attribute_id", null) . ' = ?', $attr->getId());
        $this->getSelect()->joinLeft(
                array($tableAlias => $attr->getBackend()->getTable()),
                "({$tableAlias}.entity_id=ifnull(simple_items.{$attrField},items_table.product_id) AND ({$condition}))",
                array()
            );
        if($attr->getFrontendInput()=='select' && $getValue) {
            $this->getSelect()->joinLeft(
                array($tableOptsValAlias => 'eav_attribute_option_value'),
                "({$tableOptsValAlias}.option_id={$tableAlias}.value AND ({$tableOptsValAlias}.store_id =0))",
                array($fieldAlias => $tableOptsValAlias.'.value')
            );
        }
        else {
            $this->getSelect()->columns(array($fieldAlias => 'value'),$tableAlias);
        }
        return $this;
    }
    public function addRequireFields() {
        $this->addFilterToMap('created_at','main_table.created_at');
        $this->addFilterToMap('increment_id','main_table.increment_id');
        //$this->addFilterToMap('grand_total','main_table.grand_total');
        $this->getSelect()
            ->reset(Zend_Db_Select::COLUMNS)
            // ->columns(array('order_no'=>'entity_id'), 'main_table')
            ->columns('created_at', 'main_table')
            ->columns('customer_id', 'main_table')
            // ->columns('customer_firstname', 'main_table')
            // ->columns('customer_lastname', 'main_table')
            //->columns(array('qty'=>'total_qty_ordered'), 'main_table')
            //->columns('total_qty_ordered', 'main_table')
            ->columns('base_cod_fee', 'main_table')
            ->columns('subtotal', 'main_table')
            ->columns('shipping_amount', 'main_table')
            ->columns('base_shipping_discount_amount', 'main_table')
            ->columns('increment_id', 'main_table')
            //->columns('discount_amount', 'main_table')
            /* ->from(false,array('grand_total2'=> new Zend_Db_Expr(
            '(if(main_table.shipping_amount,main_table.shipping_amount,0)+if(main_table.base_cod_fee,main_table.base_cod_fee,0))'
            ))) */
            /* ->from(false,array('grand_total'=> new Zend_Db_Expr(
            '(main_table.grand_total-if(main_table.shipping_amount,main_table.shipping_amount,0)-if(main_table.base_cod_fee,main_table.base_cod_fee,0))'
            ))) */
            ;
        return $this;
    }
    /**
     * add billing  and shipping fields "name, street, city, state, country, postcode, phone" to collection
     *
     * @return order collection
     */
    public function addAddressFields() {
        $bill = 'billing_o_a';
        $ship = 'shipping_o_a';
        $joinTable = $this->getTable('sales/order_address');

        $this
            ->addFilterToMap('billing_phone', $bill . '.telephone')

            ->addFilterToMap('shipping_phone', $ship . '.telephone');

        $this
            ->getSelect()
            ->joinLeft(
                array($bill => $joinTable),
                "(main_table.entity_id = {$bill}.parent_id"
                    . " AND {$bill}.address_type = 'billing')",
                array(
                    'customer_name'=>'CONCAT('.$bill . '.firstname'.', " ",'.$bill . '.lastname'.')',
                    'billing_phone'=>$bill . '.telephone',
                    'billing_address'=>"CONCAT(ifnull({$bill}.street,''),' ',ifnull({$bill}.sub_district,''),' ',ifnull({$bill}.city,''),' ',ifnull({$bill}.region,''),' ',ifnull({$bill}.postcode,''))",
                    'vat_type'=>$bill . '.vat_type',
                    'vat_id'=>$bill . '.vat_id',
                    'branch_no'=>$bill . '.branch_no',
                )
            )
            ->joinLeft(
                array($ship => $joinTable),
                "(main_table.entity_id = {$ship}.parent_id"
                    . " AND {$ship}.address_type = 'shipping')",
                array(
                    'shipping_address'=>"CONCAT(ifnull({$ship}.street,''),' ',ifnull({$ship}.sub_district,''),' ',ifnull({$ship}.city,''),' ',ifnull({$ship}.region,''),' ',ifnull({$ship}.postcode,''))",
                    'addressee'=>'CONCAT('.$ship . '.firstname'.', " ",'.$ship . '.lastname'.')',
                )
            );
        Mage::getResourceHelper('core')->prepareColumnsList($this->getSelect());
        return $this;
    }
}