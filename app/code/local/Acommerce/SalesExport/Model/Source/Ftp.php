<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SalesExport_Model_Source_Ftp
{
    const TYPE_SFTP	= 1;
    const TYPE_FTP	= 2;

    public function toOptionArray() {
        $types = array();
		foreach($this->types() as $v=>$k) {
			$types[] = array('value'=>$v,'label'=>$k);
		}
		return $types;
    }
	public function types() {
		return array(
			$this->sftp()	=> Mage::helper('salesexport')->__('SFTP'),
			$this->ftp()	=> Mage::helper('salesexport')->__('FTP'),
		);
	}
	public function sftp() {
		return self::TYPE_SFTP;
	}
	public function ftp() {
		return self::TYPE_FTP;
	}
}
