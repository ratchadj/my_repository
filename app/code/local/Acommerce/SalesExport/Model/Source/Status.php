<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SalesExport_Model_Source_Status
{
    const STATUS_SUCCESS	= 1;
    const STATUS_FAILED		= 2;

    public function toOptionArray() {
        return array(
            $this->success()    => Mage::helper('salesexport')->__('Exported'),
            $this->failed()   	=> Mage::helper('salesexport')->__('Not Exported')
        );
    }
	public function success() {
		return self::STATUS_SUCCESS;
	}
	public function failed() {
		return self::STATUS_FAILED;
	}
}
