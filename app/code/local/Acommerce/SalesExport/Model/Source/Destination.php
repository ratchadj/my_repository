<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SalesExport_Model_Source_Destination
{
    const STATUS_LOCAL	= 1;
    const STATUS_REMOTE	= 2;
    const STATUS_EMAIL	= 3;
    // const STATUS_ALL	= 4;

    public function getOptionArray() {
        return array(
            // $this->all()   => Mage::helper('salesexport')->__('All'),
			$this->local()    => Mage::helper('salesexport')->__('Local'),
            $this->remote()   => Mage::helper('salesexport')->__('Remote'),
            $this->email()   => Mage::helper('salesexport')->__('Email'),            
        );
    }
	public function toOptionArray() {
		$options = array();
		foreach($this->getOptionArray() as $v=>$k) {
			$options[] = array('value'=>$v,'label'=>$k);
		}
		
		return $options;
	}
	public function remote() {
		return self::STATUS_REMOTE;
	}
	public function email() {
		return self::STATUS_EMAIL;
	}
	public function all() {
		return self::STATUS_ALL;
	}
	public function local() {
		return self::STATUS_LOCAL;
	}
}
