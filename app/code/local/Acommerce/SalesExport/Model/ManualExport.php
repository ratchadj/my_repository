<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SalesExport_Model_ManualExport
{	
	protected $_items = null;
	protected $_curr_id = null;
	protected $_batch_id = null;
	protected $_last_item = null;
	protected $_last_batch = null;
	protected $_can_export = null;
	protected $_company = array();
	protected $_use_ship_pay = array();
	protected $_has_ship_pay = array();
	
	protected $_total = array();
	
	protected function helper() {
		return Mage::helper('salesexport');
	}
	public function cronExport() {
		if($this->_canExport()) {
			$this->exportCsvData();
		}
		return $this;
	}
	public function exportCsvData($ids=array()) {
		if(!is_array($ids)) {
			$ids = array($ids);
		}
		if(empty($ids)) {
			return ;
		}
		$write = Mage::getModel('salesexport/salesExport')->getResource()->getWriteConnection();
		try {
			$items = $this->_getValidItems($ids);
			if(!$items || !count($items)) {
				return;
			}
			$write->beginTransaction();
			//$batchId = $this->_createBatch();			
			$companies = $this->helper()->getCompanyOptions();
			foreach($companies as $company) {
				// same header
				$this->_company[$company['value']] = $this->_getCsvheader();
			}
			
			$tracking = array();
			//$items = $this->_addGrandTotal($items);
			$currId = null;
			$skip = false;
			foreach($items as $item) {
				$company = $item->getAisCompany();
				if(!$company || !isset($this->_company[$company])) {
					continue;
				}
				$this->_addGrandTotal($item,$items);
				$this->_validateVat($item);
				if($this->_useShipPay($company)) {
					$withFee = $item->getData('grand_total')+$item->getData('shipping_amount')+$item->getData('base_cod_fee');
					$item->setData('grand_total',$withFee);
				}
				if(isset($this->_company[$company])) {
					$this->_company[$company] .= $this->_getCsv($item);
				}
				if($currId != $item->getData('order_id')) {
					if($this->_useShipPay($company)) {
						$this->_company[$company] .= $this->_getShipPayAsItems($item);
						$currId = $item->getData('order_id');
					}
					elseif(!isset($this->_has_ship_pay[$item->getData('order_id')]) || !$this->_has_ship_pay[$item->getData('order_id')]) {
						foreach($companies as $_company) {
							if($this->_useShipPay($_company['value'])) {
								$this->_company[$_company['value']] .= $this->_getShipPayAsItems($item,true);
								$currId = $item->getData('order_id');
							}
						}
					}
				}
				
			}
			$this->saveFileLocal();
			$this->saveFileRemote();
			$this->sentEmail();
		}
		catch(Exception $e) {
			$write->rollback();
			if(isset($batchId)) {
				Mage::getModel('salesexport/batch')->load($batchId)->delete();
			}
			Mage::logException($e);
			Mage::log($e->getMessage(),null,'sales_export_error.log');
		}		
		$write->commit();
		echo count($ids).' were exported';
	}
	protected function _validateVat($item) {
		$type = $item->getVatType();
		$idValue = $item->getVatId();
		$item->setCustomerType('N');
		switch($type) {
			case 1:
				//$item->setVatId($idValue);
				break;
			case 2:
				$item->setVatId(null);
				$item->setTaxNo($idValue);
				$item->setCustomerType('P');
				break;
			case 3:
				$item->setVatId(null);
				$item->setPassport($idValue);
				break;
			default:
				// defaul same as "A Thai national"
				break;
		}
		return $this;
	}
	protected function _getWattingItems() {
		/* $items = Mage::getModel('salesexport/single')->getCollection()
				->join(
					array('order'=>'sales_flat_order'),
					'order.main',
					array()
				); */
	}
	protected function _addGrandTotal($_item,$coll) {
		$id = $_item->getOrderId();
		$cp = $_item->getAisCompany();
		if(isset($this->_total[$id][$cp])) {
			$_item->setGrandTotal($this->_total[$id][$cp]);
			return;
		}		
		$all = $_item->getSubtotal();		
		foreach($coll as $item) {
			if($id==$item->getOrderId() && $cp != $item->getAisCompany() && $id!=$item->getItemId()) {
				if($this->_useShipPay($cp) || $this->_useShipPay($item->getAisCompany())) {
					$this->_has_ship_pay[$id] = true;
				}
				$all -= $item->getRowTotal1();
			}
			elseif($id<$item->getOrderId()){
				break;
			}
		}	
		$this->_total[$id][$cp] = $all;
		$_item->setData('grand_total',$all);
		return $this;
	}
	protected function _useShipPay($company) {
		if(!isset($this->_use_ship_pay[$company])) {
			$_names = unserialize($this->helper()->getFileConfig('name'));
			foreach($_names as $name) {
				if($name['type']==$company) {
					$this->_use_ship_pay[$company] = (bool) $name['use_ship_pay'];
				}
			}
		}
		if(!isset($this->_use_ship_pay[$company])) {
			$this->_use_ship_pay[$company] = false;
		}
		return $this->_use_ship_pay[$company];
	}
	protected function _getHeader() {
		return array(
			'item_id'			=>'No.',
			'created_at' 		=>'Order Date',			
			'location_code' 	=>'Location Code',			
			'customer_name' 	=>'Customer Name',
			'billing_phone'	 	=>'Customer Mobile No.',
			'billing_address' 	=>'Billing Address',
			'shipping_address' 	=>'Shipping Address',
			'ais_brand'			=>'Brand',
			'ais_model' 		=>'Model',
			'color' 			=>'Color',
			'ais_mat_code' 		=>'Mat Code',
			'free_goods' 		=>'Mat Code Free Goods',
			'ignore1' 			=>'IMEI',
			'qty' 				=>'Qty',
			'ais_ussd_code'		=>'USSD Code',
			'access_code' 		=>'Access Code',
			'project_type'		=>'Project Type',
			'project_code'		=>'Project Code',
			'customer_id'		=>'User ID',
			'remark'			=>'Remark',
			'pay_type'			=>'Pay Type',
			'ignore12'			=>'Card Type',
			'ignore13'			=>'Bank Code',
			'ignore14'			=>'Branch Code',
			'credit_card_no'	=>'Credit Card No.',
			'ignore15'			=>'Expired Date',
			'row_total1'		=>'Total',
			'shipping_cost'		=>'Shipping Cost',
			'row_total2'		=>'Total2',
			//'row_total'			=>'Gross Amount',			
			'increment_id'		=>'Order No.',
			'created_at2' 		=>'Order Date2',			
			'ais_product_type'	=>'Product Type',
			'mobile_no'			=>'Mobile No.',
			'grand_total'		=>'Grand Total',			
			'discount_amount'	=>'Discount',
			'ignore02' 			=>'Location Code2',
			//'ignore'			=>'',
			'trade_no'			=>'Trade No.',
			'trade_name'		=>'Trade Name',
			'ignore17'			=>'Cash Back Flag',
			'ignore18'			=>'Installment Term',
			'ignore19'			=>'Installment Rate',
			'ignore120'			=>'AIS Ref',
			'ignore121'			=>'Mat Code2',
			'ignore01' 			=>'Date Receipt',
			'ignore123'			=>'Last Update Stamp',
			'ignore124'			=>'Bank Response',
			'addressee'			=>'Addressee',
			'customer_type'		=>'Customer Type',
			'ignore126'			=>'Branch No.',
			'passport'			=>'Passport No.',
			'vat_id'			=>'ID',
			'tax_no'			=>'Tax No.',
			'ignore03' 			=>'Pre Booking No',
		);
	}
	protected function _getCsvheader() {
		$csv = chr(239) . chr(187) . chr(191);
		$data = array();
        foreach ($this->_getHeader() as $header) {            
            $data[] = '"'.$header.'"';
        }
        $csv.= implode(',', $data)."\n";
		return $csv;
	}
	protected function _getCsv($item) {	
		$data = array();
		foreach($this->_getHeader() as $field=>$label) {
			$data[] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'),
			$this->getColumnValue($item,$field)) . '"';
			//echo $this->getColumnValue($item,$field).'<br/>';
		}
		return implode(',', $data)."\n";
	}
	protected function _getShipPayAsItems($item,$onlyShip = false) {
		$csvItems = '';
		//$item = clone $item;
		$item->setAsShipPay(true);
		$item->setData('ais_brand',null)
			->setData('ais_model',null)
			->setData('ais_ussd_code',null)			
			->setData('trade_no',null)			
			->setData('ais_product_type',null)
			->setData('free_goods',null)
			->setData('color',null);
		if($onlyShip) {
			$total = $item->getData('shipping_amount');
			$item->setData('grand_total',$total);
		}
		$item->setData('ais_product_type','Shipping Cost')
					->setData('row_total',$item->getData('shipping_amount'))
					->setData('discount_amount',0)
					->setData('ais_mat_code',$this->helper()->getGenaralConfig('shipping_mat'))
					->setData('qty',1)
					//->setData('grand_total',$total)
					;
		$csvItems .= $this->_getCsv($item);
		/* if($item->getData('base_cod_fee')) {
			$item->setData('ais_product_type','Payment Fees')
					->setData('row_total',$item->getData('base_cod_fee'))
					//->setData('discount_amount',0)
					->setData('ais_mat_code',$this->helper()->getGenaralConfig('payment_mat'))
					//->setData('grand_total',$total)
					;
			$csvItems .= $this->_getCsv($item);
		} */
		return $csvItems;
	}
	public function getColumnValue($item,$field) {		
		if(strpos($field,'ignore')!==false) {
			return '';
		}
		if(strpos($field,'created_at')!==false) {
			return $item->getData('created_at');
		}
		if(strpos($field,'row_total')!==false) {
			return $item->getData('row_total');
		}		
		if($field == 'billing_address' || $field == 'shipping_address') {
			$address = explode("\n", $item->getData($field));
			return implode(',', $address);
		}
		$value = $item->getData($field);
		if(!is_null($value)) {
			if($field == 'increment_id') {
				return $this->_getOrderNo($value);
				
			}
			return $value;
		}
		
		if(in_array($field,$this->helper()->getGeneralVa())) {
			return $this->helper()->getGenaralConfig($field);
		}
		
		if($field == 'project_code') {
			return $this->helper()->getGenaralConfig('project_code');
		}
		if($field == 'remark') {
            //$this->helper()->getGenaralConfig('project_code').
			return $this->_getOrderNo($item->getData('increment_id'));
		}
		
		return '';
	}
	protected function _getOrderNo($value) {
		$prefix = $this->helper()->getGenaralConfig('order_prefix');
		return $prefix.$value;
	}
	protected function _getValidItems($ids=array()) {
		if(!$this->_items) {
			if(empty($ids)) {
				$lastOrderId = $this->_getLastItem()->getOrderId()?$this->_getLastItem()->getOrderId():0;
			}
			$this->_items = Mage::getResourceModel('salesexport/sales_order_collection')
						->addRequireFields()
						->addOrderItems($this->helper()->getProductVa())
						->addAddressFields()
						->addPaymentToSelect()
						//->addFieldToFilter('entity_id',array('gt'=>0))
						->addFieldToFilter('entity_id',array('in'=>$ids))
						->setOrder('entity_id','ASC');
		}
		// echo $this->_items->getSelect();exit;
		return $this->_items;
	}	
	protected function _logInfo($write,$data) {
		if (!empty($data)) {
            $rows_result = $write->insertOnDuplicate('sales_order_export', $data);
        }
        return $this;
	}
	protected function _createBatch() {
		$batch = Mage::getModel('salesexport/batch')->setStatus(1)->save();
		$this->_batch_id = $batch->getId();
		return $this->_batch_id;
	}
	protected function _ioFile() {
		$io = new Varien_Io_File();
		$io->setAllowCreateFolders(true);		
		return $io;
	}
	protected function _canSaveTo($type) {
		$types = explode(',',$this->helper()->getFileConfig('type'));
		if(in_array($type,$types) || !$this->_batch_id) {
			return true;
		}
		return false;
	}
	public function saveFileLocal() {
		if(!$this->_canSaveTo(Acommerce_SalesExport_Model_Source_Destination::STATUS_LOCAL)) {
			return false;
		}
		$localPath = $this->helper()->getFileConfig('path');
		$io = $this->_ioFile();		
		$io->open(array('path' => $localPath));
		foreach($this->_company as $id=>$data) {
			$fileName = $this->_getFileName($this->helper()->getFileName($id,$this->_batch_id));
			$fileInfo = pathinfo($fileName);
			if(!isset($fileInfo['extension']) || !$fileInfo['extension']) {
				$fileName .= '.csv';
			}
			
            echo $localPath;
			$io->streamOpen($fileName);			
			
			$io->streamWrite($data);
			$io->streamClose();
		}	
		$io->close();
	}
	public function saveFileRemote() {
		if(!$this->_canSaveTo(Acommerce_SalesExport_Model_Source_Destination::STATUS_REMOTE)) {
			return false;
		}
		$ftp = unserialize($this->helper()->getFileConfig('ftp'));
		foreach($ftp as $server) {
			if(empty($server)) {
				continue;
			}
			$host = $server['host'];
			$usrname = $server['username'];
			$password = $server['password'];
			$path = $server['path'];

			$sftp = new Varien_Io_Sftp();
			$sftp->open(array(
							'host'      => $host,
							'username'  => $usrname,
							'password'  => $password,
						));
			if($path) {
				$sftp->cd($path);
			}
			foreach($this->_company as $id=>$data) { 
				$fileName = $this->_getFileName($this->helper()->getFileName($id,$this->_batch_id));
				
				$sftp->write($fileName, $data);
			}
			$sftp->close();
		}
	}
	protected function _getFileName($fileName) {
		$fileInfo = pathinfo($fileName);
		if(!isset($fileInfo['extension']) || !$fileInfo['extension']) {
			$fileName .= '.csv';
		}
		return $fileName;
	}
	public function sentEmail() {
		if(!$this->_canSaveTo(Acommerce_SalesExport_Model_Source_Destination::STATUS_EMAIL)) {
			return false;
		}
		$config = unserialize($this->helper()->getFileConfig('email'));
		foreach($config as $custom) {
			if(empty($custom)) {
				continue;
			}
			$mail = Mage::getModel('salesexport/mail_template');
			foreach($this->_company as $id=>$data) {
				$fileName = $this->_getFileName($this->helper()->getFileName($id,$this->_batch_id));
				$mail->getMail()
					->createAttachment(
						$data,
						Zend_Mime::TYPE_OCTETSTREAM,
						Zend_Mime::DISPOSITION_ATTACHMENT,
						Zend_Mime::ENCODING_BASE64,
						basename($fileName)
					);
			}
			$mail->sendTransactional(				
				array('title'=>$custom['title'],'body'=>$custom['content']),
				$custom['from'],
				$custom['email'],
				$custom['name']);
		}
	}
	protected function _canExport() {
		if(is_null($this->_can_export)) {
			$this->_can_export = false;
			if($this->helper()->getCronConfig('enable')) {
				$currTime = Mage::getModel('core/date')->date();
				$lastTime = $currTime;
				$lastBatch = $this->_getLastBatch();
				if($lastBatch && $lastBatch->getId()) {
					$lastTime = $lastBatch->getCreatedAt();
				}
				$configuredValue = ((float)$this->helper()->getCronConfig('time'))*60;
				if((strtotime($currTime)-strtotime($lastTime))>=$configuredValue) {
					$this->_can_export = true;
				}
			}
		}
		return $this->_can_export;
	}
	protected function _getLastBatch() {
		if(is_null($this->_last_batch)) {
			$this->_last_batch = Mage::getModel('salesexport/batch')
					->getCollection()					
					->setOrder('batch_id')
					->setCurPage(1)
					->setPageSize(1);
			if($this->_batch_id) {
				$this->_last_batch->addFieldToFilter('batch_id',array('neq'=>$this->_batch_id));
			}
			$this->_last_batch=$this->_last_batch->getFirstItem();
		}
		return $this->_last_batch;
	}
	protected function _getLastItem() {
		if(!$this->_last_item) {
			//$lastBatch = $this->_getLastBatch();
			$lastItem = Mage::getModel('salesexport/salesExport')
						//->addFieldToFilter('batch_id',$lastBatch->getId())
						->getCollection()
						->setOrder('order_id')
						->setCurPage(1)
						->setPageSize(1)
						->getFirstItem();
			$this->_last_item = $lastItem;
		}
		return $this->_last_item;
	}
} 