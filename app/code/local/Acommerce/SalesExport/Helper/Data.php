<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SalesExport_Helper_Data extends Mage_Core_Helper_Abstract
{
	/* CONST LOCATION_CODE = '';
	CONST ACCESS_CODE = '';
	CONST PROJECT_TYPE = '';
	CONST PROJECT_CODE = ''; */
	CONST CONFIG_PATH = 'salesexport/';
	protected $_config = array();
	protected $_name   = array();
	public function getFileName($company,$batchId) {
		if(!isset($this->_name[$company])) {
			$names = $this->getFileContentConfig();
			$match = null;
			if($this->isSingleFileMode() && $company) {
				foreach($names as $name) {
					if($name['type']==$company) {
						$match = $name['name'];
						break;
					}
				}
			}
			else {
				$match = $this->getFileConfig('single_name');
			}
			$var = array('batch_id'=>$batchId);
			$name = Mage::getModel('salesexport/filter')->setVariables($var)
						->filter($match);
			$this->_name[$company] = $name;
		}
		return $this->_name[$company];
	}
	public function getFileContentConfig() {
		if(!$this->isSingleFileMode()) {
				$settings = unserialize($this->helper()->getFileConfig('name'));
		}
		else {
			$settings = unserialize($this->getFileConfig('single_settings'));
			//$settings['name'] = $this->getFileConfig('single_name');
		}
		return $settings;
	}
	public function isSingleFileMode() {
		return (bool)$this->getFileConfig('use_single');
	}
	protected function _configExist($area,$path) {
		if(empty($this->_config) || !isset($this->_config[$area]) ||
		!isset($this->_config[$area][$path])) {
			return false;
		}
		return true;
	}
	public function getConfig($area,$path) {
		if(!$this->_configExist($area,$path)) {
			$this->_config[$area][$path] =  Mage::getStoreConfig(self::CONFIG_PATH.$area.'/'.$path);
		}
		return isset($this->_config[$area][$path])?$this->_config[$area][$path]:null;
	}
	public function getGenaralConfig($path) {
		return $this->getConfig('general',$path);
	}
	public function getFileConfig($path) {
		return $this->getConfig('file',$path);
	}
	public function getCronConfig($path) {
		return $this->getConfig('cron',$path);
	}
	public function getCompanyOptions() {
		$source = Mage::getResourceModel('catalog/product')
					->getAttribute('ais_company')
					->getSource();
		return $source->getAllOptions(false);
	}
	public function getGeneralVa() {
		//return array('location_code','access_code','project_type','project_code');
		return array('location_code','project_type','project_code');
	}
	public function getProductVa() {
		//return array('ais_brand','ais_model','color','ais_mat_code','ais_ussd_code','ais_product_type','option_id'=>'ais_company');
		return array('ais_brand','ais_model','color','ais_mat_code','ais_product_type','option_id'=>'ais_company');
	}
	public function getAddressline($street, $line) {
        $arr = is_array($street) ? $street : explode("\n", $street);
        if ($line > 1) {
            return str_replace('\n', ' ', implode('\n', array_slice($arr, 1)));
        }
        return (isset($arr[$line - 1])) ? $arr[$line - 1] : '';
    }
}