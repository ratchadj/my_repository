<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/
$installer = $this;
$installer->startSetup();
	$conn = $installer->getConnection();
	$single = $installer->getTable('salesexport/single');
	if(!$conn->isTableExists($single)) {
		$single = $conn->newTable($single)
			->addColumn('order_id',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
				'nullable'	=> false,
				//'identity'	=> true,
				'primary'	=> true,
			),'Order Increment ID')
			->addColumn('status',Varien_Db_Ddl_Table::TYPE_TEXT, 50, array(
				'nullable'  => true,
				'default'   => null,
			),'Valid Status to be exported')
			;
		$conn->createTable($single);
	}
$installer->endSetup();
