<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/
$installer = $this;
$installer->startSetup();
	$conn = $installer->getConnection();
	$export = $installer->getTable('salesexport/salesexport');
	$exportBatch = $installer->getTable('salesexport/batch');
	if(!$conn->isTableExists($export)) {
		$export = $conn->newTable($export)
			->addColumn('order_id',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
				'nullable'	=> false,
				//'identity'	=> true,
				'primary'	=> true,
			),'Order Increment ID')
			->addColumn('batch_id',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
				'nullable'	=> false,
			),'Export Batch ID')
			;
		$conn->createTable($export);
	}
	if(!$conn->isTableExists($exportBatch)) {
		$exportBatch = $conn->newTable($exportBatch)
			->addColumn('batch_id',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
				'unsigned'	=> true,
				'nullable'	=> false,
				'identity'	=> true,
				'primary'	=> true,
			),'Export Batch ID')
			/* ->addColumn('type',Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
				'nullable'	=> true,
				'default'	=> 1,
			),'Title')
			->addColumn('path',Varien_Db_Ddl_Table::TYPE_TEXT, 150, array(
				'nullable'  => true,
				'default'   => null,
			),'Batch File Path')
			->addColumn('file',Varien_Db_Ddl_Table::TYPE_VARCHAR, 255, array(
				'nullable'  => true,
				'default'   => null,
			),'Batch File Name') */
			->addColumn('created_at',Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
				'nullable'  => false,
				'default'   => '0000-00-00 00:00:00',
			),'Batch Created At')			
			->addColumn('updated_at',Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
				'nullable'  => false,
				'default'   => Varien_Db_Ddl_Table::TIMESTAMP_INIT_UPDATE,
			),'Batch Updated At')
			->addColumn('status',Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
				'nullable'  => true,
				'default'   => 0,
			),'Batch Status');
		$conn->createTable($exportBatch);
	}
$installer->endSetup();
