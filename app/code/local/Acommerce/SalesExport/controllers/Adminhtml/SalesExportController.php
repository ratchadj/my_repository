<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_SalesExport_Adminhtml_SalesExportController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('salesexport/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Item Manager'));
		
		return $this;
	}   
	
	public function indexAction() {
		$this->_initAction()
			->renderLayout();
	}
	public function batchAction() {
		
	}
    public function exportCsvAction() {
        $fileName   = 'sales_order_export.csv';
        $grid       = $this->getLayout()->createBlock('salesexport/adminhtml_salesexport_grid');

        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    public function exportXmlAction() {
        $fileName   = 'sales_order_export.xml';
        $grid    	= $this->getLayout()->createBlock('salesexport/adminhtml_salesexport_grid');

        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }
}
