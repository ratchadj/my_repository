<?php
class Acommerce_Actiontags_Block_Criteo_Head extends Mage_Core_Block_Template
{
    protected $_accountId = null;
    protected function _afterToHtml($html)
    {
        if($param = $this->getData('criteo_param')) {
            $this->_accountId = Mage::getModel('actiontags/action')->getConfiguration('criteo_id');
            if($this->_accountId) {
                switch($param) {
                    case 'home':
                        $html .= $this->_getHomeScript();
                        break;
                    case 'category':
                        $html .= $this->_getCategoryScript();
                        break;
                    case 'product_detail':
                        $html .= $this->_getProductDetailScript();
                        break;
                    case 'shopping_cart':
                        $html .= $this->_getCartScript();
                        break;
                    case 'checkout_success':
                        $html .= $this->_getSuccessScript();
                        break;

                }
            }
        }
        return $html;
    }
    protected function _getHomeScript() {
        return $this->_getInitialScript().'{ event: "viewHome" }'.$this->_getLast();
    }
    protected function _getCategoryScript() {
        $listBlock = $this->getLayout()->getBlock('search_result_list');
		if(!$listBlock) {
			$listBlock = $this->getLayout()->getBlock('product_list');
		}
		$toolbar = $listBlock->getToolbarBlock();
		$limit = (int)$toolbar->getLimit();
		$offset = $limit*($toolbar->getCurrentPage()-1);
		$collection = $listBlock->getLoadedProductCollection();
		$select = clone $collection->getSelect();
		$select
			->reset(Zend_Db_Select::ORDER)
			->reset(Zend_Db_Select::LIMIT_COUNT)
			->reset(Zend_Db_Select::LIMIT_OFFSET)
			->reset(Zend_Db_Select::COLUMNS)
			->columns('e.sku')
			->limit(3,$offset)
			//->order(array($toolbar->getCurrentOrder().' '.$toolbar->getCurrentDirection()))
			->resetJoinLeft()
			;
		$skus = $collection->getConnection()->fetchCol($select);
		if(!empty($skus)) {
			return $this->_getInitialScript().'{ event: "viewList", item: ["'.implode('","', $skus).'"]}'.$this->_getLast();
		}
    }
    protected function _getProductDetailScript() {
        $block = $this->getLayout()->getBlock('product.info');
        $id = '';
        if($block) {
            $id = $block->getProduct()->getId();
        }
        $script = ($id)?$this->_getInitialScript():'';
        if($id) {
            $script .= '{ event: "viewItem", item: "'.$block->getProduct()->getSku().'" }'.$this->_getLast();
        }
        return $script;
    }
    protected function _getCartScript() {
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $script = ($quote->getId())?$this->_getInitialScript():'';
        $items = $quote->getAllVisibleItems();
        if($quote->getId() && count($items) > 0) {
            $script .= '{ event: "viewBasket", item: [ ';
            $lastId = end($items)->getId();
            foreach($items as $item) {
                $separate = ($item->getId()==$lastId)?'':',';
                $script .= ' { id: "'.$item->getSku().'", price: "'.$item->getBasePrice().'", quantity: "'.$item->getQty().'" }'.$separate;
            }
            $script .= ']}'.$this->_getLast();
        }
        return $script;
    }
    protected function _getSuccessScript() {
        $ids = $this->_getOrderIds();
        $script = (empty($ids))?'':$this->_getInitialScript();
        foreach($ids as $id) {
            $order = Mage::getModel('sales/order')->load($id);
            $script .= '{ event: "trackTransaction" , id: "'.$order->getIncrementId().'", item: [ ';
            $items = $order->getAllVisibleItems();
            $lastId = end($items)->getId();
            foreach($items as $item) {
                $separate = ($item->getId()==$lastId)?'':',';
                $script .= ' { id: "'.$item->getSku().'", price: "'.$item->getBasePrice().'", quantity: "'.$item->getQtyOrdered().'" }'.$separate;
            }
            $script .= ']}'.$this->_getLast();
        }
        return $script;
    }
    protected function _getOrderIds() {
        $ids = $this->getLayout()->getBlock('google_analytics')->getOrderIds();
        return (empty($ids) || !is_array($ids))?array():$ids;
    }
    protected function _getTargetPlatform() {
        return Mage::helper('actiontags/device')->getTargetPlatform();
    }
    protected function _getInitialScript() {
        $script = '<!--  Sales page tag ---->'.
            '<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>'.
            '<script type="text/javascript">'.
            'window.criteo_q = window.criteo_q || [];'.
            'window.criteo_q.push( '.
                    '{ event: "setAccount", account: '.$this->_accountId.' },';
        if($customerId = Mage::getSingleton('customer/session')->getCustomerId()) {
            $script .= '{ event: "setCustomerId", id: "'.$customerId.'" },';
        }
        $script .= '{ event: "setSiteType", type: "'.$this->_getTargetPlatform().'"},';
        return $script;
    }
    protected function _getLast() {
        return ');</script>';
    }

}
