<?php
class Acommerce_Actiontags_Block_Page_Html_Headercookies extends Mage_Core_Block_Template
{
    protected $_accountId = null;

    /**
     * Adding header script
     *
     * @param string $html block html
     * @return string
     */
    protected function _afterToHtml($html)
    {
        $this->_accountId = Mage::getModel('actiontags/action')->getConfiguration('criteo_id');
        $html .= $this->_getActionTags();
        if((int) $this->_getCookie()->get('order')) {
            $html .= $this->_getSuccessScript();
        }
        return $html;
    }

    /**
     * Get action tags html
     *
     * @return <string>
     */
    protected function _getActionTags()
    {
        $result = '';
        $actions = Mage::getModel('actiontags/action')->getConfiguration();
        if ($actions) {
            foreach (array_keys($actions->getData()) as $actionName) {
                if ($content = $this->_getCookie()->get($actionName)) {
                    if ($content == '1') {
                        $content = null;
                    }
                    $result .= Mage::helper('actiontags')->getHtmlForAction($actionName, urldecode($content));
                    $this->_getCookie()->delete($actionName);
                }
            }
        }
        return $result;
    }

    protected function _getSuccessScript() {
        $ids = $this->_getOrderIds();
        $script = (empty($ids))?'':$this->_getInitialScript();
        foreach($ids as $id) {
            $order = Mage::getModel('sales/order')->load($id);
            $script .= '{ event: "trackTransaction" , id: "'.$order->getIncrementId().'", item: [ ';
            $items = $order->getAllVisibleItems();
            $lastId = end($items)->getId();
            foreach($items as $item) {
                $separate = ($item->getId()==$lastId)?'':',';
                $script .= ' { id: "'.$item->getSku().'", price: "'.$item->getBasePrice().'", quantity: "'.$item->getQtyOrdered().'" }'.$separate;
            }
            $script .= ']}'.$this->_getLast();
        }
        return $script;
    }

    protected function _getOrderIds() {
        $ids = $this->getLayout()->getBlock('google_analytics')->getOrderIds();
        return (empty($ids) || !is_array($ids))?array():$ids;
    }

    protected function _getInitialScript() {
        $script = '<!--  Sales page tag ---->'.
            '<script type="text/javascript" src="//static.criteo.net/js/ld/ld.js" async="true"></script>'.
            '<script type="text/javascript">'.
            'window.criteo_q = window.criteo_q || [];'.
            'window.criteo_q.push( '.
                    '{ event: "setAccount", account: '.$this->_accountId.' },';
        if($customerId = Mage::getSingleton('customer/session')->getCustomerId()) {
            $script .= '{ event: "setCustomerId", id: "'.$customerId.'" },';
        }
        $script .= '{ event: "setSiteType", type: "'.$this->_getTargetPlatform().'"},';
        return $script;
    }

    protected function _getTargetPlatform() {
        return Mage::helper('actiontags/device')->getTargetPlatform();
    }

    protected function _getLast() {
        return ');</script>';
    }

    /**
     * Get cookie model
     *
     * @return <Mage_Core_Model_Cookie>
     */
    protected function _getCookie()
    {
        return Mage::getModel('core/cookie');
    }

    /**
     * Cache for header block
     *
     * @return bool
     */
    protected function _loadCache()
    {
        return false;
    }

}
