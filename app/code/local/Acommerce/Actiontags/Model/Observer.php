<?php
class Acommerce_Actiontags_Model_Observer extends Mage_Core_Model_Abstract
{

    /**
     * Ajax add Product --> fix for AW module only
     *
     * @return void
     */
    public function addRemoveProductAjax($observer)
    {
		$block = $observer->getEvent()->getBlock();
		$request = Mage::app()->getFrontController()->getRequest();
		$parts = $request->getParam('block');
		if(is_array($parts)) {
			$transport = $observer->getEvent()->getTransport();
			$productId = Zend_Json::decode($request->getParam('actionData','[]'));

			if(!is_array($productId)) {
				return;
			}

			if(get_class($block) == 'AW_Ajaxcartpro_Block_Confirmation_Addproduct') {
				if(!isset($productId['added_product'])) {
					return;
				}
				$action = $this->getConfigForAjax($productId['added_product'],'addtocart');
			}
			elseif(get_class($block) == 'AW_Ajaxcartpro_Block_Confirmation_Removeproduct') {
				if(!isset($productId['removed_product'])) {
					return;
				}
				$action = $this->getConfigForAjax($productId['removed_product'],'removefromcart');
			}
			else {
				return ;
			}

			if($action) {
				$transport->setHtml($transport->getHtml().$action);
			}
		}
        return;
    }

	public function getConfigForAjax($prId,$action) {
		$variables = array('product_id' =>$prId);
		$action = $this->_getActionModel()->getConfiguration($action);

		return Mage::helper('actiontags')->filterValues($action,$variables);
	}

	/**
     * Customer after save observer
     *
     * @return void
     */
    public function customerAfterSave()
    {
        $customerSession = Mage::getSingleton('customer/session');

        $actionsModel = $this->_getActionModel();
        if (!$customerSession->hasData('new')) {
            if ($content = $actionsModel->getConfiguration('profileupdate')) {
				$this->_getCookie()->set('profileupdate', '1');
			}
        }
		$customerSession->unsetData('new');
        return;
    }

	public function customerRegistration($observer) {
		$actionsModel = $this->_getActionModel();
		if ($content = $actionsModel->getConfiguration('registration')) {
			$this->_getCookie()->set('registration', '1');
		}
		Mage::getSingleton('customer/session')->setData('new');
		return;
	}
    /**
     * Customer Subscription action
     *
     * @param Varien_Event_Observer $observer observer
     * @return void
     */
    public function subscribtionSuccess($observer)
    {
        $email = $observer->getEmail();
        $actionsModel = $this->_getActionModel();
        if ($actionsModel->getConfiguration('subscription')) {
            $this->_getCookie()->set('subscription', urlencode($email));
        }
    }

    /**
     * Place order action
     *
     * @return void
     */
    public function placeOrder()
    {
        $actionsModel = $this->_getActionModel();

        if ($actionsModel->getConfiguration('order')) {
            $this->_getCookie()->set('order', '1');
        }
    }

    /**
     * Get Action Model
     *
     * @return Acommerce_Actiontags_Model_Action
     */
    protected function _getActionModel()
    {
        return Mage::getModel('actiontags/action');
    }

    /**
     * Send HTTP Request
     *
     * @param string $url request url
     * @return void
     */
    protected function _sendRequest($url)
    {
        try {
            $request = new Zend_Http_Client();
            $request->setUri($url);
            $request->request('GET');
        } catch (Exception $e) {
            Mage::log($e->getMessage() . " " . $url);
        }
        return;
    }

    /**
     * Replace placeholders with values
     *
     * @param string $url    url string
     * @param array  $params template variables
     * @param array  $values variables values
     * @return string
     */
    protected function _prepareUrl($url, $params, $values)
    {
        return Mage::helper('actiontags')->_prepareUrl($url, $params, $values);
    }

    /**
     * Get cookie model
     *
     * @return <Mage_Core_Model_Cookie>
     */
    protected function _getCookie()
    {
        return Mage::getModel('core/cookie');
    }

}