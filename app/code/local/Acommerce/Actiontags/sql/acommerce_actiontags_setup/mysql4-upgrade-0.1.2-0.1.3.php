<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()
	->addColumn($installer->getTable('actiontags/action'), 'criteo_id', 
			array(
				'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
				'LENGTH'  	=> 30,
				'NULLABLE'  => true,
				'COMMENT'   => 'Criteo Account ID'
			));
$installer->endSetup();
