<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()
    ->addColumn($installer->getTable('actiontags/action'), 'success_page',
            array(
                'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
                'NULLABLE'  => true,
                'COMMENT'   => 'success'
            ));
$installer->endSetup();
