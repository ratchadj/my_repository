<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()
	->addColumn($installer->getTable('actiontags/action'), 'addtocart', 
			array(
				'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
				'NULLABLE'  => true,
				'COMMENT'   => 'addtocart'
			));
$installer->getConnection()
	->addColumn($installer->getTable('actiontags/action'), 'removefromcart', 
			array(
				'TYPE'      => Varien_Db_Ddl_Table::TYPE_TEXT,
				'NULLABLE'  => true,
				'COMMENT'   => 'removefromcart'
			));
$installer->endSetup();
