<?php
class Acommerce_Actiontags_Helper_Data extends Mage_Core_Helper_Data
{
	CONST MAIN_CATEGORY_ATTRIBUTE = 'pet_food_main_category';
    CONST SUB_CATEGORY_ATTRIBUTE = 'pet_food_cateogory';
	protected $_mainCategory = null;
	protected $_subCategory = null;
	protected $_areaConfig = null;
    /**
     * Get template variables
     *
     * @return <array>
     */
    public function getVariables()
    {
        //customer information
        if ($this->_getCuctomerSession()->isLoggedIn()) {
            $customer = $this->_getCuctomerSession()->getCustomer();
            $values = array(
                'customer_id' => $customer->getId(),
                'first_name' => $customer->getFirstname(),
                'last_name' => $customer->getLastname(),
                'customer_email' => $customer->getEmail(),
                'subscribe_status' => (int) $this->_getIsSubscribed($customer)
            );
        }
		if($this->_areaConfig == 'checkout') {
			$salesObj = Mage::helper('checkout')->getQuote();
		}
		else {
			$orderId = Mage::helper('checkout')->getCheckout()->getLastRealOrderId();
			$salesObj = Mage::getModel('sales/order')->loadByIncrementId($orderId);
		}


        if ($salesObj->getId()) {
            $values['transaction_id'] = $salesObj->getId();
            //$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
            $values['order_id'] = (isset($orderId)) ? $orderId: '';
            $values['gross_billing'] = $salesObj->getBaseGrandTotal();
            $items = array();
            $accessTradeItems = '';
            if($salesObj instanceof Mage_Sales_Model_Quote) {
				if($salesObj->isVirtual()) {
					$address = $salesObj->getBillingAddress();
				}
				else {
					$address = $salesObj->getShippingAddress();
				}
				$values['tax'] = $address->getBaseTaxAmount();
				$values['shipping_tax'] = $address->getBaseShippingTaxAmount();
				$values['shipping_discount'] = $address->getBaseDiscountAmount();
				$values['shipping'] = $address->getBaseShippingInclTax();
				foreach ($salesObj->getAllVisibleItems() as $item) {
					$stdItem = new stdclass();
					$stdItem->sku = $this->jsQuoteEscape($item->getSku());
					$stdItem->name = $this->jsQuoteEscape($item->getName());
					$stdItem->price = $item->getBasePrice();
                    $stdItem->quantity = (int) $item->getQty();

					$product = $item->getProduct();
					$topLevel = null;
					$storeRootCat = Mage::app()->getStore()->getRootCategoryId();
					foreach($product->getCategoryCollection() as $cat) {
						$path = $cat->getPathIds();

						if(!isset($path[1]) || $storeRootCat == $cat->getId()) {
							continue;
						}
						if(!isset($path[2])) {
							$toplevel = $path[1];
							break;
						}
						if($storeRootCat == $path[1]) {
							if(!is_null($topLevel) && $path[2] !=$topLevel) {
								$topLevel = $path[1];
								break;
							}
							$topLevel = $path[2];
						}
						else {
							if(!is_null($topLevel) && $path[1] !=$topLevel) {
								$topLevel = $path[0];
								break;
							}
							$topLevel = $path[1];
						}
					}

					if($topLevel) {
                        $category = Mage::getModel('catalog/category');
                        $category->setStoreId(1);

						$stdItem->category = $this->jsQuoteEscape($category->load($topLevel)->getName());
					}
					$items[] = $stdItem;

                    if ($item->getProductType=='configuration'){
                        $kit =  Mage::getModel('catalog/product')->loadByAttribute('sku',$item->getSku());
                        $productId = $kit->getId();
                    } else {
                        $productId = $product->getId();
                    }
                    $accessTradeItems .= '&goods='.$this->jsQuoteEscape($item->getSku()).'.'.(int) $item->getQty().'.'.(int) $item->getBasePrice();
				}
				$values['items'] = Zend_Json::encode($items);
                $values['accessTradeItems'] = $accessTradeItems;
			}
			else {

				$values['tax'] = $salesObj->getBaseTaxAmount();
				$values['shipping_tax'] = $salesObj->getShippingTaxAmount();
				$values['shipping_discount'] = $salesObj->getShippingDiscountAmount();
				$values['shipping'] = $salesObj->getShippingInclTax();
				$productName = 'name';
				/*$discountAmount = 'discount_amount';
				$rowTotal = 'row_total';*/
				foreach ($salesObj->getAllVisibleItems() as $item) {
					$stdItem = new stdclass();
					$stdItem->sku = $this->jsQuoteEscape($item->getSku());
					$stdItem->name = $this->jsQuoteEscape($item->getName());
                    // $stdItem->category = $this->jsQuoteEscape($item->getName());
					$stdItem->price = $item->getOriginalPrice();
                    $stdItem->quantity = (int) $item->getQtyOrdered();
					/*$stdItem->$discountAmount = $item->getDiscountAmount();
					$stdItem->$rowTotal = $item->getRowTotalInclTax();*/

                    $product = $item->getProduct();
                    $topLevel = null;
                    $storeRootCat = Mage::app()->getStore()->getRootCategoryId();
                    foreach($product->getCategoryCollection() as $cat) {
                        $path = $cat->getPathIds();

                        if(!isset($path[1]) || $storeRootCat == $cat->getId()) {
                            continue;
                        }
                        if(!isset($path[2])) {
                            $toplevel = $path[1];
                            break;
                        }
                        if($storeRootCat == $path[1]) {
                            if(!is_null($topLevel) && $path[2] !=$topLevel) {
                                $topLevel = $path[1];
                                break;
                            }
                            $topLevel = $path[2];
                        }
                        else {
                            if(!is_null($topLevel) && $path[1] !=$topLevel) {
                                $topLevel = $path[0];
                                break;
                            }
                            $topLevel = $path[1];
                        }
                    }

                    if($topLevel) {
                        $category = Mage::getModel('catalog/category');
                        $category->setStoreId(1);

                        $stdItem->category = $this->jsQuoteEscape($category->load($topLevel)->getName());
                    }

					$items[] = $stdItem;
                    if ($item->getProductType()=='configurable'){
                        $kit =  Mage::getModel('catalog/product')->loadByAttribute('sku', $item->getSku());
                        $productId = $kit->getId();
                    } else {
                        $productId = $product->getId();
                    }
                    $accessTradeItems .= '&goods='.$this->jsQuoteEscape($item->getSku()).'.'.(int) $item->getQtyOrdered().'.'.(int) $item->getBasePrice();
				}
				$values['items'] = Zend_Json::encode($items);
                $values['accessTradeItems'] = $accessTradeItems;
			}

			if($salesObj->getPayment()->getMethod()) {
				$values['payment_method'] = $salesObj->getPayment()->getMethodInstance()->getTitle();
			}
        }

        //store_id
        $values['store_id'] = Mage::app()->getStore()->getId();

        //category [removed, use category per item instead]
        //$categoryId = Mage::app()->getStore()->getRootCategoryId();
        // $values['category'] = '';//$this->jsQuoteEscape(Mage::getModel('catalog/category')->load($categoryId)->getName());

        //current_date
        $currentDate = Mage::app()->getLocale()->storeDate(null, null);
        $values['current_date'] = $currentDate->toString('Y-m-d', 'php');

        return $values;
    }

    /**
     * Get html for action tag
     *
     * @param <string> $configuration configuration
     * @param <string> $customerEmail customer email
     * @return <string>
     */
    public function getHtmlForAction($configuration, $customerEmail = null)
    {
		$this->_areaConfig = $configuration;
        $variables = $this->getVariables();
        if ($customerEmail) {
            $variables['customer_email'] = $customerEmail;
        }
        $html = Mage::getModel('actiontags/action')->getConfiguration($configuration);
        return $this->filterValues($html, $variables);
    }

    /**
     * Get customer session model
     *
     * @return <Mage_Customer_Model_Session>
     */
    protected function _getCuctomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    /**
     * Check if customer is subscribed
     *
     * @param Mage_Customer_Model_Customer $customer customer
     *
     * @return bool
     */
    protected function _getIsSubscribed($customer)
    {
        $newsletter = Mage::getModel('newsletter/subscriber')->loadByCustomer($customer);
        return $newsletter->isSubscribed();

    }
	public function filterValues($template, $variables)
    {
        $processor = Mage::getModel('core/email_template_filter')
                ->setUseAbsoluteLinks(true)
                ->setPlainTemplateMode(true)
                ->setStoreId(Mage::app()->getStore()->getId())
                ->setVariables($variables)
        ;
        $template = $processor->filter($template);
        return $template;
    }
	public function getCategoriesForJsTracking($product) {
		$jsCategory = null;
		try {
			if($product->getId()) {
				$mainOptionText = $this->_getMainCatAttSource()->getOptionText($product->getPetFoodMainCategory());
				$subOptionText = $this->_getSubCatAttSource()->getOptionText($product->getPetFoodCateogory());
				$jsCategory = $mainOptionText.'-'.$subOptionText;
			}
		}
		catch(Exception $e) {
			Mage::logException($e);
		}
		return $jsCategory;
	}
	protected function _getMainCatAttSource() {
		if(!$this->_mainCategory) {
			$this->_mainCategory = Mage::getResourceSingleton('catalog/product')
								 ->getAttribute(self::MAIN_CATEGORY_ATTRIBUTE)->getSource();
		}
		return $this->_mainCategory;
	}
	protected function _getSubCatAttSource() {
		if(!$this->_subCategory) {
			$this->_subCategory = Mage::getResourceSingleton('catalog/product')
								->getAttribute(self::SUB_CATEGORY_ATTRIBUTE)->getSource();
		}
		return $this->_subCategory;
	}

}
