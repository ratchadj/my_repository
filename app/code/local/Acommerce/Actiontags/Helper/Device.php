<?php
class Acommerce_Actiontags_Helper_Device extends Mage_Core_Helper_Data
{
	public function getTargetPlatform() {
		if($this->detectAgent() == 'tablet' || $this->detectAgent() == 'mobile') {
			$platform = 'm';
		}
		else {
			$platform = 'd';
		}
		return $platform;
	}
	public function detectAgent() {
		if (!array_key_exists('HTTP_USER_AGENT', $_SERVER)) {
			return "desktop";
		}
		$ua = $_SERVER['HTTP_USER_AGENT'];
		if ((preg_match('/GoogleTV|SmartTV|Internet.TV|NetCast|NETTV|AppleTV|boxee|Kylo|Roku|DLNADOC|CE\-HTML/i', $ua)))
		{
				$device = "tv";
		}
		// Check if user agent is a TV Based Gaming Console
		else if ((preg_match('/Xbox|PLAYSTATION.3|Wii/i', $ua)))
		{
				$device = "tv";
		}
		// Check if user agent is a Tablet
		else if($this->isTablet()) {
			return "tablet";
		}
		// Check if user agent is unique Mobile User Agent
		else if ((preg_match('/BOLT|Fennec|Iris|Maemo|Minimo|Mobi|mowser|NetFront|Novarra|Prism|RX-34|Skyfire|Tear|XV6875|XV6975|Google.Wireless.Transcoder/i', $ua)))
		{
				$device = "mobile";
		}
		// Check if user agent is an odd Opera User Agent - http://goo.gl/nK90K
		else if ((preg_match('/Opera/i', $ua)) && (preg_match('/Windows.NT.5/i', $ua)) && (preg_match('/HTC|Xda|Mini|Vario|SAMSUNG\-GT\-i8000|SAMSUNG\-SGH\-i9/i', $ua)))
		{
				$device = "mobile";
		}
		// Check if user agent is Windows Desktop
		else if ((preg_match('/Windows.(NT|XP|ME|9)/', $ua)) && (!preg_match('/Phone/i', $ua)) || (preg_match('/Win(9|.9|NT)/i', $ua)))
		{
				$device = "desktop";
		}
		// Check if agent is Mac Desktop
		else if ((preg_match('/Macintosh|PowerPC/i', $ua)) && (!preg_match('/Silk/i', $ua)))
		{
				$device = "desktop";
		}
		// Check if user agent is a Linux Desktop
		else if ((preg_match('/Linux/i', $ua)) && (preg_match('/X11/i', $ua)))
		{
				$device = "desktop";
		}
		// Check if user agent is a Solaris, SunOS, BSD Desktop
		else if ((preg_match('/Solaris|SunOS|BSD/i', $ua)))
		{
				$device = "desktop";
		}
		// Check if user agent is a Desktop BOT/Crawler/Spider
		else if ((preg_match('/Bot|Crawler|Spider|Yahoo|ia_archiver|Covario-IDS|findlinks|DataparkSearch|larbin|Mediapartners-Google|NG-Search|Snappy|Teoma|Jeeves|TinEye/i', $ua)) && (!preg_match('/Mobile/i', $ua)))
		{
				$device = "desktop";
		}
		// Otherwise assume it is a Mobile Device
		else {
				$device = "mobile";
		}
		return $device;
	}
	public function isTablet() {
		if (isset($_SERVER['HTTP_USER_AGENT'])) {
            foreach($this->getTabletAgent() as $_regex){
                if(preg_match('/'.$_regex.'/is', $_SERVER['HTTP_USER_AGENT'])) {
                   return true;

                }
            }
        }
        return false;
	}
	public function getTabletAgent() {
		return array(
			'BlackBerryTablet'  => 'PlayBook|RIM Tablet',
			'iPad'              => 'iPad|iPad.*Mobile',
			'NexusTablet'       => '^.*Android.*Nexus(?:(?!Mobile).)*$',
			'Kindle'            => 'Kindle|Silk.*Accelerated',
			'SamsungTablet'     => 'SAMSUNG.*Tablet|Galaxy.*Tab|GT-P1000|GT-P1010|GT-P6210|GT-P6800|GT-P6810|GT-P7100|GT-P7300|GT-P7310|GT-P7500|GT-P7510|SCH-I800|SCH-I815|SCH-I905|SGH-I957|SGH-I987|SGH-T849|SGH-T859|SGH-T869|SPH-P100|GT-P1000|GT-P3100|GT-P3110|GT-P5100|GT-P5110|GT-P6200|GT-P7300|GT-P7320|GT-P7500|GT-P7510|GT-P7511|GT-N8000',
			'HTCtablet'         => 'HTC Flyer|HTC Jetstream|HTC-P715a|HTC EVO View 4G|PG41200',
			'MotorolaTablet'    => 'xoom|sholest|MZ615|MZ605|MZ505|MZ601|MZ602|MZ603|MZ604|MZ606|MZ607|MZ608|MZ609|MZ615|MZ616|MZ617',
			'AsusTablet'        => 'Transformer|TF101',
			'NookTablet'        => 'Android.*Nook|NookColor|nook browser|BNTV250A|LogicPD Zoom2',
			'AcerTablet'        => 'Android.*\b(A100|A101|A200|A500|A501|A510|W500|W500P|W501|W501P)\b',
			'YarvikTablet'      => 'Android.*(TAB210|TAB211|TAB224|TAB250|TAB260|TAB264|TAB310|TAB360|TAB364|TAB410|TAB411|TAB420|TAB424|TAB450|TAB460|TAB461|TAB464|TAB465|TAB467|TAB468)',
			'MedionTablet'      => 'Android.*\bOYO\b|LIFE.*(P9212|P9514|P9516|S9512)|LIFETAB',
			'ArnovaTablet'      => 'AN10G2|AN7bG3|AN7fG3|AN8G3|AN8cG3|AN7G3|AN9G3|AN7dG3|AN7dG3ST|AN7dG3ChildPad|AN10bG3|AN10bG3DT',
			'ArchosTablet'      => 'Android.*ARCHOS|101G9|80G9',
			'AinolTablet'       => 'NOVO7|Novo7Aurora|Novo7Basic|NOVO7PALADIN',
			'SonyTablet'        => 'Sony Tablet|Sony Tablet S',
			'CubeTablet'        => 'Android.*(K8GT|U9GT|U10GT|U16GT|U17GT|U18GT|U19GT|U20GT|U23GT|U30GT)',
			'CobyTablet'        => 'MID1042|MID1045|MID1125|MID1126|MID7012|MID7014|MID7034|MID7035|MID7036|MID7042|MID7048|MID7127|MID8042|MID8048|MID8127|MID9042|MID9740|MID9742|MID7022|MID7010',
			'GenericTablet'     => 'Android.*\b97D\b|Tablet(?!.*PC)|ViewPad7|LG-V909|MID7015|BNTV250A|LogicPD Zoom2|\bA7EB\b|CatNova8|A1_07|CT704|CT1002|\bM721\b|hp-tablet',
		);
	}

}
