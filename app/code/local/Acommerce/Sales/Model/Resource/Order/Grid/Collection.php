<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */
class Acommerce_Sales_Model_Resource_Order_Grid_Collection extends Mage_Sales_Model_Resource_Order_Grid_Collection
{
    protected $_instantMethods = array(
        Mage_Paypal_Model_Config::METHOD_WPS,
        'kbank_payment',
        'bbgateway',
        'cc2p',
        //'kbank711'
    );

    protected $_one23Methods = array(
        'one23counterservice',
        'one23tescolotus',
        'one23cash'
    );

    protected $_codMethods = array(
        'cashondelivery',
        'phoenix_cashondelivery'
    );

    protected function _construct()
    {
        parent::_construct();
    }

    /**
     * join sales order collection to payment
     *
     * @return order collection
     */
    public function addPaymentToSelect()
    {
        $this->addFilterToMap('payment_method', 'payment.method');
        $this->join(
                array('payment' => 'sales/order_payment'), 'main_table.entity_id = payment.parent_id',
                array('payment_method' => 'payment.method')
        );
        return $this;
    }

    public function addEmailToSelect()
    {
        $this->addFilterToMap('customer_email', 'customer.email');
        $this->addFilterToMap('created_at', 'main_table.created_at');
        $this->addFilterToMap('store_id', 'main_table.store_id');
        $this->addFilterToMap('increment_id', 'main_table.increment_id');
        $this->getSelect()->joinLeft(
                array('customer' => $this->getTable('customer/entity')), 'customer.entity_id = main_table.customer_id',
                array('customer_email' => 'customer.email')
        );
        return $this;
    }

    public function addShippingToSelect() {
        $this->addFilterToMap('telephone', 'address.telephone');
        $this->getSelect()->joinLeft(
                array('address' => $this->getTable('sales/order_address')), "main_table.entity_id = address.parent_id AND address.address_type = 'shipping'",
                array('telephone' => 'address.telephone')
        );
        return $this;
    }

    public function addExportFlagToSelect() {
        $this->addFilterToMap('is_exported', 'IF(IFNULL(batch.batch_id, 0) = 0, 0, 1)');
        $this->getSelect()->joinLeft(
                array('batch' => 'sales_order_export'), "main_table.entity_id = batch.order_id",
                array('is_exported' => 'IF(IFNULL(batch.batch_id, 0) = 0, 0, 1)')
        );
        return $this;
    }

    protected function getInstantMethods()
    {
        return "'" . implode("','", $this->_instantMethods) . "'";
    }

    public function setInstantMethods($method) {
        if(!$method || !is_array($method) || empty($method)) {
            return $this;
        }
        $this->_instantMethods = $method;

        return $this;
    }

    protected function getOne23Methods()
    {
        return "'" . implode("','", $this->_one23Methods) . "'";
    }

    protected function getCodMethods()
    {
        return "'" . implode("','", $this->_codMethods) . "'";
    }

    public function addTimeFilter($instant=0, $one23=0, $cod=0)
    {
        $methods = $this->getInstantMethods();
        $one23Methods = $this->getOne23Methods();
        $codMethods = $this->getCodMethods();
        $where = '';
        if($instant) {
            $where.= '(main_table.created_at <= DATE_SUB(NOW(),INTERVAL ' . $instant . ' HOUR) and payment.method in (' . $methods . '))';
        }
        if($one23) {
            $where.= ($instant)?' or ':'';
            $where.='(main_table.created_at <= DATE_SUB(NOW(),INTERVAL ' . $one23 . ' HOUR) and payment.method in (' . $one23Methods . '))';
        }
        if($cod) {
            $where.= ($one23 || $instant)?' or ':'';
            $where.='(main_table.created_at <= DATE_SUB(NOW(),INTERVAL ' . $cod . ' HOUR) and payment.method in (' . $codMethods . '))';
        }
        if($where) {
            $this->getSelect()->where($where);
        }
        return $this;
    }

    public function addPendingOrders()
    {
        $this->addFieldToFilter('main_table.status', array('in' => array('pending', 'pending_payment')));
        return $this;
    }
}