<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class Acommerce_Sales_Model_Order extends Mage_Sales_Model_Order
{
	const STATE_SHIPPED             = 'shipped';
	const STATE_INVOICED            = 'invoiced';
	protected $_unchangeState = array(
		self:: STATE_COMPLETE,
		self:: STATE_CLOSED,
		self:: STATE_CANCELED,
		self:: STATE_HOLDED,
		self:: STATE_PAYMENT_REVIEW,
	);
	/**
     * ....
     */
    protected function _checkState()
    {
        if (!$this->getId()) {
            return $this;
        }
		// check and add 2 new states
		$oldState = $this->getState();
        $userNotification = $this->hasCustomerNoteNotify() ? $this->getCustomerNoteNotify() : null;
		if(!$this->canShip() && $this->hasShipments() && $oldState !== self::STATE_SHIPPED
		&& $this->_validateState($oldState)) {
			$this->_setState(self::STATE_SHIPPED, true, '', $userNotification);
		}
		if(!$this->canInvoice() && $this->hasInvoices() && $oldState !== self::STATE_INVOICED
		&& $this->_validateState($oldState)) {
			$this->_setState(self::STATE_INVOICED, true, '', $userNotification);
		}
        if (!$this->isCanceled()
            && !$this->canUnhold()
            && !$this->canInvoice()
            && !$this->canShip()) {
            if (0 == $this->getBaseGrandTotal() || $this->canCreditmemo()) {
                if ($this->getState() !== self::STATE_COMPLETE) {
                    $this->_setState(self::STATE_COMPLETE, true, '', $userNotification);
                }
            }
            /**
             * Order can be closed just in case when we have refunded amount.
             * In case of "0" grand total order checking ForcedCanCreditmemo flag
             */
            elseif (floatval($this->getTotalRefunded()) || (!$this->getTotalRefunded()
                && $this->hasForcedCanCreditmemo())
            ) {
                if ($this->getState() !== self::STATE_CLOSED) {
                    $this->_setState(self::STATE_CLOSED, true, '', $userNotification);
                }
            }
        }

        if ($this->getState() == self::STATE_NEW && $this->getIsInProcess()) {
            $this->setState(self::STATE_PROCESSING, true, '', $userNotification);
        }
        return $this;
    }

	protected function _validateState($state) {
		if(!in_array($state,$this->_unchangeState)) {
				return true;
		}
		return false;
	}

    /**
     * Send email with order data
     *
     * @return Mage_Sales_Model_Order
     * @throws Exception
     */
    public function sendNewOrderEmail()
    {
        $storeId = $this->getStore()->getId();

        if (!Mage::helper('sales')->canSendNewOrderEmail($storeId)) {
            return $this;
        }

        $emailSentAttributeValue = $this->hasEmailSent()
            ? $this->getEmailSent()
            : Mage::getModel('sales/order')->load($this->getId())->getData('email_sent');
        $this->setEmailSent((bool)$emailSentAttributeValue);
        if ($this->getEmailSent()) {
            return $this;
        }

        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_EMAIL_COPY_METHOD, $storeId);

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($this->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        // Retrieve corresponding email template id and customer name
        if ($this->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $this->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE, $storeId);
            $customerName = $this->getCustomerName();
        }

        $mailer = Mage::getModel('core/email_template_mailer');
        $emailInfo = Mage::getModel('core/email_info');
        $emailInfo->addTo($this->getCustomerEmail(), $customerName);
        if ($copyTo && $copyMethod == 'bcc') {
            // Add bcc to customer email
            foreach ($copyTo as $email) {
                $emailInfo->addBcc($email);
            }
        }
        $mailer->addEmailInfo($emailInfo);

        // Email copies are sent as separated emails if their copy method is 'copy'
        if ($copyTo && $copyMethod == 'copy') {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        $paymentText = false;
        foreach ($this->getAllItems() as $item) {
            if($item->getInstallmentTerm()) {
                $term = $item->getInstallmentTerm();
                $rate = $item->getInstallmentRate();
                $grandTotal = $this->getBaseGrandTotal();

                $perMonth = (($grandTotal * $rate) + $grandTotal) / $term;
                $perMonth = round($perMonth,2);

                $paymentText = Mage::helper('sales')->__('Installment %s%s %s Month', $rate, '%', $term);
                break;
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
                'order'        => $this,
                'billing'      => $this->getBillingAddress(),
                'payment_html' => $paymentBlockHtml,
                'payment_text' => $paymentText
            )
        );
        $mailer->send();

        $this->setEmailSent(true);
        $this->_getResource()->saveAttribute($this, 'email_sent');

        return $this;
    }


    /**
     * Send email with order update information
     *
     * @param boolean $notifyCustomer
     * @param string $comment
     * @return Mage_Sales_Model_Order
     */
    public function sendOrderUpdateEmail($notifyCustomer = true, $comment = '')
    {
        $storeId = $this->getStore()->getId();

        if (!Mage::helper('sales')->canSendOrderCommentEmail($storeId)) {
            return $this;
        }
        // Get the destination email addresses to send copies to
        $copyTo = $this->_getEmails(self::XML_PATH_UPDATE_EMAIL_COPY_TO);
        $copyMethod = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_COPY_METHOD, $storeId);
        // Check if at least one recepient is found
        if (!$notifyCustomer && !$copyTo) {
            return $this;
        }

        // Retrieve corresponding email template id and customer name
        if ($this->getCustomerIsGuest()) {
            $templateId = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_GUEST_TEMPLATE, $storeId);
            $customerName = $this->getBillingAddress()->getName();
        } else {
            $templateId = Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_TEMPLATE, $storeId);
            $customerName = $this->getCustomerName();
        }

        $mailer = Mage::getModel('core/email_template_mailer');
        if ($notifyCustomer) {
            $emailInfo = Mage::getModel('core/email_info');
            $emailInfo->addTo($this->getCustomerEmail(), $customerName);
            if ($copyTo && $copyMethod == 'bcc') {
                // Add bcc to customer email
                foreach ($copyTo as $email) {
                    $emailInfo->addBcc($email);
                }
            }
            $mailer->addEmailInfo($emailInfo);
        }

        // Email copies are sent as separated emails if their copy method is
        // 'copy' or a customer should not be notified
        if ($copyTo && ($copyMethod == 'copy' || !$notifyCustomer)) {
            foreach ($copyTo as $email) {
                $emailInfo = Mage::getModel('core/email_info');
                $emailInfo->addTo($email);
                $mailer->addEmailInfo($emailInfo);
            }
        }

        $paymentText = false;
        foreach ($this->getAllItems() as $item) {
            if($item->getInstallmentTerm()) {
                $term = $item->getInstallmentTerm();
                $rate = $item->getInstallmentRate();
                $grandTotal = $this->getBaseGrandTotal();

                $perMonth = (($grandTotal * $rate) + $grandTotal) / $term;
                $perMonth = round($perMonth,2);

                $paymentText = Mage::helper('sales')->__('Installment %s%s %s Month', $rate, '%', $term);
                break;
            }
        }

        // Set all required params and send emails
        $mailer->setSender(Mage::getStoreConfig(self::XML_PATH_UPDATE_EMAIL_IDENTITY, $storeId));
        $mailer->setStoreId($storeId);
        $mailer->setTemplateId($templateId);
        $mailer->setTemplateParams(array(
                'order'   => $this,
                'comment' => $comment,
                'billing' => $this->getBillingAddress(),
                'payment_text' => $paymentText
            )
        );
        $mailer->send();

        return $this;
    }
}