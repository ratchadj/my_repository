<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class Acommerce_Sales_Model_Cronjobs
{	protected $_helper = '';
	protected function _getHelper() {
		if(!$this->_helper) {
			$this->_helper = Mage::helper('acommerce_sales');
		}
		return $this->_helper;
	}

	public function autoCancelOrders() {
		try{
			if(!$this->_getHelper()->isCronActive()) {
				return ;
			}
			$config = $this->_getHelper()->getConfig();

			$collection = Mage::getResourceModel('acommerce_sales/order_grid_collection')
				->addPaymentToSelect()
				->addTimeFilter($config['instant_payment'],$config['one23_payment'],$config['cod_payment'])
				->addPendingOrders();
				//Mage::log($collection->getSelect()->__toString(), null, 'sqlSO.log');
				foreach($collection as $order) {
					if($order->canCancel()) {
						$order->cancel()->save();
					}
				}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null,'CancelOrders.log');
		}
	}
	public function checkInstantOrders() {
		try{
			if(!$this->_getHelper()->isInstantCronActive()) {
				return ;
			}
			$time = (int)Mage::getStoreConfig('acommerce_sales/instantconfig/time');
			$methods = explode(',',Mage::getStoreConfig('acommerce_sales/instantconfig/apply_for'));
			if($time>0) {
				$collection = Mage::getResourceModel('acommerce_sales/order_grid_collection')->setInstantMethods($methods)
					->addPaymentToSelect()
					->addTimeFilter($time)
					->addPendingOrders();
				foreach($collection as $order) {
					$order->setState(Acommerce_Sales_Model_Order::STATE_CANCELED)
					->setStatus(Acommerce_Sales_Model_Order::STATE_CANCELED)
					->cancel()
					->save();
				}
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null,'changeStatus.log');
		}
	}

	public function convertLineOrder() {
		$countryId = Mage::getStoreConfig('general/country/default');

		$collection = Mage::getModel('acommerce_sales/temp')->getCollection();
		$collection->AddFieldToFilter('is_created', array('eq' => 0))
		           ->AddFieldToFilter('is_processing', array('eq' => 0))
		           ->AddFieldToFilter('payment_method', array('eq' => 'phoenix_cashondelivery'))
		           //->AddFieldToFilter('created_at', array('lteq' => new Zend_Db_Expr("DATE_SUB(now(),INTERVAL 2 MINUTE)")))
		           ->setOrder('id', 'ASC');

		$collection->getSelect()->limit(100);
		//echo $collection->getSelect()->__toString()."\n";

		foreach ($collection as $item) {
		    $item->setIsProcessing(1);
		    $item->save();
		}

		foreach ($collection as $item) {
		    $quote = Mage::getModel('sales/quote');

		    $customer = Mage::getModel('customer/customer');
		    $customer->setWebsiteId(1);
		    $customer->loadByEmail($item->getBillingEmail());

		    if ($customer->getId()) {
		        $quote->assignCustomer($customer);
		    } else {
		        $quote->setCustomerEmail($item->getBillingEmail());
		    }

		    $stores = Mage::getModel('core/store')->getCollection()
		                ->addFieldToFilter('store_id', array('neq' => 0));

		    foreach ($stores as $store) {
		    	$local = Mage::getStoreConfig('general/locale/code', $store->getId());
		    	if($local == 'th_TH') {
		    		$quote->setStore($store);
		        	break;
		    	}
		    }

		    $options = $item->getProductOptions();

		    if($options) {
		        $options = unserialize($options);
		        //var_dump($options);
		    }

		    $productId = $item->getProductId();

		    //add qty before create order
		    $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
            $stockItem->addQty(1)
            		  ->setIsInStock(true)
                      ->setStockStatusChangedAutomaticallyFlag(true)
                      ->save();


		    $parentId = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($productId);

		    if (is_array($parentId) && count($parentId) > 0) {
		        $parentId = $parentId[0];
		        $product = Mage::getModel('catalog/product')->load($parentId);
		        $useProductAttributes = Mage::getModel('catalog/product_type_configurable')->getUsedProductAttributes($product);
		        $attributeOptoins = Mage::getResourceModel('campaigns/product_type_configurable')->getConfigurableOptions($product,
		                                                                                                                  $useProductAttributes);

		        $superAttributes = array();
		        foreach ($attributeOptoins as $key => $attributes) {
		            foreach ($attributes as $attribute) {
		                if ($attribute['entity_id'] == $productId) {
		                    $superAttributes[$key] = $attribute['option_id'];
		                }
		            }
		        }
		        $request = new Varien_Object(array('qty' => $item->getQty(), 'super_attribute' => $superAttributes));
		    } else {
		        $product = Mage::getModel('catalog/product')->load($productId);
		        $request = new Varien_Object(array('qty' => $item->getQty()));
		    }

		    $quoteItem = $quote->addProduct($product, $request);
		    if(isset($options['custome_price'])) {
		       $quoteItem->setCustomPrice($options['custome_price']);
		       $quoteItem->setOriginalCustomPrice($options['custome_price']);

		       if ($parentItem = $quoteItem->getParentItem()) {
		            $parentItem->setCustomPrice($options['custome_price']);
		            $parentItem->setOriginalCustomPrice($options['custome_price']);
			    }
		    } else {
		    	$product = Mage::getModel('catalog/product')->load($productId);
		    	$quoteItem->setCustomPrice($product->getFinalPrice());
		        $quoteItem->setOriginalCustomPrice($product->getFinalPrice());

		       if ($parentItem = $quoteItem->getParentItem()) {
		            $parentItem->setCustomPrice($product->getFinalPrice());
		            $parentItem->setOriginalCustomPrice($product->getFinalPrice());
			    }
		    }

		    $freeItems = array();
		    if(isset($options['free_goods'])) {
		        foreach ($options['free_goods'] as $key => $value) {
		            $freeItems[] = $key;
		            $productId = Mage::getModel("catalog/product")->getIdBySku($key);
		            $parentId = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($productId);

		            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
		            $stockItem->addQty(1)
            		  ->setIsInStock(true)
                      ->setStockStatusChangedAutomaticallyFlag(true)
                      ->save();

		            if (is_array($parentId) && count($parentId) > 0) {
		                $parentId = $parentId[0];
		                $product = Mage::getModel('catalog/product')->load($parentId);
		                $useProductAttributes = Mage::getModel('catalog/product_type_configurable')->getUsedProductAttributes($product);
		                $attributeOptoins = Mage::getResourceModel('campaigns/product_type_configurable')->getConfigurableOptions($product,
		                                                                                                                          $useProductAttributes);

		                $superAttributes = array();
		                foreach ($attributeOptoins as $key => $attributes) {
		                    foreach ($attributes as $attribute) {
		                        if ($attribute['entity_id'] == $productId) {
		                            $superAttributes[$key] = $attribute['option_id'];
		                        }
		                    }
		                }
		                $request = new Varien_Object(array('qty' => ($value * $item->getQty()), 'super_attribute' => $superAttributes));
		            } else {
		                $product = Mage::getModel('catalog/product')->load($productId);
		                $request = new Varien_Object(array('qty' => ($value * $item->getQty())));
		            }

		            $quoteItem = $quote->addProduct($product, $request);
		            $quoteItem->setCustomPrice(0);
		            $quoteItem->setOriginalCustomPrice(0);

		            if ($parentItem = $quoteItem->getParentItem()) {
		                if(isset($options['custome_price'])) {
		                    $parentItem->setCustomPrice(0);
		                    $parentItem->setOriginalCustomPrice(0);
		                }
		            }
		        }
		    }


		    $addressForm = Mage::getModel('customer/form');
		    $addressForm->setFormCode('customer_address_edit')
		                ->setEntityType('customer_address');

		    $shippingAddress = $quote->getShippingAddress();

		    $shippingAddress->setVatType($item->getBillingVatType());
		    $shippingAddress->setSubDistrict($item->getShippingDistrictText());
		    $shippingAddress->setFirstname($item->getShippingFirstName());
		    $shippingAddress->setLastname($item->getShippingLastName());
		    $shippingAddress->setStreet($item->getShippingAddress());
		    $shippingAddress->setCity($item->getShippingProvinceText());
		    $shippingAddress->setCountryId($countryId);
		    $shippingAddress->setRegion($item->getShippingProvinceText());
		    $shippingAddress->setRegionId($item->getShippingProvince());
		    $shippingAddress->setPostcode($item->getShippingPostcode());
		    $shippingAddress->setTelephone($item->getShippingTelephone());
		    $shippingAddress->setVatId($item->getBillingVatId());
		    $shippingAddress->save();

		    $billingAddress = $quote->getBillingAddress();
		    $billingAddress->setVatType($item->getBillingVatType());
		    $billingAddress->setSubDistrict($item->getBillingDistrictText());
		    $billingAddress->setFirstname($item->getBillingFirstName());
		    $billingAddress->setLastname($item->getBillingLastName());
		    $billingAddress->setStreet($item->getBillingAddress());
		    $billingAddress->setCity($item->getBillingProvinceText());
		    $billingAddress->setCountryId($countryId);
		    $billingAddress->setRegion($item->getBillingProvinceText());
		    $billingAddress->setRegionId($item->getBillingProvince());
		    $billingAddress->setPostcode($item->getBillingPostcode());
		    $billingAddress->setTelephone($item->getBillingTelephone());
		    $billingAddress->setVatId($item->getBillingVatId());
		    $billingAddress->save();

		    $shippingMethod = $item->getShippingMethod();
		    if($shippingMethod) {

		        $quote->getShippingAddress()->setShippingMethod($shippingMethod.'_'.$shippingMethod);
		    } else {
		        $quote->getShippingAddress()->setShippingMethod('freeshipping_freeshipping');
		    }
		    $quote->getShippingAddress()->setCollectShippingRates(true);

		    $paymentMethod['method'] = $item->getPaymentMethod();
		    $quote->getPayment()->importData($paymentMethod);

		    $quote->collectTotals();
		    $quote->save();

		    $items = $quote->getAllItems();
		    $quote->reserveOrderId();

		    $convertQuote = Mage::getSingleton('sales/convert_quote');
		    $order = $convertQuote->addressToOrder($quote->getShippingAddress());
		    $orderPayment = $convertQuote->paymentToOrderPayment($quote->getPayment());

		    $order->setBillingAddress($convertQuote->addressToOrderAddress($quote->getBillingAddress()));
		    $order->setShippingAddress($convertQuote->addressToOrderAddress($quote->getShippingAddress()));
		    $order->setPayment($convertQuote->paymentToOrderPayment($quote->getPayment()));

		    if(isset($options['campaign_id'])) {
		        $campaignId = $options['campaign_id'];
		        $campaign = Mage::getModel('campaigns/campaign')->load($campaignId);
		    }

		    $freeItems = implode('|', $freeItems);
		    foreach ($items as $quoteitem) {
		        $orderItem = $convertQuote->itemToOrderItem($quoteitem);

		        if(strpos($freeItems, $quoteitem->getSku()) === false) {
		        	$freeItems = str_replace('WDS_', '', $freeItems);
		        	$freeItems = str_replace('AWN_', '', $freeItems);
		            $orderItem->setFreeGoods($freeItems);
		            $orderItem->setCustomerMobilePhone($item->getBillingTelephone());

		            if(isset($options['campaign_id'])) {
		                $orderItem->setTradeNo($campaign->getTradeNo());
		            }
		        }

		        if ($quoteitem->getParentItem()) {
		            $orderItem->setParentItem($order->getItemByQuoteItemId($quoteitem->getParentItem()->getId()));
		        }
		        $order->addItem($orderItem);
		        Mage::getSingleton('cataloginventory/stock')->registerItemSale($orderItem);
		    }

		    try {
		        $order->place();
		        if ($order->getPayment()->getMethodInstance()->getCode() == 'phoenix_cashondelivery') {
		            $order->setCodFee($quote->getCodFee());
		            $order->setBaseCodFee($quote->getBaseCodFee());

		            $order->setCodTaxAmount($quote->getCodTaxAmount());
		            $order->setBaseCodTaxAmount($quote->getBaseCodTaxAmount());
		        }
		        $order->save();

		        $order->setCreatedAt($item->getCreatedAt());
		        $order->save();
		        $order->sendNewOrderEmail();

		        $item->setIsCreated(1);
		        $item->setIsProcessing(0);
		        $item->setIncrementId($order->getIncrementId());
		        $item->save();
		    } catch (Exception $e) {
		        echo $e->getMessage();
		    }
		}
	}
}