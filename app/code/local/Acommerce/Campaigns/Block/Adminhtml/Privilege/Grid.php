<?php
class Acommerce_Campaigns_Block_Adminhtml_Privilege_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct() {
        parent::__construct();
        $this->setId('privilege');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('desc');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getResourceModel('campaigns/privilege_log_collection')
                        //->addNetsuiteStatus()
                        ;
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    protected function _prepareColumns() {
        /*$this->addColumn('entity_id',
                         array(
            'header' => Mage::helper('sales')->__('Id'),
            'index' => 'entity_id',
            'width' => '100px',
        ));*/
        $this->addColumn('created_at',
                         array(
            'header' => Mage::helper('sales')->__('Transaction Date'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '150px',
        ));

        $this->addColumn('transactin_id', array(
            'header'        => Mage::helper('campaigns')->__('Transaction Id'),
            'align'         => 'left',
            'index'         => 'transactin_id',
        ));

        $this->addColumn('access_code', array(
            'header'        => Mage::helper('campaigns')->__('Access Code'),
            'align'         => 'left',
            'index'         => 'access_code',
        ));

        $this->addColumn('ref_order', array(
            'header'        => Mage::helper('campaigns')->__('Ref Order'),
            'align'         => 'left',
            'index'         => 'ref_order',
        ));

        $this->addColumn('product_name', array(
            'header'        => Mage::helper('campaigns')->__('Product Name'),
            'align'         => 'left',
            'index'         => 'product_name',
        ));

        $this->addColumn('customer_firstname', array(
            'header'        => Mage::helper('campaigns')->__('Customer Name'),
            'align'         => 'left',
            'index'         => array('customer_firstname','customer_lastname'),
            'separator'     =>' ',
            'renderer'      => 'adminhtml/widget_grid_column_renderer_concat',
            'filter_index'  => 'concat(customer_firstname, \' \', customer_lastname)',
        ));

         $this->addColumn('customer_email', array(
            'header'        => Mage::helper('campaigns')->__('Customer Email'),
            'align'         => 'left',
            'index'         => 'customer_email',
        ));

        $this->addColumn('customer_phone', array(
            'header'        => Mage::helper('campaigns')->__('Phone Number'),
            'align'         => 'left',
            'index'         => 'customer_phone',
        ));


        $this->addColumn('is_refunded',
                         array(
            'header' => Mage::helper('campaigns')->__('Is Refunded'),
            'index' => 'is_refunded',
            //'width' => '150px',
            'type' => 'options',
            'options' => Mage::getModel('adminhtml/system_config_source_yesno')->toArray(),
        ));

        $this->addColumn('refunded_date',
                         array(
            'header' => Mage::helper('sales')->__('Refunded Date'),
            'index' => 'refunded_date',
            'type' => 'datetime',
            'width' => '150px',
        ));

        return parent::_prepareColumns();
    }
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->setMassactionIdFieldOnlyIndexValue(true);
        $this->getMassactionBlock()->setFormFieldName('entity_id');

        $this->getMassactionBlock()->addItem('export', array(
             'label'    => Mage::helper('campaigns')->__('Refund'),
             'url'      => $this->getUrl('*/*/refund'),
             'confirm'  => Mage::helper('campaigns')->__('Are you sure?')
        ));
        return $this;
    }
    public function getGridUrl() {
        return $this->getUrl('*/*/grid',array('_current'=>true));
    }
    public function getRowUrl($row) {
        return false;
    }

}
