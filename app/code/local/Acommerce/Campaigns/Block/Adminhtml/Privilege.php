<?php
class Acommerce_Campaigns_Block_Adminhtml_Privilege extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct() {
        $this->_controller = 'adminhtml_privilege';
        $this->_blockGroup = 'campaigns';
        $this->_headerText = Mage::helper('campaigns')->__('Privilege Transaction');
        parent::__construct();
        $this->_removeButton('add');
    }

}
