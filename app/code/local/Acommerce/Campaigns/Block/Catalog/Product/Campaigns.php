<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Product View block
 *
 * @category Mage
 * @package  Mage_Catalog
 * @module   Catalog
 * @author   Magento Core Team <core@magentocommerce.com>
 */
class Acommerce_Campaigns_Block_Catalog_Product_Campaigns extends Mage_Catalog_Block_Product_View
{
    public function setCampaigns($campaignId, $price = null, $freegoods = null, $privileges = null, $installments = null) {
        $temp = explode('|', $campaignId);
        $data = array();
        $data["campaign_id"] = $temp[0];
        $data["custome_price"] = $price;
        $data["free_goods"] = $freegoods;
        $data["installments"] = (!is_null($installments))? 1: 0;
        $data["is_ais_member"] = $temp[1];
        $data['ussd'] = $privileges;
        $data  = serialize($data);
        return $data;
    }

    protected function _getCustomer() {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    protected function _getCustomerSession() {
        return Mage::getSingleton('customer/session');
    }

    protected function _isAisMember() {
        $isMember = false;
        if($this->_getCustomer()->getId()) {
            $isMember = $this->_getCustomer()->getIsAisMember();
        }
        if(!$isMember)
        {
            $isMember = $this->_getCustomerSession()->getIsAisMember();
        }
        return $isMember;
    }

    public function getInstallmentData($conditions, $banks, $installments) {

        $temp = array();
        foreach ($banks as $value) {
            $key = $value['code'];
            if(isset($value['active_flag']) && $value['active_flag'] == 'Y') {
                $temp[$key] = $value;
            }
        }
        $banks = $temp;

        $temp = array();
        foreach ($installments as $value) {
            $key = $value['bank'].$value['code'];
            $temp[$key] = $value;
        }
        $installments = $temp;

        $results = array();

        $curDate = Mage::app()->getLocale()->storeDate(null, null, true);
        $curDate = strtotime($curDate->toString('Y-m-d H:i:s', 'php'));

        foreach ($conditions as $key => $condition) {
            foreach ($condition as $key1 => $value) {
               if(isset($banks[$key])) {
                    $bank = $banks[$key];
                    if (isset($installments[$bank['bank'].$key1])) {
                        $installment = $installments[$bank['bank'].$key1];

                        $validForm = $value[0];
                        $validTo = $value[1];
                        $rate = (!isset($installment['rate']) || empty($installment['rate'])) ? 0 : $installment['rate'];

                        if(!empty($validForm) && !empty($validTo)) {
                            $validForm = strtotime($validForm);
                            $validTo = strtotime($validTo);

                            if($validForm <= $curDate && $validTo >= $curDate) {
                                $results[] = array('bank' => $bank['name'], 'period' => $installment['period_label'], 'rate' => $rate);
                            }
                        } elseif(!empty($validForm) && empty($validTo)) {
                            $validForm = strtotime($validForm);
                            if($validForm <= $curDate) {
                                $results[] = array('bank' => $bank['name'], 'period' => $installment['period_label'], 'rate' => $rate);
                            }
                        } elseif(empty($validForm) && !empty($validTo)) {
                            $validTo = strtotime($validTo);
                            if($validTo >= $curDate) {
                                $results[] = array('bank' => $bank['name'], 'period' => $installment['period_label'], 'rate' => $rate);
                            }
                        } else {
                            $results[] = array('bank' => $bank['name'], 'period' => $installment['period_label'], 'rate' => $rate);
                        }

                    }
                }
            }
        }
        return $results;
    }

    public function getInstallments($conditions) {

        $banks = unserialize(Mage::getStoreConfig('payment/cc2p/bank'));
        $installments = unserialize(Mage::getStoreConfig('payment/cc2p/period'));

        $results = $this->getInstallmentData($conditions, $banks, $installments);

        $banks = unserialize(Mage::getStoreConfig('payment/kbank711/shop_id'));
        $installments = unserialize(Mage::getStoreConfig('payment/kbank711/installment/period'));

        $results1 = $this->getInstallmentData($conditions, $banks, $installments);

        return array_merge($results, $results1);
    }

    public function getCampaigns($memberType) {

        $product = $this->getProduct();
        $campaigns = array();

        if($product->getTypeId() == "configurable") {
            $productIds = $product->getTypeInstance()->getUsedProductIds();
        } else {
            $productIds = array($product->getId());
        }


        $products = array();
        foreach($productIds as $productId) {

            $finalPrice = Mage::getModel('catalog/product')->load($productId)->getFinalPrice();

            $discounts = Mage::getResourceModel('campaigns/discount_collection')
                              ->getDiscountPrices($productId, $memberType);

            $discountPrices = array();
            $freeGoods = array();
            $privileges = array();
            $installments = array();

            if(count($discounts) > 0) {
                foreach($discounts as $item) {
                    $key = $item->getCampaignId().'|'.$item->getIsAis();
                    if($item->getSalesPrice() > 0)
                    {
                        $discountPrices[$key] = (float)$item->getSalesPrice();
                    }

                    if($item->getDiscountType() == 'Percent') {
                        if(isset($discountPrices[$key])) {
                            $temp = $discountPrices[$key];
                            $discountPrices[$key] = ($temp - (($temp * $item->getDiscountValue()) / 100));
                        } else {
                            $discountPrices[$key] = ($finalPrice - (($finalPrice * $item->getDiscountValue()) / 100));
                        }
                    } else {
                        if(isset($discountPrices[$key])) {
                            $temp = $discountPrices[$key];
                            $discountPrices[$key] = $temp - $item->getDiscountValue();
                        } else {
                            $discountPrices[$key] = $finalPrice - $item->getDiscountValue();
                        }
                    }
                }
            }

            $p = new stdClass();
            $p->productId = $productId;
            $p->discount = $discountPrices;
            $p->finalPrice = $finalPrice;


            $freegoods = Mage::getResourceModel('campaigns/freegoods_collection')
                              ->getFreeGoodItems($productId, $memberType);

            if(count($freegoods) > 0) {
                foreach($freegoods as $item) {
                    $key = $item->getCampaignId().'|'.$item->getIsAis();
                        $freeGoods[$key] = unserialize($item->getFreeGoods());
                }
            }
            $p->freeGoods = $freeGoods;

            $privilegesData = Mage::getResourceModel('campaigns/privilege_collection')
                              ->getPrivilegeByProduct($productId);

            if(count($privilegesData) > 0) {
                foreach($privilegesData as $item) {
                    $key = $item->getCampaignId().'|1';
                        $privileges[$key] = $item->getUssd();
                }
            }

            $p->privileges =  $privileges;


            $installmentData = Mage::getResourceModel('campaigns/installment_collection')
                              ->getInstallmentByProduct($productId, $memberType);

            if(count($installmentData) > 0) {
                foreach($installmentData as $item) {
                    $key = $item->getCampaignId().'|'.$item->getIsAis();
                    $installments[$key] = array('card_type' => unserialize($item->getCardType()), 'condition' => unserialize($item->getConditions()));
                }
            }

            $p->installments = $installments;
            $products[$productId] = $p;

        }
        return $products;
  }
}
