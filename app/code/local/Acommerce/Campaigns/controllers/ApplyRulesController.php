<?php

/**
 * Acommerce extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Acommerce AdminLog module to newer versions in the future.
 * If you wish to customize the Acommerce AdminLog module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @copyright  Copyright (C) 2011 Acommerce Web ltd (http://www.acommerce.asia/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Campaigns Controllers
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @subpackage Controller
 * @author     dev acommerce <dev@acommerce.asia>
 */
class Acommerce_Campaigns_ApplyRulesController extends Mage_Core_Controller_Front_Action
{
    public function indexAction() {
        $errorMessage = Mage::helper('catalogrule')->__('Unable to apply rules.');
        try {
            Mage::getModel('catalogrule/rule')->applyAll();
            Mage::getModel('catalogrule/flag')->loadSelf()
                ->setState(0)
                ->save();
            echo Mage::helper('catalogrule')->__('The rules have been applied.');
        } catch (Mage_Core_Exception $e) {
            echo $errorMessage . ' ' . $e->getMessage();
        } catch (Exception $e) {
            echo $errorMessage;
            Mage::logException($e);
        }   
    }
}