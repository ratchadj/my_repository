<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade NetsuiteExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_NetsuiteExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class Acommerce_Campaigns_Adminhtml_PrivilegeController extends Mage_Adminhtml_Controller_Action
{

    protected function _initAction() {
        $this->loadLayout()
            ->_setActiveMenu('netsuite')
            ->_addBreadcrumb(Mage::helper('adminhtml')->__('Privilege Transaction'), Mage::helper('adminhtml')->__('Privilege Transaction'));

        return $this;
    }

    public function indexAction() {
        $this->_initAction();
        $this->_addContent($this->getLayout()->createBlock('campaigns/adminhtml_privilege','privilege'));
        $this->renderLayout();
    }
    public function gridAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('campaigns/adminhtml_privilege_grid')->toHtml()
        );
    }
    public function refundAction() {
        $ids = $this->getRequest()->getParam('entity_id');
        if($ids) {
            if(!is_array($ids)) {
                $ids = array($ids);
            }
            try {
                $tranIds = array();
                foreach ($ids as $id) {
                    $log = Mage::getModel('campaigns/privilege_log')->load($id);
                    if($log->getId()) {

                        $refund = Mage::getModel('campaigns/observer')->_refundPrivilege($log->getUssdCode(), $log->getCustomerPhone(), $log->getTransactinId());

                        if($refund) {
                            $currentTime = Varien_Date::now();
                            $log->setIsRefunded(1)
                                ->setRefundedDate($currentTime)
                                ->save();
                        } else {
                            $tranIds[] = $log->getTransactinId();
                        }
                    }
                }
                if(count($tranIds) > 0) {
                    $this->_getSession()->addError('Cannot refund transaction (' . implode(', ', $tranIds) . ')');
                } else {
                    $this->_getSession()->addSuccess('Already Refunded.');
                }
            }
            catch(Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }

        }
        else {
            $this->_getSession()->addWarning($this->__('No order was selected'));
        }
        $this->_redirect('*/*/index');
    }
}