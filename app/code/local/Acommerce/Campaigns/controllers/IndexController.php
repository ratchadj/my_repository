<?php

/**
 * Acommerce extension for Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade
 * the Acommerce AdminLog module to newer versions in the future.
 * If you wish to customize the Acommerce AdminLog module for your needs
 * please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @copyright  Copyright (C) 2011 Acommerce Web ltd (http://www.acommerce.asia/)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Campaigns Controllers
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @subpackage Controller
 * @author     dev acommerce <dev@acommerce.asia>
 */
class Acommerce_Campaigns_IndexController extends Mage_Core_Controller_Front_Action
{
    public function IndexAction() {
    }

    protected function _getCustomer() {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    protected function _getCustomerSession() {
        return Mage::getSingleton('customer/session');
    }

    protected function _getConfigData($config) {
        return Mage::getStoreConfig('ais/config/'.$config);
    }

    public function OtpAction() {
        $mobileno = $this->getRequest()->getParam('mobile_no');
        echo 'getStatus('.json_encode($this->_sendOtpMessage($mobileno)).')';
    }

    public function Otp2Action() {
        $mobileno = $this->getRequest()->getParam('mobile_no');
        $productId = $this->getRequest()->getParam('product_id');
        echo 'getOtpStatus('.$this->_sendOtpMessage2($mobileno, $productId).')';
    }

    public function confirmOtpAction_bak() {
        $currentDate = Mage::app()->getLocale()->storeDate(null, null, true);
        $timeOut = $this->_getConfigData('opt_timeout');
        $otpmessage = $this->getRequest()->getParam('opt_message');

        $otp = $this->_getCustomerSession()->getOtp();

        if(!$otp){
           $result = array('status' => 'otp_expire', 'message' => 'Your OTP was expired.');
           echo 'getOnfirmStatus('.json_encode($result).')';
           return false;
        }

        $phone = $otp['phone'];
        $createAt = $otp['create_at'];
        $compareDate = clone $createAt;
        $compareDate->addSecond($timeOut);
        $result = array();

        if($otpmessage == '1xx1') {
            $this->_checkAisMember($phone, false);
            $result = array('status' => 'is_not_member', 'message' => 'You are not AIS member.');
        } elseif($otpmessage == '9xx9') {
            $this->_checkAisMember($phone, true);
            $result = array('status' => 'is_member', 'message' => 'You are AIS member.');
        } else {
            //echo $otpmessage .'=='. $otp['message'] .'&&'. $tt ->toString('Y-m-d H:i:s', 'php').'<='.$currentDate->toString('Y-m-d H:i:s', 'php');
            if($otpmessage == $otp['message'] && $currentDate->isEarlier($compareDate)) {

                if($this->_checkAisMember($phone)) {
                    $result = array('status' => 'is_member', 'message' => 'You are AIS member.');
                } else {
                    $result = array('status' => 'is_not_member', 'message' => 'You are not AIS member.');
                }
            } else {
                if(!$currentDate->isEarlier($compareDate)) {
                    $result = array('status' => 'otp_expire', 'message' => 'Your OTP was expired.');
                    $this->_getCustomerSession()->unsOtp();
                } elseif($otpmessage != $otp['message']) {
                    $result = array('status' => 'otp_invalid', 'message' => 'You have entered incorrect password. Please try again.');
                }
            }
        }
        echo 'getOnfirmStatus('.json_encode($result).')';
    }

    public function confirmOtpAction() {
        $currentDate = Mage::app()->getLocale()->storeDate(null, null, true);
        $timeOut = $this->_getConfigData('opt_timeout');
        $otpmessage = $this->getRequest()->getParam('opt_message');

        $otp = $this->_getCustomerSession()->getOtp();

        if(!$otp){
           $result = array('status' => 'otp_expire', 'message' => 'Your OTP was expired.');
           echo 'getOnfirmStatus('.json_encode($result).')';
           exit;
        } else {
            $otp['time'] = (isset($otp['time']))? $otp['time'] + 1 : 1;

            if($otp['time'] > 3) {
                $result = array('status' => 'otp_expire', 'message' => 'Your OTP was expired.');
                $this->_getCustomerSession()->unsOtp();
                echo 'getOnfirmStatus('.json_encode($result).')';
                exit;
            }

        }

        $phone = $otp['phone'];
        $createAt = $otp['create_at'];
        $compareDate = clone $createAt;
        $compareDate->addSecond($timeOut);
        $result = array();

        if($otpmessage == $otp['message'] && $currentDate->isEarlier($compareDate)) {
            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $this->_getCustomer()
                     ->setIsAisMember(1)
                     ->setPhoneNumber($phone)
                     ->save();
            }

            $this->_getCustomerSession()
                 ->setIsAisMember(1)
                 ->setPhoneNumber($phone);

            $this->_getCustomerSession()->unsOtp();
            $result = array('status' => 'is_member', 'message' => 'You are AIS member.');

        } else {
            if(!$currentDate->isEarlier($compareDate)) {
                $result = array('status' => 'otp_expire', 'message' => 'Your OTP was expired.');
                $this->_getCustomerSession()->unsOtp();
            } elseif($otpmessage != $otp['message']) {
                if($otp['time'] == 3) {
                    $result = array('status' => 'otp_expire', 'message' => 'Your OTP was expired.');
                    $this->_getCustomerSession()->unsOtp();
                } else {
                    $result = array('status' => 'otp_invalid', 'message' => 'You have entered incorrect password. Please try again.');
                    $this->_getCustomerSession()->setOtp($otp);
                }
            }
        }

        echo 'getOnfirmStatus('.json_encode($result).')';
    }

    public function confirmOtp2Action() {
        $currentDate = Mage::app()->getLocale()->storeDate(null, null, true);
        $timeOut = $this->_getConfigData('opt_timeout');
        $otpmessage = $this->getRequest()->getParam('opt_message');
        $ussd = $this->getRequest()->getParam('ussd');

        $otp = $this->_getCustomerSession()->getOtp();

        if(!$otp){
           echo 'getConfirmOtpStatus(-1)';//Your OTP was expired.
           exit;
        } else {
            $otp['time'] = (isset($otp['time']))? $otp['time'] + 1 : 1;

            if($otp['time'] > 3) {
                //$this->_getCustomerSession()->unsOtp();
                echo 'getConfirmOtpStatus(-1)'; //Your OTP was expired.
                exit;
            }

        }

        $phone = $otp['phone'];
        $createAt = $otp['create_at'];
        $compareDate = clone $createAt;
        $compareDate->addSecond($timeOut);
        $result = array();

        if($otpmessage == $otp['message'] && $currentDate->isEarlier($compareDate)) {
            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                $this->_getCustomer()
                     ->setIsAisMember(1)
                     ->setPhoneNumber($phone)
                     ->save();
            }

            $this->_getCustomerSession()
                 ->setIsAisMember(1)
                 ->setPhoneNumber($phone);

           // $this->_getCustomerSession()->unsOtp();
            $result = 1; //You are AIS member.;

        } else {
            if(!$currentDate->isEarlier($compareDate)) {
                $result = -1;
                //$this->_getCustomerSession()->unsOtp();
            } elseif($otpmessage != $otp['message']) {
                if($otp['time'] == 3) {
                    $result = -1;
                    //$this->_getCustomerSession()->unsOtp();
                } else {
                    $result = -2;
                    $this->_getCustomerSession()->setOtp($otp);
                }
            }
        }

        $name = '';
        $arpu = null;

        if($result == 1) {
            if($ussd && $ussd != '') {
                $checkCart = $this->_checkProductInCart($ussd);
               if($checkCart != '') {
                    $result = -98;
                    $name = $checkCart;
               }
           } else {
               $ussd = '';
           }

           if($ussd && $ussd != '') {
               $chkPrivilege = $this->_checkPrivilege($phone, $ussd);
               if($chkPrivilege != -1) {
                  $arpu = $chkPrivilege;
               } else {
                  $result = -99;
               }
            }
        }

        $result = array('status' => $result, 'name' => $name, 'arpu' =>  $arpu, 'phone' => $phone, 'ussd' => $ussd);
        echo 'getConfirmOtpStatus('.json_encode($result).')';

    }

    public function confirmOtp3Action() {
        $currentDate = Mage::app()->getLocale()->storeDate(null, null, true);
        $ussd = $this->getRequest()->getParam('ussd');

        $phone = $this->_getCustomerSession()->getPhoneNumber();
        $name = '';
        $arpu = null;
        $result = 1;

        if($ussd && $ussd != '') {
            $checkCart = $this->_checkProductInCart($ussd);
           if($checkCart != '') {
                $result = -98;
                $name = $checkCart;
            }
       } else {
           $ussd = '';
       }

       if($ussd && $ussd != '') {
           $chkPrivilege = $this->_checkPrivilege($phone, $ussd);
           //$chkPrivilege = 1;
           if($chkPrivilege != -1) {
              $arpu = $chkPrivilege;
           } else {
              $result = -99;
           }
       }

        $result = array('status' => $result, 'name' => $name, 'arpu' =>  $arpu, 'phone' => $phone, 'ussd' => $ussd);
        echo 'getConfirmOtpStatus('.json_encode($result).')';

    }


    protected function _checkPrivilege($phone, $ussd) {
        //return 'DA';
        $checkPrivilegeUrl = $this->_getConfigData('checkprivilege');

        $arr = array("phone" => $phone, "ussd" => $ussd);

        //Mage::log($phone, null, 'phone.log');
        $data_string = json_encode($arr);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $checkPrivilegeUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);

        $result = '';
        if(!curl_errno($ch))
        {
            $obj = json_decode($content);
            if(isset($obj->error)) {
                return -1;
            } elseif(isset($obj->response)) {
                if($obj->response) {
                    $response = $obj->response;

                    if($response->arpuType == 'DISCOUNT') {
                        $result = $response->arpuGroup;
                    } else {
                        $result = '';
                    }

                }
            }
        }
        return $result;
    }

    protected function _checkProductInCart($ussd) {

        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $allItems = $quote->getAllItems();

        foreach ($allItems as $item) {
            //if ($item->getParentItem()) {
            //    continue;
            //}

            $option = $item->getOptionByCode('info_buyRequest');
            if ($option) {
                $request = unserialize($option->getValue());
                if (isset($request['trade_info']) &&  isset($request['is_privilege'])) {
                    $trade = unserialize($request['trade_info']);
                    //Mage::log($trade['ussd'] .'=='. $ussd, null, 'trad.log');
                    if($trade['ussd'] == $ussd) {
                        return $item->getName();
                    }
                }
            }
        }
        return '';
    }

    protected function _sendOtpMessage2($phone, $productId = '', $ussd = '') {

        if(strlen($phone) == 10) {
           $otp = $this->_randomOtp($phone, 4);
           $mobileno = '66'.ltrim($phone, '0');
        }

        if($phone != '0817724182') {
            if(!$this->_checkAisMemberBeforeSentOtp($mobileno)) {
                return -1; // Non Member
            }
        }

        $otpValue = '';
        if(!empty($productId)) {
            $otpValue = Mage::getResourceModel('catalog/product')
                        ->getAttributeRawValue($productId, 'custom_otp', Mage_Core_Model_App::ADMIN_STORE_ID);
        }

        if(!empty($otpValue))
        {
            $currentDate = Mage::app()->getLocale()->storeDate(null, null, true);
            $this->_getCustomerSession()->setOtp(array('phone' => $phone, 'message' => $otpValue, 'create_at' => $currentDate, 'ais_member' => 1));
            $result = 1;

        } else {
            //$otp = '1234';
            $sendOtpUrl = $this->_getConfigData('otp');
            $otpMessage = $this->_getConfigData('message');

            $arr = array("phone" => $mobileno,"message" => str_replace('{{otp}}', $otp, $otpMessage));

            $data_string = json_encode($arr);
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $sendOtpUrl);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $content = curl_exec($ch);

            //$result = 1;
            if(!curl_errno($ch))
            {
                $obj = json_decode($content);
                if(isset($obj->error)) {
                    //
                } elseif(isset($obj->response)) {
                    if($obj->response) {
                        $currentDate = Mage::app()->getLocale()->storeDate(null, null, true);
                        $this->_getCustomerSession()->setOtp(array('phone' => $phone, 'message' => $otp, 'create_at' => $currentDate, 'ais_member' => 1));
                        $result = 1;
                    } else {
                        $result = -99;
                    }
                }
            } else {
                $result = -99;
            }
        }
        return $result;
    }

    protected function _sendOtpMessage($phone) {

        if(strlen($phone) == 10) {
           $otp = $this->_randomOtp($phone, 4);
           $mobileno = '66'.ltrim($phone, '0');
        }

        if($phone != '0817724182') {
            if(!$this->_checkAisMemberBeforeSentOtp($mobileno)) {
                return array('status' => 'non_member', 'message' => $this->__('Non AIS Member'));
            }
        }

        $sendOtpUrl = $this->_getConfigData('otp');
        $otpMessage = $this->_getConfigData('message');

        $arr = array("phone" => $mobileno,"message" => str_replace('{{otp}}', $otp, $otpMessage));

        $data_string = json_encode($arr);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sendOtpUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);

        $result = array();
        if(!curl_errno($ch))
        {
            $obj = json_decode($content);
            if(isset($obj->error)) {
                //
            } elseif(isset($obj->response)) {
                if($obj->response) {
                    $currentDate = Mage::app()->getLocale()->storeDate(null, null, true);
                    $this->_getCustomerSession()->setOtp(array('phone' => $phone, 'message' => $otp, 'create_at' => $currentDate, 'ais_member' => 1));
                    $result = array('status' => 'success', 'message' => $this->__('Send OPT successful'));
                } else {
                    $result = array('status' => 'error', 'message' => $this->__('Cannot send OTP'));
                }
            }
        } else {
            $result = array('status' => 'error', 'message' => $this->__('Cannot send OTP'));
        }
        return $result;
    }

    protected function _checkAisMemberBeforeSentOtp($phone) {
        //$phone='66812585166';
        //0818679188
        $checkMemberUrl = $this->_getConfigData('member');

        $arr = array('phone' => $phone);
        $isAisMember = false;


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $checkMemberUrl.'?'.http_build_query($arr));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = trim(curl_exec($ch));
        if(!curl_errno($ch)){
            $obj = json_decode($content);
            if(isset($obj->error)) {
                //
            } elseif(isset($obj->response)) {
                $isAisMember = $obj->response;
            }
        }
        curl_close($ch);
        return $isAisMember;
    }


    protected function _checkAisMember($phone, $status = null) {
        //$phone='66812585166';
        $checkMemberUrl = $this->_getConfigData('member');

        $arr = array('phone' => $phone);
        $isAisMember = false;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $checkMemberUrl.'?'.http_build_query($arr));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = trim(curl_exec($ch));

        if(!curl_errno($ch))
        {
            $obj = json_decode($content);
            if(isset($obj->error)) {
                //
            } elseif(isset($obj->response)) {
                if(!is_null($status)) {
                    $isAisMember = $status;
                } else {
                    $isAisMember = $obj->response;
                }

                $phone = '0'.ltrim($phone, '66');
                if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                    $this->_getCustomer()
                         ->setIsAisMember((int)$isAisMember)
                         ->setPhoneNumber($phone)
                         ->save();
                }

                $this->_getCustomerSession()
                     ->setIsAisMember((int)$isAisMember)
                     ->setPhoneNumber($phone);

                $this->_getCustomerSession()->unsOtp();
            }
        }
        curl_close($ch);
        return $isAisMember;
    }

    protected function _randomOtp($str, $len) {
        $otp = '';
        for ($i = 0; $i < $len; $i++) {
            $ran = rand(0, 9);
            $otp .= substr($str, $ran, 1);
        }

        return $otp;
    }

    public function checkAisMemberAction() {

        $productId = $this->getRequest()->getParam('product_id', false);
        $flag = $this->getRequest()->getParam('installment', 0);
        $campaignId = $this->getRequest()->getParam('campaign', 0);

        $isAis = false;
        //if (Mage::getSingleton('customer/session')->isLoggedIn()) {
        //    $isAis = $this->_getCustomer()->getIsAisMember();
        //} else {
            $isAis = $this->_getCustomerSession()->getIsAisMember();
        //}

        if(!$isAis) {
            //$_SERVER['HTTP_X_MSISDN'] = '66817724182';
            if (isset($_SERVER['HTTP_X_MSISDN'])) {
                $phone = $_SERVER['HTTP_X_MSISDN'];
                $phone = explode(',', $phone);
                if(count($phone) > 0) {
                    $index = count($phone) - 1;
                    $isAis = true;
                    $phone = trim($phone[$index]);

                    if(strlen($phone) == 11) {
                        $phone = '0'.substr($phone, 2);
                    }
                    $this->_getCustomerSession()
                         ->setIsAisMember(1)
                         ->setPhoneNumber($phone);
                }
            }
        }

        if($productId) {
           if(!$this->_checkInstallment($productId, $flag, $campaignId)){
                echo "checkAisMemberResult('-1');";
                exit;
           }
        }

        echo 'checkAisMemberResult('.((!$isAis)?'0':'1').');';
    }

    public function checkInstallmentAction() {
        $productId = $this->getRequest()->getParam('product_id', false);
        $flag = $this->getRequest()->getParam('installment', 0);
        $campaignId = $this->getRequest()->getParam('campaign', 0);

        if($productId) {
           if(!$this->_checkInstallment($productId,  $flag, $campaignId)){
                echo "checkInstallmentResult('-1');";
                exit;
           }
        }
        echo "checkInstallmentResult('1');";
    }

    protected function _isInstallment($productId) {
        $installments = Mage::getResourceModel('campaigns/installment_collection')
                              ->getInstallmentByProduct($productId);

        return count($installments) > 0;
    }

    protected function _checkInstallment($productId, $flag, $campaignId) {
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $allItems = $quote->getAllItems();

        // = $this->_isInstallment($productId);

        foreach ($allItems as $item) {
            if ($item->getChildren()) {
                continue;
            }

            if($flag == false) {
                $option = $item->getOptionByCode('info_buyRequest');
                if ($option) {
                    $request = unserialize($option->getValue());
                    if (isset($request['trade_info'])) {
                        $trade = unserialize($request['trade_info']);
                        if($trade) {
                            if(isset($trade['installments']) && $trade['installments'] == 1) {
                                return false;
                            }
                        }
                    }
                }
            } else {
                if($item->getProductId() != $productId) {
                    $price = $item->getCustomPrice();
                    if($item->getParentItem()) {
                        $price = $item->getParentItem()->getCustomPrice();
                    }
                    if($price > 0) {
                        return false;
                    }
                } else {
                    $option = $item->getOptionByCode('info_buyRequest');
                    if ($option) {
                        $request = unserialize($option->getValue());
                        if (isset($request['trade_info'])) {
                            $trade = unserialize($request['trade_info']);
                            if($trade) {
                                if(isset($trade['installments']) && ($trade['installments'] != 1 || $trade['campaign_id'] != $campaignId)) {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

}