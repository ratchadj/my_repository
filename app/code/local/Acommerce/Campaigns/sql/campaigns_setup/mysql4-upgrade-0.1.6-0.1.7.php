<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @copyright  Copyright (c) 2010 Acommerce Medien GmbH & Co. KG (http://www.phoenix-medien.de)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$this->startSetup();
$this->_conn->addColumn($this->getTable('sales_flat_order_item'), 'ussd_code', 'VARCHAR(30)');
$this->_conn->addColumn($this->getTable('sales_flat_order_item'), 'access_code', 'VARCHAR(30)');
$this->_conn->addColumn($this->getTable('sales_flat_order_item'), 'transaction_no', 'VARCHAR(30)');
$this->endSetup();
?>