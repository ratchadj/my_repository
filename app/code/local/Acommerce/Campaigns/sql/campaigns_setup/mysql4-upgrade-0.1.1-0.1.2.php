<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute('customer', 'phone_number', array(
    'label'     => Mage::helper('customer')->__('Phone Number'),
    'type'      => 'varchar',
    'input'     => 'text',
    'visible'   => true,
    'required'  => false,
    'position'  => 1100,
    'default'   => 0,   
));

$eavConfig = Mage::getSingleton('eav/config');
$attributecodes = array(
     'phone_number'
);

$usedInForms = array(
    'adminhtml_customer',
);

foreach ($attributecodes as $code) {
    $attribute = $eavConfig->getAttribute('customer', $code);
    $attribute->setData('used_in_forms', $usedInForms);
    $attribute->save();
}

$installer->endSetup();