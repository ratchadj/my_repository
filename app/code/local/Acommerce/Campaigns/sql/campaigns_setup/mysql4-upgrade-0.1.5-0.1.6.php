<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @copyright  Copyright (c) 2010 Acommerce Medien GmbH & Co. KG (http://www.phoenix-medien.de)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

$this->startSetup();
	$conn = $this->getConnection();
	$login = $this->getTable('campaigns/login_log');
	if(!$conn->isTableExists($login)) {
		$login = $conn->newTable($login)
			->addColumn('entity_id',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
				'unsigned'	=> true,
				'nullable'	=> false,
				'identity'	=> true,
				'primary'	=> true,
			),'Log ID')
			->addColumn('customer_id',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
				'nullable'	=> false,
			),'Customer ID')
			->addColumn('store_id',Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
				'nullable'	=> false,
			),'Store ID')
			->addColumn('created_at',Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
				'nullable'  => false,
				'default'   => '0000-00-00 00:00:00',
			),'Log Created At')	
			;
		$conn->createTable($login);
	}
$this->endSetup();
?>