<?php
    /* @var $installer Mage_Customer_Model_Entity_Setup */
    $installer = $this;
    $installer->startSetup();

    $setup = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup('core_setup');

    $setup->addAttribute('catalog_product', 'custom_otp', array(
        'group'                    => 'AIS',
        'type'                     => 'varchar',
        'input'                    => 'text',
        'label'                    => 'Custom OTP',
        'global'                   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible'                  => 1,
        'required'                 => 0,
        'visible_on_front'         => 1,
        'is_html_allowed_on_front' => 0,
        'is_configurable'          => 0,
        'searchable'               => 0,
        'filterable'               => 0,
        'comparable'               => 0,
        'unique'                   => false,
        'user_defined'             => 1,
        'sort_order'               => 999,
    ));
    $installer->endSetup();