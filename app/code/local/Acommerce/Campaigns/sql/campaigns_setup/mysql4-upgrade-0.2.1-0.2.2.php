<?php
    /* @var $installer Mage_Customer_Model_Entity_Setup */
    $installer = $this;
    $installer->startSetup();

    $installer->run("
        CREATE TABLE `ais_privilege_log` (
            `entity_id` INT(11) NOT NULL AUTO_INCREMENT,
            `created_at` DATETIME NOT NULL,
            `customer_email` VARCHAR(50) NULL DEFAULT NULL,
            `customer_firstname` VARCHAR(50) NULL DEFAULT NULL,
            `customer_lastname` VARCHAR(50) NULL DEFAULT NULL,
            `customer_phone` VARCHAR(50) NULL DEFAULT NULL,
            `product_id` INT(11) NOT NULL,
            `product_name` VARCHAR(50) NULL DEFAULT NULL,
            `ussd_code` VARCHAR(50) NOT NULL,
            `ref_order` VARCHAR(50) NOT NULL,
            `access_code` VARCHAR(50) NOT NULL,
            `transactin_id` VARCHAR(50) NOT NULL,
            `is_refunded` INT(1) NULL DEFAULT NULL,
            `refunded_date` DATETIME NULL DEFAULT NULL,
            PRIMARY KEY (`entity_id`),
            INDEX `product_id` (`product_id`)
        )
        COLLATE='utf8_general_ci'
        ENGINE=InnoDB
        ;
    ");
    $installer->endSetup();