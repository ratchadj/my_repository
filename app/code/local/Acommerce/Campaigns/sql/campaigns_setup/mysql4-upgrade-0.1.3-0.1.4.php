<?php
    /* @var $installer Mage_Customer_Model_Entity_Setup */
    $installer = $this;
    $installer->startSetup();

    $eavConfig = Mage::getSingleton('eav/config');

    $installer->addAttribute('customer_address', 'vat_type', array(
        'label'             => 'Type Of Tax Invoice',
        'type'              => 'int',    //backend_type
        'input'             => 'select', //frontend_input
        'frontend_class'    => '',
        'source'            => 'campaigns/vat_type',
        'backend'           => '',
        'frontend'          => '',
        'sort_order'        => 1000,
        'is_required'       => 0,
    ));


    $attribute = $eavConfig->getAttribute('customer_address', 'vat_type');
    //$attribute->addData($data);
    $usedInForms = array(
        'adminhtml_customer_address',
        'customer_address_edit',
        'customer_register_address'
    );
    $attribute->setData('used_in_forms', $usedInForms);
    $attribute->save();

    $installer->run("
        ALTER TABLE {$this->getTable('sales_flat_quote_address')} ADD COLUMN `vat_type` INT(2);
         ALTER TABLE {$this->getTable('sales_flat_order_address')} ADD COLUMN `vat_type` INT(2);
    ");
    $installer->endSetup();