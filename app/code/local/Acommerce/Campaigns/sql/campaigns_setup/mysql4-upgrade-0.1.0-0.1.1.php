<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute('customer', 'is_ais_member', array(
    'label'     => Mage::helper('customer')->__('Is AIS Member'),
    'type'      => 'int',
    'input'     => 'select',
    'visible'   => true,
    'required'  => false,
    'position'  => 1000,
    'default'   => 0,
    'source' => 'eav/entity_attribute_source_boolean',
));

$eavConfig = Mage::getSingleton('eav/config');
$attributecodes = array(
     'is_ais_member'
);

$usedInForms = array(
    'adminhtml_customer',
);

foreach ($attributecodes as $code) {
    $attribute = $eavConfig->getAttribute('customer', $code);
    $attribute->setData('used_in_forms', $usedInForms);
    $attribute->save();
}

$installer->endSetup();