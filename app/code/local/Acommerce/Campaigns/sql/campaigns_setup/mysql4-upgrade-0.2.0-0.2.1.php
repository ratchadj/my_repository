<?php
    /* @var $installer Mage_Customer_Model_Entity_Setup */
    $installer = $this;
    $installer->startSetup();

    $eavConfig = Mage::getSingleton('eav/config');

    $installer->addAttribute('customer_address', 'branch_no', array(
        'label'             => 'Branch No',
        'type'              => 'varchar',    //backend_type
        'input'             => 'text', //frontend_input
        'frontend_class'    => '',
        'frontend'          => '',
        'sort_order'        => 1000,
    ));

    $installer->updateAttribute('customer_address', 'branch_no', 'is_required', 0);
    $attribute = $eavConfig->getAttribute('customer_address', 'branch_no');
    //$attribute->addData($data);
    $usedInForms = array(
        'adminhtml_customer_address',
        'customer_address_edit',
        'customer_register_address'
    );
    $attribute->setData('used_in_forms', $usedInForms);
    $attribute->save();

    $installer->run("
        ALTER TABLE {$this->getTable('sales_flat_quote_address')} ADD COLUMN `branch_no` varchar(20);
         ALTER TABLE {$this->getTable('sales_flat_order_address')} ADD COLUMN `branch_no` varchar(20);
    ");
    $installer->endSetup();