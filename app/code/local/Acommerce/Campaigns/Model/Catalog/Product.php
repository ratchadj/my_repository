<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Catalog product model
 *
 * @method Mage_Catalog_Model_Resource_Product getResource()
 * @method Mage_Catalog_Model_Product setHasError(bool $value)
 * @method null|bool getHasError()
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Acommerce_Campaigns_Model_Catalog_Product extends Mage_Catalog_Model_Product
{
    protected $_memberPrice;
     protected $_freeGoods;
    protected $_campaigns;
    protected $_optionsPrice;
    protected $_optionsPrices;
    protected $_memberPriceDiscount;

    public function getMemberPrice($campaignId = null) {

        $productIds = array($this->getId());

        if(is_null($this->_memberPrice)) {
            $finalPrice = $this->getFinalPrice();
            $memberPrices = array();

            if($this->getTypeId() == "configurable") {
                $_configurable = $this->getTypeInstance()->getUsedProductIds();
                $productIds = array_merge($productIds, $_configurable);
            }

            $collection = Mage::getResourceModel('campaigns/discount_collection')
                          ->getDiscountPrices($productIds, 1);

            if(!is_null($campaignId)) {
                $collection->addFieldToFilter('main_table.campaign_id', array('eq' => $campaignId));
            }

            if(count($collection) > 0) {
                foreach($collection as $item) {
                    //echo $item->getId().$item->getDiscountType();
                    $finalPrice = (float)Mage::getModel('catalog/product')->load($item->getProductId())->getFinalPrice();
                    if (!$finalPrice) continue;
                    $key = $item->getCampaignId().'|'.$item->getProductId();
                    if($item->getSalesPrice() > 0)
                    {
                        $memberPrices[$key] = (float)$item->getSalesPrice();
                    }

                    if($item->getDiscountType() == 'Percent') {
                        if(isset($memberPrices[$key])) {
                            $temp = $memberPrices[$key];
                            $memberPrices[$key] = ($temp - (($temp * $item->getDiscountValue()) / 100));
                        } else {
                            $memberPrices[$key] = ($finalPrice - (($finalPrice * $item->getDiscountValue()) / 100));
                            //echo '('.$finalPrice .'-'. '(('.$finalPrice.' * '.$item->getDiscountValue().')/100))<br/>';
                        }
                    } else {
                        if(isset($memberPrices[$key])) {
                            $temp = $memberPrices[$key];
                            $memberPrices[$key] = $temp - $item->getDiscountValue();
                        } else {
                            $memberPrices[$key] = $finalPrice - $item->getDiscountValue();
                        }
                    }
                }
                $this->_memberPrice = min($memberPrices);
            } else {
                $this->_memberPrice = false;
            }
        }
        return $this->_memberPrice;

    }

    public function getMemberPriceDiscount() {

        $productIds = array($this->getId());

        if(is_null($this->_memberPriceDiscount)) {

            $finalPrice = null;
            $memberPrice = null;
            $discount = 0;

            if($this->getTypeId() == "configurable") {
                $productIds = $this->getTypeInstance()->getUsedProductIds();
            }

            foreach ($productIds as $_productId) {
                $_product = Mage::getModel('catalog/product')->load($_productId);
                if (!$_product) continue;
                $_finalPrice = $_product->setPrivProcess(true)->getFinalPrice();
                $_memberPrice = $_product->setPrivProcess(true)->getMemberPrice();
                if (!$_memberPrice) continue;
                $_discount = floor(($_finalPrice - $_memberPrice) / $_finalPrice * 100);
                if ($_memberPrice && (is_null($memberPrice) || ($_memberPrice < $_finalPrice && $_memberPrice < $memberPrice))) {
                    $discount = $_discount;
                    $finalPrice = $_finalPrice;
                    $memberPrice = $_memberPrice;
                }
            }

            if ($discount > 0) {
                if ($discount < 15) {
                    $discount = '<span>'.Mage::helper('catalog')->__('Discount').'</span><span class="percentsave">'.($finalPrice - $memberPrice).'-</span>';
                } else {
                    $discount = '<span class="percentsave">'.$discount.'%</span><span>Off</span>';
                }
            }

            $this->_memberPriceDiscount = $discount;

        }
        return $this->_memberPriceDiscount;

    }

    public function getFreeGoods($campaignId = null)
    {
        $productIds = array($this->getId());
        if(is_null($this->_freeGoods)) {

            if($this->getTypeId() == "configurable") {
                $_configurable = $this->getTypeInstance()->getUsedProductIds();
                $productIds = array_merge($productIds, $_configurable);
            }

            $collection = Mage::getResourceModel('campaigns/freegoods_collection')
                             ->getFreeGoodItems($productIds);

            if(!is_null($campaignId)) {
                $collection->addFieldToFilter('main_table.campaign_id', array('eq' => $campaignId));
            }
            //echo $collection->getSelect()->__toString();
            $freeGoods = array();
            if(count($collection) > 0) {
                foreach($collection as $item) {
                    $freeGoods = unserialize($item->getFreeGoods());
                }
            }
            $this->_freeGoods = $freeGoods;
        }
        return $this->_freeGoods;
    }

    public function getActiveCampaigns()
    {
        $productIds = array($this->getId());

        if(is_null($this->_campaigns)) {
            $campaigns = array();
            if($this->getTypeId() == "configurable") {
                $_configurable = $this->getTypeInstance()->getUsedProductIds();
                $productIds = array_merge($productIds, $_configurable);
            }

            $discounts = Mage::getResourceModel('campaigns/discount_collection')
                              ->getDiscountPrices($productIds);

            if(count($discounts) > 0) {
                foreach($discounts as $item) {
                    if(!in_array($item->getCampaignId(), $campaigns)) {
                        $campaigns[] = $item->getCampaignId();
                    }
                }
            }

            $freegoods = Mage::getResourceModel('campaigns/freegoods_collection')
                              ->getFreeGoodItems($productIds);

            if(count($freegoods) > 0) {
                foreach($freegoods as $item) {
                    if(!in_array($item->getCampaignId(), $campaigns)) {
                        $campaigns[] = $item->getCampaignId();
                    }
                }
            }

            $this->_campaigns = $campaigns;
        }
        return $this->_campaigns;
    }

    public function getProductReviews() {
        /**
        * Getting reviews collection object
        */
        $productId = $this->getId();
        $reviews = Mage::getModel('review/review')
                        ->getResourceCollection()
                        ->addStoreFilter(Mage::app()->getStore()->getId())
                        ->addEntityFilter('product', $productId)
                        ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
                        ->setDateOrder()
                        ->addRateVotes();
        /**
        * Getting average of ratings/reviews
        */
        $avg = 0;
        $ratings = array();
        if (count($reviews) > 0) {
            foreach ($reviews->getItems() as $review) {
                foreach( $review->getRatingVotes() as $vote ) {
                    $ratings[] = $vote->getPercent();
                }
            }
            if(count($ratings) > 0) {
                $avg = array_sum($ratings)/count($ratings);
            }
        }
        return array('avg' => (int)$avg, 'count' => count($ratings));
    }

    public function getOptionsPrice() {
        $finalPrice = (float)$this->getFinalPrice();
        if ($this->getTypeId() == "configurable" && is_null($this->_optionsPrice)) {
            $productIds = array($this->getId());
            $_configurable = $this->getTypeInstance()->getUsedProductIds();
            $productIds = array_merge($productIds, $_configurable);
            foreach ($productIds as $_productId) {
                if ($_productId == $this->getId()) continue;
                $_product = Mage::getModel('catalog/product')->load($_productId);
                if (!$_product) continue;
                $_finalPrice = (float)$_product->getPriceModel()->getFinalPrice(null, $_product);
                if ($_finalPrice < $finalPrice) {
                    $finalPrice = $_finalPrice;
                }
            }
            $this->_optionsPrice = $finalPrice;
        }

        return $this->_optionsPrice;
    }

    public function getOptionsPrices() {
        $finalPrice = (float)$this->getFinalPrice();
        if ($this->getTypeId() == "configurable" && is_null($this->_optionsPrices)) {
            $productIds = $this->getTypeInstance()->getUsedProductIds();
            $_priceArray = array();
            foreach ($productIds as $_productId) {
                $_product = Mage::getModel('catalog/product')->load($_productId);
                if (!$_product) continue;
                $_finalPrice = (float)$_product->getPriceModel()->getFinalPrice(null, $_product);
                $_priceArray[$_productId] = $_finalPrice;
            }
            if (!empty($_priceArray)) $this->_optionsPrices = $_priceArray;
        }

        return $this->_optionsPrices;
    }

}