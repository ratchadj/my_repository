<?php

/**
 * Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Acommerce EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.acommerce.com/LICENSE-1.0.html
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@acommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.acommerce.com/ for more information
 * or send an email to sales@acommerce.com
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @copyright  Copyright (c) 2010 Acommerce (http://www.acommerce.com/)
 * @license    http://www.acommerce.com/LICENSE-1.0.html
 */

/**
 * Campaigns extension
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @author     Acommerce Dev Team <dev@acommerce.com>
 */
class Acommerce_Campaigns_Model_Loginlog extends Mage_Core_Model_Abstract
{

    public function _construct() {
        parent::_construct();
        $this->_init('campaigns/loginlog');
    }
}

