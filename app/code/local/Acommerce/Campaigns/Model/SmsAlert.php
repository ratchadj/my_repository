<?php

/**
 * Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Acommerce EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.acommerce.com/LICENSE-1.0.html
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@acommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.acommerce.com/ for more information
 * or send an email to sales@acommerce.com
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @copyright  Copyright (c) 2010 Acommerce (http://www.acommerce.com/)
 * @license    http://www.acommerce.com/LICENSE-1.0.html
 */

/**
 * Campaigns extension
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @author     Acommerce Dev Team <dev@acommerce.com>
 */
class Acommerce_Campaigns_Model_SmsAlert extends Varien_Object
{
	
	protected $_date = null;
	protected $_lastAction = null;
	protected $_lastObject = null;
	protected $_templates = array();
    protected function _getConfig($path) {
		return Mage::getStoreConfig('ais/alert/'.$path);
	}
	public function logLogin($observer) {
		if(!$this->_getConfig('enable')) {
			return $this;
		}
		$customer = $observer->getEvent()->getCustomer();
		$log = Mage::getModel('campaigns/loginlog')->load(1);
		if(!$log->getId()) {
			$log->setId(null);
		}
		$log->setCustomerId($customer->getId())
			->setStoreId(Mage::app()->getStore()->getId())
			->setCreatedAt(Varien_Date::now());
		$log->save();
		return $this;
	}
	public function cronJobs() {
		if(!$this->_getConfig('use_cron')) {
			return $this;
		}
		$this->checkAndAlert();
	}
	public function checkAndAlert() {
		if(!$this->_getConfig('enable')) {
			return $this;
		}
		$phones = $this->getPhones();
		if(empty($phones)) {
			return $this;
		}
		$actions = array('no_neworders'=>'sales/order_grid','no_invoice'=>'sales/order_invoice','no_regist'=>'customer/customer','no_login'=>'campaigns/loginlog');
		if(empty($this->_templates)) {
			foreach($actions as $type=>$model) {
				$this->_checkActionByType($type,$model);
			}
		}
		$data = array();
		// var_dump($this->_templates);
		foreach($this->_templates as $type => $template) {
			foreach($phones as $phone) {
				$data[] = array("phone" => '66'.ltrim($phone, '0'),"message" => $template);
			}
		}
		Zend_Debug::dump($data);
		foreach($data as $sms) {
			$this->_sendAlert($sms);
		}
		return $this;
	}
	protected function getPhones() {
		return explode(',',$this->_getConfig('phones'));
	}
	protected function _getDateModel() {
		if(is_null($this->_date)) {
			$this->_date = Mage::getModel('core/date');
		}
		return $this->_date;
	}
	protected function _validateAlert($type) {
		$period = $this->_getConfig($type);		
		if(is_numeric($period) && $period) {
			$period *= 60;
			$last = ($this->_lastAction)?Varien_Date::toTimestamp($this->_lastAction):0;
			$now = Varien_Date::toTimestamp(true);
			return ($period+$last) <= $now;
		}
		return false;
	}
	protected function _sendAlert($data) {
		if(empty($data)) {
			return;
		}
		$url = Mage::getStoreConfig('ais/config/otp');
		if(!$url) {
			return;
		}
		$data_string = json_encode($data);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$content = curl_exec($ch);
		return $content;
	}
	protected function _addActionTemplate($type) {		
		$valid = $this->_validateAlert($type);
		$template = $this->_getConfig($type.'_template');
		if($valid && $template) {
			$template = Mage::getModel('core/email_template_filter')
						->setVariables(array('object'=>$this->_lastObject))
						->filter($template);
			$this->_templates[$type] = $template;
		}
		return $this;
	}
	protected function _validateAction($type) {
		if(!$this->getData($type)) {
			$this->setData(array($type=>true));
			return true;
		}
		return false;
	}
	protected function _getLastAction($model) {
		echo $model.'<br/>';
		$this->_lastObject = Mage::getResourceModel($model.'_collection')
					->setOrder('entity_id')
					->setCurPage(1)
					->setPageSize(1)
					->getFirstItem();
					
		$this->_lastAction = $this->_lastObject->getCreatedAt();		
		if($model=='campaigns/loginlog') {
			$this->_lastObject = Mage::getModel('customer/customer')->load($this->_lastObject->getCustomerId());
		}
		return $this->_lastAction;
	}
	protected function _checkActionByType($type,$model) {		
		if($this->_validateAction($type)) {	
			echo $type.'--->';
			$this->_getLastAction($model);
			$this->_addActionTemplate($type);
		}
		return $this;
	}
}

