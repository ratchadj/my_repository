<?php

/**
 * Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Acommerce EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.acommerce.com/LICENSE-1.0.html
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@acommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.acommerce.com/ for more information
 * or send an email to sales@acommerce.com
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @copyright  Copyright (c) 2010 Acommerce (http://www.acommerce.com/)
 * @license    http://www.acommerce.com/LICENSE-1.0.html
 */

/**
 * Campaigns extension
 *
 * @category   Acommerce
 * @package    Acommerce_Campaigns
 * @author     Acommerce Dev Team <dev@acommerce.com>
 */
class Acommerce_Campaigns_Model_Mysql4_Discount_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('campaigns/discount');
    }

    public function getDiscountPrices($productIds, $isMember = null) {

        $currentDate = Mage::app()->getLocale()->storeDate(null, null, true);
        $currentDate = $currentDate->toString('Y-m-d H:i:s', 'php');

        if(!is_array($productIds)) {
            $productIds = array($productIds);
        }

        $this->addFieldToFilter('main_table.product_id', array('in' => $productIds))
             ->addFieldToFilter('main_table.is_active', array('eq' => 1))
             //->addFieldToFilter('main_table.campaign_start', array('lteq' => $currentDate))
             ->addFieldToFilter(
                                array('main_table.campaign_start', 'main_table.campaign_start'),
                                    array(
                                        array('lteq'=>$currentDate),
                                        array('null'=>1)
                                    )
                                )
             //->addFieldToFilter('main_table.campaign_end', array('gteq' => $currentDate))
             ->addFieldToFilter(
                                array('main_table.campaign_end', 'main_table.campaign_end'),
                                    array(
                                        array('gteq'=>$currentDate),
                                        array('null'=>1)
                                    )
                                );

        //if($isMember) {
        $this->getSelect()
             ->joinInner(
                    array('campaign'  => 'ais_campaign'),
                    'main_table.campaign_id = campaign.campaign_id and campaign.is_active = 1',
                    array('is_ais' => 'campaign.for_ais_member')
                );

        if(!is_null($isMember)) {
            $this->addFieldToFilter('campaign.for_ais_member', array('eq' => $isMember));
        }
        //}

        return $this;
    }

    public function getDiscountPricesByCampaign($campaignId, $isMember = 1) {

        $currentDate = Mage::app()->getLocale()->storeDate(null, null, true);
        $currentDate = $currentDate->toString('Y-m-d H:i:s', 'php');

        $this->addFieldToFilter('campaign_id', array('in' => $campaignId))
             ->addFieldToFilter('is_active', array('eq' => 1))
             //->addFieldToFilter(array('campaign_start', array('lteq' => $currentDate)))
             ->addFieldToFilter(
                                array('campaign_start', 'main_table.campaign_start'),
                                    array(
                                        array('lteq'=>$currentDate),
                                        array('null'=>1)
                                    )
                                )
             //->addFieldToFilter('campaign_end', array('gteq' => $currentDate))
             ->addFieldToFilter(
                                array('campaign_end', 'main_table.campaign_end'),
                                    array(
                                        array('gteq'=>$currentDate),
                                        array('null'=>1)
                                    )
                                );

        return $this;
    }
}