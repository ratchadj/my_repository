<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2014 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */


/**
 * Configurable product type resource model
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Acommerce_Campaigns_Model_Mysql4_Product_Type_Configurable extends Mage_Catalog_Model_Resource_Product_Type_Configurable
{

    /**
     * Collect product options with values according to the product instance and attributes, that were received
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $attributes
     * @return array
     */
    public function getConfigurableOptions($product, $attributes)
    {
        $attributesOptionsData = array();
        foreach ($attributes as $superAttribute) {
            $select = $this->_getReadAdapter()->select()
                ->from(
                    array(
                        'super_attribute'       => $this->getTable('catalog/product_super_attribute')
                    ),
                    array(
                        'entity_id'             => 'entity.entity_id',
                        'sku'                   => 'entity.sku',
                        'product_id'            => 'super_attribute.product_id',
                        'attribute_id'          => 'attribute.attribute_id',
                        'attribute_code'        => 'attribute.attribute_code',
                        'option_id'             => 'option_value.option_id',
                        'option_title'          => 'option_value.value',
                        'pricing_value'         => 'attribute_pricing.pricing_value',
                        'pricing_is_percent'    => 'attribute_pricing.is_percent'
                    )
                )->joinInner(
                    array(
                        'product_link'          => $this->getTable('catalog/product_super_link')
                    ),
                    'product_link.parent_id = super_attribute.product_id',
                    array()
                )->joinInner(
                    array(
                        'attribute'             => $this->getTable('eav/attribute')
                    ),
                    'attribute.attribute_id = super_attribute.attribute_id',
                    array()
                )->joinInner(
                    array(
                        'entity'                => $this->getTable('catalog/product')
                    ),
                    'entity.entity_id = product_link.product_id',
                    array()
                )->joinInner(
                    array(
                        'entity_value'          => $superAttribute->getBackendTable()
                    ),
                    implode(
                        ' AND ',
                        array(
                            $this->_getReadAdapter()
                                ->quoteInto('entity_value.entity_type_id = ?', $product->getEntityTypeId()),
                            'entity_value.attribute_id = super_attribute.attribute_id',
                            'entity_value.store_id = 0',
                            'entity_value.entity_id = product_link.product_id'
                        )
                    ),
                    array()
                )->joinLeft(
                    array(
                        'option_value'          => $this->getTable('eav/attribute_option_value')
                    ),
                    implode(' AND ', array(
                        'option_value.option_id = entity_value.value',
                        'option_value.store_id = ' . Mage_Core_Model_App::ADMIN_STORE_ID,
                    )),
                    array()
                )->joinLeft(
                    array(
                        'attribute_pricing'     => $this->getTable('catalog/product_super_attribute_pricing')
                    ),
                    implode(' AND ', array(
                        'super_attribute.product_super_attribute_id = attribute_pricing.product_super_attribute_id',
                        'entity_value.value = attribute_pricing.value_index'
                    )),
                    array()
                )->where('super_attribute.product_id = ?', $product->getId())
                 ->where('attribute.attribute_id = ?', $superAttribute->getAttributeId());

            //echo $select->__toString();
            $attributesOptionsData[$superAttribute->getAttributeId()] = $this->_getReadAdapter()->fetchAll($select);
        }
        return $attributesOptionsData;
    }
}
