<?php

class Acommerce_Campaigns_Model_Vat_Type extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{

    /**
     * Get all options in array
     *
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = array(
                array(
                    'value' => '1',
                    'label' => Mage::helper('campaigns')->__('A Thai national'),
                ),
                array(
                    'value' => '2',
                    'label' => Mage::helper('campaigns')->__('A juristic person'),
                ),
                array(
                    'value' => '3',
                    'label' => Mage::helper('campaigns')->__('A non-Thai national'),
                )
            );
        }
        return $this->_options;
    }

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }
}

