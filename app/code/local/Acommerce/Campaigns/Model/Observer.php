<?php

class Acommerce_Campaigns_Model_Observer
{

    protected function _addFreeProductToQuote($quote, $productId, $qty)
    {
        try {

        $parentProductId = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($productId);
        if (is_array($parentProductId) && count($parentProductId) > 0) {
            $parentProductId = $parentProductId[0];
            $product = Mage::getModel('catalog/product')->load($parentProductId);
            $useProductAttributes = Mage::getModel('catalog/product_type_configurable')->getUsedProductAttributes($product);
            $attributeOptoins = Mage::getResourceModel('campaigns/product_type_configurable')->getConfigurableOptions($product,
                                                                                                                      $useProductAttributes);

            $superAttributes = array();
            foreach ($attributeOptoins as $key => $attributes) {
                foreach ($attributes as $attribute) {
                    if ($attribute['entity_id'] == $productId) {
                        $superAttributes[$key] = $attribute['option_id'];
                    }
                }
            }
            $request = new Varien_Object(array('qty' => $qty, 'super_attribute' => $superAttributes, 'custom_price' => 0,
                'original_custom_price' => 0));
        } else {
            $product = Mage::getModel('catalog/product')->load($productId);
            $request = new Varien_Object(array('qty' => $qty));
        }


        //$product->addCustomOption('is_free', 1);

        //$item = $quote->getItemByProduct($product);
        //if ($item) {
        //    return false;
        //}

        $allItems = $quote->getAllItems();
        foreach ($allItems as $item) {
            if ($item->getProductId() == $product->getId()) {
                return false;
            }
        }


        // we need this line in case the initial quote was virtual
        if (!$product->isVirtual()) {
            $quote->getBillingAddress()->setTotalAmount('subtotal', 0);
        }

        try {
            $item = $quote->addProduct($product, $request);
        } catch (Exception $e){
            $item = $quote->getItemByProduct($product);
            if ($item) {
                $item->setCustomPrice(0);
                $item->setOriginalCustomPrice(0);
                if(strpos($item->getName(), 'Free - ') === false) {
                    $item->setName('Free - ' . $item->getName());
                }
                $item->addOption(array('product_id' => $item->getProductId(), 'code' => 'is_free', 'value' => 1));

                if ($parentItem = $item->getParentItem()) {
                    $parentItem->setCustomPrice(0);
                    $parentItem->setOriginalCustomPrice(0);
                    if(strpos($parentItem->getName(), 'Free - ') === false) {
                        $item->setName('Free - ' . $parentItem->getName());
                    }
                    $parentItem->addOption(array('product_id' => $parentItem->getProductId(), 'code' => 'is_free', 'value' => 1));
                }
            }
        }

        // required custom options or configurable product
        if (!is_object($item)) {
            throw new Exception($item);
        }

        $item->setCustomPrice(0);
        $item->setOriginalCustomPrice(0);
        $item->setName('Free - ' . $item->getName());
        $item->addOption(array('product_id' => $item->getProductId(), 'code' => 'is_free', 'value' => 1));

        if ($parentItem = $item->getParentItem()) {
            $parentItem->setCustomPrice(0);
            $parentItem->setOriginalCustomPrice(0);
            $parentItem->setName('Free - ' . $parentItem->getName());
            $parentItem->addOption(array('product_id' => $parentItem->getProductId(), 'code' => 'is_free', 'value' => 1));
        }


        }
        catch (Exception $e){
            //$this->_showMessage(Mage::helper('checkout')->__(
            //    'We apologise, but there is an error while adding free items to the cart: %s', $e->getMessage()
            //));
            //return false;
        }
        return $this;
    }

    protected function _showMessage($message, $isError = true, $showEachTime = false)
    {
        // show on cart page only
        $all = Mage::getSingleton('checkout/session')->getMessages(false)->toString();
        if (false !== strpos($all, $message)) return;

        if ($isError && isset($_GET['debug'])) {
            Mage::getSingleton('checkout/session')->addError($message);
        } else {
            $arr = Mage::getSingleton('checkout/session')->getAmpromoMessages();
            if (!is_array($arr)) {
                $arr = array();
            }
            if (!in_array($message, $arr) || $showEachTime) {
                Mage::getSingleton('checkout/session')->addNotice($message);
                $arr[] = $message;
                Mage::getSingleton('checkout/session')->setAmpromoMessages($arr);
            }
        }
    }

    protected function _getCustomPrice($campaignId, $productId, $finalPrice)
    {

        $discounts = Mage::getResourceModel('campaigns/discount_collection')
                ->getDiscountPrices($productId)
                ->addFieldToFilter('main_table.campaign_id', array('eq' => $campaignId));

        $customPrice = 0;
        $check = false;

        if (count($discounts) > 0) {
            foreach ($discounts as $item) {
                if ($item->getSalesPrice() > 0) {
                    $customPrice = (float) $item->getSalesPrice();
                    $check = true;
                }

                if ($item->getDiscountType() == 'Percent') {
                    if ($check) {
                        $temp = $customPrice;
                        $customPrice = ($temp - (($temp * $item->getDiscountValue()) / 100));
                    } else {
                        $customPrice = ($finalPrice - (($finalPrice * $item->getDiscountValue()) / 100));
                    }
                } else {
                    if ($check) {
                        $temp = $customPrice;
                        $customPrice = $temp - $item->getDiscountValue();
                    } else {
                        $customPrice = $finalPrice - $item->getDiscountValue();
                    }
                }
            }
        } else {
            $customPrice = $finalPrice;
        }
        return $customPrice;
    }

    protected function _getFreeGoods($item, $campaignId)
    {
        $productId = false;
        if ($item->getProductType() == 'simple') {
            $productId = $item->getProductId();
        } elseif ($item->getProductType() == 'configurable') {
            foreach ($item->getChildren() as $child) {
                $productId = $child->getProductId();
            }
        }

        $collection = Mage::getResourceModel('campaigns/freegoods_collection')
                         ->getFreeGoodItems($productId)
                         ->addFieldToFilter('main_table.campaign_id', array('eq' => $campaignId));
        $freeGoods = array();
        if(count($collection) > 0) {
            foreach($collection as $item) {
                $freeGoods = unserialize($item->getFreeGoods());
            }
        }
        return $freeGoods;
    }

    protected function _getCampaigns($productId, $campaignId = null)
    {
        $finalPrice = Mage::getModel('catalog/product')->load($productId)->getFinalPrice();

        $discounts = Mage::getResourceModel('campaigns/discount_collection')
                        ->getDiscountPrices($productId);

        if(!is_null($campaignId)) {
            $discounts->addFieldToFilter('main_table.campaign_id', array('eq' => $campaignId));
        }

        $discountPrices = array();
        $freeGoods = array();
        if (count($discounts) > 0) {
            foreach ($discounts as $item) {
                $key = $item->getCampaignId() . '|' . $item->getIsAis();
                if ($item->getSalesPrice() > 0) {
                    $discountPrices[$key] = (float) $item->getSalesPrice();
                }

                if ($item->getDiscountType() == 'Percent') {
                    if (isset($discountPrices[$key])) {
                        $temp = $discountPrices[$key];
                        $discountPrices[$key] = ($temp - (($temp * $item->getDiscountValue()) / 100));
                    } else {
                        $discountPrices[$key] = ($finalPrice - (($finalPrice * $item->getDiscountValue()) / 100));
                    }
                } else {
                    if (isset($discountPrices[$key])) {
                        $temp = $discountPrices[$key];
                        $discountPrices[$key] = $temp - $item->getDiscountValue();
                    } else {
                        $discountPrices[$key] = $finalPrice - $item->getDiscountValue();
                    }
                }
            }
        }

        $p = new stdClass();
        $p->productId = $productId;
        $p->discount = $discountPrices;

        $freegoods = Mage::getResourceModel('campaigns/freegoods_collection')
                ->getFreeGoodItems($productId);

        if(!is_null($campaignId)) {
            $freegoods->addFieldToFilter('main_table.campaign_id', array('eq' => $campaignId));
        }

        if (count($freegoods) > 0) {
            foreach ($freegoods as $item) {
                $key = $item->getCampaignId() . '|' . $item->getIsAis();
                //if(!in_array($item->getCampaignId(), $campaigns)) {
                $freeGoods[$key] = unserialize($item->getFreeGoods());
                //}
            }
        }
        $p->freeGoods = $freeGoods;


        $privileges = Mage::getResourceModel('campaigns/privilege_collection')
                            ->getPrivilegeByProduct($productId);

        if(!is_null($campaignId)) {
            $privileges->addFieldToFilter('main_table.campaign_id', array('eq' => $campaignId));
        }

        $privilegeItems = array();
        if (count($privileges) > 0) {
            foreach ($privileges as $item) {
                $key = $item->getCampaignId() . '|1';
                //if(!in_array($item->getCampaignId(), $campaigns)) {
                $privilegeItems[$key] = $item;
                //}
            }
        }
        $p->privileges = $privilegeItems;

        $installments = array();
        $installmentData = Mage::getResourceModel('campaigns/installment_collection')
                              ->getInstallmentByProduct($productId);

        if(!is_null($campaignId)) {
            $installmentData->addFieldToFilter('main_table.campaign_id', array('eq' => $campaignId));
        }

        if(count($installmentData) > 0) {
            $banks = unserialize(Mage::getStoreConfig('payment/cc2p/bank'));
            $periods = unserialize(Mage::getStoreConfig('payment/cc2p/period'));

            $curDate = Mage::app()->getLocale()->storeDate(null, null, true);
            $curDate = strtotime($curDate->toString('Y-m-d H:i:s', 'php'));

            $temp = array();
            foreach ($banks as $value) {
                $key = $value['code'];
                if(isset($value['active_flag']) && $value['active_flag'] == 'Y') {
                    $temp[$key] = $value;
                }
            }
            $banks = $temp;

            $temp = array();
            foreach ($periods as $value) {
                $key = $value['bank'].$value['code'];
                $temp[$key] = $value;
            }
            $periods = $temp;

            foreach($installmentData as $item) {

                $conditions = unserialize($item->getConditions());
                $chk = false;
                foreach ($conditions as $key => $condition) {
                    foreach ($condition as $key1 => $value) {
                       if(isset($banks[$key])) {
                            $bank = $banks[$key];
                            if (isset($periods[$bank['bank'].$key1])) {
                                $validForm = $value[0];
                                $validTo = $value[1];

                                if(!empty($validForm) && !empty($validTo)) {
                                    $validForm = strtotime($validForm);
                                    $validTo = strtotime($validTo);

                                    if($validForm <= $curDate && $validTo >= $curDate) {
                                        $chk = true;
                                        break;
                                    }
                                } elseif(!empty($validForm) && empty($validTo)) {
                                    $validForm = strtotime($validForm);
                                    if($validForm <= $curDate) {
                                        $chk = true;
                                        break;
                                    }
                                } elseif(empty($validForm) && !empty($validTo)) {
                                    $validTo = strtotime($validTo);
                                    if($validTo >= $curDate) {
                                        $chk = true;
                                        break;
                                    }
                                } else {
                                    $chk = true;
                                    break;
                                }

                            }
                        }
                    }
                    if($chk == true) {
                        break;
                    }
                }

                if($chk == true) {
                    $key = $item->getCampaignId().'|'.$item->getIsAis();
                    $installments[$key] = array('card_type' => unserialize($item->getCardType()), 'condition' => unserialize($item->getConditions()));
                }
            }
        }

        $p->installments = $installments;


        $campaigns = array();
        if (count($p->discount) > 0) {
            foreach ($p->discount as $key => $discount) {
                $freegoods = isset($p->freeGoods[$key]) ? $p->freeGoods[$key] : null;
                $installments = isset($p->installments[$key]) ? $p->installments[$key] : null;
                $campaigns[] = $this->getCampaignOptions($key, $discount, $freegoods, $p->privileges, $installments);
            }
        }

        if (count($p->freeGoods) > 0) {
            foreach ($p->freeGoods as $key => $freeGoods) {
                if (!isset($p->discount[$key])) {
                    $installments = isset($p->installments[$key]) ? $p->installments[$key] : null;
                    $campaigns[] = $this->getCampaignOptions($key, null, $p->freeGoods[$key], $p->privileges, $installments);
                }
            }
        }

        if (count($p->installments) > 0) {
            foreach ($p->installments as $key => $installments) {
                if (!isset($p->discount[$key]) && !isset($p->freeGoods[$key])) {
                    $campaigns[] = $this->getCampaignOptions($key, null, null, $p->privileges, $installments);
                }
            }
        }
        return $campaigns;
    }

    protected function getCampaignOptions($campaignId, $price = null, $freegoods = null, $privileges = null, $installments = null)
    {
        $temp = explode('|', $campaignId);
        $data = array();
        $data["campaign_id"] = $temp[0];
        $data["custome_price"] = $price;
        $data["free_goods"] = $freegoods;
        $data["is_ais_member"] = $temp[1];
        $data["installments"] = (!is_null($installments))? 1: 0;
        if(is_null($privileges)) {
            $data["ussd"] = null;
            $data["arpu"] = null;
            $data["product_type"] = null;
            $data["is_cashback"] = null;
        } else {
            if(isset($privileges[$campaignId])) {
                $privilege = $privileges[$campaignId];
                $data["ussd"] = $privilege->getUssd();
                $data["arpu"] = $privilege->getArpu();
                $data["project_type"] = $privilege->getProjectType();
                $data["is_cashback"] = ($privilege->getIsCashback()) ? 'Y' : 'N';
            }
        }

        $data["is_ais_member"] = $temp[1];
        return $data;
    }

    protected function _checkTradeOptions($item)
    {
        if ($item->getProductType() != 'configurable') {
            $option = $item->getOptionByCode('info_buyRequest');

            if ($option) {
                $request = unserialize($option->getValue());
                if (!isset($request['trade_info'])) {
                    /*$campaigns = $this->_getCampaigns($item->getProductId());
                    if (count($campaigns) == 1) {
                        $request['trade_info'] = serialize($campaigns[0]);
                        $option->setValue(serialize($request));

                        if ($parentItem = $item->getParentItem()) {
                            $parentOption = $parentItem->getOptionByCode('info_buyRequest');
                            if ($parentOption) {
                                $parentOption->setValue(serialize($request));
                            }
                        }
                    }*/
                } else {
                    $tradeInfo = unserialize($request['trade_info']);
                    if($tradeInfo !== false) {
                        $campaigns = $this->_getCampaigns($item->getProductId(), $tradeInfo['campaign_id']);

                        if (count($campaigns) == 1) {
                            $request['trade_info'] = serialize($campaigns[0]);
                            $option->setValue(serialize($request));
                            if ($parentItem = $item->getParentItem()) {
                                $parentOption = $parentItem->getOptionByCode('info_buyRequest');
                                if ($parentOption) {
                                    $parentOption->setValue(serialize($request));
                                }
                            }
                        } else {
                            //remove trade
                            $tradeInfo = unserialize($request['trade_info']);
                            $freeGoods = $tradeInfo['free_goods'];
                            $this->_reversePrice($item);
                            $this->_removeFreeGoods($freeGoods, $item);

                            unset($request['trade_info']);
                            $option->setValue(serialize($request));
                            if ($parentItem = $item->getParentItem()) {
                                $parentOption = $parentItem->getOptionByCode('info_buyRequest');
                                if ($parentOption) {
                                    $parentOption->setValue(serialize($request));
                                }
                            }
                        }
                    }
                }
            }
        }
        return $this;
    }

    protected function _canUseCampaign($campaignId, $request) {

        $tradeInfo = unserialize($request['trade_info']);

        $campaign = Mage::getModel('campaigns/campaign')->load($campaignId);
        if(isset($tradeInfo['ussd']) && !is_null($tradeInfo['ussd']) && isset($request['is_privilege'])) {
            return (bool)$request['is_privilege'];
        } elseif(isset($tradeInfo['ussd']) && !is_null($tradeInfo['ussd']) && !isset($request['is_privilege'])) {
            return false;
        } elseif($campaign->getForAisMember()) {
            return ($campaign->getForAisMember() && $this->_isAisMember());
        } else {
            return true;
        }
    }

    protected function _checkInstallment($quoteItem) {
        $quote = $quoteItem->getQuote();
        $flag = false;
        if ($quoteItem->getChildren()) {
            foreach ($quoteItem->getChildren() as $child) {
                $productId = $child->getProductId();
                $quoteItemIds[] = $child->getItemId();
                break;
            }
        } else {
            $productId = $quoteItem->getProductId();
            $quoteItemIds[] = $quoteItem->getItemId();
        }

        $option = $quoteItem->getOptionByCode('info_buyRequest');
        if ($option) {
            $request = unserialize($option->getValue());
            if (isset($request['trade_info'])) {
                $trade = unserialize($request['trade_info']);
                if($trade) {
                    $campaignId = $trade['campaign_id'];
                    if(isset($trade['installments'])) {
                        $flag = $trade['installments'];
                    } else {
                        $flag = 0;
                    }
                }
            }
        }

        $allItems = $quote->getAllItems();
        foreach ($allItems as $item) {
            if ($item->getChildren()) {
                continue;
            }

            if($flag == false) {
                $option = $item->getOptionByCode('info_buyRequest');
                if ($option) {
                    $request = unserialize($option->getValue());
                    if (isset($request['trade_info'])) {
                        $trade = unserialize($request['trade_info']);
                        if($trade) {
                            if(isset($trade['installments']) && $trade['installments'] == 1) {
                                $quote->removeItem($quoteItem->getId());
                                $quoteItem->isDeleted(true);
                                return false;
                            }
                        }
                    }
                }
            } else {
                if($item->getProductId() != $productId) {
                    $price = $item->getCustomPrice();
                    if($item->getParentItem()) {
                        $price = $item->getParentItem()->getCustomPrice();
                    }
                    if($price > 0) {
                        $quote->removeItem($quoteItem->getId());
                        $quoteItem->isDeleted(true);
                        return false;
                    }
                } else {
                    $option = $item->getOptionByCode('info_buyRequest');
                    if ($option) {
                        $request = unserialize($option->getValue());
                        if (isset($request['trade_info'])) {
                            $trade = unserialize($request['trade_info']);
                            if($trade) {
                                if(isset($trade['installments']) && ($trade['installments'] != 1 || $trade['campaign_id'] != $campaignId)) {
                                    $quote->removeItem($quoteItem->getId());
                                    $quoteItem->isDeleted(true);
                                    return false;
                                }
                            } else {
                                $quote->removeItem($quoteItem->getId());
                                $quoteItem->isDeleted(true);
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }

    public function checkoutCartProductAddAfter(Varien_Event_Observer $observer)
    {
        $item = $observer->getQuoteItem();
        $quote = $item->getQuote();

        $this->_checkTradeOptions($item);
        $this->_checkInstallment($item);

        if ($parentItem = $item->getParentItem()) {
            $item = $parentItem;
        }

        $option = $item->getOptionByCode('info_buyRequest');

        if ($option) {
            $request = unserialize($option->getValue());
            if (isset($request['trade_info'])) {
                $tradeInfo = unserialize($request['trade_info']);
                if($this->_canUseCampaign($tradeInfo['campaign_id'], $request)) {
                    /*if (!is_null($tradeInfo['custome_price'])) {
                        $customPrice = (float)$tradeInfo['custome_price'];
                        $this->_setCustomPrice($item, $tradeInfo['campaign_id']);
                    } else {
                        $this->_reversePrice($item);
                    }*/

                    $this->_setCustomPrice($item, $tradeInfo['campaign_id']);
                    //if (!is_null($tradeInfo['free_goods'])) {
                    //    $freeGoods = $tradeInfo['free_goods'];
                    $freeGoods = $this->_getFreeGoods($item, $tradeInfo['campaign_id']);
                    if(count($freeGoods) > 0) {
                        foreach ($freeGoods as $key => $qty) {
                            if (is_array($key)) {
                                $key = $key[0];
                            }
                            $freeProduct = Mage::getModel('catalog/product');
                            $freeProductId = $freeProduct->getIdBySku($key);
                            if ($freeProductId) {
                                $this->_addFreeProductToQuote($quote, $freeProductId, ($qty * $item->getQty()));
                            }
                        }
                    }
                }
            } else {
                $this->_reversePrice($item);
            }
        }
    }

    protected function _reversePrice($item)
    {
        if ($item->getProductType() == 'simple') {
            $finalPrice = $item->getProduct()->getFinalPrice();
            $item->setCustomPrice($finalPrice);
            $item->setOriginalCustomPrice($finalPrice);
        } elseif ($item->getProductType() == 'configurable') {
            foreach ($item->getChildren() as $child) {
                $product = Mage::getModel('catalog/product')->load($child->getProductId());
                $finalPrice = $product->getFinalPrice();
                $item->setCustomPrice($finalPrice);
                $item->setOriginalCustomPrice($finalPrice);
            }
        }
    }

    protected function _setCustomPrice($item, $campaignId)
    {
        $onTopDiscount = $this->_getOnTopDiscount($item, $campaignId);

        if ($item->getProductType() == 'simple') {
            $finalPrice = $item->getProduct()->getFinalPrice();
            $customPrice = $this->_getCustomPrice($campaignId, $item->getProductId(),
                                                 $finalPrice);
            $customPrice = $customPrice - $onTopDiscount;
            $item->setCustomPrice($customPrice);
            $item->setOriginalCustomPrice($customPrice);
        } elseif ($item->getProductType() == 'configurable') {
            foreach ($item->getChildren() as $child) {
                $product = Mage::getModel('catalog/product')->load($child->getProductId());
                $finalPrice = $product->getFinalPrice();
                $customPrice = $this->_getCustomPrice($campaignId, $child->getProductId(),
                                                     $finalPrice);

                $customPrice = $customPrice - $onTopDiscount;
                $item->setCustomPrice($customPrice);
                $item->setOriginalCustomPrice($customPrice);
            }
        }

    }

    protected function _getOnTopDiscount($item, $campaignId) {
        $option = $item->getOptionByCode('info_buyRequest');
        $request = unserialize($option->getValue());
        $onTopDiscount = 0;
        if(isset($request['arpu'])) {

            $arpu = $request['arpu'];
            $privileges = Mage::getResourceModel('campaigns/privilege_collection')
                            ->addFieldToFilter('product_sku', $item->getSku())
                            ->addFieldToFilter('campaign_id', $campaignId);

            if(count($privileges) > 0) {
                $curDate = Mage::app()->getLocale()->storeDate(null, null, true);
                $curDate = strtotime($curDate->toString('Y-m-d H:i:s', 'php'));
                foreach ($privileges as $key => $value) {
                    $arpus = unserialize($value->getArpu());
                    if(isset($arpus[$arpu])) {
                        $arpu = $arpus[$arpu];
                        if($arpu[0] = '') {
                            $onTopDiscount = $arpu[0];
                        } else {
                            $eventEnd = strtotime($arpu[1]);
                            if($curDate < $eventEnd) {
                                $onTopDiscount = $arpu[0];
                            } else {
                                $onTopDiscount = 0;
                            }
                        }
                        break;
                    }
                }
            }

        }
        return $onTopDiscount;
    }

    protected function _updateItem($quote) {
        $allItems = $quote->getAllItems();
        $freeItems = array();

        foreach ($allItems as $item) {

            $this->_checkTradeOptions($item);

            if ($item->getParentItem()) {
                continue;
            }

            $option = $item->getOptionByCode('info_buyRequest');
            $ignore = $item->getOptionByCode('ignore_item');
            if ($ignore) {
                $item->removeOption('ignore_item');
            }

            if ($option) {
                $request = unserialize($option->getValue());
                if (isset($request['trade_info'])) {
                    $tradeInfo = unserialize($request['trade_info']);
                    if($this->_canUseCampaign($tradeInfo['campaign_id'], $request)) {
                        /*if (!is_null($tradeInfo['custome_price'])) {
                            $customPrice = (float)$tradeInfo['custome_price'];
                            $this->_setCustomPrice($item, $tradeInfo['campaign_id']);
                        } else {
                            $this->_reversePrice($item);
                        }*/

                        $this->_setCustomPrice($item, $tradeInfo['campaign_id']);
                        //if (!is_null($tradeInfo['free_goods'])) {
                        //    $freeGoods = $tradeInfo['free_goods'];
                        $freeGoods = $this->_getFreeGoods($item, $tradeInfo['campaign_id']);
                        if(count($freeGoods) > 0) {
                            foreach ($freeGoods as $key => $qty) {
                                if (is_array($key)) {
                                    $key = $key[0];
                                }

                                $freeProduct = Mage::getModel('catalog/product');
                                $freeProductId = $freeProduct->getIdBySku($key);
                                if ($freeProductId) {
                                    if (isset($freeItems[$freeProductId])) {
                                        $freeItems[$freeProductId] += ($qty * $item->getQty());
                                    } else {
                                        $freeItems[$freeProductId] = ($qty * $item->getQty());
                                    }
                                }
                            }
                        }
                    } else {
                        $this->_reversePrice($item);
                    }
                }
            }
        }
        if (count($freeItems) > 0) {
            foreach ($freeItems as $key => $qty) {
                $allItems = $quote->getAllItems();
                $existItem = false;
                foreach ($allItems as $item) {
                    if ($item->getProductId() == $key) {
                        $option = $item->getOptionByCode('is_free');
                        if ($option) {
                            $item->setQty($qty);
                            $item->setName('Free - ' . $item->getName());
                            if ($parentItem = $item->getParentItem()) {
                                $parentItem->setQty($qty);
                                $parentItem->setName('Free - ' . $parentItem->getName());
                            }
                            $existItem = true;
                        }
                    }
                }

                if (!$existItem) {
                    $this->_addFreeProductToQuote($quote, $key, $qty);
                }
            }
        }

        $allItems = $quote->getAllItems();
        foreach ($allItems as $item) {
            $option = $item->getOptionByCode('is_free');
            if ($option) {
                $existItem = false;
                if (isset($freeItems[$item->getProductId()])) {
                    $existItem = true;
                }
                foreach ($item->getChildren() as $child) {
                    if (isset($freeItems[$child->getProductId()])) {
                        $existItem = true;
                    }
                }
                if (!$existItem) {
                    $quote->deleteItem($item);
                }
            }
        }
    }

    public function updateFreeItems1($observer)
    {
        $cart = $observer->getEvent()->getCart();
        $quote = $cart->getQuote();
        $this->_updateItem($quote);
        return $this;
    }

    public function removeFreeItems($observer)
    {
        $item = $observer->getEvent()->getQuoteItem();
        $quote = $item->getQuote();

        if ($parentItem = $item->getParentItem()) {
            $item = $parentItem;
        }

        $option = $item->getOptionByCode('info_buyRequest');

        if ($option) {
            $request = unserialize($option->getValue());
            if (isset($request['trade_info'])) {
                $tradeInfo = unserialize($request['trade_info']);

                if($this->_canUseCampaign($tradeInfo['campaign_id'], $request)) {
                    //if (!is_null($tradeInfo['free_goods'])) {
                    //    $freeGoods = $tradeInfo['free_goods'];
                    $freeGoods = $this->_getFreeGoods($item, $tradeInfo['campaign_id']);
                    $this->_removeFreeGoods($freeGoods, $item);
                }
            }
        }
    }

    protected function _removeFreeGoods($freeGoods, $item) {
        if(count($freeGoods) > 0) {
            $freeItems = array();
            foreach ($freeGoods as $key => $qty) {
                if (is_array($key)) {
                    $key = $key[0];
                }
                $freeProduct = Mage::getModel('catalog/product');
                $freeProductId = $freeProduct->getIdBySku($key);
                if ($freeProductId) {
                    if ($freeProductId) {
                        if (isset($freeItems[$freeProductId])) {
                            $freeItems[$freeProductId] += ($qty * $item->getQty());
                        } else {
                            $freeItems[$freeProductId] = ($qty * $item->getQty());
                        }
                    }
                }
            }

            $quote = $item->getQuote();
            foreach ($freeItems as $key => $qty) {
                $allItems = $quote->getAllItems();
                foreach ($allItems as $item) {
                    if ($item->getProductId() == $key) {
                        $option = $item->getOptionByCode('is_free');
                        if ($option) {
                            $removeQty = $item->getQty() - $qty;
                            if ($removeQty < 1) {
                                $quote->deleteItem($item);
                                if ($parentItem = $item->getParentItem()) {
                                    $quote->deleteItem($parentItem);
                                }
                            } else {
                                $item->setQty($removeQty);
                                $item->setName('Free - ' . $item->getName());
                                if ($parentItem = $item->getParentItem()) {
                                    $parentItem->setQty($removeQty);
                                    $parentItem->setName('Free - ' . $parentItem->getName());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function controllerActionPredispatch($observer)
    {
        $controller = $observer->getControllerAction();

        $action = $controller->getRequest()->getActionName();
        $module = $controller->getRequest()->getModuleName();
        $quote = $this->_getOnepage()->getQuote();

        if ($module == 'onepage' && $action == 'index') {
            $this->_updateItem($quote);
        } else if($module == 'checkout' && $action == 'failure') {
            $onepage = Mage::getSingleton('checkout/type_onepage');
            $orderId = $onepage->getCheckout()->getLastOrderId();

            if($orderId) {
                $order = Mage::getModel('sales/order')->load($orderId);
                //if($order->canCancel()) {
                //    $order->cancel()->save();
                //}
            }
        }
    }

    /**
     * Get one page checkout model
     *
     * @return Mage_Checkout_Model_Type_Onepage
     */
    public function _getOnepage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }

    protected function _getCustomer()
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }

    protected function _getCustomerSession()
    {
        return Mage::getSingleton('customer/session');
    }

    protected function _isAisMember()
    {
        $isMember = false;
        //if($this->_getCustomer()->getId()) {
        //    $isMember = $this->_getCustomer()->getIsAisMember();
        //}
        //if(!$isMember)
        //{
        $isMember = $this->_getCustomerSession()->getIsAisMember();
        //}
        return $isMember;
    }

    protected function _checkAisMember($quote)
    {
        $allItems = $quote->getAllItems();
        $freeItems = array();

        foreach ($allItems as $item) {

            if ($item->getParentItem()) {
                continue;
            }

            $option = $item->getOptionByCode('info_buyRequest');
            $ignore = $item->getOptionByCode('ignore_item');

            if ($ignore) {
                continue;
            }

            if ($option) {
                $request = unserialize($option->getValue());
                if (isset($request['trade_info'])) {
                    $tradeInfo = unserialize($request['trade_info']);

                    /*$isAisMember = false;
                    if (isset($tradeInfo['is_ais_member'])) {
                        $isAisMember = $tradeInfo['is_ais_member'];
                    }*/

                    if(!$this->_canUseCampaign($tradeInfo['campaign_id'], $request)) {

                        $this->_reversePrice($item);

                        //if (!is_null($tradeInfo['free_goods'])) {
                        //    $freeGoods = $tradeInfo['free_goods'];
                        $freeGoods = $this->_getFreeGoods($item, $tradeInfo['campaign_id']);
                        if(count($freeGoods) > 0) {

                            foreach ($freeGoods as $key => $qty) {
                                if (is_array($key)) {
                                    $key = $key[0];
                                }

                                $freeProduct = Mage::getModel('catalog/product');
                                $freeProductId = $freeProduct->getIdBySku($key);
                                if ($freeProductId) {
                                    if (isset($freeItems[$freeProductId])) {
                                        $freeItems[$freeProductId] += ($qty * $item->getQty());
                                    } else {
                                        $freeItems[$freeProductId] = ($qty * $item->getQty());
                                    }
                                }
                            }
                        }
                        Mage::getSingleton('customer/session')->addError(Mage::helper('campaigns')->__('Sorry, free goods and special price are a privilege for AIS member only.'));
                        $item->addOption(array('product_id' => $item->getProductId(), 'code' => 'ignore_item', 'value' => 1));
                    }
                }
            }
        }

        if (count($freeItems) > 0) {
            foreach ($freeItems as $key => $qty) {
                foreach ($allItems as $item) {
                    if ($item->getProductId() == $key) {
                        $option = $item->getOptionByCode('is_free');
                        if ($option) {
                            $removeQty = $item->getQty() - $qty;
                            if ($removeQty < 1) {
                                $quote->deleteItem($item);
                                if ($parentItem = $item->getParentItem()) {
                                    $quote->deleteItem($parentItem);
                                }
                            } else {
                                $item->setQty($removeQty);
                                $item->setName('Free - ' . $item->getName());
                                if ($parentItem = $item->getParentItem()) {
                                    $parentItem->setQty($removeQty);
                                    $parentItem->setName('Free - ' . $parentItem->getName());
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function updateTradeToOrderItem(Varien_Event_Observer $observer)
    {
        $orderItem = $observer->getEvent()->getOrderItem();
        $quoteItem = $observer->getEvent()->getItem();

        if ($quoteItem->getParentItemId()) {
            //$quoteItem = $quoteItem->getParentItem();
            return $this;
        }

        $option = $quoteItem->getOptionByCode('info_buyRequest');
        $ignore = $quoteItem->getOptionByCode('ignore_item');

        if ($ignore) {
            return $this;
        }

        if ($option) {
            $request = unserialize($option->getValue());
            $mobilePhone = $this->_getCustomerSession()->getPhoneNumber();
            if (isset($request['trade_info'])) {
                $tradeInfo = unserialize($request['trade_info']);

                if($this->_canUseCampaign($tradeInfo['campaign_id'], $request)) {
                    if (!is_null($tradeInfo['campaign_id'])) {
                        $campaignId = $tradeInfo['campaign_id'];
                        $campaign = Mage::getModel('campaigns/campaign')->load($campaignId);
                        $orderItem->setTradeNo($campaign->getTradeNo());
                    }

                    $freeItems = array();
                    //if (!is_null($tradeInfo['free_goods'])) {
                    //    $freeGoods = $tradeInfo['free_goods'];
                    $freeGoods = $this->_getFreeGoods($quoteItem, $tradeInfo['campaign_id']);
                    if(count($freeGoods) > 0) {

                        foreach ($freeGoods as $key => $qty) {
                            if (is_array($key)) {
                                $key = $key[0];
                            }
                            $key = explode('_', $key);
                            $freeItems[] = $key[count($key) - 1];
                        }
                        $orderItem->setFreeGoods(implode('|', $freeItems));
                    }

                    if(isset($tradeInfo['ussd'])) {
                        $orderItem->setUssdCode($tradeInfo['ussd']);
                        $orderItem->setProjectType($tradeInfo['project_type']);
                        $orderItem->setIsCashback($tradeInfo['is_cashback']);
                    }
                }
            }

            if ($orderItem->getTradeNo()) {
                if ($mobilePhone) {
                    $orderItem->setCustomerMobilePhone($mobilePhone);
                }
            }
        }
    }

    public function salesOrderPlaceBefore($observer)
    {
        $order = $observer->getEvent()->getOrder();
        $items = $order->getAllItems();
        $mobilePhone = $this->_getCustomerSession()->getPhoneNumber();
        $order->setCustomerMobilePhone($mobilePhone);

        $hasError = false;
        $pname = '';
        $phone = '';

        foreach ($items as $item) {
            if($item->getParentItemId()) {
                continue;
            }

            if($item->getUssdCode()) {
                $privilage = $this->_requestPrivilege($item->getUssdCode(), $item->getCustomerMobilePhone(), $item->getSku());
                Mage::log($privilage, null, 'ranai.log');
                if($privilage) {
                    if(empty($privilage->arpuGroup) || is_null($privilage->arpuGroup))
                        $item->setAccessCode('dummy');
                    else {
                        $item->setAccessCode($privilage->arpuGroup);
                    }

                    $item->setTransactionNo($privilage->transactinId);

                    $log = Mage::getModel('campaigns/privilege_log');
                    $log->setCustomerEmail($order->getCustomerEmail())
                        ->setCustomerFirstname($order->getCustomerFirstname())
                        ->setCustomerLastname($order->getCustomerLastname())
                        ->setCustomerPhone($item->getCustomerMobilePhone())
                        ->setProductId($item->getProductId())
                        ->setProductName($item->getName())
                        ->setUssdCode($item->getUssdCode())
                        ->setRefOrder($item->getSku())
                        ->setAccessCode($item->getAccessCode())
                        ->setTransactinId($item->getTransactionNo())
                        ->save();

                } else {
                    $pname = $item->getName();
                    $phone = $item->getCustomerMobilePhone();
                    $hasError = true;
                    break;
                }
            }

            foreach ($item->getChildrenItems() as $child) {
                $child->setAccessCode($item->getAccessCode());
                $child->setTransactionNo($item->getTransactionNo());
                $child->setUssdCode($item->getUssdCode());
                $child->setProjectType($item->getProjectType());
                $child->setIsCashback($item->getIsCashback());
                $child->setCustomerMobilePhone($item->getCustomerMobilePhone());
                $child->setTradeNo($item->getTradeNo());
                $child->setFreeGoods($item->getFreeGoods());
            }
        }

        if($hasError) {
            foreach ($items as $item) {
                if($item->getParentItemId()) {
                    continue;
                }

                if($item->getTransactionNo()) {
                    $result = $this->_refundPrivilege($item->getUssdCode(), $item->getCustomerMobilePhone(), $item->getTransactionNo());
                }
            }
            throw new Exception(Mage::helper('catalog')->__('You cannot use AIS Privilege for %s.', $pname), 2001);
        }

        //throw new Exception("Error Processing Request", 1);
    }

    public function clearAisMember(Varien_Event_Observer $observer = null)
    {
        $this->_getCustomerSession()
               ->unsIsAisMember()
               ->unsPhoneNumber();
    }

    public function dailyApplyMasterPrice() {
        $errorMessage = Mage::helper('catalogrule')->__('Unable to apply rules.');
        try {
            Mage::getModel('catalogrule/rule')->applyAll();
            Mage::getModel('catalogrule/flag')->loadSelf()
                ->setState(0)
                ->save();
            $content = "Update catalog product price rules successfully.";

        } catch (Exception $e) {
            Mage::logException($e);
            $content = "Cannot update catalog product price rules.";
        }

        $mail = new Zend_Mail('utf-8');
        $mail->addTo('nattawut.m@acommerce.asia');
        $mail->addCc('ranai@acommerce.asia');

        $mail->setFrom('support@acommerce.asia');
        $mail->setSubject("AIS Price master");

        $mail->setBodyHtml($content, 'UTF-8');
        $mail->send();

    }

    protected function _getConfigData($config) {
        return Mage::getStoreConfig('ais/config/'.$config);
    }

    protected function _requestPrivilege($ussd, $phone, $orderRef) {
        /*if($ussd == '*543*383*021#') {
            $t = false;
        } else {
            $t = new stdClass();
            $t->transactinId = "00000030911350302801";
            $t->arpuGroup = "DA55675";
            $t->arpuType = "DISCOUNT";
        }
        Mage::log('go here', null, 'privilege.log');
        return $t;
        */
        $checkPrivilegeUrl = $this->_getConfigData('requestprivilege');

        $arr = array("phone" => $phone, "ussd" => $ussd, "orderRef" => $orderRef);

        $data_string = json_encode($arr);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $checkPrivilegeUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);

        if(!curl_errno($ch))
        {
            $obj = json_decode($content);
            if(isset($obj->error)) {
                //return false;
            } elseif(isset($obj->response)) {
                if($obj->response) {
                    return $obj->response;
                }
            }
        }
        return false;
    }

    public function _refundPrivilege($ussd, $phone, $transactinId) {
       $checkPrivilegeUrl = $this->_getConfigData('refundprivilege');

        $arr = array("phone" => $phone, "ussd" => $ussd, "transactionId" => $transactinId);

        $data_string = json_encode($arr);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $checkPrivilegeUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $content = curl_exec($ch);

        if(!curl_errno($ch))
        {
            $obj = json_decode($content);
            if(isset($obj->error)) {
                //return false;
            } elseif(isset($obj->response)) {
                if($obj->response) {
                    return $obj->response;
                }
            }
        }
        return false;
    }

    public function salesOrderCancelAfter($observer) {
        $order = $observer->getEvent()->getOrder();
        $items = $order->getAllItems();

        foreach ($items as $item) {
            if($item->getParentItemId()) {
                continue;
            }

            if($item->getTransactionNo()) {
                $result = $this->_refundPrivilege($item->getUssdCode(), $item->getCustomerMobilePhone(), $item->getTransactionNo());
            }
        }
    }

    public function salesOrderRefund($observer) {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order = $creditmemo->getOrder();

        $items = $order->getAllItems();

        foreach ($items as $item) {
            if($item->getParentItemId()) {
                continue;
            }

            if($item->getTransactionNo()) {
                $result = $this->_refundPrivilege($item->getUssdCode(), $item->getCustomerMobilePhone(), $item->getTransactionNo());
            }
        }
    }

}
