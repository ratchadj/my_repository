<?php
class Acommerce_PageCache_Block_Page_Html_Header extends Mage_Page_Block_Html_Header
{
    /**
     * Cache for header block
     *
     * @return bool
     */
    protected function _loadCache()
    {
        return false;
    }

}
