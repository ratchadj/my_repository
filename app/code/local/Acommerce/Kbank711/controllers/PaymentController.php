<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Kbank711 to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Kbank711
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/
class Acommerce_Kbank711_PaymentController extends Mage_Core_Controller_Front_Action
{
	protected $_order = null;
	protected $_checkout_session = null;
	protected $_pay_model = null;

	public function testAction() {
		//Zend_Debug::dump(Mage::getModel('kbank711/kbank711')->validItem(Mage::getSingleton('checkout/session')->getQuote()));
	}
    protected function getOrder() {
		if ($this->_order == null)
            $this->_order = Mage::getModel('sales/order')->loadByIncrementId($this->getSession()->getLastRealOrderId());
        return $this->_order;
    }
	public function setOrder(Mage_Sales_Model_Order $order) {
		$this->_order = $order;
	}
	protected function getSession() {
		if(!$this->_checkout_session)
			$this->_checkout_session=Mage::getSingleton('checkout/session');
		return $this->_checkout_session;
	}
	protected function _getPaymentModel() {
		if(is_null($this->_pay_model)) {
			$this->_pay_model = Mage::getModel('kbank711/kbank711')->setOrder($this->getOrder());
		}
		return $this->_pay_model;
	}
	public function sendTransactionAction() {
		try {
			$order = $this->getOrder();
			$session = $this->getSession();
			if (!$order->getId() || ($order->getIncrementId()==$session->getRedirected()) || !Mage::getModel('kbank711/kbank711')->validateRedirect($order)) {
				$session->clear();
				$this->norouteAction();
				return;
			}

			$this->getResponse()
				->setBody($this->getLayout()
				->createBlock('kbank711/redirect')
				->setModel($this->_getPaymentModel())
				->toHtml());
			$session->setRedirected($order->getIncrementId());
			$order->sendNewOrderEmail();
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'Kbank711.log');
			$this->_getCoreSession()->addError($this->__('There was an error, please try again'));
			$this->getSession()->clear();
			$this->_redirect('checkout/onepage/failure');
		}
	}
	public function returnAction() {
		try{
			$model = $this->_getPaymentModel();
            $params = $this->_getFrontendStandardParams();
            $incrementId = ltrim($params['RETURNINV'],'0');
            $orderPrefix = $model->getConfigData('order_prefix');

        	if(strlen($orderPrefix)>0){
        		$incrementId = $orderPrefix.$incrementId;
        	}

		    $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);
			if(!$order->getId()) {
				$this->_getCoreSession()->addError(Mage::helper('kbank711')->__('There is no order exists'));
				$this->_redirect('checkout/onepage/failure');
				return;
			}
		    $this->setOrder($order);
			$model->setPaymentData($params)
			->setDebugMode(Acommerce_Kbank711_Model_Kbank711::DEBUG_MODE_RESPONSE_DATA);
			$model->customDebug();
			$msg = '';
			$kbankCheckSum  = array_pop($params);
			$checkSum = $model->getCheckSum($params);
			$requestId = $model->getRequestIp();
		    if((string)$params['HOSTRESP'] == '00' /* && ($kbankCheckSum == $checkSum) */ && ((int)$params['AMOUNT']/100 ==$order->getBaseGrandTotal()) &&
			($model->getKbankIp()==$requestId)) {

				if(!$order->hasInvoices()) {
					//$model->importPaymentInfo();
					$result = $model->saveInvoice();
					if(!$result) {
						$msg = Mage::helper('kbank711')->__('Failed to invoice your order');
					}
				}
		    }
		    else {
				$order->setStatus('failed')->save();
				// $model->saveFailReason();
				$msg = Mage::helper('kbank711')->__('Your payment was refused by the gateway');
		    }
			$model->saveRequestData();
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'Kbank711.log');
			$msg = Mage::helper('kbank711')->__('There was an error occur while trying to verify your payment');
		}
		if($msg) {

			$this->_getCoreSession()->addError($msg);
			$this->_redirect('checkout/onepage/failure');
		}
		else {

			$this->_redirect('checkout/onepage/success');
		}
	}
	protected function _getFrontendStandardParams() {
		$default = array(
			'HOSTRESP'=>'',
			'REFCODE'=>'',
			'AUTHCODE'=>'',
			'RETURNINV'=>'',
			'UAID'=>'',
			'CARDNUMBER'=>'',
			'AMOUNT'=>'',
			'THBAMOUNT'=>'',
			'CURISO'=>'',
			'FXRATE'=>'',
			'FILLSPACE'=>'',
			'MID'=>'',
			'PLANID'=>'',
			'PAYMONTH'=>'',
			'INTTYPE'=>'',
			'INTRATE'=>'',
			'AMTPERMONTH'=>'',
			'TOTALAMT'=>'',
			'MANGFEE'=>'',
			'INTMODE'=>'',
			'MD5CHECKSUM'=>'',
		);
		return array_merge($default,$this->getRequest()->getParams());
	}
	protected function _getBackendStandardParams() {
		$params = $this->getRequest()->getParam('PMGWRESP2');
		$default = array(
			'TransCode'=>substr($params, 0, 4),
			'MerchantID'=>substr($params, 4, 19),
			'TerminalID'=>substr($params, 19, 27),
			'ShopNo'=>substr($params, 27, 29),
			'CurrencyCode'=>substr($params, 29, 32),
			'InvoiceNo'=>substr($params, 32, 44),
			'Date'=>substr($params, 44, 52),
			'Time'=>substr($params, 52, 58),
			'CARDNUMBER'=>substr($params, 58, 77),
			//'ExpiredDate'=>substr($params, 77, 81),
			//'CVV2'=>substr($params, 81, 85),
			'TransAmount'=>substr($params, 85, 97),
			'HOSTRESP'=>substr($params, 97, 99),//ResponseCode
			'ApprovalCode'=>substr($params, 99, 105),
			'FILLSPACE'=>substr($params, 105, 108),
			'Reference1'=>substr($params, 108, 128),
			'PlanID'=>substr($params, 128, 131),
			'PAYMONTH'=>substr($params, 131, 133),
			'INTTYPE'=>substr($params, 133, 134),
			'INTRATE'=>substr($params, 134, 140),
			'AMTPERMONTH'=>substr($params, 140, 149),
			'TOTALAMT'=>substr($params, 149, 161),
			'MANGFEE'=>substr($params, 160, 166),
			'INTMODE'=>substr($params, 166, 168),
			'FXRate'=>substr($params, 168, 188),
			'THBAmount'=>substr($params, 188, 208),
			'CustomerEmail'=>substr($params, 208, 308),
			'Description'=>substr($params, 308, 458),
			'PayerIPAddress'=>substr($params, 458, 476),
			'WarningLight'=>substr($params, 476, 477),
			'SelectedBank'=>substr($params, 477, 537),
			'IssuerBank'=>substr($params, 537, 597),
			'SelectedCountry'=>substr($params, 597, 642),
			'IPCountry'=>substr($params, 642, 687),
			'IssuerCountry'=>substr($params, 687, 732),
			'ECI'=>substr($params, 732, 736),
			'XID'=>substr($params, 736, 776),
			'CAVV'=>substr($params, 776, 816),
			'MD5CHECKSUM'=>'',
		);
		return $default;
	}
	public function merchantAction() {
		try{
			$params = $this->_getBackendStandardParams();
		    $order = Mage::getModel('sales/order')->loadByIncrementId($params['InvoiceNo']);
			if(!$order->getId()) {
				return;
			}
		    $this->setOrder($order);
			$model = $this->_getPaymentModel();
			$model->setPaymentData($params)->setDebugMode($model::DEBUG_MODE_RESPONSE_DATA_MERCHANT);
			$model->customDebug();
			if($params['PayMonth']) {
				$model->getPayMethod(Acommerce_Kbank711_Model_Kbank711::PAY_TYPE_INSTALLMENT);
			}
			$checkSum = $model->getCheckSum($params);
			$requestId = $model->getRequestIp();
		    $model->saveRequestData();
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'Kbank711.log');
		}
	}
	protected function _getCoreSession() {
		return Mage::getSingleton('core/session');
	}
}
