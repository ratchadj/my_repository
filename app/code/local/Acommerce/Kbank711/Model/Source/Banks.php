<?php
class Acommerce_Kbank711_Model_Source_Banks
{
	public function toOptionArray() {
		$_banks = array();
		foreach(Mage::getModel('kbank711/kbank711')->getShopIds() as $id => $name) {
			$_banks[]  = array('value'=>$id,'label'=>$name);
		}
		return $_banks;
    }
}
