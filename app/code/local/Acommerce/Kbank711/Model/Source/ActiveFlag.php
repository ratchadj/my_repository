<?php
class Acommerce_Kbank711_Model_Source_ActiveFlag
{
    public function toOptionArray() {
        return array(
                    array('value' => 'Y', 'label' => 'Yes'),
                    array('value' => 'N', 'label' => 'No')
                );
    }
}
