<?php
class Acommerce_Kbank711_Model_Source_GatewaySource
{
	public function getModes() {
		return array(
			"01" => "Merchant pay Int." ,
			"02" => "Customer pay Int." ,
			"03" => "Supplier pay Int." ,
			"04" => "Special Int."
		);
	}
	public function getMode($code) {
		$modes = $this->getModes();
		return (isset($modes[$code]))?$modes[$code]:$code;
	}
	public function getCardTypes() {
		return array(
			"001" => "VISA" ,
			"002" => "Master" ,
			"003" => "KBank" ,
			"004" => "JCB" ,
			"005" => "CUP" ,
			"007" => "AMEX" 
		);
	}
	public function getCard($code) {
		$cards = $this->getCardTypes();
		return (isset($cards[$code]))?$cards[$code]:$code;
	}
	public function getMethods() {
		return array(
			1 => "Effective Rate" ,
			2 => "Flat Rate " ,
			3 => "Progressive Rate" ,
		);
	}
	public function getMethod($code) {
		$methods = $this->getMethods();
		return (isset($methods[$code]))?$methods[$code]:$code;
	}
}