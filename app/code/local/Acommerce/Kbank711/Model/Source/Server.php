<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Kbank711 to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Kbank711
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/
class Acommerce_Kbank711_Model_Source_Server extends Mage_Core_Model_Config_Data
{
	public function getValue() {
		$curr =  Mage::helper('core/http')->getServerAddr();
		$configured = parent::getValue();
		if($configured && $curr != $configured) {
			return $configured;
		}
		return $curr;
    }
}
