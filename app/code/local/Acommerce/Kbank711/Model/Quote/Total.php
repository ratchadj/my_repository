<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Kbank711 to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Kbank711
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_Kbank711_Model_Quote_Total extends Mage_Sales_Model_Quote_Address_Total_Abstract
{
	public function collect(Mage_Sales_Model_Quote_Address $address) {
        return $this;
    }
    public function fetch(Mage_Sales_Model_Quote_Address $address)
    {
        $addInf = $address->getQuote()->getPayment()->getAdditionalInformation();
		if(!is_null(Mage::registry('period_total'))) {
			return $this;
		}
        if (isset($addInf['promo']) && is_numeric($addInf['promo'])) {
            $value = number_format($address->getQuote()->getBaseGrandTotal()/$addInf['promo'], 2);
            $address->addTotal(
                array(
                    'code' => $this->getCode(),
                    'title' => Mage::helper('kbank711')->__('Pay Per Month'),
                    'value' => $value
                )
            );	
			
			Mage::register('period_total',true);
        }
        return $this;
    }
}
