<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Kbank711 to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Kbank711
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/
class Acommerce_Kbank711_Model_Kbank711 extends Mage_Payment_Model_Method_Abstract
{
    protected $_code = 'kbank711';
    protected $_formBlockType = 'kbank711/form';
    protected $_infoBlockType = 'kbank711/info';
    protected $_canAuthorize = true;
    protected $_canUseInternal = true;
    protected $_canUseForMultishipping = false;
    protected $_xml = null;

	const CARD_GROUP = 'gateway_group';
	const CARD_NORMAL = 'gateway';

	const PAY_TYPE_INSTALLMENT = 'installment';
	const PAY_TYPE_CASHBACK = 'cashback';

	const DEBUG_MODE_SEND_DATA = 'send';
	const DEBUG_MODE_RESPONSE_DATA = 'receive';
	const DEBUG_MODE_RESPONSE_DATA_MERCHANT = 'receive_backend';

    public function validateRedirect($order) {
        $method = $order->getPayment()->getMethodInstance();
        if ($method->getCode() == $this->getCode()) {
            return true;
        } else {
            return false;
        }
    }

    public function isAvailable($quote = null) {
    	$active = parent::isAvailable($quote);
        if ($quote && $active) {
            if ($this->getConfigData('dev')) {
                $allowed_emails = explode(',', $this->getConfigData('email'));
                $customer_email = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
                if (!in_array($customer_email, $allowed_emails)) {
                    return false;
                }
            }

            if($this->getInstallmentItem($quote)) {
               $result = $this->validateInstallment($quote);
               if(!$result) {
                    return $result;
               }
            }
        }
        return $active;
    }

    public function validateInstallment($quote) {

        $inst = $this->getInstallment($quote);

        if($inst) {
            $conditions = unserialize($inst->getConditions());
            $banks = unserialize(Mage::getStoreConfig('payment/kbank711/shop_id'));
            $installments = unserialize(Mage::getStoreConfig('payment/kbank711/installment/period'));

            $temp = array();
            foreach ($banks as $value) {
                $key = $value['code'];
                $temp[$key] = $value;
            }
            $banks = $temp;

            $temp = array();
            foreach ($installments as $value) {
                $key = $value['bank'].$value['code'];
                $temp[$key] = $value;
            }
            $installments = $temp;

            $curDate = Mage::app()->getLocale()->storeDate(null, null, true);
            $curDate = strtotime($curDate->toString('Y-m-d H:i:s', 'php'));
            $grandTotal = $quote->getGrandTotal();

            foreach ($conditions as $key => $condition) {
                foreach ($condition as $key1 => $value) {
                   if(isset($banks[$key])) {
                        $bank = $banks[$key];
                        if (isset($installments[$bank['bank'].$key1])) {
                            $installment = $installments[$bank['bank'].$key1];

                            $validForm = $value[0];
                            $validTo = $value[1];

                            if(!empty($validForm) && !empty($validTo)) {
                                $validForm = strtotime($validForm);
                                $validTo = strtotime($validTo);
                                if($validForm <= $curDate && $validTo >= $curDate) {
                                    return true;
                                }
                            } elseif(!empty($validForm) && empty($validTo)) {
                                $validForm = strtotime($validForm);
                                if($validForm <= $curDate) {
                                    return true;
                                }
                            } elseif(empty($validForm) && !empty($validTo)) {
                                $validTo = strtotime($validTo);
                                if($validTo >= $curDate) {
                                    return true;
                                }
                            } else {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public function getInstallment($quote) {
        $item = $this->getInstallmentItem($quote);

        if(!$item) {
            return false;
        }

        $installments = Mage::getResourceModel('campaigns/installment_collection')
                              ->getInstallmentByProduct($item->getProductId())
                              ->addFieldToFilter('main_table.campaign_id', array('eq' => $item->getCampaignId()));

        if($installments) {
            return $installments->getFirstItem();
        }
        else {
            return false;
        }

    }

    public function getInstallmentItem($quote) {

        if(!is_null($quote)) {

            if(is_null($this->_installmentItem)) {
                $allItems = $quote->getAllItems();
                foreach ($allItems as $item) {

                    if ($item->getChildren()) {
                        continue;
                    }

                    $option = $item->getOptionByCode('info_buyRequest');
                    if ($option) {
                        $request = unserialize($option->getValue());
                        if (isset($request['trade_info'])) {
                            $trade = unserialize($request['trade_info']);
                            if($trade) {
                                if(isset($trade['installments']) && $trade['installments'] == 1) {
                                    $this->_installmentItem = $item;
                                    $this->_installmentItem->setCampaignId($trade['campaign_id']);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if(is_null($this->_installmentItem)) {
                $this->_installmentItem = false;
            }
            return $this->_installmentItem;
        }
        return false;
    }


    public function getRequestIp() {
		if($this->getConfigData('verify_gateway_ip')) {
			return Mage::helper('core/http')->getRemoteAddr();
		}
        return $this->getKbankIp();
    }
	public function getServerIp() {
        return $this->getConfigData('server_ip');
    }
	public function getKbankIp() {
        return $this->getConfigData('kbank_ip');
    }

    public function getStatus() {
        return $this->getConfigData('order_status');
    }

    public function getCustomText() {
        $customtext = $this->getConfigData('customtext');
        return $customtext;
    }

    public function getGatewayUrl() {
		if($this->getCardType()) {
			return $this->getConfigData($this->getCardType());
		}
        return $this->getConfigData('gateway');
    }
	public function getCctypes() {
        return explode(',', $this->getConfigData('cctypes'));
    }

    public function getMerchantId() {
		if($this->getPayMethod()) {
			return $this->getConfigData($this->getPayMethod().'/merchant_id');
		}
        return $this->getConfigData('merchant_id');
    }
	public function getTerminalId() {
		if($this->getPayMethod()) {
			return $this->getConfigData($this->getPayMethod().'/terminal_id');
		}
        return $this->getConfigData('terminal_id');
    }

    public function getReturnUrl() {
        return Mage::getUrl('kbank/payment/return', array('_secure' => true));
    }
	public function getBackendUrl() {
        return Mage::getUrl('kbank/payment/merchant', array('_secure' => true));
    }

    protected function getCurrentOrder()
    {
        $order = $this->getOrder();
        if (!($order instanceof Mage_Sales_Model_Order) || !$order->getId()) {
            Mage::throwException($this->_getHelper()->__('Cannot retrieve order object'));
        }
        return $order;
    }

    public function getOrderPlaceRedirectUrl() {
		return Mage::getUrl('kbank/payment/sendTransaction');
    }
	public function getSecretKey() {
		return $this->getConfigData('secret_key');
	}
    public function getDebug() {
        return $this->getConfigData('debug');
    }
    public function getStandardCheckoutFormFields() {
		$order = $this->getCurrentOrder();
        $detail = $order->getIncrementId();
        //foreach ($order->getAllItems() as $item) {
        //    $pr = Mage::getModel('catalog/product')->load($item->getProductId());
        //    $detail .= $pr->getUrlKey();
        //}

        $inputInfo = $order->getPayment()->getAdditionalInformation();
		$selectedId = isset($inputInfo['panBank'])?$inputInfo['panBank']:'';
		$selectedCcType = isset($inputInfo['cc_type'])?$inputInfo['cc_type']:'';
		$period = isset($inputInfo['promo'])?$inputInfo['promo']:null;
		$cashback = isset($inputInfo['cash_back'])?$inputInfo['cash_back']:null;
		//$systemShopIds = $this->getShopIds();
		$assignLabel = array();
		$shopId = '00';//null;
		if($period && is_numeric($period)) {
			$this->setPayMethod(self::PAY_TYPE_INSTALLMENT);
			$assignLabel['Use Installment'] = 'Yes';
			$assignLabel['Promotion'] = $inputInfo['period_label'];
			$shopId = $inputInfo['shop_id'];//$this->getShopIds(true);
		}

		//if(!$shopId) {
		//	$shopId = ($selectedId && isset($systemShopIds[$selectedId]))?$selectedId:'00';
		//}

		//$assignLabel['Selected Bank'] = isset($systemShopIds[$shopId])?$systemShopIds[$shopId]:$systemShopIds['01'];
		if($cashback) {
			$assignLabel['Cash Back'] = $cashback;
			$assignLabel['Use Cash Back'] = 'Yes';
		}
		$order->getPayment()->setAdditionalInformation($assignLabel)->save();
		if($selectedCcType && in_array($selectedCcType,$this->getCctypes()) && ($selectedCcType == 'CUP' || $selectedCcType == 'AE')) {
			$this->setCardType(self::CARD_GROUP);
		}
		else {
			$this->setCardType(self::CARD_NORMAL);
		}

        $fillspace = $this->getConfigData('log_card_type');
        if($fillspace==1){
            $fillspace = 'Y';
        } else {
            $fillspace = 'N';
        }

        $orderPrefix = $this->getConfigData('order_prefix');
        if(strlen($orderPrefix)>0){
            $invMerchant = substr($order->getIncrementId(), strlen($orderPrefix));
        } else {
            $invMerchant = $order->getIncrementId();
        }

        $reqData = array(
			'MERCHANT2' 	=> $this->getMerchantId(),
			'TERM2' 		=> $this->getTerminalId(),
			'AMOUNT2' 		=> $this->getAmount($order->getBaseGrandTotal()),
			'URL2' 			=> $this->getReturnUrl(),
			'RESPURL' 		=> $this->getBackendUrl(),
			'IPCUST2' 		=> $this->getServerIp(),
			'DETAIL2' 		=> substr($detail, 0, 30),
			'INVMERCHANT' 	=> $invMerchant,
			'FILLSPACE' 	=> $fillspace,
			'SHOPID' 		=> $shopId,
			'PAYTERM2' 		=> $period,

		);

		$reqData['CHECKSUM'] = $this->getCheckSum($reqData);
		$this->setDebugMode(self::DEBUG_MODE_SEND_DATA)
				->setPaymentData($reqData)
				->customDebug();

        $isInstallment = 0;
        foreach ($order->getAllItems() as $item) {
            if($item->getTradeNo()) {
                $rate = $inputInfo['rate'];
                if(empty($inputInfo['rate']))
                {
                    $rate = 0;
                }

                if(isset($inputInfo['period'])) {
                    $isInstallment = 1;
                    $item->setInstallmentTerm($inputInfo['period']);
                    $item->setInstallmentRate($rate);
                    $item->save();
                }
            }
        }

        if($isInstallment) {
            Mage::log(get_class($order).'::'.$isInstallment, null, 'dd.log');
            $order->setIsInstallment($isInstallment);
            $order->save();
        }

		return $reqData;
    }

	public function getAmount($amount) {
		return str_pad($amount * 100, 12, '0', STR_PAD_LEFT);
	}
	public function getCheckSum($data) {
		$data = implode($data);
		$data .= $this->getSecretKey();
		return md5($data);
	}
	public function getShopIds($specific=false) {
		if($specific) {
			$path = ($this->getPayMethod())?$this->getPayMethod():self::PAY_TYPE_INSTALLMENT;
			return $this->getConfigData($path.'/shop_id');
		}
		$values = unserialize($this->getConfigData('shop_id'));
		$ids = array();
		foreach($values as $value)  {
			if(isset($value['bank']) && isset($value['name']) && !empty($value['bank'])) {
				$ids[$value['bank']] = $value['name'];
			}
		}
		return $ids;
	}

	/**
     * Get available periods for quote
     *
     * @return array of period array(array(value,name,products,categories,minimum,promo,valid_from,valid_to))
     */
	public function getPromoForQuote($quote=null) {
		$keys = 'quote_periods'.$this->getPayMethod();
		if($this->getData($keys)) {
			return $this->getData($keys);
		}
		$valid = array();
		$this->setData($keys,$valid);
		if(!$quote) {
			$quote = ($this->getQuote())?$this->getQuote():Mage::getSingleton('checkout/session')->getQuote();
		}
		if(!($quote instanceof Mage_Sales_Model_Quote)) {
			return $valid;
		}
		$periods = $this->_getValidPromo($quote->getBaseGrandTotal());
		if(empty($periods)) {
			return $valid;
		}

        $quoteSkus = $this->_getQuoteSkus($quote);
		// array(value,name,products,categories,minimum,promo,valid_from,valid_to)
        Mage::log($periods, null, 'ranai.log');
		foreach($quoteSkus as $pId=>$sku) {
			foreach($periods as $p) {
				if($this->_validateProductSku($p,$sku,$pId)) {
					$valid[] = $p;
				}
			}
		}
		$this->setData($keys,$valid);
		return $this->getData($keys);
	}
	protected function _getQuoteSkus($quote) {
		$skus = array();
		foreach($quote->getAllVisibleItems() as $item) {
			$skus[$item->getProductId()] = $item->getSku();
		}
		return $skus;
	}
	protected function _validateProductSku($period,$sku,$pId) {
		if(!is_array($period) || !isset($period['products']) || !$sku || !$pId) {
			return false;
		}
		if((!$period['products'] && !$period['categories']) || ($period['products'] = $period['categories'] == '')) {
			return true;
		}
		$pSkus = explode(',',$period['products']);
		if(in_array($sku,$pSkus)) {
			return true;
		}
		return $this->_validateCategory($period,$pId);
	}
	protected function _validateCategory($period,$productId) {
		$categoryIds = $this->_getProductCategoryIds(array($productId));
		$cateIds = explode(',',$period['categories']);
		$inter = array_intersect($categoryIds,$cateIds);
		if(!empty($inter)) {
			return true;
		}
		return false;
	}
	protected function _getProductCategoryIds($productIds){
		//$key = 'product_category_'.$productIds;
		/* if($this->getData($key)) {
			return $this->getData($key);
		} */
		$ids = null;
		$res = Mage::getResourceModel('catalog/product');
		$adapter = $res->getReadConnection();
		$select = $adapter->select()
            ->from($res->getTable('catalog/category_product'), 'category_id')
            ->where('product_id in (?)', implode(',',$productIds));
        $ids = $adapter->fetchCol($select);
		//$this->setData($key,$ids);
		return $ids;
	}
	/**
     * Get available periods for product
     *
     * @return array of period array(array(value,name,products,categories,minimum,promo,valid_from,valid_to))
     */
	public function getPromoForProduct() {
		$product = $this->getProduct();
		if(is_string($product)) {
			$model = Mage::getModel('catalog/product');
			$productId = $model->getIdBySku($product);
			if($productId) {
				$product = $model->load($productId);
			}
		}
		if(is_numeric($product)) {
			$product = Mage::getModel('catalog/product')->load($product);
		}
		if(!($product instanceof Mage_Catalog_Model_Product)) {
			return array();
		}
		$cacheKey = 'product_'.$this->getPayMethod().'_'.$product->getSku();
		if($valid=$this->getData($cacheKey)) {
			return $valid;
		}
		$valid = array();
		$this->setData($cacheKey,$valid);

		$periods = $this->_getValidPromo();
		if(empty($periods)) {
			return $valid;
		}
		foreach($periods as $p) {
			$productValidation = $this->_validateProductSku($p,$product->getSku(),$product->getId());
			if(($productValidation===true)) {
				$valid[] = $p;
			}
		}
		$this->setData($cacheKey,$valid);
		return $this->getData($cacheKey);
	}
	public function getProductPeriods() {
		$this->setPayMethod(self::PAY_TYPE_INSTALLMENT);
		return $this->getPromoForProduct();
	}
	public function getQuotePeriods() {
		$this->setPayMethod(self::PAY_TYPE_INSTALLMENT);
		return $this->getPromoForQuote();
	}
	public function getCashBackForProduct() {
		$this->setPayMethod(self::PAY_TYPE_CASHBACK);
		return $this->getPromoForProduct();
	}
	public function getCashBackForQuote() {
		$this->setPayMethod(self::PAY_TYPE_CASHBACK);
		return $this->getPromoForQuote();
	}
	protected function _getValidPromo($amount=false) {
		if(!$this->getUsePromo()) {
			return array();
		}
		$path = ($this->getPayMethod())?$this->getPayMethod().'/':'';
		// array(value,name,products,categories,minimum,promo,valid_from,valid_to)
		$periods = unserialize($this->getConfigData($path.'period'));
		$currTime = Mage::getModel('core/date')->timestamp();
		$valid = array();
		foreach($periods as $period) {
			if($amount!==false && ((float)$period['minimum']>(float)$amount)) {
				continue;
			}
			if(strtotime($period['valid_from'])<=$currTime && strtotime($period['valid_to'])>$currTime) {
				$valid[] = $period;
			}
		}
		return $valid;
	}
	// temp fix for design guys
	public function getPeriods() {
		return array();
	}
	public function getUsePromo($path='') {
		if(!$path && $this->getPayMethod()) {
			$path = $this->getPayMethod();
		}
		return $this->getConfigData($path.'/active');
	}

    public function saveRequestData() {
        $this->importPaymentInfo()->save();
    }
	protected function _renderValue($value=0) {
		if($value && is_numeric($value)) {
			$value = ((int)$value/100).'%';
		}
		return $value;
	}
    protected function getInfo() {
        $payData = $this->getPaymentData();
		$source = Mage::getModel('kbank711/source_gatewaySource');
		$data =  array('Status Code'=>$payData['HOSTRESP']);
		if($this->getConfigData('log_card_type') && $payData['FILLSPACE']) {
			$data['Credit Card Type']=$source->getCard($payData['FILLSPACE']);
		}
		if($payData['PAYMONTH']) {
			$data['Selected Period'] = $payData['PAYMONTH'];
			$data['Calculation Method'] = $source->getMethod($payData['INTTYPE']);
			$data['Interest Rate'] = $this->_renderValue($payData['INTRATE']/100);
			$data['Amount per Month'] = $payData['AMTPERMONTH']/100;
			$data['Total Amount'] = $payData['TOTALAMT']/100;
			$data['Management Fee'] = $this->_renderValue($payData['MANGFEE']/100);
			$data['Pay Mode'] = $source->getMode($payData['INTMODE']);
		}
		if(isset($payData['CARDNUMBER'])) {
			$data['Card No.'] = $payData['CARDNUMBER'];
		}
		if(isset($payData['IssuerBank'])) {
			$data['Selected Bank'] = $payData['IssuerBank'];
		}
		return $data;
    }

    protected function importPaymentInfo() {
        $payment = $this->getOrder()->getPayment();
        $was = $payment->getAdditionalInformation();
        $info = $this->getInfo();
        $new = array_merge($was,$info);
        $payment->setAdditionalInformation($new);
        $payment->setCcNumberEnc($info['Card No.']);
        //$payment->setCcType(substr($info['Credit Card Type'], 0,2));
        return $payment;
    }

    public function saveInvoice() {
        try {
            $order = $this->getOrder();
            $payment = $this->importPaymentInfo();
			$res = $this->getPaymentData();
            if ($order->canInvoice()) {
                $payment->setCcTransId($order->getIncrementId())
                        ->setCcApproval($res['HOSTRESP'])
                        ->setTransactionId($order->getIncrementId())
                        ->registerCaptureNotification($order->getBaseGrandTotal());
                $order->save();
            }
            $invoice = $payment->getCreatedInvoice();
            if ($invoice && !$order->getEmailSent()) {

                $order->sendNewOrderEmail()->addStatusHistoryComment(
                                Mage::helper('kbank711')->__('Notified customer about invoice #%s.',
                                                         $invoice->getIncrementId())
                        )
                        ->setIsCustomerNotified(true)
                        ->save();
            }
            return true;
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'Kbank711.log');
        }
        return false;
    }

    public function assignData($data) {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }

        $promo = $data->getData('promo');

        //$promo = ($data->getData('promo'))?$data->getData('promo'):null;
        $info = $this->getInfoInstance();
        if($promo) {

            $this->setQuote($info->getQuote());
            $this->setPayMethod(self::PAY_TYPE_INSTALLMENT);
            $path = ($this->getPayMethod())?$this->getPayMethod().'/':'';
            $periods = unserialize($this->getConfigData($path.'period'));

            $allowed = false;
            foreach($periods as $p) {
                $key = trim($p['bank'].$p['period_value'].$p['minimum'].$p['code']);
                if($key==$promo) {
                    $data->setPeriod($p['period_value']);
                    $data->setPromo($p['period_value']);
                    $data->setPeriodLabel($p['period_label']);
                    $data->setData('panBank', $p['bank']);
                    $data->setData('rate', $p['rate']);
                    $data->setData('shop_id', $p['shop_id']);
                    $allowed = true;
                }
            }

            if(!$allowed) {
                $data->setPeriod(null);
                Mage::throwException(Mage::helper('kbank711')->__('Selected installment period is not available'));
            }
            //ranai

        }

        $info->addData(array('additional_information'=>$data->getData()));
        $info->setCcType($data->getCcType());
        return $this;
    }


	public function assignData1($data) {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }

        $datas = array();
        if($data->getData('promo')) {
            $datas = explode('-', $data->getData('promo'));
            $promo = $datas[1];
        } else {
            $promo = null;
        }

        //$promo = ($data->getData('promo'))?$data->getData('promo'):null;
		$info = $this->getInfoInstance();
		if($promo) {

			$this->setQuote($info->getQuote());
			if(is_numeric($promo)) {
				$allowedPeriods = $this->getQuotePeriods();
				$allowed = false;
				foreach($allowedPeriods as $p) {
					if(isset($p['value']) && $p['value']==$promo) {
						$data->setPeriod($p['value']);
						$data->setPeriodLabel($p['name']);
						$allowed = true;
					}
				}
				if(!$allowed) {
					$data->setPeriod(null);
					Mage::throwException(Mage::helper('kbank711')->__('Selected installment period is not available'));
				}
			}
			else {
				$cashbs = $this->getCashBackForQuote();
				$valid = false;
				foreach($cashbs as $cashb) {
					if('c'.$cashb['shop_id']==$promo) {
						$valid = true;
						$data->setCashBack($cashb['name']);
					}
				}
				if(!$valid) {
					$data->setCashBack(null);
					Mage::throwException(Mage::helper('kbank711')->__('Selected cash back is not available'));
				}
			}
		}

		$info->addData(array('additional_information'=>$data->getData()));
        $info->setCcType($data->getCcType());
        return $this;
    }

	public function setPaymentData($data) {
		//$this->setDebugPrivateKey(array_keys($data));
		$this->setData('payment_data',$data);
		return $this;
	}
	public function setDebugPrivateKey($data) {
		$this->_debugReplacePrivateDataKeys = $data;
		return $this;
	}
	protected function _debug($debugData) {
        if ($this->getDebugFlag()) {
            Mage::getModel('core/log_adapter', 'payment_' . $this->getCode() . '_' . $this->getDebugMode() . '.log')
               ->setFilterDataKeys($this->_debugReplacePrivateDataKeys)
               ->log($debugData);
        }
    }
	public function customDebug() {
		$this->debugData($this->getPaymentData());
	}
}
