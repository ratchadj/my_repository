<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Kbank711 to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Kbank711
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_Kbank711_Block_Adminhtml_System_Config_Form_Field_Render_Select extends Mage_Core_Block_Abstract
{
    protected function _toHtml() {		
		$column = $this->getColumn();
		$html = '<select name="'.$this->getInputName().'" id="#{_id}_'.$this->getColumnName().'" value="#{'.$this->getColumnName().'}" '.
		(isset($column['style']) ? ' style="'.$column['style'] . '"' : '') .'>';		
        $values = Mage::getModel($column['source'])->toOptionArray();
        if ($values) {
            foreach ($values as $option) {
                $html.= $this->_optionToHtml($option);
            }
        }

        $html.= '</select>';
		return $html;
    }
    protected function _optionToHtml($option) {
        $html = '<option value="'.$this->_escape($option['value']).'"';
		$html.= '>'.$this->_escape($option['label']). '</option>';
        return $html;
    }
    protected function _escape($string) {
        return htmlspecialchars($string, ENT_COMPAT);
    }
}
