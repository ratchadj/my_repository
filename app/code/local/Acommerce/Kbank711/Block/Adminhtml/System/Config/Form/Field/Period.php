<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Kbank711 to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Kbank711
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_Kbank711_Block_Adminhtml_System_Config_Form_Field_Period extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function __construct() {

        $this->_addAfter = true;
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add');
        parent::__construct();
        $this->setTemplate('cc2p/system/config/form/field/array.phtml');
    }
    protected function _prepareLayout() {
        $this->addColumn('bank', array(
            'label' => Mage::helper('adminhtml')->__('Bank Name'),
            'type'  => 'select',
            'renderer' =>  $this->getLayout()->createBlock('kbank711/adminhtml_system_config_form_field_render_select'),
            'source'=>'kbank711/source_banks',
            'style' => 'width:120px',
        ));
        $this->addColumn('code', array(
            'label' => Mage::helper('adminhtml')->__('Installment Code'),
            'style' => 'width:80px',
        ));
        $this->addColumn('period_label', array(
            'label' => Mage::helper('adminhtml')->__('Installment Label'),
            'style' => 'width:120px',
        ));
        $this->addColumn('period_value', array(
            'label' => Mage::helper('adminhtml')->__('Installment Term'),
            'style' => 'width:100px',
        ));

         $this->addColumn('active_flag', array(
            'label' => Mage::helper('adminhtml')->__('Is Actived'),
            'type'  => 'select',
            'renderer' =>  $this->getLayout()->createBlock('kbank711/adminhtml_system_config_form_field_render_select'),
            'source'=>'kbank711/source_activeFlag',
            'style' => 'width:80px',
        ));

        $this->addColumn('rate', array(
            'label' => Mage::helper('adminhtml')->__('Installment Rate'),
            'style' => 'width:80px',
        ));

        $this->addColumn('interest_type', array(
            'label' => Mage::helper('adminhtml')->__('Payment Type'),
            'type'  => 'select',
            'renderer' =>  $this->getLayout()->createBlock('kbank711/adminhtml_system_config_form_field_render_select'),
            'source'=>'cc2p/source_interestType',
            'style' => 'width:144px',
        ));

        $this->addColumn('shop_id', array(
            'label' => Mage::helper('adminhtml')->__('Shop Id'),
            'style' => 'width:50px',
        ));

        $this->addColumn('minimum', array(
            'label' => Mage::helper('adminhtml')->__('Minimum'),
            'style' => 'width:50px',
        ));
        $this->addColumn('promo', array(
            'label' => Mage::helper('adminhtml')->__('Promo Code'),
            'style' => 'width:70px',
        ));
        $this->addColumn('valid_from', array(
            'label' => Mage::helper('adminhtml')->__('Valid From'),
            'style' => 'width: 70px',
            'type'  => 'date',
            //'renderer' => new EMST_Cc2p_Block_Adminhtml_System_Config_Form_Field_Render_Date,
            'renderer' => $this->getLayout()->createBlock('kbank711/adminhtml_system_config_form_field_render_date')
        ));
        $this->addColumn('valid_to', array(
            'label' => Mage::helper('adminhtml')->__('Valid To'),
            'style' => 'width:70px',
            'type'  => 'date',
            'renderer' => new Acommerce_Kbank711_Block_Adminhtml_System_Config_Form_Field_Render_Date
        ));
        return parent::_prepareLayout();
    }

    public function addColumn($name, $params) {
        $this->_columns[$name] = array(
            'label'     => empty($params['label']) ? 'Column' : $params['label'],
            'size'      => empty($params['size'])  ? false    : $params['size'],
            'style'     => empty($params['style'])  ? null    : $params['style'],
            'class'     => empty($params['class'])  ? null    : $params['class'],
            'type'      => empty($params['type']) ? '' : $params['type'],
            'source'    => empty($params['source']) ? '' : $params['source'],
            'renderer'  => false,
        );
        if ((!empty($params['renderer'])) && ($params['renderer'] instanceof Mage_Core_Block_Abstract)) {
            $this->_columns[$name]['renderer'] = $params['renderer'];
        }
    }

}
