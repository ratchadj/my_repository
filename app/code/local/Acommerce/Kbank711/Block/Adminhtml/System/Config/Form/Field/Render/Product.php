<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Onestepcheckout to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Onestepcheckout
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class Acommerce_Kbank711_Block_Adminhtml_System_Config_Form_Field_Render_Product extends Mage_Core_Block_Abstract
{
    protected function _toHtml() {		
		$column = $this->getColumn();
		$column = $this->getColumn();
		$class = isset($column['class'])?$column['class']:'';
		$id = isset($column['config_chooser_id'])?$column['config_chooser_id']:'';		
		$url =  Mage::helper('adminhtml')->getUrl('adminhtml/promo_widget/chooser/attribute/sku/form/'.$id);
		$html = '<li style="list-style:none;">'.
				'<span class="rule-param rule-param-edit">'.
				'<span class="element" style="display:inline-flex">'.
			'<input type="text" name="'.$this->getInputName().'" value="#{' . $this->getColumnName() . '}" class="'.$class.'">'.
			'<a style="padding-left:9px" href="javascript:void(0)" class="rule-chooser-trigger"><img src="'.Mage::getDesign()->getSkinUrl('images/rule_chooser_trigger.gif').
			'" class="v-middle rule-chooser-trigger" title="'.Mage::helper('adminhtml')->__('Open Chooser').'" /></a>'.
			'<a href="javascript:void(0)" style="display: none;" class="rule-param-apply"><img src="'.$this->getSkinUrl('images/rule_component_apply.gif').
			'" class="v-middle" alt="'.$this->__('Apply').'" title="'.$this->__('Apply').'" /></a>'.
			'</span>'.
			'</span>'.
			'<div class="rule-chooser" url="'.$url.'" style="display: none;">'.
			'</li>';
		return $html;
    }
}
