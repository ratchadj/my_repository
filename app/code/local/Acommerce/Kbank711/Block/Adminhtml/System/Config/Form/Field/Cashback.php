<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Kbank711 to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Kbank711
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_Kbank711_Block_Adminhtml_System_Config_Form_Field_Cashback extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function __construct() {
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add');
        parent::__construct();
        $this->setTemplate('kbank711/system/config/form/field/array.phtml');
    }
	public function getChooserContainerId() {
		return 'config_cashback_chooser_id';
	}
    protected function _prepareLayout() {
		$this->addColumn('shop_id', array(
            'label' => Mage::helper('adminhtml')->__('Apply For'),
			'renderer' =>  $this->getLayout()->createBlock('kbank711/adminhtml_system_config_form_field_render_select'),
            'source'=>'kbank711/source_banks',
            'style' => 'width:120px',
        ));
		$this->addColumn('name', array(
            'label' => Mage::helper('adminhtml')->__('Description'),
            'style' => 'width:80px',
        ));
		$this->addColumn('products', array(
            'label' => Mage::helper('adminhtml')->__('Available For Products(SKUs)'),
            'style' => 'width:290px',
			'config_chooser_id'	=> 'config_cashback_chooser_id',
			'class'	=> 'input-text element-value-changer',
			'renderer' => $this->getLayout()->createBlock('kbank711/adminhtml_system_config_form_field_render_product')
        ));
		$this->addColumn('categories', array(
            'label' => Mage::helper('adminhtml')->__('Available For Categories(IDs)'),
            'style' => 'width:290px',
			'class'	=> 'input-text element-value-changer',
			'config_chooser_id'	=> 'config_cashback_chooser_id',
			'renderer' => $this->getLayout()->createBlock('kbank711/adminhtml_system_config_form_field_render_category')
        ));
		
        $this->addColumn('minimum', array(
            'label' => Mage::helper('adminhtml')->__('Minimum'),
            'style' => 'width:50px',
        ));
        $this->addColumn('valid_from', array(
            'label' => Mage::helper('adminhtml')->__('Valid From'),
            'style' => 'width: 70px',
            'type'	=> 'date',            
            'renderer' => $this->getLayout()->createBlock('kbank711/adminhtml_system_config_form_field_render_date')
        ));
        $this->addColumn('valid_to', array(
            'label' => Mage::helper('adminhtml')->__('Valid To'),
            'style' => 'width:70px',
            'type'	=> 'date',
            'renderer' => $this->getLayout()->createBlock('kbank711/adminhtml_system_config_form_field_render_date')
        ));
        return parent::_prepareLayout();
	}
    
    public function addColumn($name, $params) {
        $this->_columns[$name] = array(
            'label'     => empty($params['label']) ? 'Column' : $params['label'],
            'size'      => empty($params['size'])  ? false    : $params['size'],
            'style'     => empty($params['style'])  ? null    : $params['style'],
            'class'     => empty($params['class'])  ? null    : $params['class'],
            'type'  	=> empty($params['type']) ? '' : $params['type'],
            'source'  	=> empty($params['source']) ? '' : $params['source'],
            'config_chooser_id'  => empty($params['config_chooser_id']) ? '' : $params['config_chooser_id'],
            'renderer'  => false,
        );
        if ((!empty($params['renderer'])) && ($params['renderer'] instanceof Mage_Core_Block_Abstract)) {
            $this->_columns[$name]['renderer'] = $params['renderer'];
        }
    }
	
}
