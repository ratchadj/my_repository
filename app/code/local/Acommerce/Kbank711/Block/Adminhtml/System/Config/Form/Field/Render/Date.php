<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Onestepcheckout to newer
 * versions in the future.
 *
 * @category    EMST
 * @package     EMST_Onestepcheckout
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class Acommerce_Kbank711_Block_Adminhtml_System_Config_Form_Field_Render_Date extends Mage_Core_Block_Abstract
{
    protected function _toHtml() {		
		$column = $this->getColumn();
		$html = '<div style="min-width:93px;"><input type="text" id="#{_id}_'.$this->getColumnName().'" name="' . $this->getInputName() . '" value="#{' . $this->getColumnName() . '}" ' .
		($column['size'] ? 'size="' . $column['size'] . '"' : '') . ' class="' .
		(isset($column['class']) ? $column['class'] : 'input-text') . '"'.
		(isset($column['style']) ? ' style="'.$column['style'] . '"' : '') . '/>'.
		'<img src="' . Mage::getDesign()->getSkinUrl('images/grid-cal.gif') . '" alt="" class="v-middle" id="#{_id}_'.$this->getColumnName().'_trig" title="'.$this->htmlEscape(Mage::helper('adminhtml')->__('Date selector')).'" style="padding-left:3px;"/></div>';
		return $html;
    }
}
