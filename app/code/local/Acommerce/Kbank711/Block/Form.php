<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Kbank711 to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Kbank711
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/
class Acommerce_Kbank711_Block_Form extends Mage_Payment_Block_Form
{
	protected $_payModel = null;
	protected $_periods = null;
	protected $_cashback = null;
    protected function _construct() {
		parent::_construct();
        $this->setTemplate('kbank711/form.phtml');
    }
	public function getShopIds() {
		return $this->_getPaymentModel()->getShopIds();
	}
	public function getPeriods1() {
		if(is_null($this->_periods)) {
			$this->_periods = Mage::helper('kbank711')->getQuotePeriods();
		}
		return $this->_periods;
	}

	public function getCashback() {
		if(is_null($this->_cashback)) {
			$this->_cashback = Mage::helper('kbank711')->getCashbackForQuote($this->getQuote());
		}
		return $this->_cashback;
	}
	protected function _getConfig() {
        return Mage::getSingleton('payment/config');
    }

    /**
     * Retrieve availables credit card types
     *
     * @return array
     */
    public function getCcAvailableTypes() {
        $types = $this->_getConfig()->getCcTypes();
        if ($method = $this->getMethod()) {
            $availableTypes = $method->getCctypes();
            if (!empty($availableTypes)) {
                foreach ($types as $code=>$name) {
                    if (!in_array($code, $availableTypes)) {
                        unset($types[$code]);
                    }
                }
            }
        }
        return $types;
    }
	protected function _getPaymentModel() {
		if(!$this->_payModel) {
			$this->_payModel = Mage::getModel('kbank711/kbank711')->setOrder($this->getQuote());
		}
		return $this->_payModel;
	}
	public function getQuote() {
		return Mage::getSingleton('checkout/session')->getQuote();
	}
	public function getCustomText() {
		return $this->getMethod()->getCustomText();
	}


    public function getInstallment() {
        $item = $this->getInstallmentItem();

        if(!$item) {
            return false;
        }

        $installments = Mage::getResourceModel('campaigns/installment_collection')
                              ->getInstallmentByProduct($item->getProductId())
                              ->addFieldToFilter('main_table.campaign_id', array('eq' => $item->getCampaignId()));

        if($installments) {
            return $installments->getFirstItem();
        }
        else {
            return false;
        }

    }

    public function getPeriods() {

        $inst = $this->getInstallment();
        $results = array();
        if($inst) {
            $conditions = unserialize($inst->getConditions());
            $banks = unserialize(Mage::getStoreConfig('payment/kbank711/shop_id'));
            $installments = unserialize(Mage::getStoreConfig('payment/kbank711/installment/period'));

            $temp = array();
            foreach ($banks as $value) {
                $key = $value['code'];
                $temp[$key] = $value;
            }
            $banks = $temp;

            $temp = array();
            foreach ($installments as $value) {
                $key = $value['bank'].$value['code'];
                $temp[$key] = $value;
            }
            $installments = $temp;

            $curDate = Mage::app()->getLocale()->storeDate(null, null, true);
            $curDate = strtotime($curDate->toString('Y-m-d H:i:s', 'php'));
            $grandTotal = $this->getQuote()->getGrandTotal();

            foreach ($conditions as $key => $condition) {
                foreach ($condition as $key1 => $value) {
                   if(isset($banks[$key])) {
                        $bank = $banks[$key];
                        if (isset($installments[$bank['bank'].$key1])) {
                            $installment = $installments[$bank['bank'].$key1];

                            $validForm = $value[0];
                            $validTo = $value[1];

                            if(!empty($validForm) && !empty($validTo)) {
                                $validForm = strtotime($validForm);
                                $validTo = strtotime($validTo);
                                if($validForm <= $curDate && $validTo >= $curDate) {
                                    $k = trim($installment['bank'].$installment['period_value'].$installment['minimum'].$installment['code']);

                                    // (Total  (0.8 M)) +Total)/M
                                    $rate = (is_null($installment['minimum']) || empty($installment['minimum']))? 0 : $installment['minimum'];
                                    $perMonth = (($grandTotal * $rate) + $grandTotal) / $installment['period_value'];
                                    $perMonth = round($perMonth,2);
                                    $label = $installment['period_label'];
                                    if(strpos($label, '(') !== false) {
                                        $label = substr($installment['period_label'], 0, strpos($label, '(')-1) ;
                                    }
                                    $results[$k] = $label.$this->__(' installment amount %s', Mage::helper('core')->currency($perMonth));
                                }
                            } elseif(!empty($validForm) && empty($validTo)) {
                                $validForm = strtotime($validForm);
                                if($validForm <= $curDate) {
                                    $k = trim($installment['bank'].$installment['period_value'].$installment['minimum'].$installment['code']);
                                    $rate = (is_null($installment['minimum']) || empty($installment['minimum']))? 0 : $installment['minimum'];
                                    $perMonth = (($grandTotal * $rate) + $grandTotal) / $installment['period_value'];
                                    $perMonth = round($perMonth,2);
                                    $label = $installment['period_label'];
                                    if(strpos($label, '(') !== false) {
                                        $label = substr($installment['period_label'], 0, strpos($label, '(')-1) ;
                                    }
                                    $results[$k] = $label.$this->__(' installment amount %s', Mage::helper('core')->currency($perMonth));
                                }
                            } elseif(empty($validForm) && !empty($validTo)) {
                                $validTo = strtotime($validTo);
                                if($validTo >= $curDate) {
                                    $k = trim($installment['bank'].$installment['period_value'].$installment['minimum'].$installment['code']);
                                    $rate = (is_null($installment['minimum']) || empty($installment['minimum']))? 0 : $installment['minimum'];
                                    $perMonth = (($grandTotal * $rate) + $grandTotal) / $installment['period_value'];
                                    $perMonth = round($perMonth,2);
                                    $label = $installment['period_label'];
                                    if(strpos($label, '(') !== false) {
                                        $label = substr($installment['period_label'], 0, strpos($label, '(') - 1) ;
                                    }
                                    $results[$k] = $label.$this->__(' installment amount %s', Mage::helper('core')->currency($perMonth));
                                }
                            } else {
                                    $k = trim($installment['bank'].$installment['period_value'].$installment['minimum'].$installment['code']);
                                    $rate = (is_null($installment['minimum']) || empty($installment['minimum']))? 0 : $installment['minimum'];
                                    $perMonth = (($grandTotal * $rate) + $grandTotal) / $installment['period_value'];
                                    $perMonth = round($perMonth,2);
                                    $label = $installment['period_label'];
                                    if(strpos($label, '(') !== false) {
                                        $label = substr($installment['period_label'], 0, strpos($label, '(')-1) ;
                                    }
                                    $results[$k] = $label.$this->__(' installment amount %s', Mage::helper('core')->currency($perMonth));
                            }
                        }
                    }
                }
            }
        }
        return $results;
        //$periods = $this->_getPaymentModel()->getPeriods($this->getQuote(),false);
        //return $periods;
    }

    public function getInstallmentItem() {
        $quote = $this->getQuote();

        if(!is_null($quote)) {

            if(is_null($this->_installmentItem)) {
                $allItems = $quote->getAllItems();
                foreach ($allItems as $item) {

                    if ($item->getChildren()) {
                        continue;
                    }

                    $option = $item->getOptionByCode('info_buyRequest');
                    if ($option) {
                        $request = unserialize($option->getValue());
                        if (isset($request['trade_info'])) {
                            $trade = unserialize($request['trade_info']);
                            if($trade) {
                                if(isset($trade['installments']) && $trade['installments'] == 1) {
                                    $this->_installmentItem = $item;
                                    $this->_installmentItem->setCampaignId($trade['campaign_id']);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if(is_null($this->_installmentItem)) {
                $this->_installmentItem = false;
            }
            return $this->_installmentItem;
        }
        return false;
    }
}
