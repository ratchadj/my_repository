<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Kbank711 to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Kbank711
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_Kbank711_Block_Redirect extends Mage_Core_Block_Abstract
{
	protected function _toHtml()
	{
		$kbank711 = $this->getModel();
        $form = new Varien_Data_Form();
        $form
            ->setId('kbank711_payment_checkout')
            ->setName('kbank711_payment_checkout')
            ->setMethod('POST')
            ->setUseContainer(true);
        foreach ($kbank711->getStandardCheckoutFormFields() as $field => $value) {
            $form->addField($field, 'hidden', array('name' => $field, 'value' => $value));
        }
		$form->setAction($kbank711->getGatewayUrl());
        $formHTML = $form->toHtml();

        $html = '<html>';
		$html .= '<head><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></head>';
		$html .= '<body>';
        $html.= $this->__('You will be redirected to Kbank gateway in a few seconds.');
        $html.= $formHTML;
        $html.= '<script type="text/javascript">document.getElementById("kbank711_payment_checkout").submit();</script>';
        $html.= '</body></html>';


        return $html;
    }

}
