<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Kbank711 to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Kbank711
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_Kbank711_Block_Info extends Mage_Payment_Block_Info
{
	protected function _prepareSpecificInformation($transport = null)
    {
		$transport = parent::_prepareSpecificInformation($transport);
		if(!Mage::app()->getStore()->isAdmin()) {
			return $transport;
		}
        $payment = $this->getInfo();
        $transport->addData($payment->getAdditionalInformation());
        //$transport->addData(array('Status'=>$payment->getCcApproval()));
       // $transport->addData(array('Credit card transaction id'=>$payment->getCcTransId()));
		return $transport;
    }

}
