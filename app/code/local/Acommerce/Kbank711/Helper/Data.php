<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Kbank711 to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Kbank711
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_Kbank711_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getPeriodsForProduct($product) {
		return $this->_getPaymentModel()
				->setProduct($product)
				->getProductPeriods();
	}
	public function getQuotePeriods($quote=null) {		
		return $this->_getPaymentModel()->setQuote($quote)->getQuotePeriods();
	}
	public function getCashbackForProduct($product) {
		return $this->_getPaymentModel()
				->setProduct($product)
				->getCashBackForProduct();
	}
	public function getCashbackForQuote($quote=null) {		
		return $this->_getPaymentModel()->setQuote($quote)->getCashBackForQuote();
	}	
	public function getPormosQuote($quote=null) {
		return array_merge($this->getQuotePeriods(),$this->getCashbackForQuote());
	}
	public function getPromosProduct($product) {
		return array_merge($this->getCashbackForProduct($product),$this->getPeriodsForProduct($product));
	}
	protected function _getPaymentModel() {
		return Mage::getSingleton('kbank711/kbank711');
	}
}