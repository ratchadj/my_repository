<?php
class Acommerce_One23cash_Block_Info extends Mage_Payment_Block_Info
{
    
    protected function _prepareSpecificInformation($transport = null)
    {
         if(!Mage::app()->getStore()->isAdmin()) {
            return parent::_prepareSpecificInformation($transport);
         }
        $transport = parent::_prepareSpecificInformation($transport);
        $payment = $this->getInfo();
        return $transport->addData($payment->getAdditionalInformation());
    }
}
