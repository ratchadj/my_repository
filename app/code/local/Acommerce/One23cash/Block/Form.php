<?php
class Acommerce_One23cash_Block_Form extends Mage_Payment_Block_Form
{
	protected $_source = null;
	protected function _getSource() {
		if(is_null($this->_source)) {
			$this->_source = Mage::getSingleton('one23cash/source_channelAgents');
		}		
		return $this->_source;
	}
    protected function _construct() {
		parent::_construct();
        $this->setTemplate('one23cash/form.phtml');        
    }
	public function getValue($data) {
		if(!is_array($data)) {
			return $data;
		}
		return (isset($data['value']))?$data['value']:'';
	}
	public function getClass($data) {
		if(!is_array($data)) {
			return $data;
		}
		return (isset($data['class']))?$data['class']:'';
	}
	public function getLabel($data) {
		if(!is_array($data)) {
			return $data;
		}
		return (isset($data['label']))?$data['label']:'';
	}
}
