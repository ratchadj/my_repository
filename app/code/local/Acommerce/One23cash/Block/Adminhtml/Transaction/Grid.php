<?php
class Acommerce_One23cash_Block_Adminhtml_Transaction_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct() {
		parent::__construct();
		$this->setId('one23Grid');
		$this->setDefaultSort('created_at');
		$this->setDefaultDir('ASC');
		$this->setUseAjax(true);		
		$this->setSaveParametersInSession(true);
	}

	protected function _prepareCollection() {
		$collection = Mage::getResourceModel('one23cash/order_grid_collection')->addTransactionTracking();
		$this->setCollection($collection);		
		return parent::_prepareCollection();
	}
	protected function _prepareColumns() {
		$this->addColumn('increment_id', array(
            'header'        => Mage::helper('one23cash')->__('#'),
            'align'         => 'left',
            'width'         => '20px',
            'index'         => 'increment_id',
            'filter_index'  => 'main_table.increment_id',
        ));        
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id',
                             array(
                'header' => Mage::helper('sales')->__('Purchased From (Store)'),
                'index' => 'store_id',
                'type' => 'store',
				'width' => '150px',
                'store_view' => true,
                'display_deleted' => true,
            ));
        }
		$this->addColumn('created_at',
                         array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '150px',
        ));
		$this->addColumn('inquery_created_at',
                         array(
            'header' => Mage::helper('sales')->__('Inquery At'),
            'index' => 'inquery_created_at',
			'default' => 'Not query yet',
            'type' => 'datetime',
            'width' => '80px',
        ));
		$this->addColumn('customer_id',
                         array(
            'header' => Mage::helper('sales')->__('Customer ID'),
            'index' => 'customer_id',
            'width' => '50px',
        ));

        /* $this->addColumn('billing_name',
                         array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));

        $this->addColumn('customer_email',
						 array(
			'header' => Mage::helper('sales')->__('Customer Email'),
			'index' => 'customer_email',
			'is_system' => !$this->isAdmin()
		)); */
		$this->addColumn('grand_total',
                         array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type' => 'currency',
            'currency' => 'order_currency_code',
        ));
		$this->addColumn('status',
                         array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));
		$this->addColumn('channel_code',
                         array(
            'header' => Mage::helper('sales')->__('Channel'),
            'index' => 'channel_code',
			'default' => 'Not query yet',
        ));
		$this->addColumn('agent_code',
                         array(
            'header' => Mage::helper('sales')->__('Agent'),
			'default' => 'Not query yet',
            'index' => 'agent_code',
        ));
		
		$this->addColumn('trans_status',
                         array(
            'header' => Mage::helper('sales')->__('Transaction Status'),
            'index' => 'trans_status',			
            'type' => 'options',
            'options' => Mage::getSingleton('one23cash/source_responseCode')->getResponseCode(),
        ));
		$this->addColumn('sent_sms',
                         array(
            'header' => Mage::helper('sales')->__('Sent SMS'),
            'index' => 'sent_sms',			
            'type' => 'options',
            'options' => array(
				''=>Mage::helper('sales')->__('No'),
				Acommerce_One23cash_Model_One23cash::SMS_STATUS_FAILED=>Mage::helper('sales')->__('Failed'),
				Acommerce_One23cash_Model_One23cash::SMS_STATUS_SENT=>Mage::helper('sales')->__('Yes'),
			)
        ));
		$this->addColumn('notify_sms',
                         array(
            'header' => Mage::helper('sales')->__('Sent Notification SMS'),
            'index' => 'notify_sms',			
            'type' => 'options',
            'options' => array(
				''=>Mage::helper('sales')->__('No'),
				Acommerce_One23cash_Model_One23cash::SMS_STATUS_FAILED=>Mage::helper('sales')->__('Failed'),
				Acommerce_One23cash_Model_One23cash::SMS_STATUS_SENT=>Mage::helper('sales')->__('Yes'),
			)
        ));
		return parent::_prepareColumns();
	}
	protected function _prepareMassaction()
    {
        $this->setMassactionIdField('increment_id');
        $this->getMassactionBlock()->setFormFieldName('order_id');

        $this->getMassactionBlock()->addItem('query', array(
             'label'    => Mage::helper('one23cash')->__('Query'),
             'url'      => $this->getUrl('*/*/query'),
             'confirm'  => Mage::helper('one23cash')->__('Are you sure?')
        ));
		// $this->getMassactionBlock()->addItem('invoice', array(
             // 'label'    => Mage::helper('one23cash')->__('Invoice'),
             // 'url'      => $this->getUrl('*/*/invoice'),
             // 'confirm'  => Mage::helper('one23cash')->__('Are you sure?')
        // ));
		$this->getMassactionBlock()->addItem('queryInvoice', array(
             'label'    => Mage::helper('one23cash')->__('Query and Invoice'),
             'url'      => $this->getUrl('*/*/queryInvoice'),
             'confirm'  => Mage::helper('one23cash')->__('Are you sure?')
        ));
        return $this;
    }
	public function getGridUrl() {
		return $this->getUrl('*/*/grid',array('_current'=>true));
	}
	public function getRowUrl($row) {		
		return '#';//$this->getUrl('*/*/detail',array('id'=>$row->getIncrementId()));
	}
	
}
