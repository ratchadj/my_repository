<?php
class Acommerce_One23cash_Block_Adminhtml_Transaction extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct() {
		$this->_controller = 'adminhtml_transaction';
		$this->_blockGroup = 'one23cash';
		$this->_headerText = Mage::helper('one23cash')->__('123Cash Transaction Tracking');
		parent::__construct();
		$this->_removeButton('add');
	}
	
}
