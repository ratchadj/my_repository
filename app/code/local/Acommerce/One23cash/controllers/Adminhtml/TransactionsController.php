<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class Acommerce_One23cash_Adminhtml_TransactionsController extends Mage_Adminhtml_Controller_Action
{
	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('acommerce/123payment')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Transactions Tracking'), Mage::helper('adminhtml')->__('Transactions Tracking'));
		
		return $this;
	}   
 
	public function indexAction() {
		$this->_initAction()
			->_addContent($this->getLayout()->createBlock('one23cash/adminhtml_transaction','acommerce_one23cash_transactions'))
			->renderLayout();
	}

	public function gridAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('one23cash/adminhtml_transaction_grid')->toHtml()
        );
    }
	public function queryInvoiceAction() {
		$this->_query(true);        
	}
	public function queryAction() {		
		$this->_query();       
	}
	protected function _query($invoice = false) {
		if ($ids = $this->getRequest()->getParam('order_id')) {
            try {
                
                if(!is_array($ids)) {
					$ids = array($ids);
				}
				$result = Mage::helper('one23cash')->transactionInquiry($ids,$invoice);
				$number = isset($result[0])?$result[0]:array(0,0);
				$query = 0;
				$inv = 0;
				if(is_array($number)) {
					$query = isset($number[1])?$number[1]:0;
					$inv = isset($number[1])?$number[1]:0;
				}
				if($query>0) {
					$this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('%s order(s) have been queried.',$query));
				}
				if($invoice  && $inv>0) {
					$this->_getSession()->addSuccess(Mage::helper('adminhtml')->__('%s order(s) have been invoiced.',$inv));
				}
				if(isset($result[1]) && is_array($result[1]) && !empty($result[1])) {
					foreach($result[1] as $k=>$message) {
						$this->_getSession()->addError($k.': '.$message);
					}
				}
            } catch (Exception $e){
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError(Mage::helper('adminhtml')->__("Order Id is not specified."));
        }
		 $this->_redirect('*/*/index');
	}
	public function invoiceAction() {
		if ($ids = $this->getRequest()->getParam('order_id')) {
            try {
                
                if(!is_array($ids)) {
					$ids = array($ids);
				}
				$inv = 0;				
				$fail = array();
				foreach($ids as $id) {
					$order = Mage::getModel('sales/order')->loadByIncrementId($id);
					if($order->getId()) {
						try {
							$invoice = $order->prepareInvoice();
							$invoice->register();
							$invoice->setEmailSent(true);
							$invoice->getOrder()->setCustomerNoteNotify(true);
							$invoice->getOrder()->setIsInProcess(true);
							$transactionSave = Mage::getModel('core/resource_transaction')
								->addObject($invoice)
								->addObject($invoice->getOrder());
							$transactionSave->save();	
							$inv++;
						}
						catch(Exception $e) {
							$this->_getSession()->addError($this->__('Can not invoice %s: '.$e->getMessage(),$id));
							continue;
						}
						try {
							$invoice->sendEmail(true);
						} catch (Exception $e) {
							Mage::logException($e);
							$this->_getSession()->addError($this->__('Unable to send the invoice email for %s.',$id));
						}
					}
					else {
						$this->_getSession()->addError($this->__('%s does not exist.',$id));
					}
				}
				
            } catch (Exception $e){
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
			if($inv>0) {
				$this->_getSession()->addError($this->__('%s order(s) have been invoiced.',$inv));
			}
        } else {
            $this->_getSession()->addError(Mage::helper('adminhtml')->__("Order Id is not specified."));
        }
		
	}
}
