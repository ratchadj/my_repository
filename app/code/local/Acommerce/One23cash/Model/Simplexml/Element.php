<?php
class Acommerce_One23cash_Model_Simplexml_Element extends Varien_Simplexml_Element
{

    protected function _asArray($isCanonical = false) {
        $result = array();
        if (!$isCanonical) {
            // add attributes
            foreach ($this->attributes() as $attributeName => $attribute) {
                if ($attribute) {
                    $result['@'][$attributeName] = (string)$attribute;
                }
            }
        }
        // add children values
        if ($this->hasChildren()) {
            foreach ($this->children() as $childName => $child) {				
				if($child->attributes()) {
					foreach ($child->attributes() as $attributeName => $attribute) {
						if ($attribute) {
							$result[(string)$attribute] = $child->_asArray($isCanonical);
						}
					}
					
				}
				elseif(!$child->hasChildren()) {
					$val = $child->_asArray($isCanonical);
					if(trim($val)) {
						$result[] = $val;
					}
				}
				else{
					$result[$childName] = $child->_asArray($isCanonical);
				}
            }
        } else {
            if (empty($result)) {
                // return as string, if nothing was found
                $result = (string) $this;
            } else {
                // value has zero key element
                $result[0] = (string) $this;
            }
        }
        return $result;
    }

}
