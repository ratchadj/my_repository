<?php
class Acommerce_One23cash_Model_Simplexml_config extends Varien_Simplexml_Config
{
    /**
     * Constructor
     *
     */
    public function __construct($sourceData=null) {
        $this->_elementClass = 'Acommerce_One23cash_Model_Simplexml_Element';
        parent::__construct($sourceData);
    }
	
}
