<?php
class Acommerce_One23cash_Model_Source_Comment
{
	public function getCommentText() {
		return Mage::helper('one23cash')->__('Need to <a href="%s" target="_blank">test</a>? You have to login as this account at frontend before go to sms testing page',Mage::getUrl('one23cash/payment_test'));
	}
	
}