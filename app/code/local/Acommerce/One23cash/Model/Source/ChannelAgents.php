<?php
class Acommerce_One23cash_Model_Source_ChannelAgents
{

	protected $_channelAgents = array(
		'IBANKING' 		=> array('label'=>	'Ibanking',
								 'value'=>	array(
												array('value'=>'BAY',	'label'	=> 'BAY','class'=>'bay-123cash'),
								 				array('value'=>'BBL',	'label'	=> 'BBL','class'=>'bbl-123cash'),
								 				//array('value'=>'CIMB',	'label'	=> 'CIMB','class'=>'cimb-123cash'),
								 				array('value'=>'KBANK',	'label'	=> 'KBANK','class'=>'kbank-123cash'),
								 				array('value'=>'KTB',	'label'	=> 'KTB','class'=>'ktb-123cash'),
												array('value'=>'SCB',	'label'	=> 'SCB','class'=>'scb-123cash'),
												//array('value'=>'TBANK',	'label'	=> 'TBANK','class'=>'tbank-123cash'),
												//array('value'=>'TMB',	'label'	=> 'TMB','class'=>'tmb-123cash'),
												//array('value'=>'UOB',	'label'	=> 'UOB','class'=>'uob-123cash')
											)
							),
		'OVERTHECOUNTER' => array('label'=>	'Over The Counter',
								 'value' =>	array(
												// array('value'=>'7ELEVEN',	'label'	=> '7ELEVEN','class'=>'seven-eleven-123cash'),
								 				// array('value'=>'TRUEMONEY',	'label'	=> 'TRUE MONEY','class'=>'truemoney-123cash'),
								 				array('value'=>'BIGC',		'label'	=> 'BIGC','class'=>'bigc-123cash'),
								 				array('value'=>'FAMILYMART','label'	=> 'FAMILYMART','class'=>'familymart-123cash'),
								 				array('value'=>'MPAY',		'label'	=> 'MPAY','class'=>'mpay-123cash'),
								 				array('value'=>'PAYATPOST',	'label'	=> 'PAY AT POST','class'=>'payatpost-123cash'),
												array('value'=>'TESCO',		'label'	=> 'TESCO','class'=>'tesco-123cash'),
												array('value'=>'TOT',		'label'	=> 'TOT','class'=>'tot-123cash')
											)
							),
		'ATM' 			=> array('label'=>	'ATM',
								 'value'=>	array(
								 				array('value'=>'BAY',	'label'	=> 'BAY','class'=>'bay-123cash'),
								 				array('value'=>'BBL',	'label'	=> 'BBL','class'=>'bbl-123cash'),
								 				//array('value'=>'CIMB',	'label'	=> 'CIMB','class'=>'cimb-123cash'),
								 				array('value'=>'KBANK',	'label'	=> 'KBANK','class'=>'kbank-123cash'),
								 				array('value'=>'KTB',	'label'	=> 'KTB','class'=>'ktb-123cash'),
												array('value'=>'SCB',	'label'	=> 'SCB','class'=>'scb-123cash'),
												//array('value'=>'TBANK',	'label'	=> 'TBANK','class'=>'tbank-123cash'),
												//array('value'=>'TMB',	'label'	=> 'TMB','class'=>'tmb-123cash'),
												//array('value'=>'UOB',	'label'	=> 'UOB','class'=>'uob-123cash')

											)
							),
		'BANKCOUNTER' 	=> array('label'=>	'Bank Counter',
								 'value'=>	array(
												array('value'=>'BAY',	'label'	=> 'BAY','class'=>'bay-123cash'),
								 				array('value'=>'BBL',	'label'	=> 'BBL','class'=>'bbl-123cash'),
								 				//array('value'=>'CIMB',	'label'	=> 'CIMB','class'=>'cimb-123cash'),
								 				array('value'=>'KBANK',	'label'	=> 'KBANK','class'=>'kbank-123cash'),
								 				array('value'=>'KTB',	'label'	=> 'KTB','class'=>'ktb-123cash'),
												array('value'=>'SCB',	'label'	=> 'SCB','class'=>'scb-123cash'),
												//array('value'=>'TBANK',	'label'	=> 'TBANK','class'=>'tbank-123cash'),
												//array('value'=>'TMB',	'label'	=> 'TMB','class'=>'tmb-123cash'),
												//array('value'=>'UOB',	'label'	=> 'UOB','class'=>'uob-123cash')
											)
							),
		'WEBPAY' 		=> array('label'=>	'WebPay',
								 'value'=>	array(
								 				array('value'=>'BAY',	'label'	=> 'BAY','class'=>'bay-123cash'),
												array('value'=>'BBL',	'label'	=> 'BBL','class'=>'bbl-123cash'),
												array('value'=>'KTB',	'label'	=> 'KTB','class'=>'ktb-123cash'),
												array('value'=>'SCB',	'label'	=> 'SCB','class'=>'scb-123cash'),
												array('value'=>'TMB',	'label'	=> 'TMB','class'=>'tmb-123cash'),
												array('value'=>'UOB',	'label'	=> 'UOB','class'=>'uob-123cash')
											)
							),
	);
	public function getAgentOptionArray() {
		$val = array();
		foreach($this->_channelAgents as $v=>$k) {
			if(is_array($k) && isset($k['value']) && is_array($k['value'])) {
				foreach($k['value'] as $ag) {
					if(!in_array($ag,$val)) {
						$val[] = $ag;
					}
				}
			}
		}
		return $val;
	}
	public function getChannelOption() {
		$val = array();
		foreach($this->_channelAgents as $v=>$k) {
			$val[$v] = $k['label'];
		}
		return $val;
	}
	public function getOverCounterOption() {
		return $this->_getAgentsWithChannelCode('OVERTHECOUNTER');
	}
	public function getAtmOption() {
		return $this->_getAgentsWithChannelCode('ATM');
	}
	public function getBankCounterOption() {
		return $this->_getAgentsWithChannelCode('BANKCOUNTER');
	}
	public function getIbankingOption() {
		return $this->_getAgentsWithChannelCode('IBANKING');
	}
	public function getWebPayOption() {
		return $this->_getAgentsWithChannelCode('WEBPAY');
	}
	public function toOptionArray() {
		$val = $this->_channelAgents;
		foreach($val as $code=>$value) {
			if(isset($value['value']) && is_array($value['value'])) {
				$this->_addChannelCodeToAgents($code,$val[$code]['value']);
			}
		}
		return $val;
	}
	protected function _addChannelCodeToAgents($code,array &$agents) {
		foreach($agents as $k=>$v) {
			if(is_array($v) && isset($v['value']) && isset($v['label'])) {
				$agents[$k]['value'] = $code.'_'.$v['value'];
			}
		}
	}
	protected function _getAgentsWithChannelCode($code) {
		$agents = $this->_channelAgents[$code]['value'];
		$this->_addChannelCodeToAgents($code,$agents);
		return $agents;
	}
}
