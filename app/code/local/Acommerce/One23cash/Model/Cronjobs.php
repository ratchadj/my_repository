<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Regions to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_One23Cash
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/
class Acommerce_One23cash_Model_Cronjobs
{
    public function sendSms() {
		$model = Mage::getModel('one23cash/one23cash')->setSmsType($model::SMS_TYPE_EXP_NOTIFY);
		if($model->isSmsEnabled()) {
			$duration = $model->getDurationTime();
			$before = $model->getExpIntervalTime();
			$orders = Mage::getResourceModel('one23cash/order_grid_collection')
						->add123CashMethod()
						->addTransactionTracking()
						->addSmsNotifyFailedStatusFilter()
						->addTimeFilter($duration,$before);
			foreach($orders as $order) {
				$order = Mage::getModel('sales/order')->loadByIncrementId($order->getIncrementId());
				$model->setOrder($order)->sendSms();
			}
		}
	}
}
