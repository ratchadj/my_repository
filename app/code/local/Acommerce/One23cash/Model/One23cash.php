<?php


/**
 * One23cash payment model
 *
 */
class Acommerce_One23cash_Model_One23cash extends Mage_Payment_Model_Method_Abstract
{
    protected 	$_code  = 'one23cash';
    protected 	$_formBlockType = 'one23cash/form';
	protected 	$_infoBlockType = 'one23cash/info';
    protected 	$_canAuthorize            = true;
    protected 	$_canUseInternal          = false;
    protected 	$_canUseForMultishipping  = false;
	protected 	$_xml = null;
	protected 	$_root = null;
	CONST		SMS_TYPE_NOTIFY = 'notify';
	CONST		SMS_TYPE_EXP_NOTIFY = 'exp_notification';
	CONST		SMS_STATUS_SENT = 1;
	CONST		SMS_STATUS_FAILED = 0;

	public function validateRedirect($order) {
		$method = $order->getPayment()->getMethodInstance();
		if($method->getCode() == $this->getCode()) {
			return true;
		}
		else {
			return false;
		}
	}

	public function isAvailable($quote=null)
	{
		$active = parent::isAvailable($quote);
      	if($quote && $active) {
			if($this->getConfigData('dev')) {
				$allowed_emails = explode(',',$this->getConfigData('email'));
				$customer_email=Mage::getSingleton('customer/session')->getCustomer()->getEmail();
				if(!in_array($customer_email,$allowed_emails)) {
					return false;
			    }
			}

			//check installment
            $allItems = $quote->getAllItems();
            foreach ($allItems as $item) {
                $option = $item->getOptionByCode('info_buyRequest');
                if ($option) {
                    $request = unserialize($option->getValue());
                    if (isset($request['trade_info'])) {
                        $trade = unserialize($request['trade_info']);
                        if($trade) {
                            if(isset($trade['installments']) && $trade['installments'] == 1) {
                                return false;
                            }
                        }
                    }
                }
            }
      	}
      	return $active;
	}

    public function getStatus()
	{
		return $this->getConfigData('order_status');
	}
	public function getGatewayUrl()
	{
		return $this->getConfigData('gateway');
	}

	public function getMerchantId()
	{
		return $this->getConfigData('merchant_id');
	}
	public function getMerchantUrl()
	{
		return Mage::getUrl('one23cash/payment/merchant',array('_secure' => true));
	}
	public function getApiCallUrl() {
		return Mage::getUrl('one23cash/payment/apiCallUrl',array('_secure' => true));
	}
	public function getOrderPlaceRedirectUrl()
    {
        return Mage::getUrl('one23cash/payment/sendTransaction');
    }
	public function getVersion()
	{
		return $this->getConfigData('version');
	}
	public function getPassphrase() {
		return $this->getConfigData('passphrase');
	}
	public function getPublicKey() {
        return $this->getConfigData('one123_public_key');
	}
	public function getPublicKeyDecrypt() {
		return $this->getConfigData('public_key');
	}
	public function getPrivateKey() {
		return $this->getConfigData('private_key');
	}
	public function getKeepFiles() {
		return $this->getConfigData('keepfiles');
	}
	public function getKeepInquiryFiles() {
		return $this->getConfigData('keepinquiryfiles');
	}
	public function getSecretKey() {
		return $this->getConfigData('secret_key');
	}
	public function getInquiryUrl() {
		return $this->getConfigData('inquiry_url');
	}
	public function getSlipInfo() {
		return $this->getConfigData('slip_info');
	}
	public function getCountryCode() {
		return 'THA';
	}
	public function getSmsTypes() {
		return array(self::SMS_TYPE_NOTIFY,self::SMS_TYPE_EXP_NOTIFY);
	}
	protected function _validSmsType() {
		if(!in_array($this->getSmsType(),$this->getSmsTypes())) {
			return false;
		}
		return true;
	}
	public function getSmsConfig($field) {
		return ($this->_validSmsType())?trim($this->getConfigData("sms_settings/{$this->getSmsType()}/{$field}")):null;
	}
	public function getSmsGateway() {
		return trim($this->getConfigData('sms_settings/gateway'));
	}
	public function isSmsEnabled() {
		return $this->getSmsConfig('enable');
	}
	/* public function useCronNotify() {
		return $this->getSmsConfig('use_cron');
	} */
	public function getExpIntervalTime() {
		return ((int)$this->getSmsConfig('before'))*3600; //config is in hours, convert to seconds
	}
	public function getDurationTime() {
		return ((int)$this->getConfigData('sms_settings/duration'))*24*3600;//config is days, convert to seconds
	}
	public function getSmsTemplate() {
		return $this->getSmsConfig('template');
	}
	public function getTimestamp() {
		$millisec = round((microtime(true)-time())*1000);
		return Mage::getModel('core/date')->date('Y-m-d H:i:s:').$millisec;
	}
	public function getResponseData() {
		if(!$this->getData('response_data')) {
			return array(
							'Version' 		=> $this->getVersion(),
							'TimeStamp' 	=> $this->getTimestamp(),
							'MessageID' 	=> '',
							'Result' 		=> '',
							'FailureReason' => '',
						);
		}
		else {
			return $this->getData('response_data');
		}
	}
	public function getStandardCheckoutFormFields() {
        return array(
					   'OneTwoThreeReq'  => $this->getEncryptedReq()
					);
	}
	protected function getXml()
	{
		if(!$this->_xml) {
			$this->_xml = new DOMDocument('1.0','ISO-8859-1');
			$this->_xml->formatOutput = true;
		}
		return $this->_xml;
	}
	protected function getRoot($root)
	{
		if(!$this->_root) {
			$xml = $this->getXml();
			$this->_root = $xml->createElement($root);
			$xml->appendChild($this->_root);
		}
		return $this->_root;
	}
	public function getSimpleXml(array $array,$root = 'OneTwoThreeReq',$child = '')
	{
		$xml = $this->getXml();
		if($child) {
			$root = $child;
		}
		else {
			$root = $this->getRoot($root);
		}
        foreach($array as $k=>$v) {
			if(is_numeric($k) && is_array($v)) {
				foreach($v as $sub_el=>$att) {
					$sub_el=$xml->createElement($sub_el);
					if(is_array($att)) {
						foreach($att as $at1=>$av1) {
							$at1 = $xml->createAttribute($at1);
							$at1->value = $av1;
							$sub_el->appendChild($at1);
						}
					}
					else {
						throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Invalid element attribute'));
					}
					$root->appendChild($sub_el);
				}
			}
			elseif(!is_numeric($k)) {
				$k=$xml->createElement($k);
				$root->appendChild($k);
			}
			else {
				throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Invalid array format'));
			}
			if(!is_array($v)) {
				if($v) {
					$k->appendChild($xml->createTextNode($v));
				}
			}
            elseif(!is_numeric($k)) {

				$this->getSimpleXml($v,'',$k);

			}
        }

		if($child) {
			return;
		}

        $xml = $this->removeFirst($xml->saveXML());

		return $xml;
    }
	protected function getKey($type='Public',$theirs = false)
	{
		$key = null;
		if(($type=='Public')) {
			$ext = "cer";
			if($theirs) {
				$ext = "cer_decrypt";
				$key = $this->getPublicKeyDecrypt();
			}
			else {
				$key = $this->getPublicKey();
			}
		}
		else {
			$ext = "pem";
			$key = $this->getPrivateKey();
		}

		if(trim($key)) {
			return $key;
		}

		$key = glob(Mage::getBaseDir('lib').DS.'one23cash'.DS.'Cert'.DS.$type.DS."*.".$ext);
		if(empty($key)) {
			throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Cannot find '.$ext.' '.$type.' key file'));
		}
		return file_get_contents($key[0]);
	}

	public function writeRequest($xml,$type=array('Request','Xml','Encrypt'))
	{
		$io = new Varien_Io_File();
		$path = $this->getOutputFolder($type[0]);

		if($this->getOrder()) {
			$id = $this->getOrder()->getIncrementId();
		}
		else {
			$id = uniqid();
		}

        $file = $path . $type[1]. $id . '.txt';
        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $path));
        $io->write($file,$xml);
        $io->close();
		$encrypt = $path . $type[2].$id . '.txt';
		file_put_contents($encrypt, '');
		chmod($encrypt, 0777);
		return array('sourcefile'=>$file,'outputfile'=>$encrypt);
	}
	protected function removeFirst($xml)
	{
		$return ='';
		$lines = explode("\n", $xml, 2);
		if(!preg_match('/^\<\?xml/', $lines[0])) {
			$return = $lines[0];
		}
		$return .= $lines[1];
		return $return;
	}
	public function getUserDefined($num) {
		return $this->getConfigData('userDefined'.$num);
	}
	public function getPayCategoryID() {
		return $this->getConfigData('payCategoryID');
	}
	public function getCurrencyCode() {
		$code = $this->getConfigData('currencyCode');
		if(!$code) {
			$code = Mage::app()->getBaseCurrencyCode();
			$currencies = Mage::getModel('one23cash/source_currencyCode')->getCurrencyCode();
			$code = $currencies[$code];
		}

		return $code;
	}
	public function getInquiryRequestData() {
		$order = $this->getOrder();
		if(!$order) {
			throw Mage::exception('Mage_Payment',$this->_getHelper()->__('Order doesn\'t exist'));
		}
		$ref = $order->getPayment()->getAdditionalInformation('RefNo1');
		if(!$ref) {
			throw Mage::exception('Mage_Payment',$this->_getHelper()->__('No 123Cash RefNo for this order.'));
		}
		$amount = $order->getBaseGrandTotal()*100;
		$amount = str_pad($amount,12,'0',STR_PAD_LEFT);

		$orderPrefix = $this->getConfigData('order_prefix');
        if(strlen($orderPrefix)>0){
            $invNo = substr($order->getIncrementId(), strlen($orderPrefix));
        } else {
            $invNo = $order->getIncrementId();
        }

		$hash = $this->hashData($this->getMerchantId().$invNo.$amount);
		$req = array(
			'Version'		=> $this->getVersion(),
			'TimeStamp'		=> $this->getTimestamp(),
			'MessageID'		=> $this->_getInquiryId($order->getIncrementId()),
			'MerchantID'	=> $this->getMerchantId(),
			'InvoiceNo'		=> $invNo,
			'Amount'		=> $amount,
			'RefNo1'		=> $ref,
			'HashValue'		=> $hash,
			'UserDefined1'	=> $this->getUserDefined(1),
			'UserDefined2'	=> $this->getUserDefined(2),
			'UserDefined3'	=> $this->getUserDefined(3),
			'UserDefined4'	=> $this->getUserDefined(4),
			'UserDefined5'	=> $this->getUserDefined(5),
		);
		return $this->writeRequest($this->getSimpleXml($req,'InquiryReq'),array($this->getType(),'Xml','Encrypt'));
	}
	public function inquiry($order = null,$type = null,$asArray = true) {
		if($order && $order instanceof Mage_Sales_Model_Order) {
			$this->setOrder($order);
		}
		if(!$this->getType()) {
			$this->setType('Inquiry');
		}
		$data = $this->getInquiryRequestData();
		$encrypted = $this->setDatafile($data['sourcefile'])
					->setEncryptfile($data['outputfile'])
					->encryptData($this->getKeepInquiryFiles())
					->getEncryptedReq();
		$data = http_build_query(array('InquiryReq'=>$encrypted));
        $result = $this->curlQuery($this->getInquiryUrl(),$data);
		$decrypted = null;
		if ($result) {
			$this->setType('Inquiry_Response')
				->setEncryptResponse($result)
				->decryptData();
			if($asArray) {
				$decrypted = Mage::getModel('core/config_base',$this->getDecryptedRes())->getNode()->asArray();
			}
			else {
				$decrypted = $this->getDecryptedRes();
			}
		}
		return $decrypted;

	}
	public function curlQuery($url,$data,$method = Zend_Http_Client::POST) {
		$http = new Varien_Http_Adapter_Curl();
		$http->write($method,$url, '1.1', array('Content-Type: text/xml'), $data);
        $result = $http->read();
		if (Zend_Http_Response::extractCode($result) == 200) {
			$result = preg_split('/^\r?$/m', $result, 2);
			$result = trim($result[1]);
			return $result;
		}
		return null;
	}
	protected function _getInquiryId($orderId) {
		return $orderId.'-'.microtime(true);
	}
	public function getRequestData()
	{
		$order = $this->getOrder();
		$detail = '';
		$items = array();
		$i = 1;
		foreach ($order->getItemsCollection(array(),true) as $item) {
			if ($item->isDeleted()) {
                continue;
            }
			$itemName = Mage::helper('core')->escapeHtml($item->getNetsuiteItemName());
			if(!$itemName) {
				$itemName = $item->getSku();
			}
			$detail .= ($detail)?'-'.$itemName:$itemName;
			$price = $item->getBaseRowTotalInclTax()*100;
			$price = str_pad($price,12,'0',STR_PAD_LEFT);
			$items[] = array('PaymentItem'=>array('id'=>$i,'name'=>$itemName,'price'=>$price,'quantity'=>$item->getQtyOrdered()));
			$i++;
		}

		$amount = $order->getBaseGrandTotal()*100;
		$amount = str_pad($amount,12,'0',STR_PAD_LEFT);
		$customer = Mage::getSingleton('customer/session')->getCustomer();
		if($customer->getEmail()!=''){
			$name = $customer->getFirstname().' '.$customer->getLastname();
			$email = $customer->getEmail();
		} else {
			$name = $order->getCustomerFirstname().' '.$order->getCustomerLastname();
			$email = $order->getCustomerEmail();
		}

		$payment = $this->getOrder()->getPayment();
		$channel = $payment->getAdditionalInformation('Channel');
		$agent = $payment->getAdditionalInformation('Bank');

		$orderPrefix = $this->getConfigData('order_prefix');
        if(strlen($orderPrefix)>0){
            $invNo = substr($order->getIncrementId(), strlen($orderPrefix));
        } else {
            $invNo = $order->getIncrementId();
        }

		$hash = $this->hashData($this->getMerchantId().$invNo.$amount);


		$req_data = array(
			'Version' 					=> $this->getVersion(),
			'TimeStamp' 				=> $this->getTimestamp(),
			'MessageID' 				=> $order->getIncrementId(),
			'MerchantID' 				=> $this->getMerchantId(),
			'InvoiceNo' 				=> $invNo,
			'Amount' 					=> $amount,
			'Discount' 					=> str_pad('00',12,'0',STR_PAD_LEFT),
			'ServiceFee' 				=> str_pad('00',12,'0',STR_PAD_LEFT),
			'ShippingFee' 				=> str_pad('00',12,'0',STR_PAD_LEFT),
			'CurrencyCode' 				=> $this->getCurrencyCode(),
			'CountryCode' 				=> $this->getCountryCode(),
			'ProductDesc' 				=> substr($detail,0,255),
			'PaymentItems' 				=> $items,
			'PayerName' 				=> $name,
			'PayerEmail' 				=> $email,
			'ShippingAddress' 			=> '',
			'MerchantUrl' 				=> $this->getMerchantUrl(),
			'APICallUrl' 				=> $this->getApiCallUrl(),
			'AgentCode' 				=> $agent,
			'ChannelCode' 				=> $channel,
			'PayInSlipInfo' 			=> $this->getSlipInfo(),
			'userDefined1' 				=> $this->getUserDefined(1),
			'userDefined2' 				=> $this->getUserDefined(2),
			'userDefined3' 				=> $this->getUserDefined(3),
			'userDefined4' 				=> $this->getUserDefined(4),
			'userDefined5' 				=> $this->getUserDefined(5),
			'HashValue' 				=> $hash,

		);

		$xml = $this->getSimpleXml($req_data);
		return $this->writeRequest($xml);
	}

	protected function getOutputFolder($type) {
		return Mage::getBaseDir('var') . DS . 'one23cash' . DS.$type.DS;
	}

	public function encryptData($keep = null,$type = null)
	{
		if($type) {
			$this->setType($type);
		}
		if(!$this->getType()) {
			$this->setType('Request');
		}
		if(!$keep) {
			$keep = $this->getKeepFiles();
		}
		$data = $this->getDatafile();
		$encrypt = $this->getEncryptfile();
		$encrypted_req = '';

		if(openssl_pkcs7_encrypt($data, $encrypt, $this->getKey(), array())) {
			$io = new Varien_Io_File();
			$io->setAllowCreateFolders(true);
			$io->open(array('path' => $this->getOutputFolder($this->getType())));
			$encrypted_req = $io->read($encrypt);
			$io->close();
			if(!$keep) {
				$io->rm($data);
				$io->rm($encrypt);
			}

		}
		else {
			throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Fail to encrypt data'));
		}
		$encrypted_req = trim($this->removeHeader($encrypted_req));

		$this->setEncryptedReq($encrypted_req);
		return $this;
	}
	protected function removeHeader($content)
	{
		$content=str_replace("MIME-Version: 1.0","",$content);
		$content=str_replace("Content-Disposition: attachment; filename=\"smime.p7m\"","",$content);
		$content=str_replace("Content-Type: application/x-pkcs7-mime; smime-type=enveloped-data; name=\"smime.p7m\"","",$content);
		$content=str_replace("Content-Transfer-Encoding: base64","",$content);
		return $content;
	}
	public function decryptData()
	{
		$encrypt_response = 'MIME-Version: 1.0'.PHP_EOL.
		'Content-Disposition: attachment; filename="smime.p7m"'.PHP_EOL.
		'Content-Type: application/x-pkcs7-mime; smime-type=enveloped-data; name="smime.p7m"'.PHP_EOL.
		'Content-Transfer-Encoding: base64'.PHP_EOL.PHP_EOL.
		$this->getEncryptResponse();

		// add by yoa
		$encrypt_response=wordwrap($encrypt_response, 64, "\n", true);

		$src = $this->writeRequest($encrypt_response,array($this->getType(),'Encrypt','Decrypt'));
		$encrypted = $src['sourcefile'];
		$decrypted = $src['outputfile'];
		$passphrase = $this->getPassphrase();
		$private_key = array($this->getKey('Private'), $passphrase);
		if (openssl_pkcs7_decrypt($encrypted, $decrypted, $this->getKey('Public',true), $private_key)) {
			$io = new Varien_Io_File();
			$io->setAllowCreateFolders(true);
			$io->open(array('path' => $this->getOutputFolder($this->getType())));
			$decrypted_res = Mage::getModel('core/config_base',$io->read($decrypted))->getNode();
			if($this->getKeepFiles() && ($orderid = (string) $decrypted_res->InvoiceNo)) {
				$io->mv($encrypted,$this->getOutputFolder($this->getType()).'Encrypt'.$orderid.'.txt');
				$io->mv($decrypted,$this->getOutputFolder($this->getType()).'Decrypt'.$orderid.'.txt');
				$io->close();
			}
			else {
				$io->close();
				$io->rm($encrypted);
				$io->rm($decrypted);
			}

		}
		else {
			throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Fail to decrypt data'));
		}
		$this->setDecryptedRes($decrypted_res);
		return $this;
	}

	public function decryptData1()
	{
		$encrypt_response = 'MIME-Version: 1.0'.PHP_EOL.
		'Content-Disposition: attachment; filename="smime.p7m"'.PHP_EOL.
		'Content-Type: application/x-pkcs7-mime; smime-type=enveloped-data; name="smime.p7m"'.PHP_EOL.
		'Content-Transfer-Encoding: base64'.PHP_EOL.PHP_EOL.
		$this->getEncryptResponse();

		// add by yoa
		$encrypt_response=wordwrap($encrypt_response, 64, "\n", true);

		$src = $this->writeRequest($encrypt_response,array($this->getType(),'Encrypt','Decrypt'));
		$encrypted = $src['sourcefile'];
		$decrypted = $src['outputfile'];
		$passphrase = $this->getPassphrase();
		$private_key = array($this->getKey('Private'), $passphrase);

		echo $encrypted .'<br/>'. $decrypted.'<br/>';
		if (openssl_pkcs7_decrypt($encrypted, $decrypted, $this->getKey('Public',true), $private_key)) {

			echo $this->getOutputFolder($this->getType()).'ccccc<br/>';
			/*$io = new Varien_Io_File();
			$io->setAllowCreateFolders(true);
			$io->open(array('path' => $this->getOutputFolder($this->getType())));
			$decrypted_res = Mage::getModel('core/config_base',$io->read($decrypted))->getNode();
			if($this->getKeepFiles() && ($orderid = (string) $decrypted_res->InvoiceNo)) {
				$io->mv($encrypted,$this->getOutputFolder($this->getType()).'Encrypt'.$orderid.'.txt');
				$io->mv($decrypted,$this->getOutputFolder($this->getType()).'Decrypt'.$orderid.'.txt');
				$io->close();
			}
			else {
				$io->close();
				$io->rm($encrypted);
				$io->rm($decrypted);
			}*/

		}
		else {
			echo 'xxxxxxxxxxxxxxxx';
			throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Fail to decrypt data'));
		}
		//$this->setDecryptedRes($decrypted_res);
		//return $this;
	}

	protected function sendMail($type,$data) {
		try {
			mail('kaittisak@acommerce.asia', $data['subject'], $data);
		}
		catch(Exception $e) {
			Mage::logException($e);
		}
	}
	public function hashData($data)
	{
		$signData = hash_hmac('sha1', $data, $this->getSecretKey(), false);
		$signData =  strtoupper($signData);
		return urlencode($signData);
	}
	public function logRequest()
	{
		$this->importPaymentInfo($this->getParsedData())->save();
		//$order->save();
	}
	public function parseXml($xml)
	{
		$return = array();
		foreach($xml as $k=>$v) {
			$return[$k] = (string)$v;
		}
		return $return;
	}
	public function getSmsXmlTemplate($channel = null,$agent = 'default') {
		if(!$channel) {
			return null;
		}
		$temps = Mage::getModel('one23cash/simplexml_config',$this->getSmsTemplate())->getNode()->asCanonicalArray();
		$template = null;
		$default = null;
		foreach($temps as $k=>$temp) {
			if(strtolower($k)==strtolower($channel)) {
				if($temp && is_array($temp)) {
					foreach($temp as $temAgent=>$val) {
						if(strtolower($agent)==strtolower($temAgent)) {
							$template = $temp;
							break;
						}
					}

				}
				break;
			}
			if(strtolower($k) =='default') {
				$default = $temp;
			}
		}
		if(is_null($template)) {
			$template = $default;
		}
		return $this->translateTemplate($template);
	}
	public function translateTemplate($data) {
		$val = null;
		if($data && is_array($data)) {
			foreach($data as $k=>$v) {
				if(is_array($v)) {
					$val .= $this->translateTemplate($v);
				}
				else {
					$val .= $this->_getHelper()->__($k).$v.PHP_EOL;
				}
			}
		}
		return $val;
	}
	protected function _validateChannelAgent($code) {
		$code = strtoupper($code);
		$config = explode(',',$this->getSmsConfig('for'));
		if(in_array($code,$config)) {
			return true;
		}
		return false;
	}
	public function sendSms() {
		try {
			$test = $this->getSmsActionType()=='test';
			$sent = self::SMS_STATUS_SENT;
			$failed = self::SMS_STATUS_FAILED;
			if($test) {
				if(!$this->_validSmsType()) {
					return $this->_getHelper()->__('Invalid SMS type');
				}
				if(!$this->getSmsGateway()) {
					return $this->_getHelper()->__('No SMS gateway was defined');
				}
				if(!$this->_validateChannelAgent($code)) {
					return $this->_getHelper()->__('SMS is not enabled for this method: %s',$code);
				}
				$channel = $this->getChannel();
				$agent = $this->getAgent();
				$telephone = $this->getTelephone();
				$ref1 = $this->getRef1();
				$amount = $this->getAmount();
				$increment = $this->getIncrement();
				$createdAt = $this->getCreatedAt();
			}
			else {
				if(!$this->getOrder() || !$this->getOrder()->getId()) {
					return false;
				}
				elseif(!$this->isSmsEnabled() || !$this->_validSmsType()||!$this->getSmsGateway()) {
					$this->saveTransactionData($failed);
					return false;
				}

				$payment = $this->getOrder()->getPayment();
				$channel = $payment->getAdditionalInformation('Channel');
				$agent = $payment->getAdditionalInformation('Bank');
				$telephone = $this->getOrder()->getBillingAddress()->getTelephone();
				$ref1 = $payment->getAdditionalInformation('RefNo1');
				$amount = $this->getOrder()->getBaseGrandTotal();
				$increment = $this->getOrder()->getIncrementId();
				$createdAt = $this->getOrder()->getCreatedAt();
			}
			$code = strtoupper($channel.'_'.$agent);
			if(!$this->_validateChannelAgent($code)) {
				if($test) {
					return $this->_getHelper()->__('SMS is not enabled for this method: %s',$code);
				}
				else {
					$this->saveTransactionData($failed);
					return false;
				}
			}
			$template = $this->getSmsXmlTemplate($channel,$agent);

			if($template) {
				$createdAt = strtotime($createdAt) + $this->getDurationTime();
				$createdAt = Mage::getModel('core/date')->date('d/m/Y H:i:s',$createdAt);
				$templateVars = array(
					'agent'=>ucfirst(strtolower($agent)),
					'amount'=>sprintf('%s',number_format($amount,2,'.',',')),
					'payment_ref'=>$ref1,
					'order_id'=>$increment,
					'expire'=>$createdAt,
				);
				$data['msisdn'] = $telephone;
				$data['message'] = $this->getFilter($templateVars)->filter($template);

				$data = http_build_query($data);

				$url = $this->getSmsGateway().'&'.$data;
				$result = $this->curlQuery($url,null,Zend_Http_Client::GET);
				if($test) {
					return $result;
				}
				$result = Mage::getModel('core/config_base',$result)->getNode();
				if($result) {
					$status = $failed;
					if($result->Status) {
						$status = $sent;
					}
					$this->saveTransactionData($status);
					return $result->Status;
				}
				$this->saveTransactionData($failed);
				return false;
			}
			elseif($test) {
				return $this->_getHelper()->__('No SMS Template, please check configuration');
			}
		}
		catch(Exception $e) {
			if($test) {
				return $e->__toString();
			}
			Mage::log("\n" . $e->__toString(), Zend_Log::ERR, 'smsError.log');
		}
		if($test) {
			return $this->__getHelper()->__('Failed to send sms');
		}
		else {
			return false;
		}
	}
	public function saveTransactionData($sent_sms=self::SMS_STATUS_FAILED) {
		$order = $this->getOrder();
		$data = array();
		$transactionModel = Mage::getModel('one23cash/transaction')->load($order->getIncrementId());
		if(!$transactionModel->getId()) {
			$transactionModel->setId($order->getIncrementId());
			$payment = $this->getOrder()->getPayment();
			$data = array(
				'order_increment'=>$order->getIncrementId(),
				'transaction_id'=>$payment->getAdditionalInformation('RefNo1'),
				'payer_email'=>$order->getCustomerEmail(),
				'paid_amount'=>$order->getBaseGrandTotal(),
				'agent_code'=>$payment->getAdditionalInformation('Bank'),
				'channel_code'=>$payment->getAdditionalInformation('Channel'),
				'created_at'=>Varien_Date::now(),
				// 'status'=>'', //not quired yet
				// 'additional_info'=>'', //not add additional info
				//'sent_sms'=>$sent_sms,
			);
			$transactionModel->setData($data);
		}
		if($this->getSmsType()==self::SMS_TYPE_NOTIFY) {
			$transactionModel->setSentSms($sent_sms);
		}
		else {
			$transactionModel->setNotifySms($sent_sms);
		}
		return $transactionModel->save();
	}
	public function getFilter(array $vars) {
		$filter = Mage::getModel('core/email_template_filter');
		$filter->setVariables($vars);
		return $filter;
	}
	protected function getSuccessInfo($parsed)
	{
		$res_code = Mage::getModel('one23cash/source_responseCode')->getResponseCode();
		$ResponseCode = isset($res_code[$parsed['ResponseCode']])? $res_code[$parsed['ResponseCode']]: $parsed['ResponseCode'];
		return array(
			'Version' 			=> $parsed['Version'],
			'TimeStamp' 		=> $parsed['TimeStamp'],
			'CompletedDateTime' => $parsed['CompletedDateTime'],
			'MessageID' 		=> $parsed['MessageID'],
			'ResponseCode' 		=> $ResponseCode,
			'RefNo1' 			=> $parsed['RefNo1'],

		);
	}
	protected function importPaymentInfo($parsed) {
		$payment = $this->getOrder()->getPayment();
		$was = $payment->getAdditionalInformation();
		$new = array_merge($this->getSuccessInfo($parsed),$was);
		$payment->setAdditionalInformation($new);
		return $payment;
	}
	public function saveInvoice()
    {
		try {
			$order = $this->getOrder();
			$parsed = $this->getParsedData();
			$payment = $this->importPaymentInfo($parsed);
			$payment->setTransactionId($parsed['RefNo1'])
					->registerCaptureNotification($order->getGrandTotal());
			$order->save();
			$invoice = $payment->getCreatedInvoice();
			if ($invoice && !$order->getEmailSent()) {
				$order->sendNewOrderEmail()->addStatusHistoryComment(
						$this->_getHelper()->__('Notified customer about invoice #%s.', $invoice->getIncrementId(), 'processing')
					)
					->setIsCustomerNotified(true)
					->save();
			}
			/* if ($order->canShip()) {
				$shipment = $order->prepareShipment()->register();
				$shipment->getOrder()->setActionFlag(Mage_Sales_Model_Order::ACTION_FLAG_SHIP, true);
				Mage::getModel('core/resource_transaction')	->addObject($shipment->getOrder())
															->addObject($shipment)->save();
			} */
			$this->setSmsType(self::SMS_TYPE_NOTIFY)->sendSms();
			return true;
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null, 'One23cash.log');
		}
        return false;
    }
	public function assignData($data)
    {

		if(!$data) {
			return ;
		}
		if(!is_object($data) ) {
			$data = new Varien_Object($data);
		}
		$additional_inf = array();
		$channel_agent = explode('_',$data->getData('agent_'.$this->_code));
		if(sizeof($channel_agent)<=1) {
			return ;
		}
		$additional_inf['additional_information'] = array('Channel'=>strtoupper($channel_agent[0]),'Bank'=>strtoupper($channel_agent[1]));
       return parent::assignData($additional_inf);
    }
	protected function _getHelper() {
        return Mage::helper('one23cash');
    }


    public function getRequestData1()
	{
		$req_data = array(
			'Version' 					=> '1.1',
			'TimeStamp' 				=> '2015-06-10 14:07:22:234',
			'MessageID' 				=> 'WS15060900011',
			'MerchantID' 				=> '444',
			'InvoiceNo' 				=> '15060900011',
			'RefNo1' 				    => '2124918',
			'Amount' 					=> '000000309000',
			'PaidAmount' 				=> '000000309000',
			'ResponseCode' 				=> '000',
			'CompletedDateTime' 		=> '2015-06-10 13:58:15',
			'AgentCode' 				=> 'PAYATPOST',
			'ChannelCode' 				=> 'OVERTHECOUNTER',
			'UserDefined1' 				=> '',
			'UserDefined2' 				=> '',
			'UserDefined3' 				=> '',
			'UserDefined4' 				=> '',
			'UserDefined5' 			    => '',
			'HashValue' 				=> '5A396C3BFFFD78F29BAEF0838032ABE5569A2D8B',
			'SlipUrl' 				    => 'http://q.2c2p.com/sk9x'
		);

		$xml = $this->getSimpleXml($req_data);
		return $this->writeRequest($xml);
	}

	public function encryptData1($keep = null,$type = null)
	{
		if($type) {
			$this->setType($type);
		}
		if(!$this->getType()) {
			$this->setType('Request');
		}
		if(!$keep) {
			$keep = $this->getKeepFiles();
		}
		$data = $this->getDatafile();
		$encrypt = $this->getEncryptfile();
		$encrypted_req = '';

		if(openssl_pkcs7_encrypt($data, $encrypt, $this->getKey('Public',true), array())) {
			$io = new Varien_Io_File();
			$io->setAllowCreateFolders(true);
			$io->open(array('path' => $this->getOutputFolder($this->getType())));
			$encrypted_req = $io->read($encrypt);
			$io->close();
			if(!$keep) {
				$io->rm($data);
				$io->rm($encrypt);
			}

		}
		else {
			throw new Mage_Payment_Model_Info_Exception($this->_getHelper()->__('Fail to encrypt data'));
		}
		$encrypted_req = trim($this->removeHeader($encrypted_req));

		$this->setEncryptedReq($encrypted_req);
		return $this;
	}
}
