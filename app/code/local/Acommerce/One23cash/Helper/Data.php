<?php
class Acommerce_One23cash_Helper_Data extends Mage_Core_Helper_Abstract 
{
	protected function _validData($data) {
		if(!is_array($data)) {
			$data = array($data);
		}
		return array_merge(array(
					'Version'=>'',
					'TimeStamp'=>'',
					'MessageID'=>'',
					'MerchantID'=>'',
					'InvoiceNo'=>'',
					'RefNo1'=>'',
					'ResponseCode'=>'',
					'CompletedDateTime'=>'',
					'Amount'=>'',
					'PaidAmount'=>'',
					'PayerName'=>'',
					'PayerEmail'=>'',
					'SelectedAgentCode'=>'',
					'SelectedChannelCode'=>'',
					'PaymentAgentCode'=>'',
					'PaymentChannelCode'=>'',
					'ApprovalCode'=>'',
					'AgentRef'=>'',
					'ChannelService'=>'',
					'HashValue'=>'',
					'UserDefined1'=>'',
					'UserDefined2'=>'',
					'UserDefined3'=>'',
					'UserDefined4'=>'',
					'UserDefined5'=>'',
				),$data);
	}
	protected function _prepareDataForSave($data) {
		$data = $this->_validData($data);
		$paid = ((float)$data['PaidAmount'])/100;
		return array(			
			'transaction_id'=>$data['RefNo1'],
			'payer_email'=>$data['PayerEmail'],
			'paid_amount'=>$paid,
			'agent_code'=>$data['PaymentAgentCode'],
			'channel_code'=>$data['PaymentChannelCode'],
			'status'=>$data['ResponseCode'],
			'additional_info'=>array(
				'MessageID'=>$data['MessageID'],
				'CompletedDateTime'=>$data['CompletedDateTime'],
				'Amount'=>$data['Amount'],
				'PayerName'=>$data['PayerName'],
				'SelectedAgentCode'=>$data['SelectedAgentCode'],
				'SelectedChannelCode'=>$data['SelectedChannelCode'],
				'ApprovalCode'=>$data['ApprovalCode'],
				'AgentRef'=>$data['AgentRef'],
				'ChannelService'=>$data['ChannelService']				
			)
		);
	}
	public function rawInquiry($orderIds) {
		if(!is_array($orderIds)) {
			$orderIds = array($orderIds);
		}
		
		$query = 0;
		$messages = array();
		$result = '';		
		foreach($orderIds as $id) {
			$order = Mage::getModel('sales/order')->loadByIncrementId($id);			
			if($order->getId()) {
			
				try {
					$payModel = Mage::getModel('one23cash/one23cash');
					$is123 = $order->getPayment()->getMethod() == $payModel->getCode();					
					$result .= ($is123)?$payModel->inquiry($order,'Inquiry',false):null;
					if($result) {	
						$result .= '<br/>';
						++$query;
					}
					else {
						$messages[$id] = Mage::helper('one23cash')->__('Can not query the transaction from gateway');;
					}
				}
				catch(Exception $e) {
					$messages[$id] = $e->getMessage();
				}
			}
			else {
				$messages[$id] = Mage::helper('one23cash')->__('Order doesn\'t exist.');;
			}
		}
		return array($query,$result,$messages);
	}
	public function transactionInquiry($orderIds,$invoice = false) {
		if(!is_array($orderIds)) {
			$orderIds = array($orderIds);
		}
		
		$query = 0;
		$inv = 0;
		$messages = array();
		foreach($orderIds as $id) {
			$order = Mage::getModel('sales/order')->loadByIncrementId($id);			
			if($order->getId()) {
				try {					
					$payModel = Mage::getModel('one23cash/one23cash');
					$result = $payModel->inquiry($order,'Inquiry');
					if($result) {
						$resHash = isset($result['HashValue'])?$result['HashValue']:'';
						$data = $this->_prepareDataForSave($result);						
						if(!$payModel->load($id)->getId()) {	
							$data['order_increment'] = $id;
							$payModel->setId($id);
						}
						$transaction = $payModel->setData($data)->save();
						++$query;
						if($invoice && (string)$transaction->getStatus()=='000') {
							$hash = $payModel->hashData($payModel->getMerchantId().$id.$transaction->getTransactionId());
							if((string)$resHash == $hash && $order->canInvoice()) {
								$payModel->setOrder($order)
										->setParsedData($result)
										->saveInvoice();
									++$inv;
							}
						}
					}
					else {
						$messages[$id] = Mage::helper('one23cash')->__('Can not query the transaction from gateway');;
					}
				}
				catch(Exception $e) {
					$messages[$id] = $e->getMessage();
				}
			}
			else {
				$messages[$id] = Mage::helper('one23cash')->__('Order doesn\'t exist.');;
			}
		}
		return array(array($query,$inv),$messages);
	}
} 
