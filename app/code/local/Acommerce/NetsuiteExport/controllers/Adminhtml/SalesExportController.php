<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade NetsuiteExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_NetsuiteExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class Acommerce_NetsuiteExport_Adminhtml_SalesExportController extends Mage_Adminhtml_Controller_Action
{

	protected function _initAction() {
		$this->loadLayout()
			->_setActiveMenu('sales/netsuiteexport')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Order Export'), Mage::helper('adminhtml')->__('Order Export'));
		
		return $this;
	}   
 
	public function indexAction() {			
		$this->_initAction();
		$this->_addContent($this->getLayout()->createBlock('netsuiteexport/adminhtml_salesExport','salesorderexport'));
		$this->renderLayout();
	}
	public function gridAction() {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('netsuiteexport/adminhtml_salesExport_grid')->toHtml()
        );
    }
    public function exportCsvAction()
    {
        $fileName   = 'SalesExport.csv';
        $content    = $this->getLayout()->createBlock('netsuiteexport/adminhtml_salesExport_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'SalesExport.xml';
        $content    = $this->getLayout()->createBlock('netsuiteexport/adminhtml_salesExport_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }
}