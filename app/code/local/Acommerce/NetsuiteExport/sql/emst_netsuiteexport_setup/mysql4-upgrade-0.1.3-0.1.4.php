<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()->addColumn($this->getTable('sales/quote_item'), 'item_external_id', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'LENGTH'    => 11,
        'NULLABLE'  => true,
        'COMMENT'   => 'Item external id'
    ));
$installer->getConnection()->addColumn($this->getTable('sales/order_item'), 'item_external_id', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'LENGTH'    => 11,
        'NULLABLE'  => true,
        'COMMENT'   => 'Item external id'
    ));
$installer->getConnection()->addColumn($this->getTable('sales/invoice_item'), 'item_external_id', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'LENGTH'    => 11,
        'NULLABLE'  => true,
        'COMMENT'   => 'Item external id'
    ));
$installer->getConnection()->addColumn($this->getTable('sales/shipment_item'), 'item_external_id', array(
        'TYPE'      => Varien_Db_Ddl_Table::TYPE_INTEGER,
        'LENGTH'    => 11,
        'NULLABLE'  => true,
        'COMMENT'   => 'Item external id'
    ));
$installer->endSetup();
