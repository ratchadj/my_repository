<?php

$installer = $this;
$installer->startSetup();
$installer->getConnection()->changeColumn($this->getTable('netsuite_sales_order'), 'order_increment_id', 'order_increment_id',
        'varchar( 15 ) NOT NULL');
$installer->endSetup();
