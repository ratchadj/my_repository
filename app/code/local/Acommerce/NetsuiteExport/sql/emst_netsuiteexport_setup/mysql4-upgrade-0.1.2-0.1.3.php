<?php

$installer = $this;
$installer->startSetup();
$this->_conn->addColumn($this->getTable('sales/order'), 'account_type', 'smallint(5)');
$installer->addAttribute('customer', 'acc_type', array(
    'type'      		=> 'int',
    'label'     		=> 'Customer Account Type',
    'input'     		=> 'text',
    'visible'   		=> false,
    'is_system' 		=> false,
    'is_user_defined' 	=> true,
    'required'  		=> false
));
$installer->endSetup();
