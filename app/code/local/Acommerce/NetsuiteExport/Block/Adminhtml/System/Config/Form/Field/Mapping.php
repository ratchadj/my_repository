<?php
/**
 * Developed by Acommerce
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.Acommerce.asia/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade SalesExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_SalesExport
 * @copyright   Copyright (c) 2012 Acommerce (http://www.Acommerce.asia)
 * @license     http://www.Acommerce.asia/LICENSE-E.txt
*/

class Acommerce_NetsuiteExport_Block_Adminhtml_System_Config_Form_Field_Mapping extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    public function __construct() {
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('adminhtml')->__('Add');
        parent::__construct();
        $this->setTemplate('netsuiteexport/system/config/form/field/array.phtml');
    }

    public function addColumn($name, $params) {
        $this->_columns[$name] = array(
            'label'     => empty($params['label']) ? 'Column' : $params['label'],
            'size'      => empty($params['size'])  ? false    : $params['size'],
            'style'     => empty($params['style'])  ? null    : $params['style'],
            'class'     => empty($params['class'])  ? null    : $params['class'],
            'type'      => empty($params['type']) ? '' : $params['type'],
            'source'    => empty($params['source']) ? '' : $params['source'],
            'renderer'  => false,
        );
        if (isset($params['renderer']) && $params['renderer'] instanceof Mage_Core_Block_Abstract) {
            $this->_columns[$name]['renderer'] = $params['renderer'];
        }
    }

    protected function _prepareLayout() {
        $this->addColumn('code', array(
            'label' => Mage::helper('adminhtml')->__('Payment Method'),
            'style' => 'width:150px',
            'type'  => 'select',
            'renderer' =>  $this->getLayout()->createBlock('netsuiteexport/adminhtml_system_config_form_field_render_select'),
            'source'=>'netsuiteexport/source_payments'
        ));

        $this->addColumn('mapping', array(
            'label' => Mage::helper('adminhtml')->__('Display Payment'),
            'style' => 'width:120px',
        ));

        return parent::_prepareLayout();
    }
}
