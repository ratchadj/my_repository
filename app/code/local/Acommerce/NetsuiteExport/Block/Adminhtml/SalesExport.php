<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade NetsuiteExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_NetsuiteExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class Acommerce_NetsuiteExport_Block_Adminhtml_SalesExport extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_salesExport';
    $this->_blockGroup = 'netsuiteexport';
    $this->_headerText = Mage::helper('netsuiteexport')->__('Order Export');
    parent::__construct();
	$this->_removeButton('add');
  }
}