<?php
class Acommerce_NetsuiteExport_Block_Adminhtml_SalesExport_Render_Shipping extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text
{
    public function render(Varien_Object $row)
    {
        $options = $this->getColumn()->getOptions();
        $showMissingOptionValues = (bool)$this->getColumn()->getShowMissingOptionValues();
        if (!empty($options) && is_array($options)) {
            $value = $row->getData($this->getColumn()->getIndex());
            if (is_array($value)) {
                $res = array();
                foreach ($value as $item) {
					$found = $this->_getShippings($options,$item);
                    if ($found) {
                        $res[] = $this->escapeHtml($found);
                    }
                    elseif ($showMissingOptionValues) {
                        $res[] = $this->escapeHtml($item);
                    }
                }
                return implode(', ', $res);
            } else {
				$found = $this->_getShippings($options,$value);
				if($found) {
					return $this->escapeHtml($found);
				}
				elseif(in_array($value, $options)) {
					return $this->escapeHtml($value);
				}
            } 
        }
    }
	protected function _getShippings($options,$value) {
		foreach($options as $k=>$v) {
			if(strpos($value,$k)!==false) {
				return $v;
			}
		}
		return null;
	}
}