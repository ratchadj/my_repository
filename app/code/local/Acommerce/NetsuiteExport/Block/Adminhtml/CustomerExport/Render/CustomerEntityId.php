<?php
class Acommerce_NetsuiteExport_Block_Adminhtml_CustomerExport_Render_CustomerEntityId extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Text
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
		if($value) {
			return $this->_getPrefix().$value;
		}
		return $value;
    }
	protected function _getPrefix() {
		if(Mage::helper('core')->isModuleEnabled('Acommerce_IncrementPrefix')) {
			return Mage::helper('incrementprefix')->getPrefix('customer');
		}
		return null;
	}
}