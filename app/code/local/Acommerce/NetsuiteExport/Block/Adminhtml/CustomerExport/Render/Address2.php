<?php
class Acommerce_NetsuiteExport_Block_Adminhtml_CustomerExport_Render_Address2 extends Acommerce_NetsuiteExport_Block_Adminhtml_CustomerExport_Render_Address
{
    public function render(Varien_Object $row)
    {
        $value = $row->getData($this->getColumn()->getIndex());
		if($value) {
			return $this->escapeHtml($this->getBillingStreet($value,2));
		}
		return $value;
    }
}