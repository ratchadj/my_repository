<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade NetsuiteExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_NetsuiteExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/
class Acommerce_NetsuiteExport_Block_Adminhtml_CustomerExport_Widget_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

	// Netsuite Fields
	protected $_netsuite = array();
	/**
     * Add additional columns for csv export file
     *
     * @return array
     */
	protected function _getNetsuiteFields($item) {
		if(empty($this->_netsuite) || $this->_storeId != $item->getStoreId()) {
			$this->_netsuite = Mage::helper('netsuiteexport')->getNetsuiteConfigFields($item);
		}
		return $this->_netsuite;
	}
	protected function _addExtraColumns(array $data,$item) {
		$netsuite = $this->_getNetsuiteFields($item);
		$data[] = '"'.$netsuite['netsuite_status'].'"';
		$data[] = '"'.$netsuite['subsidiary'].'"';
		$data[] = '"T"';
		$data[] = '"T"';
		$data[] = '"T"';
		return $data;
	}
	protected function _addExtraHeader(array $data) {
		$data[] = '"Customer Status"';
		$data[] = '"Subsidiary"';
		$data[] = '"Individual"';
		$data[] = '"Default Billing"';
		$data[] = '"Default Shipping"';
		return $data;
	}
     /**
     * Retrieve Grid data as CSV
     *
     * @return string
     */
    public function getCsv()
    {
        $csv = '';
        $this->_isExport = true;
        $this->_prepareGrid();
        $this->getCollection()->getSelect()->limit();
        $this->getCollection()->setPageSize(0);
        $this->getCollection()->load();
        $this->_afterLoadCollection();

        $data = array();
        foreach ($this->_columns as $column) {
            if (!$column->getIsSystem()) {
                $data[] = '"'.$column->getExportHeader().'"';
            }
        }
		$data = $this->_addExtraHeader($data);
        $csv.= implode(',', $data)."\n";

        foreach ($this->getCollection() as $item) {
            $data = array();
            foreach ($this->_columns as $column) {
                if (!$column->getIsSystem()) {
                    $data[] = '"' . str_replace(array('"', '\\'), array('""', '\\\\'),
                        $column->getRowFieldExport($item)) . '"';
                }
            }
			$data = $this->_addExtraColumns($data,$item);
            $csv.= implode(',', $data)."\n";
        }

        return $csv;
    }
}
