<?php
class Acommerce_NetsuiteExport_Block_Adminhtml_Netsuiteconnect_Order_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct() {
        parent::__construct();
        $this->setId('netsuiteconnect');
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('desc');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection() {
        $collection = Mage::getResourceModel('netsuiteexport/sales_order_grid_collection')
                        ->addPaymentToSelect()
                        //->addNetsuiteStatus()
                        ;
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    protected function _prepareColumns() {
        $this->addColumn('export_status',
                         array(
            'header' => Mage::helper('sales')->__('Acommerce Status'),
            'index' => 'is_exported',
            'width' => '250px',
            'type' => 'options',
            'options' => Mage::getResourceModel('netsuiteexport/sales')->getStatusOptions(),
        ));
        $this->addColumn('payment_method',
                         array(
            'header' => Mage::helper('sales')->__('Payment Type'),
            'index' => 'payment_method',
            'type' => 'options',
            // 'width' => '120px',
            'options' => Mage::helper('netsuiteexport')->getStorePaymentMethods(),
        ));
        $this->addColumn('increment_id', array(
            'header'        => Mage::helper('netsuiteexport')->__('#'),
            'align'         => 'left',
            'width'         => '20px',
            'index'         => 'increment_id',
            'filter_index'  => 'main_table.increment_id',
        ));
        $this->addColumn('status',
                         array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            //'width' => '150px',
            'type' => 'options',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));
        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id',
                             array(
                'header' => Mage::helper('sales')->__('Purchased From (Store)'),
                'index' => 'store_id',
                'type' => 'store',
                //'width' => '150px',
                'store_view' => true,
                'display_deleted' => true,
            ));
        }
        $this->addColumn('created_at',
                         array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '150px',
        ));
        $this->addColumn('billing_name',
                         array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));
        /*

        $this->addColumn('customer_email',
                         array(
            'header' => Mage::helper('sales')->__('Customer Email'),
            'index' => 'customer_email',
            'is_system' => !$this->isAdmin()
        )); */
        $this->addColumn('grand_total',
                         array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type' => 'currency',
            'currency' => 'order_currency_code',
        ));

        return parent::_prepareColumns();
    }
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('entity_id');
        $this->setMassactionIdFieldOnlyIndexValue(true);
        $this->getMassactionBlock()->setFormFieldName('order_id');

        $this->getMassactionBlock()->addItem('export', array(
             'label'    => Mage::helper('netsuiteexport')->__('Export'),
             'url'      => $this->getUrl('*/*/export'),
             'confirm'  => Mage::helper('netsuiteexport')->__('Are you sure?')
        ));
        return $this;
    }
    public function getGridUrl() {
        return $this->getUrl('*/*/grid',array('_current'=>true));
    }
    public function getRowUrl($row) {
        return $this->getUrl('*/sales_order/view',array('order_id'=>$row->getId()));
    }

}
