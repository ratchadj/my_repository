<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class Acommerce_NetsuiteExport_Model_Observer
{
	public function setNetsuiteItemName($observer) {		
		$item = $observer->getEvent()->getQuoteItem();
		if($item->getNetsuiteItemName()) {
			return;
		}
		$product = $observer->getEvent()->getProduct();		
		$typeId = $product->getTypeId();	
		if($typeId == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
			$options = $product->getCustomOption('simple_product');
			if($options) {
				$simple = Mage::getModel('catalog/product')->load($options->getValue());				
			}
		}
		else {
			$simple = Mage::getModel('catalog/product')->load($product->getId());
		}
		if($simple) {
			$item->setNetsuiteItemName($simple->getNetsuiteItemName());
			//$item->setItemExternalId($simple->getItemExternalId());
		}
	}
	
	public function updateToNetsuite($observer) {
		try {
			$order = $observer->getEvent()->getAddress()->getOrder();
			$logItem = Mage::getModel('netsuiteexport/sales')->load($order->getIncrementId(),'order_increment_id');
			if($logItem->getId() && $logItem->getStatus() == Acommerce_Model_Resource_Sales::SUCCESS 
			&& Mage::helper('netsuiteexport')->isValidToUpdate($order)) {
				$logItem->setStatus(Acommerce_Model_Resource_Sales::UPDATE)->save();
				// Mage::getModel('netsuiteexport/cronjobs')->exportUpdatedOrders(array($order->getIncrementId()));
			}
			// if(Mage::helper('netsuiteexport')->isValidToUpdate($order)) {
								
			// }
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null,'OnOrderChangeUpdate.log');
		}
	}
	public function checkAccType($observer) {
		try {
			$order = $observer->getEvent()->getOrder();
			if($order->getAccountType() !== 0 && $order->getCustomerId()) {
				$customer = Mage::getModel('customer/customer')->load($order->getCustomerId());
				$type = null;
				
				if($customer->getAccType()!==Acommerce_Model_Cronjobs::NETSUITE_CUSTOMER_EXIST) {
					$coll = Mage::getResourceModel('sales/order_grid_collection');
					$logTable = Mage::getSingleton('netsuiteexport/sales')->getResource()->getTable('netsuiteexport/netsuite_sales_order');
					$coll->getSelect()
					->joinInner(
						array('exported' =>$logTable),
						"main_table.increment_id = exported.order_increment_id"
					);
				
					if(!$coll->getFirstItem()->getEntityId()) {
				
						$type = 1;
					}
				}
				
				$order->setAccountType($type);
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage());
		}
	}
	public function updateCancelOrders($observer) {
		try{
			$incrementId = $observer->getEvent()->getOrder()->getIncrementId();
			$logItem = Mage::getModel('netsuiteexport/sales')->load($incrementId,'order_increment_id');
			if($logItem->getId() && $logItem->getStatus() == Acommerce_Model_Resource_Sales::SUCCESS) {
				$logItem->setStatus(Acommerce_Model_Resource_Sales::CANCEL_UPDATE)->save();
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null,'OnOrderCancelUpdate.log');
		}
	}
	public function invoiceZeroOrder($observer) {
		try{
			$order = $observer->getEvent()->getOrder();
			if(Mage::app()->getStore()->roundPrice($order->getGrandTotal()) == 0 && $order->canInvoice()) {
				$invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();
				$invoice->register();
				$invoice->setEmailSent(true);
				$invoice->getOrder()->setCustomerNoteNotify(true);				
				$transactionSave = Mage::getModel('core/resource_transaction')
					->addObject($invoice)
					->addObject($invoice->getOrder())->save();
				$invoice->sendEmail(true);
			}
		}
		catch(Exception $e) {
			Mage::log($e->getMessage(),null,'AutoInvoiceError.log');
		}		
	}
} 