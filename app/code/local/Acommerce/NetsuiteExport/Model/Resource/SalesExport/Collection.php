<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade NetsuiteExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_NetsuiteExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class Acommerce_NetsuiteExport_Model_Resource_SalesExport_Collection extends Mage_Sales_Model_Resource_Order_Collection
{
	
	
	protected function _construct()
    {
		parent::_construct();
    }
	
	public function addStatusFilter($status = null) {
		if(is_array($status)) {
			$this->addFieldToFilter('status',array('in'=>$status));
		}
		else {
			if($status) {
				$this->addFieldToFilter('status',$status);
			}
			else {
				/** 
				 * default collection is filtered:
				 * 1. COD: STATE_NEW (STATE_COMPLETE -- removed)
				 * 2. All other methods: STATE_INVOICED
				 */
				// $this->getSelect()->where('main_table.state in("'.
					// Acommerce_Sales_Model_Order::STATE_INVOICED .'"
					// ) or (main_table.state in ("'.Acommerce_Sales_Model_Order::STATE_NEW.'","'.Acommerce_Sales_Model_Order::STATE_COMPLETE.'") AND (payment.method = "cashondelivery") )');
				
				$onlineState = Acommerce_Sales_Model_Order::STATE_INVOICED;
				$offlineState = Acommerce_Sales_Model_Order::STATE_NEW;
				$offlineMethod = "'cashondelivery'";
				
				$this->getSelect()->where("((main_table.state = '{$onlineState}' AND payment.method not in({$offlineMethod})) or
					main_table.state = '{$offlineState}' AND (payment.method in ({$offlineMethod})))");
					// ->orWhere("");
			}		
		}
		return $this;
	}
	public function addPaymentFilter($payment = null) {
		if(is_array($payment)) {
			$this->addFieldToFilter('payment.method',array('in'=>$payment));
		}
		elseif($payment) {
			$this->addFieldToFilter('payment.method',$payment);
		}
		return $this;
	}
	public function addTimeFilter($value) {
		if($value && $value !== 'all' && intval($value)) {
			$this->getSelect()->where('main_table.created_at >= DATE_SUB(?, INTERVAL '.intval($value).' DAY)',Varien_Date::now());
		}
		return $this;
	}
	public function fillterLoggedItems($time) {
		if($time && $time !== 'all' && intval($time)) {
			$adapter = $this->getResource()->getReadConnection();
			$loggedItems = $adapter->select()
				->from(array('logged'=>$this->getTable('netsuiteexport/netsuite_sales_order')),'order_increment_id')
				->where('logged.status = ?',Acommerce_NetsuiteExport_Model_Resource_Sales::SUCCESS)
				->where('logged.created_at >= DATE_SUB(?, INTERVAL '.intval($time).' DAY)',Varien_Date::now());
			$this->addFieldToFilter('main_table.increment_id',array('nin'=>$loggedItems));
		}
		return $this;
	}
	public function addShippingFilter($shipping = null) {
		$shipping = Mage::helper('netsuiteexport')->matchMagentoShit($shipping);
		if(is_array($shipping)) {
			$this->addFieldToFilter('main_table.shipping_method',array('in'=>$shipping));
		}
		elseif($shipping) {
			$this->addFieldToFilter('main_table.shipping_method',$shipping);
		}
		return $this;
	}
	
	
	/**
     * join sales order collection to payment
     *
     * @return order collection
     */
    public function addPaymentToSelect()
    {
		$this->addFilterToMap('payment_method', 'payment.method');
        $this->join(
				array('payment'=>'sales/order_payment'),
				'main_table.entity_id = payment.parent_id',
				array('payment_method'=>'payment.method')
				);
		return $this;
    }
	public function addOrderItems() {
		$itemAliasName = 'items_table';
        $joinTable = $this->getTable('sales/order_item');
		$this
            ->addFilterToMap('item_qty', $itemAliasName . '.qty_ordered')
            // ->addFilterToMap('item_name', $itemAliasName . '.name')
            ->addFilterToMap('product_price', $itemAliasName . '.price_incl_tax');
		$this
            ->getSelect()
            ->joinLeft(
                array($itemAliasName => $joinTable),
                "(main_table.entity_id = {$itemAliasName}.order_id 
				AND {$itemAliasName}.parent_item_id IS NULL
				)",
                array(
                    'item_id'=>$itemAliasName . '.item_id',
                    'item_qty'=>$itemAliasName . '.qty_ordered',
                    'product_id'=>$itemAliasName . '.product_id',
                    'sku'=>$itemAliasName . '.sku',
                    'netsuite_item_name'=>$itemAliasName . '.netsuite_item_name',
                    'item_external_id'=>$itemAliasName . '.item_external_id',
                    'name'=>$itemAliasName . '.name',
                    'item_discount_amount'=>$itemAliasName . '.discount_amount',
                    'product_price'=>$itemAliasName . '.price_incl_tax'
                )
            );
		Mage::getResourceHelper('core')->prepareColumnsList($this->getSelect());
        return $this;
	}
	public function addInvoiceFields() {
		$invoiceAliasName = 'invoice_table';
        $joinTable = $this->getTable('sales/invoice');
		$this->addFilterToMap('invoice_created_at', $invoiceAliasName . '.created_at');
		$this
            ->getSelect()
            ->joinLeft(
                array($invoiceAliasName => $joinTable),
                "main_table.entity_id = {$invoiceAliasName}.order_id",
                array(
                    'invoice_created_at'=>$invoiceAliasName . '.created_at',
                    'payment_received'=>'IF('.$invoiceAliasName . '.state = '.Mage_Sales_Model_Order_Invoice::STATE_PAID.',"T","F")',
                )
            );
		Mage::getResourceHelper('core')->prepareColumnsList($this->getSelect());
        return $this;
	}
	public function addRequireFields() {
		$this->addFilterToMap('order_created_at','main_table.created_at');
		$this->addFilterToMap('increment_id','main_table.increment_id');
		$this->addFilterToMap('grand_total','main_table.grand_total');
		$this->getSelect()
			->reset(Zend_Db_Select::COLUMNS)
			//->columns($this->getResource()->getIdFieldName(), 'main_table')
			->columns('customer_id', 'main_table')
			->columns('customer_firstname', 'main_table')
			->columns('customer_lastname', 'main_table')
			->columns('increment_id', 'main_table')
			// ->columns('rewardpoints', 'main_table')
			->columns('shipping_amount', 'main_table')
			->columns('shipping_tax_amount', 'main_table')
			->columns('shipping_incl_tax', 'main_table')
			->columns('shipping_discount_amount', 'main_table')
			->columns('shipping_description', 'main_table')
			->columns(array('order_created_at'=>'created_at'), 'main_table')
			->columns('customer_email', 'main_table')
			->columns('shipping_method', 'main_table')
			->columns('store_id', 'main_table')
			// ->columns('total_qty_ordered', 'main_table')
			//->columns('grand_total', 'main_table')
			->columns('grand_total', 'main_table')
			->columns('order_currency_code', 'main_table')
			// ->columns('total_paid', 'main_table')
			->columns('discount_amount', 'main_table')
			// ->columns('discount_invoiced', 'main_table')
			->columns('cod_fee', 'main_table')
			->columns('applied_rule_ids', 'main_table')
			->columns('coupon_rule_name', 'main_table')
			->columns('cod_tax_amount', 'main_table')
			// ->columns('cod_fee_invoiced', 'main_table')
			->columns('status', 'main_table')
			->columns('account_type', 'main_table');
		return $this;
	}
	/**
     * add billing  and shipping fields "name, street, city, state, country, postcode, phone" to collection
     *
     * @return order collection
     */
	protected function _addAddressFields()
    {
        $billingAliasName = 'billing_o_a';
        $shippingAliasName = 'shipping_o_a';
        $joinTable = $this->getTable('sales/order_address');

        $this
            ->addFilterToMap('billing_phone', $billingAliasName . '.telephone')
            ->addFilterToMap('billing_city', $billingAliasName . '.city')
            ->addFilterToMap('billing_region', $billingAliasName . '.region')
            ->addFilterToMap('billing_street', $billingAliasName . '.street')
            ->addFilterToMap('billing_postcode', $billingAliasName . '.postcode')

            ->addFilterToMap('shipping_phone', $shippingAliasName . '.telephone')
            ->addFilterToMap('shipping_lastname', $shippingAliasName . '.lastname')
            ->addFilterToMap('shipping_city', $shippingAliasName . '.city')
            ->addFilterToMap('shipping_region', $shippingAliasName . '.region')
            ->addFilterToMap('shipping_street', $shippingAliasName . '.street')
            ->addFilterToMap('shipping_postcode', $shippingAliasName . '.postcode');

        $this
            ->getSelect()
            ->joinLeft(
                array($billingAliasName => $joinTable),
                "(main_table.entity_id = {$billingAliasName}.parent_id"
                    . " AND {$billingAliasName}.address_type = 'billing')",
                array(
                    'billing_name'=>'CONCAT('.$billingAliasName . '.firstname'.', " ",'.$billingAliasName . '.lastname'.')',
                    'billing_phone'=>$billingAliasName . '.telephone',
                    'billing_city'=>$billingAliasName . '.city',
                    'billing_region'=>$billingAliasName . '.region',
                    'billing_country_id'=>$billingAliasName . '.country_id',
                    'billing_street'=>$billingAliasName . '.street',
                    'billing_postcode'=>$billingAliasName . '.postcode',
                    'billing_company'=>$billingAliasName . '.company'
                )
            )
            ->joinLeft(
                array($shippingAliasName => $joinTable),
                "(main_table.entity_id = {$shippingAliasName}.parent_id"
                    . " AND {$shippingAliasName}.address_type = 'shipping')",
                array(
                    'shipping_name'=>'CONCAT('.$shippingAliasName . '.firstname'.', " ",'.$shippingAliasName . '.lastname'.')',
                    'shipping_phone'=>$shippingAliasName . '.telephone',
					'shipping_city'=>$shippingAliasName . '.city',
                    'shipping_region'=>$shippingAliasName . '.region',
                    'shipping_country_id'=>$shippingAliasName . '.country_id',
                    'shipping_street'=>$shippingAliasName . '.street',
                    'shipping_postcode'=>$shippingAliasName . '.postcode',
                    'shipping_company'=>$shippingAliasName . '.company',
                )
            );
        Mage::getResourceHelper('core')->prepareColumnsList($this->getSelect());
        return $this;
    }
}