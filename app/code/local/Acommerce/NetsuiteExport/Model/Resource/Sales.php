<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade NetsuiteExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_NetsuiteExport
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class Acommerce_NetsuiteExport_Model_Resource_Sales extends Mage_Core_Model_Resource_Db_Abstract
{
	// status for magento log table
	CONST SUCCESS 	= 1; // successfully exported
	CONST FAILURE 	= 0; // fail
	CONST UPDATE 	= 2; // update
	CONST CANCEL_UPDATE = 3; // canceled udpate
	CONST NOT_EXPORTED = -1; // canceled udpate
	protected function _construct() {
        $this->_init('netsuiteexport/netsuite_sales_order', 'order_increment_id');
    }
	protected function _beforeSave(Mage_Core_Model_Abstract $object) {
		$object->setCreatedAt(Varien_Date::now());
        return $this;
    }
	public function getWriteConnection() {
		return $this->_getWriteAdapter();
	}
	public function getStatusOptions() {
		/*return array(
			self::NOT_EXPORTED=>'Not Exported',
			self::SUCCESS=>'Exported',
			self::UPDATE=>'Exported But Need To Upate',
			self::CANCEL_UPDATE=>'Exported But Need To Update(canceled)',
			self::FAILURE=>'Failed',
		);*/

        return array(
			0 => 'Not Exported',
			1 => 'Exported',
		);
	}
}