<?php
class Acommerce_NetsuiteExport_Model_Source_Payments
{	
	public function toOptionArray() {       
        $options = array();    
        $methods = Mage::getSingleton('payment/config')->getActiveMethods();
        foreach ($methods as $method) {           
            $options[] = array(
               'value' => $method->getCode(),
               'label' => $method->getTitle()
            );
        }                
        return $options;
    }
}
