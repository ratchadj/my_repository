<?php
class Acommerce_NetsuiteExport_Model_Source_Type
{
    public function toOptionArray() {
        $options = array();

        $options[] = array(
               'value' => 1,
               'label' => "Standard"
            );

        $options[] = array(
               'value' => 2,
               'label' => "Dropship"
            );
        return $options;
    }
}
