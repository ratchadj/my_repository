<?php
/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade NetsuiteExport to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
*/

class Acommerce_Model_ExternalDb extends Mage_Core_Model_Resource
{
	protected $_dbConfig = array();
	protected $_connections = array();
	protected $_skippedConnections = array();
	
	CONST CONNECTIONCLASS = 'Varien_Db_Adapter_Pdo_Mysql';
	CONST CONNECTIONNAME = 'netsuite';
	
	public function getConnection($name)
    {
        $name = self::CONNECTIONNAME ;
		if (isset($this->_connections[$name])) {
            $connection = $this->_connections[$name];
            if (isset($this->_skippedConnections[$name]) && !Mage::app()->getIsCacheLocked()) {
                $connection->setCacheAdapter(Mage::app()->getCache());
                unset($this->_skippedConnections[$name]);
            }
            return $connection;
        }
        $connConfig = $this->_getConfig();
		
        if ($connConfig['active']!==1) {
            return false;
        }
        $connection = $this->_newConnection('',$connConfig);
        if ($connection) {
            if (Mage::app()->getIsCacheLocked()) {
                $this->_skippedConnections[$name] = true;
            } else {
                $connection->setCacheAdapter(Mage::app()->getCache());
            }
        }

        $this->_connections[$name] = $connection;
		
        return $connection;
    }
	protected function _newConnection($type,$config)
    {
        if (!is_array($config)) {
            return false;
        }

        $connection = false;
        $className  = self::CONNECTIONCLASS;
        // define profiler settings
		$config['profiler'] = isset($config['profiler']) && $config['profiler'] != 'false';

		$connection = new $className($config);
		if ($connection instanceof Varien_Db_Adapter_Interface) {
			// run after initialization statements
			if (!empty($config['initStatements'])) {
				$connection->query($config['initStatements']);
			}
		} else {
			$connection = false;
		}

        return $connection;
    }
	protected function _getConfig() {
		if(empty($this->_dbConfig)) {
			$this->_dbConfig = array(
				'host' => $this->_getStoreConfig('host'),
				'username' => $this->_getStoreConfig('usrname'),
				'password' => $this->_getStoreConfig('password'),
				'dbname' => $this->_getStoreConfig('dbname'),
				'initStatements' => 'SET NAMES utf8',
				'model' => 'mysql4',
				'type' => 'pdo_mysql',
				'pdoType' => '',
				'active' => 1,
			);
		}
		return $this->_dbConfig;
	}
	protected function _getStoreConfig($k) {
		return Mage::getStoreConfig('netsuiteexport/cronconfig/'.$k);
	}
}