<?php

/**
 * Developed by Emagest
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the End-user License Agreement(EULA)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.emagest.com/LICENSE-E.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Sales to newer
 * versions in the future.
 *
 * @category    Acommerce
 * @package     Acommerce_Sales
 * @copyright   Copyright (c) 2012 Emagest (http://www.emagest.com)
 * @license     http://www.emagest.com/LICENSE-E.txt
 */
class Acommerce_NetsuiteExport_Model_Export
{
    protected $_helper = null;
    protected $_countryNames = false;
    protected $_timeComfig = false;


    protected function _getOffLinePayments() {
        $codPayments = Mage::getStoreConfig('netsuiteexport/condition/cod');
        if($codPayments) {
           $codPayments = explode(',', $codPayments);
        } else {
           $codPayments = array('unknown_payment');
        }
        return $codPayments;
    }

    protected function _getOffLineStatuses() {
        $codStatuses = Mage::getStoreConfig('netsuiteexport/condition/codstatus');
        if($codStatuses) {
           $codStatuses = explode(',', $codStatuses);
        } else {
           $codStatuses = array();
        }
        return $codStatuses;
    }

    protected function _getOnLineStatuses() {
        $ncodStatuses = Mage::getStoreConfig('netsuiteexport/condition/ncodstatus');
        if($ncodStatuses) {
           $ncodStatuses = explode(',', $ncodStatuses);
        } else {
           $ncodStatuses = array();
        }
        return $ncodStatuses;
    }

    protected function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = Mage::helper('netsuiteexport');
        }
        return $this->_helper;
    }

    protected function _getTimeConfig() {
        if(!$this->_timeComfig) {
            $this->_timeComfig = Mage::getStoreConfig('netsuiteexport/cronconfig/timefilter');
        }
        return $this->_timeComfig;
    }

    protected function _getShipType() {
        $type = Mage::getStoreConfig('netsuiteexport/condition/type');
        if(!$type) {
           $type = 1;
        }
        return $type;
    }

    protected function _getDropShipItem() {
        $itemName = Mage::getStoreConfig('netsuiteexport/condition/itemname');
        if(!$itemName) {
           $itemName = 'Drop Shipping Item';
        }
        return $itemName;
    }

    protected function _getFreeShippingText() {
        $itemName = Mage::getStoreConfig('netsuiteexport/condition/freeshipping');
        if(!$itemName) {
           $itemName = 'Free Shipping';
        }
        return $itemName;
    }

    protected function _getShippingText() {
        $itemName = Mage::getStoreConfig('netsuiteexport/condition/shipping');
        if(!$itemName) {
           $itemName = 'Dropship';
        }
        return $itemName;
    }

    protected function _useDefaultCustomerId() {
        return Mage::getStoreConfig('netsuiteexport/condition/usecustdefault');
    }

    protected function _getServicCompany() {
        return Mage::getStoreConfig('netsuiteexport/condition/service_company');
    }

    protected function _getOrderPrefix() {
        $prefix = Mage::getStoreConfig('netsuiteexport/condition/order_prefix');
        return ($prefix) ? $prefix : '';
    }


    protected function _getRestrictedPayments() {

        $restrictedPayments = Mage::getStoreConfig('netsuiteexport/condition/restrictedpayments');
        if($restrictedPayments) {
           $restrictedPayments = explode(',', $restrictedPayments);
        } else {
           $restrictedPayments = array('unknown_payment');
        }
        return $restrictedPayments;
    }



    protected function _getOffLineSalesOrder()
    {
        $salesOrder = Mage::getModel('sales/order')->getCollection()
                ->join(
                        array('payment' => 'sales/order_payment'), 'main_table.entity_id = payment.parent_id',
                        array('payment_method' => 'payment.method'));


        $salesOrder->addFieldToFilter('main_table.status', array('in' => $this->_getOffLineStatuses()))
                   ->addFieldToFilter('main_table.status', array('neq' => 'holded'))
                   ->addFieldToFilter('main_table.is_exported', array('eq' => 0))
                   ->addFieldToFilter('payment.method', array('in' => $this->_getOffLinePayments()))
                   ->addFieldToFilter('payment.method', array('nin' => $this->_getRestrictedPayments()));

        $timeFilter = $this->_getTimeConfig();
        if($timeFilter != 'all') {
            $salesOrder->addFieldToFilter('main_table.created_at', array('gteq' => new Zend_Db_Expr('DATE_SUB(\''.Varien_Date::now(true).'\', INTERVAL ' . intval($timeFilter) . ' DAY)')));
        }
        //echo $salesOrder->getSelect()->__toString()."\n\n";
        return $salesOrder;
    }

    protected function _getOnLineSalesOrder()
    {
        $salesOrder = Mage::getModel('sales/order')->getCollection()
                ->join(
                        array('payment' => 'sales/order_payment'), 'main_table.entity_id = payment.parent_id',
                        array('payment_method' => 'payment.method'));

        $salesOrder->addFieldToFilter('main_table.status', array('in' => $this->_getOnLineStatuses()))
                   ->addFieldToFilter('main_table.status', array('neq' => 'holded'))
                   ->addFieldToFilter('main_table.is_exported', array('eq' => 0))
                   ->addFieldToFilter('payment.method', array('nin' => $this->_getOffLinePayments()))
                   ->addFieldToFilter('payment.method', array('nin' => $this->_getRestrictedPayments()));;

        $timeFilter = $this->_getTimeConfig();
        if($timeFilter != 'all') {
            $salesOrder->addFieldToFilter('main_table.created_at', array('gteq' => new Zend_Db_Expr('DATE_SUB(\''.Varien_Date::now(true).'\', INTERVAL ' . intval($timeFilter) . ' DAY)')));
        }
        //echo $salesOrder->getSelect()->__toString()."\n\n";
        return $salesOrder;
    }

    protected function _getCountryNames() {
        if(!$this->_countryNames) {
            $countrieNames = array();
            $countries = Mage::getModel('directory/country')
                    ->getResourceCollection()
                    ->toOptionArray();

            foreach ($countries as $country) {
                $countrieNames[$country['value']] = $country['label'];
            }
            $this->_countryNames = $countrieNames;
        }
        return $this->_countryNames;
    }

    protected function _convertOrderToArray($orders) {
        $datas = array();
        $countries = $this->_getCountryNames();
        $type = $this->_getShipType();
        $dropShipItem = $this->_getDropShipItem();
        $prefix = $this->_getOrderPrefix();

        //$freeShippingText = $this->_getFreeShippingText();
        //$shiptext = $this->_getShippingText();
        $useDefaultCustId = $this->_useDefaultCustomerId();

        foreach ($orders as $order) {
            $address = $order->getShippingAddress();
            $payment = $order->getPayment()->getMethod();
            $orderDate = $order->getCreatedAtStoreDate()->toString('d/m/Y', 'php');
            $invoiceDate = false;
            $streets = $address->getStreet();

            if($order->hasInvoice()) {
                foreach($order->getAllInvoices as $invoice) {
                    $invoiceDate = $invoice->getCreatedAtStoreDate()->toString('d/m/Y', 'php');
                    break;
                }
            }

            if($type == 1) {
                foreach($order->getAllItems() as $item) {
                    if($item->getProductType() == 'simple') {
                        $temp = array();

                        $temp['order_id'] = $this->_setCsvFormat($prefix.$order->getIncrementId());
                        $temp['order_date'] = $this->_setCsvFormat($orderDate);
                        $temp['invoice_date'] = $this->_setCsvFormat((($invoiceDate)?$invoiceDate:$orderDate));
                        $temp['email'] = $this->_setCsvFormat($order->getCustomerEmail());

                        if(!$useDefaultCustId) {
                            $temp['customer_id'] = $this->_setCsvFormat($order->getCustomerId());
                        } else {
                            $temp['customer_id'] = $this->_setCsvFormat('');
                        }

                        $temp['external_customer_id'] = $temp['customer_id'];
                        $temp['customer_firstname'] = $this->_setCsvFormat($order->getCustomerFirstname());
                        $temp['customer_lastname'] = $this->_setCsvFormat($order->getCustomerLastname());
                        $temp['shipping_addressee'] = $this->_setCsvFormat((($address->getFirstname())? $address->getFirstname():'').' '.(($address->getLastname())? $address->getLastname():''));

                        $subDistrict = ($address->getSubDistrict())? $address->getSubDistrict() : '';
                        $temp['shipping_address_line_1'] = $this->_setCsvFormat($this->_getAddress($streets, 1));
                        $temp['shipping_address_line_2'] = $this->_setCsvFormat($this->_getAddress($streets, 2).' '.$subDistrict);

                        $temp['shipping_address_city'] = $this->_setCsvFormat($address->getCity());
                        $temp['shipping_address_state'] = $this->_setCsvFormat($address->getRegion());
                        $temp['shipping_address_country'] = $this->_setCsvFormat($countries[$address->getCountryId()]);
                        $temp['shipping_address_postal_code'] = $this->_setCsvFormat($address->getPostcode());
                        $temp['shipping_address_postal_phone'] = $this->_setCsvFormat($address->getTelephone());
                        $temp['item_id'] = $this->_setCsvFormat($item->getSku());
                        $temp['item_qty'] = $this->_setCsvFormat($item->getQtyOrdered());

                        if($parentItem = $item->getParentItem()) {
                            $temp['rate'] = $this->_setCsvFormat((($parentItem->getPriceInclTax())? $parentItem->getPriceInclTax() : 0));
                            $temp['gross_total'] = $this->_setCsvFormat((($parentItem->getRowTotalInclTax())? $parentItem->getRowTotalInclTax() : 0));
                        } else {
                            $temp['rate'] = $this->_setCsvFormat((($item->getPriceInclTax())? $item->getPriceInclTax() : 0));
                            $temp['gross_total'] = $this->_setCsvFormat((($item->getRowTotalInclTax())? $item->getRowTotalInclTax() : 0));
                        }

                        $temp['payment_type'] = $this->_setCsvFormat($this->_getPaymentMethod($order));
                        $temp['shipping_type'] = $this->_setCsvFormat($this->_getHelper()->getShippingMethod($order->getShippingMethod()));

                        $temp['main_sales_channel'] = $this->_setCsvFormat('');
                        $temp['sub_sales_channel'] = $this->_setCsvFormat('');
                        $temp['sales_channel_source'] = $this->_setCsvFormat('');
                        $temp['company_code'] = $this->_setCsvFormat($this->_getCompanyValue($item->getProductId()));

                        $datas[] = implode(',', $temp);
                    }
                }

                if($order->getCodFee() > 0.001) {
                    $datas[] = $this->_createOptionData($order, Acommerce_NetsuiteExport_Helper_Data::ITEM_TYPE_COD_FEE);
                }

                if($order->getShippingInclTax()  > 0.001) {
                   $datas[] = $this->_createOptionData($order, Acommerce_NetsuiteExport_Helper_Data::ITEM_TYPE_SHIPPING_AMOUNT);
                }

                $discount = abs($order->getDiscountAmount());
                if((float)$discount> 0.001){
                    $datas[] = $this->_createOptionData($order, Acommerce_NetsuiteExport_Helper_Data::ITEM_TYPE_DISCOUNT_RULES);
                }
            }  else {
                $temp = array();
                $temp['order_id'] = $this->_setCsvFormat($prefix.$order->getIncrementId());
                $temp['order_date'] = $this->_setCsvFormat($orderDate);
                $temp['invoice_date'] = $this->_setCsvFormat((($invoiceDate)?$invoiceDate:$orderDate));
                $temp['email'] = $this->_setCsvFormat($order->getCustomerEmail());

                if(!$useDefaultCustId) {
                    $temp['customer_id'] = $this->_setCsvFormat($order->getCustomerId());
                } else {
                    $temp['customer_id'] = $this->_setCsvFormat('');
                }

                $temp['external_customer_id'] = $temp['customer_id'];
                $temp['customer_firstname'] = $this->_setCsvFormat($order->getCustomerFirstname());
                $temp['customer_lastname'] = $this->_setCsvFormat($order->getCustomerLastname());
                $temp['shipping_addressee'] = $this->_setCsvFormat((($address->getFirstname())? $address->getFirstname():'').' '.(($address->getLastname())? $address->getLastname():''));

                $subDistrict = ($address->getSubDistrict())? $address->getSubDistrict() : '';
                $temp['shipping_address_line_1'] = $this->_setCsvFormat($this->_getAddress($streets, 1));
                $temp['shipping_address_line_2'] = $this->_setCsvFormat($this->_getAddress($streets, 2).' '.$subDistrict);

                $temp['shipping_address_city'] = $this->_setCsvFormat($address->getCity());
                $temp['shipping_address_state'] = $this->_setCsvFormat($address->getRegion());
                $temp['shipping_address_country'] = $this->_setCsvFormat($countries[$address->getCountryId()]);
                $temp['shipping_address_postal_code'] = $this->_setCsvFormat($address->getPostcode());
                $temp['shipping_address_postal_phone'] = $this->_setCsvFormat($address->getTelephone());
                $temp['item_id'] = $this->_setCsvFormat($dropShipItem);
                $temp['item_qty'] = '1';

                $grossTotal = $order->getBaseGrandTotal();
                /*foreach($order->getAllItems() as $item) {
                    if($item->getProductType() == 'simple') {
                        if($parentItem = $item->getParentItem()) {
                            $grossTotal += $parentItem->getRowTotalInclTax();
                        } else {
                            $grossTotal += $item->getRowTotalInclTax();
                        }
                    }
                }*/

                $temp['rate'] = $this->_setCsvFormat( $grossTotal);
                $temp['gross_total'] = $this->_setCsvFormat($grossTotal);
                $temp['payment_type'] = $this->_setCsvFormat($this->_getPaymentMethod($order));

                if($order->getShippingInclTax() > 0.001) {
                    $temp['shipping_type'] = "Standard Shipping";//$this->_setCsvFormat($this->_getHelper()->getShippingMethod($order->getShippingMethod()));
                } else {
                    $temp['shipping_type'] = $this->_setCsvFormat("Free");
                }

                $temp['main_sales_channel'] = $this->_setCsvFormat('');
                $temp['sub_sales_channel'] = $this->_setCsvFormat('');
                $temp['sales_channel_source'] = $this->_setCsvFormat('');
                $datas[] = implode(',', $temp);

                /*if($order->getCodFee() > 0.001) {
                    $datas[] = $this->_createOptionData($order, Acommerce_NetsuiteExport_Helper_Data::ITEM_TYPE_COD_FEE);
                }

                if($order->getShippingInclTax()  > 0.001) {
                   $datas[] = $this->_createOptionData($order, Acommerce_NetsuiteExport_Helper_Data::ITEM_TYPE_SHIPPING_AMOUNT);
                }

                $discount = abs($order->getDiscountAmount());
                if((float)$discount> 0.001){
                    $datas[] = $this->_createOptionData($order, Acommerce_NetsuiteExport_Helper_Data::ITEM_TYPE_DISCOUNT_RULES);
                }*/
            }
            $order->setIsExported(1);
            $order->save();
        }

        return $datas;
    }

    public function _getPaymentMethod($order)
    {
        $method = $order->getPaymentMethod();
        $payment = '';
        $mappings = unserialize(Mage::getStoreConfig('netsuiteexport/ftpexport/mapping', 0));

        if(count($mappings)) {
            foreach ($mappings as $mapping) {
                if($mapping['code'] == $method) {
                    $payment = $mapping['mapping'];
                    break;
                }
            }
            if($order->getIsInstallment()) {
                $payment .= '-INST';
            }
        } else {
            $codPayments = Mage::getStoreConfig('netsuiteexport/condition/cod');
            if($codPayments) {
               $codPayments = explode(',', $codPayments);
            } else {
               $codPayments = array();
            }

            if(in_array($method, $codPayments)) {
                $payment = 'COD';
            } else {
                $payment = 'NON COD';
            }
        }

        /**/
        return $payment;
    }

    protected function _getAddress($streets, $row) {
        $streets = (is_array($streets)) ? $streets : explode("\n", $streets);
        $itemCount =  floor(count($streets) / 2);
        $itemCount = ($itemCount < 1) ? 1 : $itemCount;

        if($row == 1) {
            return preg_replace('~\r\n?~', ' ', implode("\r\n", array_slice($streets, 0, $itemCount)));
        } else {
            return preg_replace('~\r\n?~', ' ', implode("\r\n", array_slice($streets, $itemCount)));
        }
    }

    protected function _createOptionData($order, $type) {
        $countries = $this->_getCountryNames();
        $address = $order->getShippingAddress();
        $payment = $order->getPayment()->getMethod();
        $orderDate = $order->getCreatedAtStoreDate()->toString('d/m/Y', 'php');
        $invoiceDate = false;
        $streets = $address->getStreet();
        $useDefaultCustId = $this->_useDefaultCustomerId();
        $prefix = $this->_getOrderPrefix();

        if($order->hasInvoice()) {
            foreach($order->getAllInvoices as $invoice) {
                $invoiceDate = $invoice->getCreatedAtStoreDate()->toString('d/m/Y', 'php');
                break;
            }
        }

        $temp = array();
        $temp['order_id'] = $this->_setCsvFormat($prefix.$order->getIncrementId());
        $temp['order_date'] = $this->_setCsvFormat($orderDate);
        $temp['invoice_date'] = $this->_setCsvFormat((($invoiceDate)?$invoiceDate:$orderDate));
        $temp['email'] = $this->_setCsvFormat($order->getCustomerEmail());

        if(!$useDefaultCustId) {
            $temp['customer_id'] = $this->_setCsvFormat($order->getCustomerId());
        } else {
            $temp['customer_id'] = $this->_setCsvFormat('');
        }

        $temp['external_customer_id'] = $temp['customer_id'];
        $temp['customer_firstname'] = $this->_setCsvFormat($order->getCustomerFirstname());
        $temp['customer_lastname'] = $this->_setCsvFormat($order->getCustomerLastname());
        $temp['shipping_addressee'] = $this->_setCsvFormat((($address->getFirstname())? $address->getFirstname():'').' '.(($address->getLastname())? $address->getLastname():''));

        $subDistrict = ($address->getSubDistrict())? $address->getSubDistrict() : '';
        $temp['shipping_address_line_1'] = $this->_setCsvFormat($this->_getAddress($streets, 1));
        $temp['shipping_address_line_2'] = $this->_setCsvFormat($this->_getAddress($streets, 2).' '.$subDistrict);

        $temp['shipping_address_city'] = $this->_setCsvFormat($address->getCity());
        $temp['shipping_address_state'] = $this->_setCsvFormat($address->getRegion());
        $temp['shipping_address_country'] = $this->_setCsvFormat($countries[$address->getCountryId()]);
        $temp['shipping_address_postal_code'] = $this->_setCsvFormat($address->getPostcode());
        $temp['shipping_address_postal_phone'] = $this->_setCsvFormat($address->getTelephone());

        $itemId = '';
        $rate = 0;
        if($type == Acommerce_NetsuiteExport_Helper_Data::ITEM_TYPE_COD_FEE) {
            $itemId = Mage::getStoreConfig('netsuiteexport/salesexport/codfee_name');
            if(!$itemId) {
                $itemId = "COD Charges";
            }
            $rate = $order->getCodFee() + $order->getCodTaxAmount();
        } elseif($type == Acommerce_NetsuiteExport_Helper_Data::ITEM_TYPE_SHIPPING_AMOUNT) {
            $itemId = Mage::getStoreConfig('netsuiteexport/salesexport/shipping_name');
            if(!$itemId) {
                $itemId = "All Shipping Charges";
            }
            $rate = $order->getShippingInclTax();
        } elseif($type == Acommerce_NetsuiteExport_Helper_Data::ITEM_TYPE_DISCOUNT_RULES) {
            $ruleIds = $order->getAppliedRuleIds();
            $ruleName = $order->getCouponRuleName();
            $itemId = 'Discount';
            $rate = - abs($order->getDiscountAmount());
        }

        $temp['item_id'] = $this->_setCsvFormat($itemId);
        $temp['item_qty'] = $this->_setCsvFormat(1);
        $temp['rate'] = $this->_setCsvFormat($rate);
        $temp['gross_total'] = $this->_setCsvFormat($rate);

        $temp['payment_type'] = $this->_setCsvFormat($this->_getPaymentMethod($order));

        $shipType = $this->_getShipType();
        if($shipType == 1) {
            $temp['shipping_type'] = $this->_setCsvFormat($this->_getHelper()->getShippingMethod($order->getShippingMethod()));
        } else {
            if($order->getShippingInclTax() > 0.001) {
                $temp['shipping_type'] = $this->_setCsvFormat("Standard Shipping");//$this->_setCsvFormat($this->_getHelper()->getShippingMethod($order->getShippingMethod()));
            } else {
                $temp['shipping_type'] = $this->_setCsvFormat("Free");
            }
        }


        $temp['main_sales_channel'] = $this->_setCsvFormat('');
        $temp['sub_sales_channel'] = $this->_setCsvFormat('');
        $temp['sales_channel_source'] = $this->_setCsvFormat('');
        $temp['company_code'] = $this->_setCsvFormat($this->_getServicCompany());

        return implode(',', $temp);
    }

    protected function _setCsvFormat($data) {
        return '"' . str_replace(array('"', '\\'), array('""', '\\\\'), trim($data)) . '"';
    }

    protected function _getHeaders() {
        return array('Order #', 'Order Date', 'Invoice Date', 'Email', 'Customer ID', 'External Customer ID', 'Customer First Name', 'Customer Last Name', 'Shipping Addressee', 'Shipping Address Line 1', 'Shipping Address Line 2', 'Shipping Address City', 'Shipping Address State/Province', 'Shipping Address Country', 'Shipping Address Postal Code', 'Shipping Address Phone', 'Item ID', 'Item Qty', 'Rate', 'Gross Total', 'Payment Type', 'Shipping Type', 'Main Sales Channel(Optional)', 'Sub Sales Channel(Optional)', 'Sales Channel Source(Optional)', 'Company Code');
    }

    public function ExportCsvData()
    {
        if (!$this->_getHelper()->isCronActive()) {
            return;
        }

        $onlineOrders = $this->_getOnLineSalesOrder();
        $offlineOrders = $this->_getOffLineSalesOrder();

        if($onlineOrders->getSize() > 0 || $offlineOrders->getSize() > 0) {
            $header[] = implode(',', $this->_getHeaders());
            $onlineOrders = $this->_convertOrderToArray($onlineOrders);
            $offlineOrders = $this->_convertOrderToArray($offlineOrders);
            $currentDate = Varien_Date::now();
            $currentDate = str_replace(array('-', ':', ' '), array('', '', '_'), $currentDate);

            $io = $this->getFile();
            $path = $this->getOutputFolder();
            $io->write($path.'SalesOrder_'.$currentDate.'.csv', implode(PHP_EOL, array_merge($header, $onlineOrders, $offlineOrders)));
            $io->close();
            $this->_ftpFile($path.'SalesOrder_'.$currentDate.'.csv');
        }
    }

    protected function getOutputFolder()
    {
        $path = Mage::getBaseDir('var') . DS . 'export';

        if (!is_dir($path . DS . 'sales_order')) {
            mkdir($path . DS . 'sales_order', 0777, true);
        }
        return $path . DS . 'sales_order' . DS;
    }

    protected function getFile() {
        $io = new Varien_Io_File();
        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $this->getOutputFolder()));
        return $io;
    }


    protected function _ftpFile($path) {
        try {
            $host = Mage::getStoreConfig('netsuiteexport/ftpexport/host');
            $usrname = Mage::getStoreConfig('netsuiteexport/ftpexport/usrname');
            $password = Mage::getStoreConfig('netsuiteexport/ftpexport/password');
            $cd = Mage::getStoreConfig('netsuiteexport/ftpexport/path');

            $sftp = new Varien_Io_Sftp();
            $sftp->open(
                    array(
                            'host'      => $host,
                            'username'  => $usrname,
                            'password'  => $password,
                        )
                    );

            $pos = strrpos($path, DS);
            if($pos !== false) {
                $fileName = substr($path, $pos + 1, strlen($path) - ($pos + 1));
            }

            $content = file_get_contents($path);

            if($cd) {
                $sftp->cd($cd);
            }

            $sftp->write($fileName, $content);
            $sftp->close();
        } catch (Exception $e) {
          Mage::log($e->getMessage(), null, 'sales_order_export.log');
        }
    }

    public function ManuallyExport($orderIds) {

        $orders = $this->_getSalesOrderByIds($orderIds);

        if($orders->getSize() > 0) {
            $header[] = implode(',', $this->_getHeaders());
            $manuallyOrders = $this->_convertOrderToArray($orders);
            $currentDate = Varien_Date::now();
            $currentDate = str_replace(array('-', ':', ' '), array('', '', '-'), $currentDate);

            $io = $this->getFile();
            $path = $this->getOutputFolder();
            $io->write($path.'SalesOrder_'.$currentDate.'.csv', implode(PHP_EOL, array_merge($header, $manuallyOrders)));
            $io->close();
            $this->_ftpFile($path.'SalesOrder_'.$currentDate.'.csv');
            return true;
        }
        return false;
    }


    protected function _getSalesOrderByIds($orderIds)
    {
        $salesOrder = Mage::getModel('sales/order')->getCollection()
                ->join(
                        array('payment' => 'sales/order_payment'), 'main_table.entity_id = payment.parent_id',
                        array('payment_method' => 'payment.method'));

        $salesOrder->addFieldToFilter('main_table.entity_id', array('in' => $orderIds))
                    ->addFieldToFilter('payment.method', array('nin' => $this->_getRestrictedPayments()));
        return $salesOrder;
    }

    protected function _getCompanyValue($productId) {
        $_resource = Mage::getSingleton('catalog/product')->getResource();
        $optionId = $_resource->getAttributeRawValue($productId, 'ais_company', Mage::app()->getStore());

        $attr = $_resource->getAttribute("ais_company");
        $optionText = '';
        if ($attr->usesSource()) {
            $optionText = $attr->getSource()->getOptionText($optionId);
        }
        Mage::log($optionText, null, 'option.log');
        return $optionText;
    }

}

