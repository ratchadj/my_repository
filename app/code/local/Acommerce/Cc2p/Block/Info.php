<?php


class Acommerce_Cc2p_Block_Info extends Mage_Payment_Block_Info
{
	protected function _prepareSpecificInformation($transport = null)
    {
		$transport = parent::_prepareSpecificInformation($transport);
		if(!Mage::app()->getStore()->isAdmin()) {
			return $transport;
		}
        $payment = $this->getInfo();
        $info = $payment->getAdditionalInformation();
        $transport->addData(array('masked_cc_number' => ((isset($info['masked_cc_number'])) ? $info['masked_cc_number'] : ''),
                                  'approval_code' =>  ((isset($info['approval_code'])) ? $info['approval_code'] : ''),
                                  'eci' =>  ((isset($info['eci'])) ? $info['eci'] : ''),
                                  'fail_reason' =>  ((isset($info['fail_reason'])) ? $info['fail_reason'] : '')
                                )
                            );
        $transport->addData(array('Status'=>$payment->getCcApproval()));
        $transport->addData(array('Credit card transaction id'=>$payment->getCcTransId()));
		return $transport;
    }

}
