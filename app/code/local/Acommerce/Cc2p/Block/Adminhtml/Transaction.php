<?php
class Acommerce_Cc2p_Block_Adminhtml_Transaction extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct() {
		$this->_controller = 'adminhtml_transaction';
		$this->_blockGroup = 'cc2p';
		$this->_headerText = Mage::helper('cc2p')->__('2C2P Transaction Tracking');
		parent::__construct();
		$this->_removeButton('add');
	}

}
