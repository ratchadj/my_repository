<?php
class Acommerce_Cc2p_Block_Adminhtml_Transaction_Render_Status extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Options
{
    public function render(Varien_Object $row)
    {
        $options = $this->getColumn()->getOptions();
        if (!empty($options) && is_array($options)) {
			if($row intance of Mage_Sales_Model_Abstract) {
				$payment = $row->getPayment();
				return $this->_renderOptions($options,$payment->getAdditionalInformation($this->getColumn()->getIndex()));

			}
        }
		return $row->getData($this->getColumn()->getIndex());
    }
	protected function _renderOptions($options,$value) {
		if (is_array($value)) {
			$res = array();
			foreach ($value as $item) {
				if (isset($options[$item])) {
					$res[] = $this->escapeHtml($options[$item]);
				}
				elseif ($showMissingOptionValues) {
					$res[] = $this->escapeHtml($item);
				}
			}
			return implode(', ', $res);
		} elseif (isset($options[$value])) {
			return $this->escapeHtml($options[$value]);
		} elseif (in_array($value, $options)) {
			return $this->escapeHtml($value);
		}
	}
}