<?php
class Acommerce_Cc2p_Block_Adminhtml_Customer_Edit_Tabs_Card_Info extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('customer_view_cardinfo_grid');
        $this->setDefaultSort('addeded_at', 'desc');
        $this->setSortable(true);
        $this->setPagerVisibility(true);
        $this->setFilterVisibility(false);
        $this->setUseAjax(true);
        $this->setHideAll(false);
    }

    protected function _prepareCollection()
    {
        $c_id = Mage::registry('current_customer')->getId();
        $collection = Mage::getModel('cc2p/card')->getCollection()
                        ->addCustomerIdFilter($c_id);

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('id', array(
            'header' => Mage::helper('cc2p')->__('ID'),
            'align' => 'center',
            'index' => 'id',
            'width' => '50px'
        ));

        $this->addColumn('added_at', array(
            'header' => Mage::helper('cc2p')->__('Date Added'),
            'index' => 'added_at',
            'type' => 'datetime',
        ));
        $this->addColumn('cc_stored_unique_id', array(
            'header' => Mage::helper('cc2p')->__('storeCardUniqueID'),
            'index' => 'cc_stored_unique_id',
        ));

        $this->addColumn('cc_owner', array(
            'header' => Mage::helper('cc2p')->__('Cardholder Name'),
            'index' => 'cc_owner',
        ));
		$this->addColumn('cc_type', array(
            'header' => Mage::helper('cc2p')->__('Card Type'),
            'index' => 'cc_type',
        ));
		$this->addColumn('cc_number', array(
            'header' => Mage::helper('cc2p')->__('Card Number'),
            'index' => 'cc_number',
        ));
		$this->addColumn('cc_exp', array(
            'header' => Mage::helper('cc2p')->__('Expiration'),
            'index' => 'cc_exp',
        ));
		$this->addColumn('cc_currency', array(
            'header' => Mage::helper('cc2p')->__('Currency'),
            'index' => 'cc_currency',
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($item)
    {
        return false;
    }

}
