<?php
class Acommerce_Cc2p_Block_Form extends Mage_Payment_Block_Form_Cc
{
	protected $_cards = array();
	protected $_payModel = null;

    protected $_installmentItem = null;
    protected $_banks = null;

    protected function _construct() {
		parent::_construct();
        $this->setTemplate('cc2p/form.phtml');
    }
	public function getPeriods() {

        $inst = $this->getInstallment();
        $results = array();
        if($inst) {
            $conditions = unserialize($inst->getConditions());
            $banks = unserialize(Mage::getStoreConfig('payment/cc2p/bank'));
            $installments = unserialize(Mage::getStoreConfig('payment/cc2p/period'));

            $temp = array();
            foreach ($banks as $value) {
                $key = $value['code'];
                $temp[$key] = $value;
            }
            $banks = $temp;

            $temp = array();
            foreach ($installments as $value) {
                $key = $value['bank'].$value['code'];
                $temp[$key] = $value;
            }
            $installments = $temp;

            $curDate = Mage::app()->getLocale()->storeDate(null, null, true);
            $curDate = strtotime($curDate->toString('Y-m-d H:i:s', 'php'));
            $grandTotal = $this->getQuote()->getGrandTotal();

            foreach ($conditions as $key => $condition) {
                foreach ($condition as $key1 => $value) {
                   if(isset($banks[$key])) {
                        $bank = $banks[$key];
                        if (isset($installments[$bank['bank'].$key1])) {
                            $installment = $installments[$bank['bank'].$key1];

                            $validForm = $value[0];
                            $validTo = $value[1];

                            if(!empty($validForm) && !empty($validTo)) {
                                $validForm = strtotime($validForm);
                                $validTo = strtotime($validTo);
                                if($validForm <= $curDate && $validTo >= $curDate) {
                                    $k = trim($installment['bank'].$installment['period_value'].$installment['minimum'].$installment['code']);

                                    // (Total  (0.8 M)) +Total)/M
                                    $rate = (is_null($installment['minimum']) || empty($installment['minimum']))? 0 : $installment['minimum'];
                                    $perMonth = (($grandTotal * $rate) + $grandTotal) / $installment['period_value'];
                                    $perMonth = round($perMonth,2);
                                    $label = $installment['period_label'];
                                    if(strpos($label, '(') !== false) {
                                        $label = substr($installment['period_label'], 0, strpos($label, '(')-1) ;
                                    }
                                    $results[$k] = $label.$this->__(' installment amount %s', Mage::helper('core')->currency($perMonth));
                                }
                            } elseif(!empty($validForm) && empty($validTo)) {
                                $validForm = strtotime($validForm);
                                if($validForm <= $curDate) {
                                    $k = trim($installment['bank'].$installment['period_value'].$installment['minimum'].$installment['code']);
                                    $rate = (is_null($installment['minimum']) || empty($installment['minimum']))? 0 : $installment['minimum'];
                                    $perMonth = (($grandTotal * $rate) + $grandTotal) / $installment['period_value'];
                                    $perMonth = round($perMonth,2);
                                    $label = $installment['period_label'];
                                    if(strpos($label, '(') !== false) {
                                        $label = substr($installment['period_label'], 0, strpos($label, '(')-1) ;
                                    }
                                    $results[$k] = $label.$this->__(' Installment Amount %s', Mage::helper('core')->currency($perMonth));
                                }
                            } elseif(empty($validForm) && !empty($validTo)) {
                                $validTo = strtotime($validTo);
                                if($validTo >= $curDate) {
                                    $k = trim($installment['bank'].$installment['period_value'].$installment['minimum'].$installment['code']);
                                    $rate = (is_null($installment['minimum']) || empty($installment['minimum']))? 0 : $installment['minimum'];
                                    $perMonth = (($grandTotal * $rate) + $grandTotal) / $installment['period_value'];
                                    $perMonth = round($perMonth,2);
                                    $label = $installment['period_label'];
                                    if(strpos($label, '(') !== false) {
                                        $label = substr($installment['period_label'], 0, strpos($label, '(') - 1) ;
                                    }
                                    $results[$k] = $label.$this->__(' installment amount %s', Mage::helper('core')->currency($perMonth));
                                }
                            } else {
                                    $k = trim($installment['bank'].$installment['period_value'].$installment['minimum'].$installment['code']);
                                    $rate = (is_null($installment['minimum']) || empty($installment['minimum']))? 0 : $installment['minimum'];
                                    $perMonth = (($grandTotal * $rate) + $grandTotal) / $installment['period_value'];
                                    $perMonth = round($perMonth,2);
                                    $label = $installment['period_label'];
                                    if(strpos($label, '(') !== false) {
                                        $label = substr($installment['period_label'], 0, strpos($label, '(')-1) ;
                                    }
                                    $results[$k] = $label.$this->__(' installment amount %s', Mage::helper('core')->currency($perMonth));
                            }
                        }
                    }
                }
            }
        }
        return $results;

        //$periods = $this->_getPaymentModel()->getPeriods($this->getQuote(),false);
        //return $periods;
	}
	public function hasPeriod() {
		return count($this->getPeriods());
	}
	public function periodEnabled() {
		return $this->_getPaymentModel()->getUseInstallment();
	}
	public function getBanks() {
        if(is_null($this->_banks)) {
            $banks = Mage::getStoreConfig('payment/cc2p/bank');
            $banks = unserialize($banks);

            $installments = $this->getInstallment();

            if(!$installments) {
                foreach($banks as $bank) {
                    if(isset($bank['active_flag']) && $bank['active_flag'] == 'Y') {
                        $this->_banks[$bank['bank']] = $bank['name'];
                    }
                }
            } else {
                $banksInstallments = unserialize($installments->getConditions());
                $curDate = Mage::app()->getLocale()->storeDate(null, null, true);
                $curDate = strtotime($curDate->toString('Y-m-d H:i:s', 'php'));

                    if(count($banksInstallments) > 0) {
                    foreach($banks as $bank) {
                        $code = $bank['code'];
                        if(isset($bank['active_flag']) && $bank['active_flag'] == 'Y' && isset($banksInstallments[$code])) {

                            $conditions = $banksInstallments[$code];
                            foreach ($conditions as $key => $value) {
                                $validForm = $value[0];
                                $validTo = $value[1];

                                if(!empty($validForm) && !empty($validTo)) {
                                    $validForm = strtotime($validForm);
                                    $validTo = strtotime($validTo);

                                    if($validForm <= $curDate && $validTo >= $curDate) {
                                        $this->_banks[$bank['bank']] = $bank['name'];
                                    }
                                } elseif(!empty($validForm) && empty($validTo)) {
                                    $validForm = strtotime($validForm);
                                    if($validForm <= $curDate) {
                                        $this->_banks[$bank['bank']] = $bank['name'];
                                    }
                                } elseif(empty($validForm) && !empty($validTo)) {
                                    $validTo = strtotime($validTo);
                                    if($validTo >= $curDate) {
                                        $this->_banks[$bank['bank']] = $bank['name'];
                                    }
                                } else {
                                    $this->_banks[$bank['bank']] = $bank['name'];
                                }
                            }

                        }
                    }
                }
            }
        }
        return $this->_banks;

	}
	public function getPeriodsBankJson() {
		$bJs = array();
		foreach($this->getPeriods() as $vl=>$lb) {
			$period = $this->_getPaymentModel()->getPeriod($vl,true);
			if($period) {
				$bJs[$vl] = $period['bank'];
			}
		}
		return Mage::helper('core')->jsonEncode($bJs);
	}
	public function getPeriodsBankJson1() {
		$bJs = array();
		foreach($this->getBanks() as $code=>$name) {
			$bankPeriods = array();
			foreach($this->getPeriods() as $vl=>$lb) {
				$period = $this->_getPaymentModel()->getPeriod($vl,true);
				if($period && $period['bank']==$code) {
					$bankPeriods[] = array('value'=>$vl,'lable'=>$lb);
				}
			}
			$bJs[$code] = $bankPeriods;
		}
		return Mage::helper('core')->jsonEncode($bJs);
	}
	protected function _getPaymentModel() {
		if(!$this->_payModel) {
			$this->_payModel = Mage::getModel('cc2p/cc2p')->setOrder($this->getQuote());
		}
		return $this->_payModel;
	}
	public function getCcStored() {
		if(sizeof($this->_cards)<=1) {
			$cards = Mage::getModel('cc2p/card')
					->getCollection()
					->addCustomerIdFilter(Mage::getSingleton('customer/session')->getCustomer()->getId());
			foreach($cards as $card) {
				$this->_cards[$card->getCcType()] = $card->getCcNumber();
			}
			//$this->_cards['new'] = $this->__('new');
		}
		return $this->_cards;
	}

    function getInstallmentItem() {
        $quote = $this->getQuote();

        if(!is_null($quote)) {

            if(is_null($this->_installmentItem)) {
                $allItems = $quote->getAllItems();
                foreach ($allItems as $item) {

                    if ($item->getChildren()) {
                        continue;
                    }

                    $option = $item->getOptionByCode('info_buyRequest');
                    if ($option) {
                        $request = unserialize($option->getValue());
                        if (isset($request['trade_info'])) {
                            $trade = unserialize($request['trade_info']);
                            if($trade) {
                                if(isset($trade['installments']) && $trade['installments'] == 1) {
                                    $this->_installmentItem = $item;
                                    $this->_installmentItem->setCampaignId($trade['campaign_id']);
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            if(is_null($this->_installmentItem)) {
                $this->_installmentItem = false;
            }
            return $this->_installmentItem;
        }
        return false;
    }

	public function hasCards() {

        if(!$this->getInstallmentItem()) {
            return false;
        }

		$cards = $this->getCcStored();
		if(sizeof($cards)>=1) {
			return true;
		}
		else {
			return false;
		}
	}

	public function getQuote() {
		return Mage::getSingleton('checkout/session')->getQuote();
	}
	public function getHeaderText() {
		return $this->getMethod()->getHeaderText();
	}
	public function getFooterText() {
		return $this->getMethod()->getFooterText();
	}
	public function getDes() {
		$items = $this->getQuote()->getAllItems();
		$pr_id = '';
		foreach ($items as $item) {
			$pr_id = $item->getProductId();
		}
		return Mage::getModel('catalog/product')->load($pr_id)->getUrlKey();
	}
	public function getAmount() {
		return $this->getQuote()->getGrandTotal();
	}
	public function getCurrency() {
		return $this->getMethod()->getCurrencyCode();
	}
	public function getUserDefined($num) {
		return $this->getMethod()->getUserDefined($num);
	}
	public function getPayCategoryID() {
		return $this->getMethod()->getPayCategoryID();
	}
	public function getCardHolderName() {
		if($this->getInfoData('cc_owner')) {
			return $this->getInfoData('cc_owner');
		}
		else {
			$customer = Mage::getSingleton('customer/session')->getCustomer();

			return $customer->getFirstname().' '.$customer->getLastname();
		}
	}
	 public function getCountryCollection()
    {
        $collection = $this->getData('country_collection');
        if (is_null($collection)) {
            $collection = Mage::getModel('directory/country')->getResourceCollection();
			if($this->getCountryFilter()) {
				$collection ->addCountryCodeFilter($this->getCountryFilter());
			}
            $this->setData('country_collection', $collection);
        }

        return $collection;
    }

    public function getCountryHtmlSelect($defValue=null, $name='payment[panCountry]', $id=null, $title='Country of Issuer Bank')
    {
        if (is_null($defValue)) {
            $defValue = $this->getCountryId();
        }
		if(!$id) {
			$id = $this->getMethodCode().'_cc_country';
		}
        $cacheKey = 'DIRECTORY_COUNTRY_SELECT_ALL_'.Mage::app()->getStore()->getCode();
        if (Mage::app()->useCache('config') && $cache = Mage::app()->loadCache($cacheKey)) {
            $options = unserialize($cache);
        } else {
            $options = $this->getCountryCollection()->toOptionArray($this->__('--Please Select--'));
            if (Mage::app()->useCache('config')) {
                Mage::app()->saveCache(serialize($options), $cacheKey, array('config'));
            }
        }
        $html = $this->getLayout()->createBlock('core/html_select')
            ->setName($name)
            ->setId($id)
            ->setTitle(Mage::helper('directory')->__($title))
            ->setClass('validate-select')
            ->setValue($defValue)
            ->setOptions($options)
            ->getHtml();
        return $html;
    }
	public function getCountryId()
    {
        $countryId = $this->getData('country_id');
        if (is_null($countryId)) {
            $countryId = Mage::helper('core')->getDefaultCountry();
        }
        return $countryId;
    }
	public function getCcAvailableCountry() {
		$selected = $this->getInfoData('panCountry')?$this->getInfoData('panCountry'):null;
		if($this->getMethod()->getConfigData('allowspecific')==1){
            $availableCountries = explode(',', $this->getMethod()->getConfigData('specificcountry'));
			$this->setCountryFilter($availableCountries);
        }
		return $this->getCountryHtmlSelect($selected);
	}

	public function getCustomerEmail() {
		$stored_email = $this->htmlEscape($this->getInfoData('cardholderEmail'));
		$customer_email = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
		return $stored_email ? $stored_email: $customer_email;
	}


    public function getInstallment() {
        $item = $this->getInstallmentItem();

        if(!$item) {
            return false;
        }

        $installments = Mage::getResourceModel('campaigns/installment_collection')
                              ->getInstallmentByProduct($item->getProductId())
                              ->addFieldToFilter('main_table.campaign_id', array('eq' => $item->getCampaignId()));

        if($installments) {
            return $installments->getFirstItem();
        }
        else {
            return false;
        }

    }

    /**
     * Retrieve availables credit card types
     *
     * @return array
     */
    public function getCcAvailableTypes()
    {
        $types = $this->_getConfig()->getCcTypes();
        if ($method = $this->getMethod()) {
            $availableTypes = $method->getConfigData('cctypes');
            if ($availableTypes) {
                $availableTypes = explode(',', $availableTypes);
                foreach ($types as $code=>$name) {
                    if (!in_array($code, $availableTypes)) {
                        unset($types[$code]);
                    }
                }
            }
        }

        //validate card type
        $cardTypes = array();
        $installment = $this->getInstallment();
        if($installment) {
            $cardTypes = unserialize($installment->getCardType());

            if($cardTypes) {
                //Mage::log($cardTypes, null, 'card_type.log');
                foreach ($types as $code=>$name) {
                    //Mage::log($name, null, 'card_type.log');
                    if($name == 'MasterCard') {
                        $name = 'Master';
                    }

                    if (!in_array(strtoupper($name), $cardTypes)) {
                        unset($types[$code]);
                    }
                }
            }
        }
        return $types;
    }
}
