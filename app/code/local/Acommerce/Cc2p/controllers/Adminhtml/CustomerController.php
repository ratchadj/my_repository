<?php
require_once 'Mage/Adminhtml/controllers/CustomerController.php';
class Acommerce_Cc2p_Adminhtml_CustomerController extends Mage_Adminhtml_CustomerController
{
	public function CardAction() {
		$this->_initCustomer();
		$this->getResponse()->setBody($this->getLayout()
            ->createBlock('cc2p/adminhtml_customer_edit_tabs_card_info')->toHtml());
	}
}