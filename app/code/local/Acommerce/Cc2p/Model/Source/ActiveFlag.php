<?php
class Acommerce_Cc2p_Model_Source_ActiveFlag
{
    public function toOptionArray() {
        return array(
                    array('value' => 'Y', 'label' => 'Yes'),
                    array('value' => 'N', 'label' => 'No')
                );
    }
}
