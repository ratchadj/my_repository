<?php
class Acommerce_Cc2p_Model_Source_InterestType
{
	public function toOptionArray() {
		return array(
					array('value' => 'C', 'label' => 'Customer Pay Interest'),
					array('value' => 'M', 'label' => 'Merchant Pay Interest')
				);
    }
}
