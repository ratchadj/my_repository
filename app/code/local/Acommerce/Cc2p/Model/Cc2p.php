<?php

/**
 * Cc2p payment model
 *
 */
class Acommerce_Cc2p_Model_Cc2p extends Mage_Payment_Model_Method_Cc
{
    protected $_code = 'cc2p';
    protected $_formBlockType = 'cc2p/form';
    protected $_infoBlockType = 'cc2p/info';
    protected $_canAuthorize = true;
    protected $_canUseInternal = true;
    protected $_canUseForMultishipping = false;
    protected $_xml = null;
    protected $_root = null;
    protected $_periods = array();
    protected $_validItem = null;
    protected $_banks = array();

	const PROCESS_TYPE_INQUIRY = 'I';
	const PROCESS_TYPE_RECURRING = 'CR';

	const ACTION_TYPE_REQUEST = 'Request';
	const ACTION_TYPE_RESPONSE = 'Response';
	const ACTION_TYPE_BACKEND_RESPONSE = 'Response-Backend';

	const TYPE_PAYMENT = 'Payment';
	const TYPE_INQUIRY = 'Inquiry';

    public function validateRedirect($order)
    {
        $method = $order->getPayment()->getMethodInstance();
        if ($method->getCode() == $this->getCode()) {
            return true;
        } else {
            return false;
        }
    }

    public function isAvailable($quote = null)
    {
        $active = parent::isAvailable($quote);
        if ($quote && $active) {
            if ($this->getConfigData('dev')) {
                $allowed_emails = explode(',', $this->getConfigData('email'));
                $customer_email = Mage::getSingleton('customer/session')->getCustomer()->getEmail();
                if (in_array($customer_email, $allowed_emails)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return true;
            }
        }
        return $active;
    }
    public function validItem($quote) {
		if(is_null($this->_validItem) && $quote) {
			$skus = trim($this->getConfigData('allow_for'));
			$valid = true;
			if($skus) {
				$skus = explode(',',$skus);

				$items=$quote->getAllItems();
				$match = false;
				foreach($items as $item) {
					if(!in_array(trim($item->getSku()),$skus)) {
						$valid = false;
						break;
					}
					else {
						$match = true;
					}
				}
				if(!$match) {
					$valid = false;
				}
			}
			$this->_validItem = $valid;
		}

		return $this->_validItem;
	}
	public function isCronEnable() {
		return $this->getConfigData('check_status');
	}
	public function cronInvoice() {
		return $this->getConfigData('auto_invoice');
	}
	public function getInquiryUrl() {
		return $this->getConfigData('inquiry_url');
	}

    public function validate()
    {
        return Mage_Payment_Model_Method_Abstract::validate();
    }

    public function getClientIp()
    {
        return $this->getOrder()->getRemoteIp();
    }

    public function getStatus()
    {
        return $this->getConfigData('order_status');
    }

    public function getCustomText()
    {
        $customtext = $this->getConfigData('customtext');
        return $customtext;
    }

    public function getGatewayUrl()
    {
        return $this->getConfigData('gateway');
    }

    public function getMerchantId()
    {
        return $this->getConfigData('merchant_id');
    }

    public function getReturnUrl()
    {
        return Mage::getUrl('cc2p/payment/cc2p', array('_secure' => true));
    }

    protected function getCurrentOrder()
    {
        $order = $this->getOrder();
        if (!($order instanceof Mage_Sales_Model_Order) || !$order->getId()) {
            Mage::throwException($this->_getHelper()->__('Cannot retrieve order object'));
        }
        return $order;
    }

    public function getOrderPlaceRedirectUrl()
    {
		if(!$this->use3DS()) {
			return Mage::getUrl('cc2p/payment/sendTransaction');
		}
		else {
			return Mage::getUrl('cc2p/payment/none3DSPosting');
		}
    }
    public function use3DS() {
		return $this->getConfigData('use_3ds');
	}
	public function getInquiryVersion() {
		return $this->getConfigData('inquiry_ver');
	}
    public function getVersion()
    {
        return $this->getConfigData('version');
    }

	public function getInstallmentVersion() {
		return $this->getConfigData('installment_version');
	}
	public function getSecretKey() {
		return $this->getConfigData('secret_key');
	}
    public function getHeaderText()
    {
        return $this->getConfigData('heder_text');
    }

    public function getFooterText()
    {
        return $this->getConfigData('footer_text');
    }

    public function getKeepFiles()
    {
        return $this->getConfigData('keepfiles');
    }
	public function getKeepInquiryFiles() {
		return $this->getConfigData('keep_inquiry_file');
	}
    public function getStandardCheckoutFormFields()
    {
        return array(
            'paymentRequest' => $this->getEncryptedReq()
        );
    }

    protected function getXml()
    {
        if (!$this->_xml) {
            $this->_xml = new DOMDocument('1.0', 'ISO-8859-1');
            $this->_xml->formatOutput = true;
        }
        return $this->_xml;
    }

    protected function getRoot($root)
    {
        if (!$this->_root) {
            $xml = $this->getXml();
            $this->_root = $xml->createElement($root);
            $xml->appendChild($this->_root);
        }
        return $this->_root;
    }

    public function getSimpleXml(array $array, $root = 'PaymentRequest', $child = '')
    {
        $xml = $this->getXml();
        if ($child) {
            $root = $child;
        } else {
            $root = $this->getRoot($root);
        }
        foreach ($array as $k => $v) {
            $k = $xml->createElement($k);
            $root->appendChild($k);
            if (!is_array($v)) {
                $k->appendChild($xml->createTextNode($v));
            } else {

                $this->getSimpleXml($v, '', $k);
            }
        }

        if ($child) {
            return;
        }

        $xml = $this->removeFirst($xml->saveXML());
        return $this->writeRequest($xml);
    }

    protected function getKey($owner, $type = 'Public')
    {
        $key = glob(Mage::getBaseDir('lib') . DS . 'cc2p' . DS . 'Cert' . DS . $owner . DS . $type . DS . "*.pem");
        if (empty($key)) {
            if ($type == 'Private') {
                $msg = 'Cannot find .pem private key file';
            } else {
                $msg = 'Cannot find .pem public key file';
            }
            Mage::throwException($this->_getHelper()->__($msg));
        }
        return file_get_contents($key[0]);
    }

    protected function writeRequest($xml) {
		if(!$this->getActionType()) {
			$this->setActionType(self::ACTION_TYPE_REQUEST);
		}
		if(!$this->getType()) {
			$this->setType(self::TYPE_PAYMENT);
		}
        $io = $this->getIoFile();
        $path = $this->getOutputFolder();
        if ($this->getOrder()) {
            $id = $this->getOrder()->getIncrementId();
        } else {
            $id = uniqid();
        }

        $input = "{$path}decrypt{$id} .txt";
        $io->write($input, $xml);
        $io->close();
        $encrypt = "{$path}encrypt{$id} .txt";
        file_put_contents($encrypt, '');
        chmod($encrypt, 0777);
        return array('sourcefile' => $input, 'outputfile' => $encrypt);
    }

    protected function removeFirst($xml)
    {
        $return = '';
        $lines = explode("\n", $xml, 2);
        if (!preg_match('/^\<\?xml/', $lines[0])) {
            $return = $lines[0];
        }
        $return .= $lines[1];
        return $return;
    }

    public function getUserDefined($num)
    {
        return $this->getConfigData('userDefined' . $num);
    }

    public function getPayCategoryID()
    {
        return $this->getConfigData('payCategoryID');
    }

    public function getCurrencyCode()
    {
        $code = $this->getConfigData('currencyCode');
        if (!$code) {
            $code = Mage::app()->getBaseCurrencyCode();
            $currencies = Mage::getModel('cc2p/source_currencyCode')->getCurrencyCode();
            $code = $currencies[$code];
        }

        return $code;
    }
	public function getUseInstallment() {
		return $this->getConfigData('installment');
	}
	public function getInterestType() {
		//return $this->getConfigData('interest_type');
		return $this->getConfigData('interest_type');
	}
    public function getRequestData()
    {
        $order = $this->getOrder();
        $detail = '';
        foreach ($order->getAllItems() as $item) {
            $pr = Mage::getModel('catalog/product')->load($item->getProductId());
            $detail .= $pr->getUrlKey();
        }
        // $input_info = $order->getPayment()->getAdditionalInformation();
		$input_info = Mage::getSingleton('checkout/session')->getCreditCardData();
		$pank_bank = ($input_info['panBank'] && $input_info['panBank'] != 'other') ? $input_info['panBank'] : $input_info['panBank2'];

        $req_data = array(
            'version' => $this->getVersion(),
            'timeStamp' => null,
            'merchantID' => $this->getMerchantId(),
            'uniqueTransactionCode' => $order->getIncrementId(),
            'desc' => substr($detail, 0, 50),
            'amt' => $this->getAmount($order->getBaseGrandTotal()),
            'currencyCode' => $this->getCurrencyCode(),
            'securityCode' => $input_info['cc_cid'],
            'clientIP' => $this->getClientIp(),
            'payCategoryID' => $this->getPayCategoryID(),
            'userDefined1' => $this->getUserDefined(1),
            'userDefined2' => $this->getUserDefined(2),
            'userDefined3' => $this->getUserDefined(3),
            'userDefined4' => $this->getUserDefined(4),
            'userDefined5' => $this->getUserDefined(5),
            'ippTransaction' => null,
            'installmentPeriod' => null,
            //'interestType' => $this->getInterestType(),
            'recurring' => null,
            'invoicePrefix' => null,
            'recurringAmount' => null,
            'allowAccumulate' => null,
            'maxAccumulateAmt' => null,
            'recurringInterval' => null,
            'recurringCount' => null,
            'chargeNextDate' => null,
            'promotion' => null,
            'hashValue' => $this->getRequestHash(),
        );
        $period = isset($input_info['period'])?$this->getPeriod($input_info['period'],true):false;
        if(isset($input_info['period']) && $input_info['period'] && $period !== false) {
			$req_data['ippTransaction'] = 'Y';
			$req_data['installmentPeriod'] = $period['period_value'];
			$req_data['promotion'] = $period['promo'];
			$req_data['interestType'] = $period['interest_type'];
			$req_data['version'] = $this->getInstallmentVersion();

            foreach ($order->getAllItems() as $item) {
                if($item->getTradeNo()) {
                    $rate = $period['rate'];
                    if(empty($period['rate']))
                    {
                        $rate = 0;
                    }
                    if(isset($period['period_value'])) {
                        $item->setInstallmentTerm($period['period_value']);
                        $item->setInstallmentRate($rate);
                        $item->save();
                    }
                }
            }
            $order->setIsInstallment(1)->save();
		}
        if (isset($input_info['cc_select_stored_card']) && $input_info['cc_select_stored_card'] != 'new') {
            $card_info = array(
                'storeCardUniqueID' => $this->getStoredCc($input_info['cc_select_stored_card']),
                //'version' => $this->getVersionForStoredCard(),
            );
        } else {
            $store_card = (isset($input_info['store_cc_info']) && $input_info['store_cc_info']) ? 'Y' : null;
            $cc_owner = $input_info['cc_owner_firstname']. ' ' .$input_info['cc_owner_lastname'];
            $card_info = array(
                'expiry' => array('month' => $input_info['cc_exp_month'], 'year' => $input_info['cc_exp_year']),
                'pan' => $input_info['cc_number'],
                'panCountry' => $input_info['panCountry'],
                'panBank' => $pank_bank,
                'cardholderName' => $cc_owner,
                'cardholderEmail' => $input_info['cardholderEmail'],
                'storeCard' => $store_card
            );
        }
        $req_data = array_merge($req_data, $card_info);
        Mage::getSingleton('checkout/session')->unsCreditCardData();
        return $this->getSimpleXml($req_data);
    }
	public function getPeriod($period,$isArray = false) {
		foreach($this->getPeriods() as $_period) {
            //Mage::log($this->getPeriodUnique($_period) .'=='. trim($period), null, 'period.log');
			if($this->getPeriodUnique($_period) == trim($period)) {
				if($isArray) {
					return $_period;
				}
				else {
					return $_period['period_value'];
				}
			}
		}
		return false;
	}
	public function getPeriodUnique(array $period) {
		if(isset($period['period_value']) && isset($period['bank']) && isset($period['minimum']) && isset($period['code'])){
			return trim($period['bank'].$period['period_value'].$period['minimum'].$period['code']);
		}
		return null;
	}
	public function getBanks($bankCode = null) {
		$banks = unserialize($this->getConfigData('bank'));
		if(is_array($banks) && empty($this->_banks)) {
			foreach($banks as $bank) {
				$this->_banks[$bank['bank']] = $bank['name'];
			}
		}
		if($bankCode) {
			foreach($this->_banks as $code => $name) {
				if($code==$bankCode) {
					return $name;
				}
			}
		}
		return $this->_banks;
	}
	public function getPeriods($salesModel = null,$toOption = false) {
		if(is_null($salesModel)) {
			$salesModel = $this->getOrder()?$this->getOrder():$this->getQuote();
		}
		if(!($salesModel instanceof Varien_Object)) {
			return $this->_periods;
		}
		if(/*$this->getUseInstallment() &&*/ empty($this->_periods) /*&&  $this->validItem($salesModel)*/) {
			$periods = unserialize($this->getConfigData('period'));
			$amount = $salesModel->getBaseGrandTotal();
			$_periods = array();
			//$period array('period_label','period_value','bank','minimum','promo','valid_from','valid_to');
			$currTime = Mage::getModel('core/date')->timestamp()-24*3600;
			foreach($periods as $period) {
				if(is_array($period)) {

                    $minimum = (isset($period['minimum']))?(float)$period['minimum']:0;
					if($minimum <= (float)$amount /*&& strtotime($period['valid_from'])<=$currTime && strtotime($period['valid_to'])>=$currTime*/) {
						$match = false;
						// remove the lower values. get best value for IPP
						foreach($_periods as $k => $_p) {
							if($_p['bank'] == $period['bank'] && $_p['period_value'] == $period['period_value'] && $_p['code'] == $period['code']) {
								if($_p['minimum'] < $period['minimum']) {
									$_periods[$k] = $period;
								}
								$match = true;
							}
						}
						if(!$match) {
							$_periods[] = $period;
						}
					}
				}
			}
			$this->_periods = $_periods;
		}
		if($toOption) {
			$options = array();
			foreach($this->_periods as $period) {
				$unique = $this->getPeriodUnique($period);
				$options[$unique] = $period['period_label'];
			}
			return $options;
		}
		return $this->_periods;
	}
	public function getRequestHash() {
		//merchantID + uniqueTransactionCode + amt
		$order = $this->getOrder();
		$data = $this->getMerchantId().$order->getIncrementId().$this->getAmount($order->getBaseGrandTotal());
		return $this->hashData($data);
	}
	public function getResponseHash($tranref,$amount) {
		//merchantID + tranRef + amt
		$data = $this->getMerchantId().$tranref.$this->getAmount($amount);
		return $this->hashData($data);
	}
    public function getStoredCc($cc_number)
    {
        $cards = Mage::getModel('cc2p/card')
                ->getCollection()
                ->addCustomerIdFilter($this->getOrder()->getCustomerId())
                ->addCcNumberFilter($cc_number)
        //->addStoredIdFilter($unique)
        ;

        if (!$cards->count()) {
			$log = 'CustomerID: '.$this->getOrder()->getCustomerId().', cc_number: '.$cc_number.', query: '.$cards->getSelect()->__toString();
			Mage::log($log,null,'StoredCC-error.log');
            throw new Exception("Invalid Stored card info");
        } else {
            return $cards->getFirstItem()->getCcStoredUniqueId();
        }
    }

    protected function getOutputFolder()
    {
		$type = $this->getType()?$this->getType().DS:null;
		$actionType = $this->getActionType()?$this->getActionType().DS:null;
        return Mage::getBaseDir('var') . DS . 'cc2p' . DS . $type.$actionType;
    }

    public function encryptData($keepFiles = null)
    {
		if(is_null($keepFiles)) {
			$keepFiles = $this->getKeepFiles();
		}
        $data = $this->getDatafile();
        $encrypt = $this->getEncryptfile();
        $encrypted_req = '';

        $cc2pPublicKey = $this->getCc2pPublicKey();
        if (openssl_pkcs7_encrypt($data, $encrypt, $cc2pPublicKey, array())) {
            $io = $this->getIoFile();
            $encrypted_req = $io->read($encrypt);
            $io->close();
            if (!$keepFiles) {
                $io->rm($data);
                $io->rm($encrypt);
            }
        } else {
            Mage::throwException($this->_getHelper()->__('Fail to encrypt data'));
        }
        $encrypted_req = trim($this->removeHeader($encrypted_req));

        $this->setEncryptedReq($encrypted_req);
        return $this;
    }
	public function getAmount($amount) {
		return str_pad($amount * 100, 12, '0', STR_PAD_LEFT);
	}
	public function none3DSPosting($asArray = false) {
		$data = $this->getStandardCheckoutFormFields();
		if(empty($data)) {
			return ;
		}
        $result = $this->curlQuery($this->getGatewayUrl(),http_build_query($data));
		$decrypted = null;
		if ($result) {
			$this->setActionType(self::ACTION_TYPE_RESPONSE);
			$this->setEncryptResponse($result)
				 ->decryptData();
			if($asArray) {
				$decrypted = $this->getDecryptedRes()->asArray();
			}
			else {
				$decrypted = $this->getDecryptedRes();
			}
		}
		return $decrypted;
	}
	public function curlQuery($url,$data,$method = Zend_Http_Client::POST) {
		$http = new Varien_Http_Adapter_Curl();
		$http->write($method,$url, '1.1', array(), $data);
        $result = $http->read();
		if (Zend_Http_Response::extractCode($result) == 200) {
			$result = preg_split('/^\r?$/m', $result, 2);
			$result = trim($result[1]);
			return $result;
		}
		return null;
	}
	public function inquiry($order = null,$asArray = true) {
		if($order && $order instanceof Mage_Sales_Model_Order) {
			$this->setOrder($order);
		}
		if(!$this->getType()) {
			$this->setType(self::TYPE_INQUIRY);
		}
		$this->setActionType(self::ACTION_TYPE_REQUEST);
		$data = $this->getInquiryRequestData();
		$encrypted = $this->setDatafile($data['sourcefile'])
					->setEncryptfile($data['outputfile'])
					->encryptData($this->getKeepInquiryFiles())
					->getEncryptedReq();
		$data = http_build_query(array('paymentRequest'=>$encrypted));
        $result = $this->curlQuery($this->getInquiryUrl(),$data);
		$decrypted = null;
		if ($result) {
			$this->setActionType(self::ACTION_TYPE_RESPONSE);
			$this->setEncryptResponse($result)
				 ->decryptData();
			if($asArray) {
				$decrypted = $this->getDecryptedRes()->asArray();
			}
			else {
				$decrypted = $this->getDecryptedRes();
			}
		}
		return $decrypted;

	}
	public function getInquiryRequestData() {
		$order = $this->getOrder();
		if(!$order) {
			throw Mage::exception('Mage_Payment',$this->_getHelper()->__('Order doesn\'t exist'));
		}
		if(!$this->getProcessType()) {
			$this->setProcessType(self::PROCESS_TYPE_INQUIRY);
		}
		// PaymentProcessRequest
		$req = array(
			'version'			=> $this->getInquiryVersion(),
			'timeStamp'			=> $this->getTimestamp(),
			'merchantID'		=> $this->getMerchantId(),
			'recurringUniqueID'	=> null,
			'invoiceNo'			=> $order->getIncrementId(),
			'processType'		=> $this->getProcessType(),
			'hashValue'			=> $this->getInquiryRequestHash($order),
		);

		return $this->getSimpleXml($req,'PaymentProcessRequest');;
	}
	public function getInquiryRequestHash($order) {
		// version + merchantID + invoiceNo + recurringUniqueID + processType
		$data = $this->getInquiryVersion().$this->getMerchantId().$order->getIncrementId().$this->getRecurringId().$this->getProcessType();
		return $this->hashData($data);
	}
	public function getInquiryResponseHash(array $res) {
		$order = $this->getOrder();
		// version + respCode + pan + amt + invoiceNo + tranRef + approvalCode + eci + dateTime + status + failReason
		$data = $this->getInquiryVersion().$res['respCode'].$res['pan'].$this->getAmount($order->getBaseGrandTotal()).
		$order->getIncrementId().$res['tranRef'].$res['approvalCode'].$res['eci'].$res['dateTime'].$res['status'].$res['failReason'];
		return $this->hashData($data);
	}
	protected function hashData($data) {
		$signData = hash_hmac('sha1', $data,$this->getSecretKey(), false);
		$signData =  strtoupper($signData);
		return urlencode($signData);
	}
	public function getTimestamp() {
		// ddMMyyHHmmss
		return Mage::getModel('core/date')->date('dmyHis');
	}
    protected function removeHeader($content)
    {
        $content = str_replace("MIME-Version: 1.0", "", $content);
        $content = str_replace("Content-Disposition: attachment; filename=\"smime.p7m\"", "", $content);
        $content = str_replace("Content-Type: application/x-pkcs7-mime; smime-type=enveloped-data; name=\"smime.p7m\"",
                               "", $content);
        $content = str_replace("Content-Transfer-Encoding: base64", "", $content);
        return $content;
    }

    public function decryptData($keepFiles=null)
    {
		if(is_null($keepFiles)) {
			$keepFiles = $this->getKeepFiles();
		}
		$this->setActionType('Response');
        $encrypt_response = 'MIME-Version: 1.0' . PHP_EOL .
                'Content-Disposition: attachment; filename="smime.p7m"' . PHP_EOL .
                'Content-Type: application/x-pkcs7-mime; smime-type=enveloped-data; name="smime.p7m"' . PHP_EOL .
                'Content-Transfer-Encoding: base64' . PHP_EOL . PHP_EOL .
                $this->getEncryptResponse();

        // add by yoa
        $encrypt_response = wordwrap($encrypt_response, 64, "\n", true);
        $src = $this->writeRequest($encrypt_response);

        /*
          echo "### before ##<br>";
          echo $encrypt_response;
          echo "### After ##<br>";
          echo wordwrap($encrypt_response, 64, "\n", true);
         */

        //var_dump($src);
        //return;

        $encrypted = $src['sourcefile'];
        $decrypted = $src['outputfile'];

        $privaetKey = $this->getPrivateKey();
        $private_key = array($privaetKey, "2c2p");
        $publicKey = $this->getPublicKey();

        if (openssl_pkcs7_decrypt($encrypted, $decrypted, $publicKey, $private_key)) {
            $io = $this->getIoFile();
            $decrypted_res = Mage::getModel('core/config_base', $io->read($decrypted))->getNode();
            if ($keepFiles && ($orderid = (string) $decrypted_res->uniqueTransactionCode)) {
                $io->mv($encrypted, $this->getOutputFolder() . 'encrypt-' . $orderid . '.txt');
                $io->mv($decrypted, $this->getOutputFolder() . 'decrypt-' . $orderid . '.txt');
                $io->close();
            } else {
                $io->close();
                $io->rm($encrypted);
                $io->rm($decrypted);
            }
        } else {
            Mage::throwException($this->_getHelper()->__('Fail to decrypt data'));
        }
        $this->setDecryptedRes($decrypted_res);
        return $this;
    }

	public function getIoFile() {
		$io = new Varien_Io_File();
		$io->setAllowCreateFolders(true);
		$io->open(array('path' => $this->getOutputFolder()));
		return $io;
	}

    public function send()
    {
        $data = 'paymentRequest=' . $this->getEncryptedReq();

        $http = new Varien_Http_Adapter_Curl();
        $http->write(Zend_Http_Client::POST, $this->getGatewayUrl(), '1.1', array(), $data);
        return $this;
    }

    public function saveFailReason()
    {
        $this->importPaymentInfo($this->getParsedData())->save();
        // $order->save();
    }

    protected function getFailInfo($parsed)
    {
        $data = $this->getSuccessInfo();
        $currencies = Mage::getModel('cc2p/source_responseCode')->getResponseCode();
        $data['response_code'] = $currencies[$parsed['pan']];
        $data['amount'] = $parsed['amt'];
        $data['invoice_no'] = $parsed['uniqueTransactionCode'];
        return $data;
    }

    public function parseXml($xml)
    {
        $return = array();
        foreach ($xml as $k => $v) {
            if ($k == 'amt') {
                $v = $v / 100;
            }
            $return[$k] = trim((string) $v);
        }
        return $return;
    }

    protected function getSuccessInfo($parsed)
    {
        return array(
            'masked_cc_number' => $parsed['pan'],
            'approval_code' => $parsed['approvalCode'],
            // 'cc_trans_id' 			=> $parsed['refNumber'],
            'eci' => $parsed['eci'],
            // 'status' 				=> $parsed['status'],
            'fail_reason' => $parsed['failReason']
        );
    }

    protected function importPaymentInfo($parsed)
    {
        $payment = $this->getOrder()->getPayment();
        $was = $payment->getAdditionalInformation();
        //$new = array_merge($this->getSuccessInfo($parsed), $was);
        $new = $this->getSuccessInfo($parsed);
        $payment->setAdditionalInformation($new);
		$payment->setCcNumberEnc($parsed['pan']);
        return $payment;
    }

    public function saveInvoice()
    {
        try {
            $order = $this->getOrder();
            $parsed = $this->getParsedData();
            $payment = $this->importPaymentInfo($parsed);
            if ($order->canInvoice()) {

                $payment->setCcTransId($parsed['refNumber'])
                        ->setCcApproval($parsed['status'])
                        ->setTransactionId($parsed['tranRef'])
                        ->registerCaptureNotification($parsed['amt'])
                        ;
                $order->save();
            }
            $invoice = $payment->getCreatedInvoice();
            if ($invoice && !$order->getEmailSent()) {

                $order->sendNewOrderEmail()->addStatusHistoryComment(
                                //Mage::helper('cc2p')->__('Notified customer about invoice #%s.', $invoice->getIncrementId())

                                Mage::helper('cc2p')->__('Notified customer about invoice #%s.',
                                                         $invoice->getIncrementId())
                        )
                        ->setIsCustomerNotified(true)
                        ->save();
            }
            if ($invoice && isset($parsed['storeCardUniqueID']) && $parsed['storeCardUniqueID']) {
                $cards = Mage::getModel('cc2p/card')
                        ->getCollection()
                        ->addCustomerIdFilter($order->getCustomerId())
                        ->addStoredIdFilter($parsed['storeCardUniqueID']);

                if (!$cards->count()) {
                    Mage::getModel('cc2p/card')->setCustomerId($this->getOrder()->getCustomerId())
                            ->setCcStoredUniqueId($parsed['storeCardUniqueID'])
                            ->setCcOwner($payment->getCcOwner())
                            ->setCcType($payment->getCcType())
                            ->setCcNumber($this->hideCardNumber($parsed['pan']))
                            ->setCcCurrency($order->getBaseCurrencyCode())
                            ->setCcExp($payment->getCcExpMonth() . '/' . $payment->getCcExpYear())
                            ->save()
                    ;
                }
            }
            /* if ($order->canShip()) {
              $shipment = $order->prepareShipment()->register();
              $shipment->getOrder()->setActionFlag(Mage_Sales_Model_Order::ACTION_FLAG_SHIP, true);
              Mage::getModel('core/resource_transaction')	->addObject($shipment->getOrder())
              ->addObject($shipment)->save();
              } */
            return true;
        } catch (Exception $e) {
            Mage::log($e->getMessage(), null, 'Cc2p.log');
        }
        return false;
    }

    public function hideCardNumber($number)
    {
        $last_2 = substr($number, -2);
        return 'XXXX-XXXX-XXXX-XX' . $last_2;
    }

    public function assignData($data) {

        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }

        $info = $this->getInfoInstance();
        if($data->getData('panBank')) {
			$bank = $this->getBanks($data->getData('panBank'));
		}
        if($data->getData('period')!='0' && $data->getPeriod() && isset($bank)) {
			$this->setQuote($info->getQuote());
			$period = $this->getPeriod($data->getPeriod(),true);
			if(!$period || $period['bank'] != $data->getData('panBank')) {
				throw Mage::exception('Mage_Payment_Model_Info',Mage::helper('cc2p')->__('Selected installment period is not for "%s".',$bank));
			}
		}
		else {
			$data->getData('period',false);
		}
		if(isset($bank)) {
			$data->setData('panBank',$bank);
		}
		// $info->addData(array('additional_information'=>$data->getData()));
        Mage::getSingleton('checkout/session')->setCreditCardData($data->getData());
        $CcOwner = $data->getCcOwnerFirstname() . ' ' . $data->getCcOwnerLastname();
		$maskedNumber = substr($data->getCcNumber(), 0, 6).'XXXXXX'.substr($data->getCcNumber(), -4);
        $info->setCcType($data->getCcType())
                ->setCcOwner($CcOwner)
                ->setCcLast4(substr('XX' . $data->getCcNumber(), -2))
                ->setCcNumberEnc($maskedNumber)
                //->setCcCid($data->getCcCid())
                ->setCcExpMonth($data->getCcExpMonth())
                ->setCcExpYear($data->getCcExpYear())
        ;

        return $this;
    }

    public function getCc2pPublicKey()
    {
        return $this->getConfigData('cc2p_key');
    }

    public function getPublicKey()
    {
        return $this->getConfigData('public_key');
    }

    public function getPrivateKey()
    {
        return $this->getConfigData('private_key');
    }

}
