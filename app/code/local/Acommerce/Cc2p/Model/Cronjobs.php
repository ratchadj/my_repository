<?php
class Acommerce_Cc2p_Model_Cronjobs {
	protected $_paymodel = null;
	public function checkTransaction() {
		$cc2p = $this->getPayModel();
		if($cc2p->isCronEnable() && $cc2p->getInquiryUrl()) {

			$collection = Mage::getResourceModel('cc2p/order_grid_collection')
							->addFieldToFilter('main_table.status', array('nin' => $this->_getExcludedStates()))
							->addFieldToFilter('payment.cc_number_enc',array('null'=> true))
                            ->addFieldToFilter('created_at',array('lteq'=> new Zend_Db_Expr("DATE_SUB(NOW(), INTERVAL 30 MINUTE)")));;

            $ids = $collection->getAllIds();
			Mage::helper('cc2p')->transactionInquiry($ids,$cc2p->cronInvoice());
		}
	}
	 protected function _getExcludedStates() {
        return array(
            Mage_Sales_Model_Order::STATE_CLOSED,
            Mage_Sales_Model_Order::STATE_CANCELED,
            Mage_Sales_Model_Order::STATE_HOLDED,
            Mage_Sales_Model_Order::STATE_PROCESSING,
            Mage_Sales_Model_Order::STATE_COMPLETE,
        );
    }
	public function getPayModel() {
		if(!$this->_paymodel) {
			$this->_paymodel = Mage::getModel('cc2p/cc2p');
		}
		return $this->_paymodel;
	}
}
