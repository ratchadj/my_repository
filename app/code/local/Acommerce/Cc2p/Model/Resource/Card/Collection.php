<?php

class Acommerce_Cc2p_Model_Resource_Card_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('cc2p/card');
    }
	public function addCustomerIdFilter($c_id) {
		$this->getSelect()->where('customer_id=?',$c_id);
		return $this;
	}
	public function addStoredIdFilter($s_id) {
		$this->getSelect()->where('cc_stored_unique_id=?',$s_id);
		return $this;
	}
	public function addCcNumberFilter($s_id) {
		$this->getSelect()->where('cc_number=?',$s_id);
		return $this;
	}
}