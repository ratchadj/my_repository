<?php
class Acommerce_Cc2p_Helper_Data extends Mage_Core_Helper_Abstract {
	protected function _validData($data) {
		if(!is_array($data)) {
			$data = array($data);
		}

		if(isset($data['amt'])) {
			$data['amt'] = $data['amt'] / 100.00;
		}

		return array_merge(array(
					'Version'=>'',
					'timeStamp'=>'',
					'respCode'=>'',
					'pan'=>'',
					'amt'=>'',
					'invoiceNo'=>'',
					'tranRef'=>'',
					'approvalCode'=>'',
					'eci'=>'',
					'dateTime'=>'',
					'status'=>'',
					'failReason'=>'',
					'recurringActive'=>'',
					'hashValue'=>'',
				),$data);
	}
	public function transactionInquiry($orderIds,$invoice = false) {
		if(!is_array($orderIds)) {
			$orderIds = array($orderIds);
		}

		$query = 0;
		$inv = 0;
		$messages = array();
		$InvoicedOrderIds = array();

		foreach($orderIds as $id) {
			$order = Mage::getModel('sales/order')->loadByIncrementId($id);
			//Mage::log($id, null, 'cc2p_inquiry.log');
			if($order->getId() /*&& !in_array($order->getState(),array(Mage_Sales_Model_Order::STATE_COMPLETE,Mage_Sales_Model_Order::STATE_CLOSED))*/) {
				try {

					$payModel = Mage::getModel('cc2p/cc2p');
					if(!$payModel->getInquiryUrl()) {
						throw Mage::exception('Mage_Payment',$this->__('Inquiry URL was not set correctly'));
					}

					$result = $this->_validData($payModel->inquiry($order));
					if($result) {
						$resHash = isset($result['HashValue'])?$result['HashValue']:'';
						++$query;
						$payModel->setParsedData($result);

						if($result['respCode'] == '00' && $result['hashValue'] == $payModel->getInquiryResponseHash($result) && ($result['status']=='A' || $result['status']=='S')) {
							//$hash = $payModel->hashData($payModel->getMerchantId().$id.$transaction->getTransactionId());
							if($invoice && $order->canInvoice()) {
								$payModel->saveInvoice();
								$InvoicedOrderIds[] = $order->getId();
								++$inv;
							} elseif($invoice && $order->hasInvoices() && !$order->canInvoice()) {

					            $payment = $order->getPayment();
					            $data = array(
								            'masked_cc_number' => $result['pan'],
								            'approval_code' => $result['approvalCode'],
								            'eci' => $result['eci'],
								            'fail_reason' => $result['failReason']
								        );
					            $payment->setAdditionalInformation($data);
					            $payment->setCcNumberEnc($result['pan'])
					            		->setCcTransId($result['refNumber'])
					                    ->setCcApproval($result['status'])
					                    ->setTransactionId($result['tranRef']);

					            $payment->save();
								$InvoicedOrderIds[] = $order->getId();
							}
						}
						else {
							//$messages[$id] = Mage::helper('cc2p')->__('The result was saved to order');
							//$payModel->saveFailReason();
						}
					}
					else {
						//$messages[$id] = Mage::helper('cc2p')->__('Can not query the transaction from gateway');;
					}
				}
				catch(Exception $e) {
					$messages[$id] = $e->getMessage();
				}
			}
			elseif(!$order->getId()) {
				$messages[$id] = Mage::helper('cc2p')->__('Order doesn\'t exist.');
			}
			else {
				$messages[$id] = Mage::helper('cc2p')->__('Orders with status "complete","closed" won\'t be queried.');;
			}
		}

		if(count($InvoicedOrderIds)  > 0) {
			Mage::getModel('salesexport/export')->forceExportCsvData($InvoicedOrderIds);
		}

		return array(array($query,$inv),$messages);
	}
}
