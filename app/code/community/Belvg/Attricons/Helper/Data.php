<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Attricons
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Attricons_Helper_Data extends Mage_Core_Helper_Abstract
{
    const TYPE_IMAGE = "image";
    const TYPE_TEXT  = "text";
    const TYPE_COLOR = "color";
    const TYPE_CATEGORY_COLOR_PRODUCT_IMAGE = "color_image";
    const TYPE_CATEGORY_IMAGE_PRODUCT_IMAGE = "image_image";
    
    const STATUS_SELECTED = 'selected';
    const STATUS_DISABLED = 'disabled';

    public function getIcoTypes()
    {
        return array(
            $this->__('Image') => self::TYPE_IMAGE,
            $this->__('Text')  => self::TYPE_TEXT,
            $this->__('Color') => self::TYPE_COLOR,
            $this->__('Color - Category / Image - Product Page') => self::TYPE_CATEGORY_COLOR_PRODUCT_IMAGE,
            $this->__('Image - Category / Image - Product Page') => self::TYPE_CATEGORY_IMAGE_PRODUCT_IMAGE,
        );
    }

    /**
     * Get allowed image formats
     * 
     * @return array
     */
    public function getAllowedExtensions()
    {
        return array('jpg', 'jpeg', 'gif', 'png');
    }

    /**
     * Get swatches dir
     * 
     * @param string $filename
     * @return string
     */
    public function getImagePath($filename = '')
    {
        return Mage::getBaseDir('media') . DS . 'attricons' . DS . $filename;
    }

    /**
     * Get swatches path
     * 
     * @param string $filename
     * @return string
     */
    public function getImage($filename = '')
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . 'attricons/' . $filename;
    }

    /**
     * Get resized swatch file path
     * 
     * @param string $swatch
     * @return string
     */
    public function getSwatchImage($swatch = NULL, $width = FALSE, $height = FALSE)
    {        
        if (is_string($swatch)) {
            $filename = $swatch;
        } elseif ($swatch instanceof Varien_Object) {
            //$filename = $swatch->getImagePath();
            $filename = $swatch->getImage();
        } else {
            return '';
        }

        if (!$width || !$height) {
            return $this->getImage($filename);
        }

        $resizedFolder = sprintf('%dx%d', $width, $height);
        
        if (!file_exists($this->getImagePath($resizedFolder . DS . $filename))) {
            if (!file_exists($this->getImagePath($resizedFolder))) {
                @mkdir($this->getImagePath($resizedFolder), 0777);
            }

            try {
                $imageObj = new Varien_Image($this->getImagePath($filename));
                $imageObj->constrainOnly(TRUE);
                $imageObj->keepFrame(FALSE);
                $imageObj->keepTransparency(TRUE);
                $imageObj->resize($width, $height);

                $imageObj->save($this->getImagePath($resizedFolder . DS . $filename));
            } catch (Exception $e) {
                Mage::log($e->getMessage(), NULL, 'attricons.log');
            }
        }

        return $this->getImage($resizedFolder . '/' . $filename);
    }

    public function getSwatchIco($optionId, $productId = FALSE, $default)
    {
        $swatchesProduct = Mage::getResourceModel('attricons/product_collection')->addFieldToFilter('value_index', $optionId);
        if ($productId) {
            $swatchesProduct->addFieldToFilter('product_id', $productId);
        }

        $swatch = $swatchesProduct->getLastItem();
        if ($swatch->getId()) {
            return $this->generateIcoHtml($swatch, $default);
        }

        $swatch = Mage::getResourceModel('attricons/attribute_collection')->getItemByColumnValue('option_id', $optionId);
        if ($swatch) {
            return $this->generateIcoHtml($swatch, $default);
        }

        //return $default;
        return $this->generateIcoHtml(new Varien_Object, $default);
    }

    public function generateIcoCategoryHtml($swatch, $default, $module = FALSE, $additionalClass = FALSE, $status = FALSE, $width = FALSE, $height = FALSE)
    {
        if (!$width) {
            if ($module) {
                if (in_array($swatch->getType(), array(self::TYPE_CATEGORY_IMAGE_PRODUCT_IMAGE, self::TYPE_IMAGE))) { 
                    $width  = (int)Mage::getStoreConfig('attricons/settings/' . $module . '_catalog_image_width');
                } else {
                    $width  = (int)Mage::getStoreConfig('attricons/settings/' . $module . '_catalog_icon_width');
                }
            } else {
                $width  = (int)Mage::getStoreConfig('attricons/settings/catalog_icon_width');
            }
        }

        if (!$height) {
            if ($module) {
                if (in_array($swatch->getType(), array(self::TYPE_CATEGORY_IMAGE_PRODUCT_IMAGE, self::TYPE_IMAGE))) { 
                    $height  = (int)Mage::getStoreConfig('attricons/settings/' . $module . '_catalog_image_height');
                } else {
                    $height  = (int)Mage::getStoreConfig('attricons/settings/' . $module . '_catalog_icon_height');
                }
            } else {
                $height  = (int)Mage::getStoreConfig('attricons/settings/catalog_icon_height');
            }
        }
        
        $selected_image_src = Mage::getBaseUrl('media') . $module . DS . Mage::getStoreConfig('attricons/settings/' . $module . '_selected_image');
        
        $class = 'belvg-ico';
        if ($additionalClass) {
            $class .= ' ' . $additionalClass;
        }
        
        $src = ($status)?Mage::getBaseUrl('media') . $module . DS . Mage::getStoreConfig('attricons/settings/' . $module . '_' . $status . '_image'):FALSE;
        if ($src) {
            $marker = '<img class="belvg-ico-selected" style="position:absolute;width: ' . $width . 'px; height:' . $height . 'px" src="' . $src . '"/>';
        } else {
            $marker = FALSE;
        }
        
        switch ($swatch->getType()) {
            case self::TYPE_CATEGORY_IMAGE_PRODUCT_IMAGE:
                if ($swatch->getImageCategory()) {
                    $class .= ' belvg-ico-image';
                    $ico = $marker . '<img class="' . $class . '" src="' . $this->getSwatchImage($swatch->getImageCategory(), $width, $height) . '">';
                }

                // ico = category image
                break;

            case self::TYPE_IMAGE:
                if ($swatch->getImage()) {
                    $class .= ' belvg-ico-image';
                    $ico =  $marker . '<img class="' . $class . '" src="' . $this->getSwatchImage($swatch->getImage(), $width, $height) . '">';
                }

                // ico = product view image
                break;

            case self::TYPE_TEXT:
                if ($swatch->getText()) {
                    $class .= ' belvg-ico-text';
                    $ico =  $marker . '<span class="' . $class . '">' . $swatch->getText() . '</span>';
                }

                // ico = text icon
                break;

            case self::TYPE_CATEGORY_COLOR_PRODUCT_IMAGE:
            case self::TYPE_COLOR:
                if ($swatch->getColor()) {
                    $class .= ' belvg-ico-color';
                    $ico = $marker . '<span class="' . $class . '" style="background:#' . $swatch->getColor() . '; width: ' . $width . 'px; height:' . $height . 'px"></span>';
                }

                // ico = color box
                break;

            default:
                $ico = FALSE;
                break;
        }

        if (!isset($ico) || !$ico) {
            $ico = '<span class="belvg-ico belvg-ico-text">' . $default . '</span>';
        }

        return $ico . $this->_generateDescriptionHtml($swatch);
    }

    public function generateIcoHtml($swatch, $default, $width = FALSE, $height = FALSE)
    {
        if (!$width) {
            $width  = (int) Mage::getStoreConfig('attricons/settings/swatch_width');
        }

        if (!$height) {
            $height = (int) Mage::getStoreConfig('attricons/settings/swatch_height');
        }

        switch ($swatch->getType()) {
            case self::TYPE_CATEGORY_COLOR_PRODUCT_IMAGE:
            case self::TYPE_CATEGORY_IMAGE_PRODUCT_IMAGE:
            case self::TYPE_IMAGE:
                if ($swatch->getImage()) {
                    $ico = '<img class="belvg-colorswatch-ico belvg-colorswatch-ico-image" src="' . $this->getSwatchImage($swatch, $width, $height) . '">';
                }

                // ico = category image / product view image
                break;

            case self::TYPE_TEXT:
                if ($swatch->getText()) {
                    $ico = '<span class="belvg-colorswatch-ico belvg-colorswatch-ico-text">' . $swatch->getText() . '</span>';
                }

                // ico = text icon
                break;

            case self::TYPE_COLOR:
                if ($swatch->getColor()) {
                    $ico    = '<span class="belvg-colorswatch-ico belvg-colorswatch-ico-color" style="background:#' . $swatch->getColor() . '; width: ' . $width . 'px; height:' . $height . 'px"></span>';
                }

                // ico = color box
                break;

            default:
                $ico = FALSE;
                break;
        }

        if (!isset($ico) || !$ico) {
            $ico = '<span class="belvg-colorswatch-ico belvg-colorswatch-ico-text">' . $default . '</span>';
        }

        return $ico . $this->_generateDescriptionHtml($swatch);
    }

    protected function _generateDescriptionHtml($swatch)
    {
        $data = $swatch->getData();
        if ((isset($data['zoom']) && $data['zoom']) || (isset($data['description']) && $data['description'])) {
            return Mage::app()->getLayout()->createBlock('core/template')
                    ->setData($data)
                    ->setTemplate('belvg/attricons/description.phtml')
                    ->toHtml();
        }

        return '';
    }
}
