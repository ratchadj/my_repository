<?php

$installer = $this;
$installer->startSetup();

if (!$this->tableExists($this->getTable('attricons/product'))) {
    $installer->run("
    -- DROP TABLE IF EXISTS {$this->getTable('attricons/product')};

    CREATE TABLE IF NOT EXISTS {$this->getTable('attricons/product')} (
      `image_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Image ID',
      `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product ID',
      `product_super_attribute_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Product Super Attribute ID',
      `value_index` varchar(255) DEFAULT NULL COMMENT 'Value Index',
      `type` varchar(12) NOT NULL,
      `image` varchar(255) DEFAULT NULL COMMENT 'Image',
      `image_category` varchar(255) DEFAULT NULL COMMENT 'Icon Image in Category',
      `text` varchar(25) NOT NULL,
      `color` varchar(7) NOT NULL,
      `store_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Store ID',
      `zoom` varchar(255) NOT NULL,
      `main` varchar(255) NOT NULL,
      `description` varchar(255) NOT NULL,
      PRIMARY KEY (`image_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Super Attribute Images Table';
    ");

    /*$installer->run("
      KEY `IDX_BELVG_ATTRIBUTEICON_PRODUCT_PRODUCT_ID` (`product_id`),
      KEY `IDX_BELVG_ATTRIBUTEICON_PRODUCT_PRODUCT_SUPER_ATTRIBUTE_ID` (`product_super_attribute_id`),
      KEY `IDX_BELVG_ATTRIBUTEICON_PRODUCT_STORE_ID` (`store_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Super Attribute Images Table';

    ALTER TABLE {$this->getTable('attributeicon/product')} 
      ADD CONSTRAINT `FK_BELVG_ATTRIBUTEICON_PRD_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES {$installer->getTable('catalog/product')} (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
      ADD CONSTRAINT `FK_BELVG_ATTRIBUTEICON_PRODUCT_PRODUCT_SUPER_ATTRIBUTE_ID` FOREIGN KEY (`product_super_attribute_id`) REFERENCES {$installer->getTable('catalog/product_super_attribute_label')} (`product_super_attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
      ADD CONSTRAINT `FK_BELVG_ATTRIBUTEICON_PRODUCT_STORE_ID_CORE_STORE_STORE_ID` FOREIGN KEY (`store_id`) REFERENCES {$installer->getTable('core/store')} (`store_id`) ON DELETE CASCADE ON UPDATE CASCADE;
    ");*/
}

if (!$this->tableExists($this->getTable('attricons/attribute'))) {
    $installer->run("
    -- DROP TABLE IF EXISTS {$this->getTable('attricons/attribute')};

    CREATE TABLE IF NOT EXISTS {$this->getTable('attricons/attribute')} (
      `image_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Image ID',
      `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id',
      `option_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Option Id',
      `type` varchar(12) NOT NULL,
      `image` varchar(255) DEFAULT NULL COMMENT 'Image',
      `image_category` varchar(255) DEFAULT NULL COMMENT 'Icon Image in Category',
      `text` varchar(25) NOT NULL,
      `color` varchar(7) NOT NULL,
      `zoom` varchar(255) NOT NULL,
      `description` varchar(255) NOT NULL,
      PRIMARY KEY (`image_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Attribute Option Images Table';
    ");

    /*$installer->run("
      KEY `IDX_BELVG_ATTRIBUTEICON_ATTRIBUTE_ATTRIBUTE_ID` (`attribute_id`),
      KEY `IDX_BELVG_ATTRIBUTEICON_ATTRIBUTE_OPTION_ID` (`option_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Attribute Option Images Table';

    ALTER TABLE {$this->getTable('attributeicon/attribute')}
      ADD CONSTRAINT `FK_BELVG_ATTRIBUTEICON_ATTR_ATTR_ID_EAV_ATTR_ATTR_ID` FOREIGN KEY (`attribute_id`) REFERENCES {$installer->getTable('eav/attribute')} (`attribute_id`) ON DELETE CASCADE ON UPDATE CASCADE,
      ADD CONSTRAINT `FK_BELVG_ATTRIBUTEICON_ATTR_OPT_ID_EAV_ATTR_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES {$installer->getTable('eav/attribute_option')} (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE;
    ");*/
}

if (!$this->tableExists($this->getTable('attricons/loader'))) {
    $installer->run("
    -- DROP TABLE IF EXISTS {$this->getTable('attricons/loader')};

    CREATE TABLE IF NOT EXISTS {$this->getTable('attricons/loader')} (
      `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
      `product_id` int(11) unsigned NOT NULL,
      `option_id` int(11) unsigned NOT NULL,
      `image` varchar(255) NOT NULL,
      PRIMARY KEY (`id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Images Table';
    ");

    /*$installer->run("
      KEY `IDX_BELVG_ATTRIBUTEICON_LOADER_ATTRIBUTE_ID` (`product_id`),
      KEY `IDX_BELVG_ATTRIBUTEICON_LOADER_OPTION_ID` (`option_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Images Table';
    ALTER TABLE {$this->getTable('attributeicon/loader')} 
      ADD CONSTRAINT `FK_BELVG_ATTRIBUTEICON_LOADER_PRD_ID_CAT_PRD_ENTT_ENTT_ID` FOREIGN KEY (`product_id`) REFERENCES {$installer->getTable('catalog/product')} (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE,
      ADD CONSTRAINT `FK_BELVG_ATTRIBUTEICON_LOADER_OPT_ID_EAV_ATTR_OPT_OPT_ID` FOREIGN KEY (`option_id`) REFERENCES {$installer->getTable('eav/attribute_option')} (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE;
    ");*/
}

$installer->run("
    ALTER TABLE `{$this->getTable('attricons/product')}` ADD `attribute_id` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'Attribute Id';
");

$installer->endSetup();

