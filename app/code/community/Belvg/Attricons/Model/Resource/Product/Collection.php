<?php
if (version_compare(Mage::getVersion(), '1.6.1', '>')) {
    abstract class Belvg_Attricons_Model_Resource_Product_Collection_Abstract extends Mage_Core_Model_Resource_Db_Collection_Abstract {};
} else {
    abstract class Belvg_Attricons_Model_Resource_Product_Collection_Abstract extends Mage_Core_Model_Mysql4_Collection_Abstract {};
}

class Belvg_Attricons_Model_Resource_Product_Collection extends Belvg_Attricons_Model_Resource_Product_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('attricons/product');
    }
    
    protected function _afterLoad()
    {
        parent::_afterLoad();
        return $this;
    }
}
