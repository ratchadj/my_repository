<?php
if (version_compare(Mage::getVersion(), '1.6.1', '>')) {
    abstract class Belvg_Attricons_Model_Resource_Product_Abstract extends Mage_Core_Model_Resource_Db_Abstract {};
} else {
    abstract class Belvg_Attricons_Model_Resource_Product_Abstract extends Mage_Core_Model_Mysql4_Abstract {};
}

class Belvg_Attricons_Model_Resource_Product extends Belvg_Attricons_Model_Resource_Product_Abstract
{
    protected function _construct()
    {
        $this->_init('attricons/product', 'image_id');
    }
}
