<?php
class Belvg_Attricons_Model_Attribute extends Belvg_Attricons_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('attricons/attribute');
    }

    public function saveSwatches()
    {
        foreach ($this->getSwatches() AS $data) {
            if (isset($data['option_id'])) {
                $swatch = Mage::getModel('attricons/attribute')->load($data['option_id'], 'option_id');
                $id     = $swatch->getId();

                $swatch->setData($data);
                if ($id) {
                    $swatch->setId($id);
                }

                $swatch->save();
            }
        }

        return $this;
    }
}
