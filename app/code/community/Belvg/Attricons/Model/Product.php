<?php
class Belvg_Attricons_Model_Product extends Belvg_Attricons_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('attricons/product');
    }

    public function saveSwatches()
    {
        foreach ($this->getSwatches() AS $data) {
            if (isset($data['value_index'])) {
                $swatch = Mage::getModel('attricons/product')->getCollection()
                    ->addFieldToFilter('value_index', $data['value_index'])
                    ->addFieldToFilter('product_id', $data['product_id'])
                    ->getLastItem();
                $id = $swatch->getId();

                $swatch->setData($data);
                if ($id) {
                    $swatch->setId($id);
                }

                $swatch->save();
            }
        }

        return $this;
    }
}
