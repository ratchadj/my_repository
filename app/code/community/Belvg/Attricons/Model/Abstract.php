<?php
abstract class Belvg_Attricons_Model_Abstract extends Mage_Core_Model_Abstract
{
    protected $_swatches = array();

    public function getSwatches()
    {
        return $this->_swatches;
    }

    public function addSwatche($data)
    {
        $this->_swatches[] = $data;

        return $this;
    }

    public function unsetValues()
    {
        $this->_swatches = array();

        return $this;
    }

    abstract public function saveSwatches();
}
