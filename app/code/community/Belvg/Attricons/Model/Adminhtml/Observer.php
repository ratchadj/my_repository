<?php
class Belvg_Attricons_Model_Adminhtml_Observer
{
    public function saveProductSwatches(Varien_Event_Observer $observer)
    {
        $attributs = Mage::app()->getRequest()->getPost('attricons');
        if (!$attributs) {
            return $this;
        }

        $swatch    = Mage::getModel('attricons/product');
        $files     = Mage::app()->getRequest()->getPost('attricons-file');
        $productId = $observer->getProduct()->getId();
        try {
            foreach ($attributs AS $productSuperAttributeId => $options) {
                foreach ($options AS $optionId => $data) {
                    if (isset($data['use_default'])) {
                        $item = Mage::getModel('attricons/product')->getCollection()
                            ->addFieldToFilter('value_index', $optionId)
                            ->addFieldToFilter('product_id', $productId)
                            ->getLastItem();
                        if ($item->getId()) {
                            $item->delete();
                        }
                    } else {
                        $data['product_super_attribute_id'] = $productSuperAttributeId;
                        $data['value_index']   = $optionId;
                        $data['product_id']    = $productId;

                        if (isset($files[$productSuperAttributeId][$optionId])) {
                            $newFiles          = $this->savePreview($files[$productSuperAttributeId][$optionId], $optionId, $productId);
                            if (isset($data['image']) && isset($newFiles[$data['image']])) {
                                $data['image'] = $newFiles[$data['image']];
                            } else {
                                $data['image'] = '';
                            }

                            if (isset($data['image_category']) && isset($newFiles[$data['image_category']])) {
                                $data['image_category'] = $newFiles[$data['image_category']];
                            } else {
                                $data['image_category'] = '';
                            }

                            if (isset($data['zoom']) && isset($newFiles[$data['zoom']])) {
                                $data['zoom']  = $newFiles[$data['zoom']];
                            } else {
                                $data['zoom']  = '';
                            }
                        }

                        $swatch->addSwatche($data);
                    }
                }
            }

            $swatch->saveSwatches();
        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError($e->getMessage());
        }

        return $this;
    }

    public function saveAttributeSwatches(Varien_Event_Observer $observer)
    {
        $attribute = $observer->getEvent()->getAttribute();
        $attributs = Mage::app()->getRequest()->getPost('attricons');
        if (!$attributs) {
            return $this;
        }

        $swatch    = Mage::getModel('attricons/attribute');
        $files     = Mage::app()->getRequest()->getPost('attricons-file');
        $productId = 0;
        try {
            foreach ($attributs AS $productSuperAttributeId => $options) {
                foreach ($options AS $optionId => $data) {
                    //$data['type']         = $type;
                    $data['attribute_id']  = $attribute->getId();
                    $data['option_id']     = $optionId;

                    if (isset($files[$productSuperAttributeId][$optionId])) {
                        $newFiles          = $this->savePreview($files[$productSuperAttributeId][$optionId], $optionId, $productId);
                        if (isset($data['image']) && isset($newFiles[$data['image']])) {
                            $data['image'] = $newFiles[$data['image']];
                        } else {
                            $data['image'] = '';
                        }

                        if (isset($data['image_category']) && isset($newFiles[$data['image_category']])) {
                            $data['image_category'] = $newFiles[$data['image_category']];
                        } else {
                            $data['image_category'] = '';
                        }

                        if (isset($data['zoom']) && isset($newFiles[$data['zoom']])) {
                            $data['zoom']  = $newFiles[$data['zoom']];
                        } else {
                            $data['zoom']  = '';
                        }
                    }

                    $swatch->addSwatche($data);
                }
            }

            $swatch->saveSwatches();
        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError($e->getMessage());
        }

        return $this;
    }

    public function savePreview($files, $optionId, $productId)
    {
        $newFiles = array();
        if (is_array($files) && count($files)) {
            foreach ($files AS $id => $data) {
                if (isset($data['remove']) && $data['remove']) {
                    if (isset($data['id']) && $data['id']) {
                        $item = Mage::getModel('attricons/loader')->load($data['id']);
                        if ($item->getId()) {
                            $item->delete();
                            //@unlink(Mage::helper('attricons')->getImagePath($item->getImage()));
                        }
                    }
                } else {
                    if (isset($data['id']) && $data['id']) {
                        $newFiles[$data['path']] = $data['path'];
                    } else {
                        $item = Mage::getSingleton('attricons/loader');
                        $item->setData($data);
                        $item->setOptionId($optionId);
                        $item->setProductId($productId);

                        $newFiles[$data['path']] = $item->moveFile();
                        $item->save();
                    }
                }
            }
        }

        return $newFiles;
    }

    public function loadProductSwatchesToCollection(Varien_Event_Observer $observer)
    {
        //$product    = Mage::registry('product'); 
        $collection = $observer->getEvent()->getCollection();

        if (/*$product && */($collection instanceof Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Type_Configurable_Attribute_Collection || $collection instanceof Mage_Catalog_Model_Resource_Product_Type_Configurable_Attribute_Collection) && $collection->count()) {
            $productId = (int) Mage::app()->getRequest()->getParam('id');
            $swatchesAttribute = Mage::getModel('attricons/attribute')
                ->getResourceCollection()
                ->addFieldToFilter('attribute_id', array(
                    'in' => $collection->getColumnValues('attribute_id')
                ));

            $swatchesProduct = Mage::getModel('attricons/product')
                ->getResourceCollection()
                /* ->addFieldToFilter('product_super_attribute_id', array(
                    'in' => $collection->getAllIds()
                )) */
                ->addFieldToFilter('product_id', $productId);

            foreach ($collection AS $item) {
                foreach ($swatchesAttribute AS $swatch) {
                    $swatches = (array) $item->getSwatchDefault();
                    $swatches[$swatch->getOptionId()] = $swatch;
                    $item->setSwatchDefault($swatches);
                    //$swatch->setAttributeSwatch(TRUE);
                }

                $superAttrId     = $item->getData('product_super_attribute_id');
                $_swatchesProduct = $swatchesProduct->getItemsByColumnValue('product_super_attribute_id', $superAttrId);
                foreach ($_swatchesProduct AS $swatch) {
                    $swatches = (array) $item->getSwatch();
                    $swatches[$swatch->getValueIndex()] = $swatch;
                    $item->setSwatch($swatches);
                    //$swatch->setAttributeSwatch(FALSE);
                }
            }
        }

        return $this;
    }

    public function loadAttributeSwatchesToCollection(Varien_Event_Observer $observer)
    {
        $collection = $observer->getCollection();
        
        if (($collection instanceof Mage_Eav_Model_Mysql4_Entity_Attribute_Option_Collection || $collection instanceof Mage_Eav_Model_Resource_Entity_Attribute_Option_Collection) && $collection->hasFlag('load_swatches') && $collection->count()) {
            $firstItem = $collection->getFirstItem();

            $swatchesAttribute = Mage::getModel('attricons/attribute')
                ->getResourceCollection()
                ->addFieldToFilter('attribute_id', array(
                    'in' => $firstItem->getAttributeId()
                ));

            foreach ($swatchesAttribute as $swatch) {
                $item = $collection->getItemById($swatch->getOptionId());
                if ($item) {
                    $item->addData($swatch->getData());
                }
            } 
        }

        return $this;
    }
}

