<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
/**********************************************
 *        MAGENTO EDITION USAGE NOTICE        *
 **********************************************/
/* This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
/**********************************************
 *        DISCLAIMER                          *
 **********************************************/
/* Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 **********************************************
 * @category   Belvg
 * @package    Belvg_Attricons
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

class Belvg_Attricons_Model_Loader extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('attricons/loader');
    }
    
    public function moveFile()
    {
        $tmpPath = $this->getPath();
        if (!$tmpPath) {
            return FALSE;
        }

        $newPath = $this->_changeFilePath($tmpPath);
        $this->_moveTmpFile($tmpPath, $newPath);
        $this->setImage($newPath);
        
        return $newPath;
    }

    protected function _changeFilePath($tmpPath)
    {
        $newPath = '';
        if ($tmpPath) {
            $fileInfo = pathinfo($tmpPath);
            if (isset($fileInfo['extension'])) {
                $optionId = (int) $this->getOptionId();
                $newPath  = $optionId . '/' . $fileInfo['basename'];
            }
        }

        return $newPath;
    }

    /*public function saveByObject($optionId, $filesData, $newFiles = FALSE)
    {
        $editCollection = $this->getCollectionByObject($optionId);
        if (is_array($filesData)) {
            foreach ($filesData AS $key => $fileData) {
                if (isset($fileData['id']) && $fileData['id']) {
                    // Edit
                    $file = $editCollection->getItemById($fileData['id']);
                    if (is_null($file)) {
                        continue;
                    }
                } else {
                    // New
                    if (!isset($fileData['path'])) {
                        continue; // no file to upload
                    }

                    if (isset($fileData['remove'])) {
                        continue; // change of heart
                    }

                    $file = Mage::getModel('attricons/loader');
                }

                if (isset($fileData['remove'])) {
                    $file->delete();
                } else {
                    $file->setData($fileData)->setObjectId($optionId);

                    if (isset($fileData['path']) && $fileData['path']) {
                        if (isset($newFiles[$key]) && $newFiles[$key]) {
                            $file->setUrl($this->_moveTmpFile($fileData['path'], $newFiles[$key]));
                        } else {
                            $file->setUrl($this->_moveTmpFile($fileData['path'], $fileData['path']));
                        }
                    }

                    $file->save();
                }
            }
        }
    }*/

    protected function _moveTmpFile($tmpFile, $newFile = FALSE)
    {
        $config = Mage::getSingleton('attricons/loader_config');
        if ($tmpFile) {
            $tmpPathFull       = $config->getTmpMediaPath($tmpFile);
            if ($newFile) {
                $mediaPathFull = $config->getMediaPath($newFile);
            } else {
                $mediaPathFull = $config->getMediaPath($tmpFile);
            }

            $this->createWritableDir(pathinfo($mediaPathFull, PATHINFO_DIRNAME));
            @copy($tmpPathFull, $mediaPathFull);

            if ($newFile) {
                return $config->getMediaUrl($newFile);
            } else {
                return $config->getMediaUrl($tmpFile);
            }
        } else {
            return NULL;
        }
    }

    public function createWritableDir($path)
    {
        $io = new Varien_Io_File();

        if (!$io->isWriteable($path) && !$io->mkdir($path, 0777, TRUE)) {
            Mage::throwException(Mage::helper('attricons')->__("Cannot create writable directory '%s'.", $path));
        }

        return $this;
    }
}