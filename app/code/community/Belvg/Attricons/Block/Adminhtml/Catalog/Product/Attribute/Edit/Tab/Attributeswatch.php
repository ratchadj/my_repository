<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Attricons
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Attricons_Block_Adminhtml_Catalog_Product_Attribute_Edit_Tab_Attributeswatch extends Mage_Adminhtml_Block_Widget implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_options;
    
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('belvg/attricons/catalog/product/attribute/edit/tab/attributeswatch.phtml');
    }

    /**
     * Return Tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->helper('attricons')->__('Attribute Icons');
    }

    /**
     * Return Tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->helper('attricons')->__('Attribute Icons');
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        $attribute = Mage::registry('entity_attribute');

        if ($attribute->getId() AND $attribute->getIsGlobal() AND $attribute->getFrontendInput() == 'select' AND $attribute->getIsConfigurable()) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        $_attribute   = Mage::registry('entity_attribute');
        $attributeIds = Mage::getStoreConfig('attricons/settings/attributes');
        $attributeIds = explode(',', $attributeIds);

        return !in_array($_attribute->getId(), $attributeIds);
    }

    /**
     * Get product options collection
     *
     * @return Varien_Data_Collection
     */
    public function getOptions()
    {
        if (!$this->_options) {
            $attribute      = Mage::registry('entity_attribute');
            $this->_options = Mage::getResourceModel('eav/entity_attribute_option_collection')
                ->setAttributeFilter($attribute->getId())
                ->setPositionOrder(Varien_Data_Collection::SORT_ORDER_ASC, TRUE)
                ->setFlag('load_swatches')
                ->load();
        }

        return $this->_options;
    }

    public function currentAttributeType()
    {
        $types       = $this->helper('attricons')->getIcoTypes();
        $options     = $this->getOptions();
        $optionFirst = $options->getFirstItem();
        $currentType = $optionFirst->getData('type');
        if (!in_array($currentType, $types)) {
            $currentType = $types[key($types)];
        }

        return $currentType;
    }

    /**
     * Get item row html
     *
     * @param   $item
     * @return  string
     */
    public function getItemHtml($item)
    {
        $block = $this->getLayout()->getBlock('attricons_options_item');
        $block->setItem($item);
        if (! $block instanceof Mage_Core_Block_Template) {
            return '';
        }

        return $block->toHtml();
    }
    
}