<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Attricons
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Attricons_Block_Adminhtml_Catalog_Product_Edit_Tab_Productswatch extends Mage_Adminhtml_Block_Widget implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_productAttributes;

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('belvg/attricons/catalog/product/edit/tab/productswatch.phtml');
    }

    /**
     * Return Tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return $this->helper('attricons')->__('Attribute Icons');
    }

    /**
     * Return Tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return $this->helper('attricons')->__('Attribute Icons');
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     */
    public function canShowTab()
    {
        $_product = Mage::registry('current_product');
        if ($_product->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     */
    public function isHidden()
    {
        return ($this->getRequest()->getParam('store') != 0) ? TRUE : FALSE;
    }

    /**
     * Get product attributes
     *
     * @return array
     */
    public function getProductAttributes()
    {
        if (is_null($this->_productAttributes)) {
            $_product = Mage::registry('current_product');
            $this->_productAttributes = $_product->getTypeInstance()->getConfigurableAttributes($_product);
        }

        return $this->_productAttributes;
    }

    /**
     * Check store config to show/not show swathes form for a particular attribute
     * 
     * @param int $attrId
     * @return boolean
     */
    public function checkAttributes($attrId = NULL)
    {
        $attributeIds = Mage::getStoreConfig('attricons/settings/attributes');
        $attributeIds = explode(',', $attributeIds);

        if ($attrId) {
            return in_array($attrId, $attributeIds);
        }

        foreach ($this->getProductAttributes() AS $attr) {
            if (in_array($attr->getAttributeId(), $attributeIds)) {
                return TRUE;
            }
        }

        return FALSE;
    }

    public function currentAttributeType($attribute)
    {
        $types       = $this->helper('attricons')->getIcoTypes();
        $options     = (array) $attribute->getPrices();
        $currentType = FALSE;
        if (count($options)) {
            $first   = $options[key($options)];
            if (isset($attribute['swatch_default'][$first['value_index']])) {
                $swatch      = $attribute['swatch_default'][$first['value_index']];
                $currentType = $swatch->getType();
            }
        }

        if (!in_array($currentType, $types)) {
            $currentType = $types[key($types)];
        }

        return $currentType;
    }

    /**
     * Get item row html
     *
     * @param   $item
     * @return  string
     */
    public function getItemHtml($item)
    {
        $block = $this->getLayout()->getBlock('attricons_options_item');
        $block->setItem($item);
        if (! $block instanceof Mage_Core_Block_Template) {
            return '';
        }

        return $block->toHtml();
    }

    /**
     * Get item Default row html
     *
     * @param   $item
     * @return  string
     */
    public function getItemHtmlDefault($item)
    {
        $block = $this->getLayout()->getBlock('attricons_options_item_default');
        $block->setItem($item);
        if (! $block instanceof Mage_Core_Block_Template) {
            return '';
        }

        return $block->toHtml();
    }
}