<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Attricons
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Attricons_Block_Form_Element_Loader extends Mage_Adminhtml_Block_Widget
{
    protected $_optionId;
    protected $_uploader = array();

    protected function _getOptionId()
    {
        $item = $this->getItem();
        return (int) $item->getOptionId();
    }

    public function getFileCollection()
    {
        $productId  = (int) $this->getRequest()->getParam('id');
        $collection = Mage::getModel('attricons/loader')->getCollection()
            ->addFieldToFilter('option_id', $this->_getOptionId())
            ->addFieldToFilter('product_id', array('in' => array(0, $productId)));

        return $collection;
    }

    /*protected function _prepareLayout()
    {
        $this->setChild('uploader' . $this->_getOptionId(),
            $this->getLayout()->createBlock('adminhtml/media_uploader')
        );

        $this->getUploader()->getConfig()
            ->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('attricons/loader/upload'))
            ->setFileField('file')
            ->setFilters(array(
                'images' => array(
                    'label' => Mage::helper('adminhtml')->__('Images (.gif, .jpg, .png)'),
                    'files' => array('*.gif', '*.jpg', '*.png')
                ),
            ));

        return parent::_prepareLayout();
    }*/

    /**
     * Retrive uploader block
     *
     * @return Mage_Adminhtml_Block_Media_Uploader
     */
    public function getUploader()
    {
        //return $this->getChild('uploader' . $this->_getOptionId());
        $optionId = $this->_getOptionId();
        if (!isset($this->_uploader[$optionId])) {
            $this->_uploader[$optionId] = $this->getLayout()->createBlock('adminhtml/media_uploader');
            //$this->_uploader[$optionId] = $this->getLayout()->createBlock('attricons/form_element_loader_buttons');
            $this->_uploader[$optionId]->getConfig()
                ->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('attricons/loader/upload'))
                ->setFileField('file')
                ->setFilters(array(
                    'images' => array(
                        'label' => Mage::helper('adminhtml')->__('Images (.gif, .jpg, .png)'),
                        'files' => array('*.gif', '*.jpg', '*.png')
                    ),
                ));
            $this->_uploader[$optionId]->setTemplate('belvg/attricons/loader/buttons.phtml');
        }

        return $this->_uploader[$optionId];
    }

    /**
     * Retrive uploader block html
     *
     * @return string
     */
    public function getUploaderHtml()
    {
        return $this->getUploader()->toHtml();
    }

}