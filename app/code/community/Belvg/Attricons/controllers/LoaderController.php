<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
 *******************************************************************
 * @category   Belvg
 * @package    Belvg_Attricons
 * @version    1.0.0
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */
class Belvg_Attricons_LoaderController extends Mage_Core_Controller_Front_Action
{
    protected function _isAllowed()
    {
        return TRUE;
    }

    public function uploadAction()
    {
        try {
            $uploader = new Mage_Core_Model_File_Uploader('file');
            $uploader->setAllowedExtensions(array('jpg','gif','png'));
            $uploader->setAllowRenameFiles(FALSE);
            $uploader->setFilesDispersion(TRUE);
            $result   = $uploader->save(
                Mage::getSingleton('attricons/loader_config')->getBaseTmpMediaPath()
            );

            $result['url'] = Mage::getSingleton('attricons/loader_config')->getTmpMediaUrl($result['file']);
        } catch (Exception $e) {
            $result = array(
                'error'     => $e->getMessage(),
                'errorcode' => $e->getCode());
        }

        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }
}