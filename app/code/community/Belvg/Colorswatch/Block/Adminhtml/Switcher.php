<?php

class Belvg_Colorswatch_Block_Adminhtml_Switcher extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    /**
     * Render element html
     *
     * @param Varien_Data_Form_Element_Abstract
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $switcher      = Mage::helper('belvgcolorswatch/switcher');
        $extensionName = $switcher->getExtensionName();
        $sizes         = $switcher->getSizes();
        $id   = $element->getHtmlId();
        $html = '<tr id="row_' . $id . '" class="system-fieldset-sub-head row_' . $id . '">
                    <td colspan="5">
                        <input id="' . $id . '" name="' . $element->getName() . '" value="" type="hidden">
                        <h4>' . $element->getLabel() . '</h4>
                        ' . (($element->getComment()) ? '<p class="note"><span>' . $element->getComment() . '</span></p>' : '') . '
                        <table>
                            ' . $this->_renderSizes($element) . '
                        </table>
                    </td>
                 </tr>';

        return $html;
    }

    protected function _renderSizes($element)
    {
        $switcher      = Mage::helper('belvgcolorswatch/switcher');
        $extensionName = $switcher->getExtensionName();
        $sizes         = $switcher->getSizes($this->getElementOriginalData($element, 'size_type'));
        $html          = '';

        if ($extensionName) {
            $html .= '<tr class="system-fieldset-sub-head">
                        <td colspan="5">' . $this->__('Inherited from Extnsion: %s', $extensionName) . '</td>
                      </tr>';
        }


        if (!isset($sizes['mainHeight']) || !$sizes['mainHeight']) {
            $sizes['mainHeight'] = 'auto';
        }

        $html .= '<tr class="system-fieldset-sub-head">
                    <td class="label">' . $this->__('Main Width') . '</td>
                    <td class="value" colspan="4">' . $sizes['mainWidth'] . '</td>
                  </tr>
                  <tr class="system-fieldset-sub-head">
                    <td class="label">' . $this->__('Main Height') . '</td>
                    <td class="value" colspan="4">' . $sizes['mainHeight'] . '</td>
                  </tr>
                  <tr class="system-fieldset-sub-head">
                    <td class="label">' . $this->__('Thumb Width') . '</td>
                    <td class="value" colspan="4">' . $sizes['thumbWidth'] . '</td>
                  </tr>
                  <tr class="system-fieldset-sub-head">
                    <td class="label">' . $this->__('Thumb Height') . '</td>
                    <td class="value" colspan="4">' . $sizes['thumbHeight'] . '</td>
                  </tr>';

        return $html;
    }

    /**
     * Get element data by key
     *
     * @param Varien_Data_Form_Element_Abstract $element
     * @param string
     * @return string
     */
    public function getElementOriginalData(Varien_Data_Form_Element_Abstract $element, $key)
    {
        $data = $element->getOriginalData();
        return isset($data[$key]) ? $data[$key] : FALSE;
    }
}
