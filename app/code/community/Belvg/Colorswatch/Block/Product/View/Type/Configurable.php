<?php
class Belvg_Colorswatch_Block_Product_View_Type_Configurable extends Mage_Catalog_Block_Product_View_Type_Configurable
{
    protected function _construct()
    {
        //print_r($this->getBlockAlias()); die;
        /*$this->addData(array(
            'cache_lifetime' => 3600,
            'cache_tags'     => array(Mage_Catalog_Model_Product::CACHE_TAG),
            'cache_key'      => Belvg_Colorswatch_Helper_Data::CACHE_KEY . $this->getProduct()->getId(),
        ));*/
    }

    /**
     * Composes configuration for js
     *
     * @return string
     */
    public function getJsonConfigFromBase()
    {
        $attributes = array();
        $options    = array();
        $store      = $this->getCurrentStore();
        $taxHelper  = Mage::helper('tax');
        $currentProduct = $this->getProduct();
        $finalPrices = array();

        $preconfiguredFlag = $currentProduct->hasPreconfiguredValues();
        if ($preconfiguredFlag) {
            $preconfiguredValues = $currentProduct->getPreconfiguredValues();
            $defaultValues       = array();
        }

        foreach ($this->getAllowProducts() as $product) {
            $productId  = $product->getId();

            foreach ($this->getAllowAttributes() as $attribute) {
                $productAttribute   = $attribute->getProductAttribute();
                $productAttributeId = $productAttribute->getId();
                $attributeValue     = $product->getData($productAttribute->getAttributeCode());
                if (!isset($options[$productAttributeId])) {
                    $options[$productAttributeId] = array();
                }

                if (!isset($options[$productAttributeId][$attributeValue])) {
                    $options[$productAttributeId][$attributeValue] = array();
                }
                $options[$productAttributeId][$attributeValue][] = $productId;
                $finalPrices[$productId] = $product->getFinalPrice();
            }
        }

        $this->_resPrices = array(
            $this->_preparePrice($currentProduct->getFinalPrice())
        );

        foreach ($this->getAllowAttributes() as $attribute) {
            $productAttribute = $attribute->getProductAttribute();
            $attributeId = $productAttribute->getId();
            $info = array(
               'id'        => $productAttribute->getId(),
               'code'      => $productAttribute->getAttributeCode(),
               'label'     => $attribute->getLabel(),
               'options'   => array()
            );

            $optionPrices = array();
            $prices = $attribute->getPrices();
            if (is_array($prices)) {
                foreach ($prices as $value) {
                    if(!$this->_validateAttributeValue($attributeId, $value, $options)) {
                        continue;
                    }
                    $currentProduct->setConfigurablePrice(
                        $this->_preparePrice($value['pricing_value'], $value['is_percent'])
                    );
                    $currentProduct->setParentId(true);
                    Mage::dispatchEvent(
                        'catalog_product_type_configurable_price',
                        array('product' => $currentProduct)
                    );
                    $configurablePrice = $currentProduct->getConfigurablePrice();

                    if (isset($options[$attributeId][$value['value_index']])) {
                        $productsIndex = $options[$attributeId][$value['value_index']];
                    } else {
                        $productsIndex = array();
                    }

                    $finalPrice = 0;
                    if(is_array($productsIndex)) {
                        $finalPrice = (isset($finalPrices[$productsIndex[0]])) ? $finalPrices[$productsIndex[0]] : 0;
                    } else {
                        $finalPrice = $finalPrices[$productsIndex];
                    }

                    if($finalPrice == 0) {
                        $finalPrice = $configurablePrice;
                    } else {
                        $finalPrice = $finalPrice - $currentProduct->getFinalPrice();
                    }


                    $info['options'][] = array(
                        'id'        => $value['value_index'],
                        'label'     => $value['label'],
                        'price'     => $finalPrice,//$configurablePrice,
                        'oldPrice'  => $this->_prepareOldPrice($value['pricing_value'], $value['is_percent']),
                        'products'  => $productsIndex,
                    );
                    $optionPrices[] = $configurablePrice;
                }
            }
            /**
             * Prepare formated values for options choose
             */
            foreach ($optionPrices as $optionPrice) {
                foreach ($optionPrices as $additional) {
                    $this->_preparePrice(abs($additional-$optionPrice));
                }
            }
            if($this->_validateAttributeInfo($info)) {
               $attributes[$attributeId] = $info;
            }

            // Add attribute default value (if set)
            if ($preconfiguredFlag) {
                $configValue = $preconfiguredValues->getData('super_attribute/' . $attributeId);
                if ($configValue) {
                    $defaultValues[$attributeId] = $configValue;
                }
            }
        }

        $taxCalculation = Mage::getSingleton('tax/calculation');
        if (!$taxCalculation->getCustomer() && Mage::registry('current_customer')) {
            $taxCalculation->setCustomer(Mage::registry('current_customer'));
        }

        $_request = $taxCalculation->getDefaultRateRequest();
        $_request->setProductClassId($currentProduct->getTaxClassId());
        $defaultTax = $taxCalculation->getRate($_request);

        $_request = $taxCalculation->getRateRequest();
        $_request->setProductClassId($currentProduct->getTaxClassId());
        $currentTax = $taxCalculation->getRate($_request);

        $taxConfig = array(
            'includeTax'        => $taxHelper->priceIncludesTax(),
            'showIncludeTax'    => $taxHelper->displayPriceIncludingTax(),
            'showBothPrices'    => $taxHelper->displayBothPrices(),
            'defaultTax'        => $defaultTax,
            'currentTax'        => $currentTax,
            'inclTaxTitle'      => Mage::helper('catalog')->__('Incl. Tax')
        );

        $config = array(
            'attributes'        => $attributes,
            'template'          => str_replace('%s', '#{price}', $store->getCurrentCurrency()->getOutputFormat()),
            'basePrice'         => $this->_registerJsPrice($this->_convertPrice($currentProduct->getFinalPrice())),
            'oldPrice'          => $this->_registerJsPrice($this->_convertPrice($currentProduct->getPrice())),
            'productId'         => $currentProduct->getId(),
            'chooseText'        => Mage::helper('catalog')->__('Choose an Option...'),
            'taxConfig'         => $taxConfig
        );

        if ($preconfiguredFlag && !empty($defaultValues)) {
            $config['defaultValues'] = $defaultValues;
        }

        $config = array_merge($config, $this->_getAdditionalConfig());

        return Mage::helper('core')->jsonEncode($config);
    }

    /**
     * Composes configuration for js
     *
     * @return string
     */
    public function getJsonConfig()
    {
        Mage::helper('catalog/product')->setSkipSaleableCheck(true);
        $config = $this->getJsonConfigFromBase();
        $attributeIds = $optionIds = array();
        foreach ($this->getAllowAttributes() as $attribute) {
            $attributeIds[]  = $attribute->getAttributeId();
            foreach ($attribute->getPrices() as $value) {
                $optionIds[] = $value['value_index'];
            }
        }

        $super_attributes = array();
        foreach ($this->getProduct()->getTypeInstance()->getConfigurableAttributesAsArray() as $attr) {
            $super_attributes[] = $attr['id'];
        }

        /*$swatchesAttribute = Mage::getModel('attricons/attribute')
            ->getResourceCollection()
            ->addFieldToFilter('attribute_id', array('in' => $attributeIds));

        $swatchesProduct   = Mage::getModel('attricons/product')
            ->getResourceCollection()
            ->addFieldToFilter('value_index', array('in' => $optionIds))
            ->addFieldToFilter('product_id', $this->getProduct()->getId());*/

        $config = Mage::helper('core')->jsonDecode($config); $productsIds = array();
        foreach ($config['attributes'] as $attrId => &$attribute) {
            foreach ($attribute['options'] as &$option) {
                $option['swatch'] = $this->helper('attricons')->getSwatchIco($option['id'], $this->getProduct()->getId(), $option['label']);
                $productsIds      = array_merge($productsIds, $option['products']);
            }
        }

        $config['attributeIdsToChange']     = explode(',', Mage::getStoreConfig('attricons/settings/attributes'));
        $config['attributeIdsThatChange']   = explode(',', Mage::getStoreConfig('belvgcolorswatch/imageswitcher/attributes'));
        $config['showNotAvailable']         = (int) Mage::getStoreConfig('belvgcolorswatch/settings/show_not_available');
        $config['imageSwitcher']['enabled'] = (int) Mage::getStoreConfig('belvgcolorswatch/imageswitcher/enabled');
        if ($config['imageSwitcher']['enabled']) {
            $config['imageSwitcher']['collection'] = $this->_getProductsData($productsIds);
            $config['imageSwitcher']['collection'][$this->getProduct()->getId()] = $this->_getMainProductImages();
        }

        $config['imageSwitcher']['notAvailable'] = (int) Mage::getStoreConfig('belvgcolorswatch/imageswitcher/not_available');
        if ($config['imageSwitcher']['notAvailable']) {
            $message = trim(Mage::getStoreConfig('belvgcolorswatch/imageswitcher/not_available_message'));
            $config['imageSwitcher']['notAvailableTemplate'] = '<div class="not-available-message" style="display:none">' . (($message) ? $message : $this->__('photo not available')) . '</div>';
        }

        $config['dataSwitcher']['stockEnabled'] = (int) Mage::getStoreConfig('belvgcolorswatch/dataswitcher/stock_enabled');
        if ($config['dataSwitcher']['stockEnabled']) {
            $config['dataSwitcher']['stockUpperLimit'] = (int) Mage::getStoreConfig('belvgcolorswatch/dataswitcher/stock_upper_limit');
            $message = trim(Mage::getStoreConfig('belvgcolorswatch/dataswitcher/stock_message'));
            $config['dataSwitcher']['stockMessageTemplate'] = '<div class="stock-message">' . (($message) ? $message : $this->__('Only <span>{{qty}}</span> left in stock - order soon.')) . '</div>';
        }

        return Mage::helper('core')->jsonEncode($config);
    }

    /**
     * Get product resized main image and thumbs
     *
     * @param array $productsIds
     * @return array
     */
    protected function _getProductsData($productsIds = array())
    {
        $data       = array('sizes' => Mage::helper('belvgcolorswatch/switcher')->getSizes(Mage::registry('media_size_type')));
        $helper      = Mage::helper('catalog/image');
        $mainProduct = $this->getProduct();
        $productsIds = array_unique($productsIds);

        foreach ($productsIds as $productId) {
            $product = ($mainProduct->getId() == $productId) ? $mainProduct : Mage::getModel('catalog/product')->load($productId);
            $data[$productId]['qty'] = (int) $product->getStockItem()->getQty();

            if ($product->getImage() && $product->getImage() != 'no_selection') {
                $data[$productId]['main'] = array(
                    //'resized'  => (string) $helper->init($product, 'image')->resize($data['sizes']['mainWidth'], $data['sizes']['mainHeight']),
                    'resized'  => (string) $helper->init($product, 'image')
                        ->keepFrame(false)
                        ->constrainOnly(true)
                        ->resize($data['sizes']['mainWidth']),
                    'original' => (string) $helper->init($product, 'image'),
                    'label'    => $product->getData('image_label') ? $product->getData('image_label') : $product->getName(),
                );

                if (isset($data['sizes']['mainHeight'])) {
                    $data[$productId]['main']['resized'] = (string) $helper->init($product, 'image')->resize($data['sizes']['mainWidth'], $data['sizes']['mainHeight']);
                } else {
                    $data[$productId]['main']['resized'] = (string) $helper->init($product, 'image')
                        ->keepFrame(false)
                        ->constrainOnly(true)
                        ->resize($data['sizes']['mainWidth']);
                }
            }

            $gallery = $product->getMediaGalleryImages();
            if ($gallery->getSize()) {
                foreach ($gallery as $image) {
                    if (isset($data['sizes']['mainHeight'])) {
                        $data[$productId]['thumbs'][] = array(
                            'thumb'    => (string) $helper->init($product, 'image', $image->getFile())->resize($data['sizes']['thumbWidth'], $data['sizes']['thumbHeight']),
                            'resized'  => (string) $helper->init($product, 'image', $image->getFile())->resize($data['sizes']['mainWidth'], $data['sizes']['mainHeight']),
                            'original' => (string) $helper->init($product, 'image', $image->getFile()),
                            'label'    => $image->getLabel(),
                        );
                    } else {
                        $data[$productId]['thumbs'][] = array(
                            'thumb'    => (string) $helper->init($product, 'image', $image->getFile())->resize($data['sizes']['thumbWidth'], $data['sizes']['thumbHeight']),
                            'resized'  => (string) $helper->init($product, 'image', $image->getFile())
                                ->keepFrame(false)
                                ->constrainOnly(true)
                                ->resize($data['sizes']['mainWidth']),
                            'original' => (string) $helper->init($product, 'image', $image->getFile()),
                            'label'    => $image->getLabel(),
                        );
                    }
                }
            } else {
                $data[$productId]['thumbs'] = array();
            }
        }

        return $data;
    }

    protected function _getMainProductImages()
    {
        $product = $this->getProduct();
        $images  = $this->_getProductsData(array($product->getId()));

        return $images[$product->getId()];
    }
}
