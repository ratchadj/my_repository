<?php
/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
/**********************************************
 *        MAGENTO EDITION USAGE NOTICE        *
 **********************************************/
/* This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
/**********************************************
 *        DISCLAIMER                          *
 **********************************************/
/* Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 **********************************************
 * @category   Belvg
 * @package    Belvg_Colorswatch
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

class Belvg_Colorswatch_Block_Product_List extends Mage_Catalog_Block_Product_List
{
    protected $_ids = FALSE;

    /**
     * Getting current product ids list
     * @return array
     */
    public function getProductIds() 
    {
        if ($this->_ids === FALSE) {
            $collection = $this->getLoadedProductCollection();
            $this->_ids = $collection->getColumnValues('entity_id');
        }

        return $this->_ids;
    }

    public function getSwatches() 
    {
        $swatches = array();
        $collection = $this->getLoadedProductCollection();
        foreach ($collection as $_product) {
            if ($_product->getSwatchesData()) {
                $swatches[$_product->getId()] = $_product->getSwatchesData();
            }
        }

        return $swatches;
    }
}