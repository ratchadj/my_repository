<?php
class Belvg_Colorswatch_Helper_Data extends Mage_Core_Helper_Abstract
{
    const CACHE_KEY = 'colorswatch_attribute_icons_product';

    /**
     * Is the extension enabled?
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return Mage::getStoreConfig('belvgcolorswatch/settings/enabled');
    }

    public function storeConfig($path)
    {
        return Mage::getStoreConfig('belvgcolorswatch/' . $path);
    }

    /**
     * Replace media template if extension is enabled
     * 
     * @param string $name
     * @param string $block
     * @return string
     */
    public function switchTemplateIf($name, $block)
    {
        if ($this->isEnabled() && Mage::getStoreConfig('belvgcolorswatch/imageswitcher/enabled')) {
            return $name;
        }

        if ($blockObject = Mage::getSingleton('core/layout')->getBlock($block)) {
            return $blockObject->getTemplate();
        }

        return '';
    }

    public function getListSettings()
    {
        $settings = array(
            'image_switcher_attributes' => explode(',', Mage::getStoreConfig('belvgcolorswatch/imageswitcher/attributes')),
            'category_attributes'       => explode(',', Mage::getStoreConfig('belvgcolorswatch/category/attributes')),
            'catalog_icon_width'        => (int) Mage::getStoreConfig('attricons/settings/catalog_icon_width'),
            'catalog_icon_height'       => (int) Mage::getStoreConfig('attricons/settings/catalog_icon_height'),
        );

        $settings['imageSwitcher']['notAvailable'] = (int) Mage::getStoreConfig('belvgcolorswatch/imageswitcher/not_available');
        if ($settings['imageSwitcher']['notAvailable']) {
            $message = trim(Mage::getStoreConfig('belvgcolorswatch/imageswitcher/not_available_message'));
            $settings['imageSwitcher']['notAvailableTemplate'] = '<div class="not-available-message" style="display:none">' . (($message) ? $message : $this->__('photo not available')) . '</div>';
        }

        return $settings;
    }
}
