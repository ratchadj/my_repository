<?php
class Belvg_Colorswatch_Helper_Switcher extends Mage_Core_Helper_Abstract
{
    public function getExtensionName()
    {
        if ($this->isProductViewGalleryEnabled()) {
            return 'Product View Gallery';
        } elseif ($this->isProductZoomLightEnabled()) {
            return 'Product Zoom Light';
        }

        return FALSE;
    }

    /**
     * image switcher is enabled/disabled
     *
     * @return boolean
     */
    public function isEnabled()
    {
        return Mage::getStoreConfig('belvgcolorswatch/imageswitcher/enabled', Mage::app()->getStore());
    }

    /**
     * The Product View Gallery is enabled/disabled
     *
     * @return boolean
     */
    public function isProductViewGalleryEnabled()
    {
        if (Mage::helper('core')->isModuleEnabled('Belvg_Productviewgallery') && Mage::getStoreConfig('productviewgallery/settings/enabled', Mage::app()->getStore())) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * The Product Zoom Light is enabled/disabled
     *
     * @return boolean
     */
    public function isProductZoomLightEnabled()
    {
        if (Mage::helper('core')->isModuleEnabled('Belvg_ProductZoomLight') && (Mage::getStoreConfig('productzoomlight/cloudzoom/enabled', Mage::app()->getStore()) || Mage::getStoreConfig('productzoomlight/fancybox/enabled', Mage::app()->getStore()))) {
            return TRUE;
        }

        return FALSE;
    }

    public function getSizes($type = '')
    {
        if ($this->isProductViewGalleryEnabled()) {
            return array(
                'mainWidth'   => Mage::helper('productviewgallery')->getMainWidth(),
                'mainHeight'  => Mage::helper('productviewgallery')->getMainHeight(),
                'thumbWidth'  => Mage::helper('productviewgallery')->getThumbWidth(),
                'thumbHeight' => Mage::helper('productviewgallery')->getThumbHeight(),
            );
        } elseif ($this->isProductZoomLightEnabled()) {
            return Mage::helper('productzoomlight')->getSizes($type);
        } else {
            return array(
                'mainWidth'   => 900,
                //'mainHeight'  => 1200,
                'thumbWidth'  => 150,
                'thumbHeight' => 150,
            );
        }
    }
}