<?php
class Belvg_Colorswatch_Model_Observer
{
    public function initConfigurableBlockCache(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('belvgcolorswatch');
        $block  = $observer->getEvent()->getBlock();

        if ($helper->isEnabled() && $block instanceof Mage_Catalog_Block_Product_View_Type_Configurable/* && $block->getBlockAlias() == 'options_configurable'*/) {
            $block->addData(array(
                'cache_lifetime' => 3600,
                'cache_tags'     => array(Mage_Catalog_Model_Product::CACHE_TAG),
                'cache_key'      => Belvg_Colorswatch_Helper_Data::CACHE_KEY . $block->getProduct()->getId() . '_block_' . $block->getBlockAlias(),
            ));
        }
    }

    public function addSwatchesHtml(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('belvgcolorswatch');
        $block  = $observer->getEvent()->getBlock();
        if ($helper->isEnabled() && $block instanceof Mage_Catalog_Block_Product_View_Type_Configurable && $block->getBlockAlias() == 'options_configurable') {
            $transport = $observer->getEvent()->getTransport();

            if (preg_match_all('/<select name="super_attribute\[(\d+)\]" [^>]*>(.*?)<\/select>/s', $transport->getHtml(), $matches, PREG_SET_ORDER)) {
                $attributeIds = explode(',', Mage::getStoreConfig('attricons/settings/attributes'));
                $html         = $transport->getHtml();
                foreach ($matches AS $i => $match) {
                    if (in_array($match[1], $attributeIds)) {
                        $html = str_replace($match[0], '<div class="belvg-colorswatch"><div style="clear:both;"></div></div><div class="belvg-options-wrapper">' . $match[0] . '</div>', $html);
                    }
                }

                $transport->setHtml($html);
            }
        }
    }

    public function loadSwatchesToProductCollection(Varien_Event_Observer $observer)
    {
        $helper = Mage::helper('belvgcolorswatch');
        if ($helper->isEnabled()) {
            $optionLabels     = array();
            $swatches         = array();
            $swatchesObjects  = array();
            $optionIds        = array();
            $attributeIds     = array();

            $collection = $observer->getEvent()->getCollection();
            $filters    = $collection->getLimitationFilters();

            if (isset($filters['category_id']) && $filters['category_id']) {
                $setting = $helper->getListSettings();
                foreach ($collection AS $item) {
                    if ($item->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
                        $attributeCollection = $item->getTypeInstance()->getConfigurableAttributes();
                        foreach ($attributeCollection AS $attribute) {
                            $attributeId         = $attribute->getProductAttribute()->getAttributeId();
                            if (in_array($attributeId, $setting['category_attributes'])) {
                                $attributeCode   = $attribute->getProductAttribute()->getAttributeCode();
                                $attributeValues = $attribute->getProductAttribute()->getSource()->getAllOptions();
                                foreach ($attributeValues AS $attributeValue) {
                                    $optionLabels[$attributeValue['value']] = $attributeValue['label'];
                                }

                                $usedProducts = $item->getTypeInstance()->getUsedProducts();
                                foreach ($usedProducts AS $usedProduct) {
                                    $optionId = $usedProduct->getData($attributeCode);
                                    $optionIds[$optionId]    = $optionId;
                                    $attributeIds[$optionId] = $attributeId;

                                    if ($usedProduct->getSmallImage() && $usedProduct->getSmallImage() != 'no_selection') {
                                        $swatches[$item->getId()][$attributeId][$optionId]['img'] = Mage::helper('catalog/image')->init($usedProduct, 'small_image');

                                        $width  = (int) $helper->storeConfig('category/product_list_img_width');
                                        $height = (int) $helper->storeConfig('category/product_list_img_height');
                                        if ($width && $height) {
                                            $swatches[$item->getId()][$attributeId][$optionId]['img'] = (string) $swatches[$item->getId()][$attributeId][$optionId]['img']->resize($width, $height);
                                        } else {
                                            $swatches[$item->getId()][$attributeId][$optionId]['img'] = (string) $swatches[$item->getId()][$attributeId][$optionId]['img'];
                                        }
                                    } else {
                                        if (!isset($swatches[$item->getId()][$attributeId][$optionId]['img'])) {
                                            $swatches[$item->getId()][$attributeId][$optionId]['img'] = '';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (count($optionIds)) {
                    $swatchesAttribute = Mage::getModel('attricons/attribute')
                        ->getResourceCollection()
                        ->addFieldToFilter('option_id', array('in' => $optionIds));

                    $swatchesProduct = Mage::getModel('attricons/product')
                        ->getResourceCollection()
                        ->addFieldToFilter('value_index', array('in' => $optionIds));

                    foreach ($swatchesProduct AS $swatch) {
                        $swatchesObjects[$swatch->getProductId()][$attributeIds[$swatch->getValueIndex()]][$swatch->getValueIndex()] = $swatch;
                    }

                    foreach ($swatchesAttribute AS $swatch) {
                        if (!isset($swatchesObjects[0][$swatch->getAttributeId()][$swatch->getOptionId()])) {
                            $swatchesObjects[0][$swatch->getAttributeId()][$swatch->getOptionId()] = $swatch;
                        }
                    }

                    foreach ($swatches AS $productId => $attributes) {
                        foreach ($attributes AS $attributeId => $options) {
                            foreach ($options AS $optionId => $item) {
                                if (!isset($swatchesObjects[$productId][$attributeId][$optionId])) {
                                    if (isset($swatchesObjects[0][$attributeId][$optionId])) {
                                        $swatchesObjects[$productId][$attributeId][$optionId] = $swatchesObjects[0][$attributeId][$optionId];
                                    } else {
                                        $swatchesObjects[$productId][$attributeId][$optionId] = new Varien_Object;
                                    }
                                }

                                if (isset($optionLabels[$optionId])) {
                                    $swatches[$productId][$attributeId][$optionId]['icon'] = Mage::helper('attricons')->generateIcoCategoryHtml($swatchesObjects[$productId][$attributeId][$optionId], $optionLabels[$optionId], FALSE, FALSE, FALSE, $setting['catalog_icon_width'], $setting['catalog_icon_height']);
                                } else {
                                    unset($swatches[$productId][$attributeId][$optionId]);
                                }
                            }
                        }
                    }

                    foreach ($collection AS &$item) {
                        if (isset($swatches[$item->getId()])) {
                            $item->setData('swatches_data', $swatches[$item->getId()]);
                        }
                    }
                }
            }
        }

        return $this;
    }
}
