/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
/**********************************************
 *        MAGENTO EDITION USAGE NOTICE        *
 **********************************************/
/* This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
/**********************************************
 *        DISCLAIMER                          *
 **********************************************/
/* Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 **********************************************
 * @category   Belvg
 * @package    Belvg_Framer
 * @copyright  Copyright (c) 2010 - 2013 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

var Loader = {};
Loader.Files = Class.create();
Loader.Files.prototype = {
    containerId :'',
    container   : null,
    uploader    : null,
    template    : null,
    classPrefix : 'attributeicon-preview-',
    index       : 0,
    name        : '',
    initialize : function(containerId, name, prefix, uploader)
    {
        this.containerId = containerId, this.container = $(this.containerId);
        this.name        = name;
        this.classPrefix = prefix;

        // init template
        //this.template = new Template(this.getElement('template').innerHTML);

        // init uploader*/
        this.uploader = uploader;
        if (this.uploader) {
            this.uploader.onFilesComplete = this.handleUploadComplete.bind(this);
        }
    },

    createNewRow: function(json)
    {
        var index  = this.getIncIndex();
        var newRow = new Element('tr').addClassName('preview');
        this.template = new Template(this.getElement('template').innerHTML);
        newRow.update(this.template.evaluate(json));

        // id input
        var idInput      = this.getNewRowElement(newRow, 'id');
        if (typeof(json.id) !== 'undefined' && json.id) {
            idInput.name = this.getNewRowElementName('id', index);
            idInput.setValue(json.id);
        }
        
        // input path
        var idInput  = this.getNewRowElement(newRow, 'path');
        idInput.name = this.getNewRowElementName('path', index);
        idInput.setValue(json.path || '');

        // img
        var img = this.getNewRowElement(newRow, 'url');
        img.src = json.image;
        
        // radio Icon Image
        var icoInput          = this.getNewRowElement(newRow, 'image');
        if (icoInput.value == json.path) {
            icoInput.checked  = true;
        } else {
            icoInput.writeAttribute('value', json.path);
            icoInput.checked  = false;
        }
        
        // radio Icon Image of Category
        var icoCategoryInput          = this.getNewRowElement(newRow, 'image_category');
        if (icoCategoryInput.value == json.path) {
            icoCategoryInput.checked  = true;
        } else {
            icoCategoryInput.writeAttribute('value', json.path);
            icoCategoryInput.checked  = false;
        }

        // radio Description Image
        var zoomInput         = this.getNewRowElement(newRow, 'zoom');
        if (zoomInput.value == json.path) {
            zoomInput.checked = true;
        } else {
            zoomInput.writeAttribute('value', json.path);
            zoomInput.checked = false;
        }

        // checkbox remove
        var removeInput     = this.getNewRowElement(newRow, 'remove');
        removeInput.name    = this.getNewRowElementName('remove', index);
        removeInput.checked = false;

        return newRow;
    },

    getNewRowElementName: function(type, index)
    {
        var name = this.name + '[' + index + '][' + type + ']';

        return name;
    },

    getNewRowElement: function(newRow, type)
    {
        var element = newRow.select('.' + this.classPrefix + type).first();

        return element;
    },

    getIncIndex: function()
    {
        return this.index++;
    },

    addFileInfo: function(fileJson)
    {
        var newRow = this.createNewRow(fileJson);
        var list   = this.getElement('list');

        list.appendChild(newRow);
    },

    getElement : function(name)
    {
        return $(this.containerId + '_' + name);
    },

    handleUploadComplete: function(files)
    {
        files.each( function(item) {
            if (!item.response.isJSON()) {
                try {
                    alert(item.response);
                    console.log(item.response);
                } catch (e2) {
                    alert(item.response);
                }
                return;
            }

            var response = item.response.evalJSON();
            if (response.error) {
                alert(response.error); // TEST
                return;
            }

            // create new file
            var newFile   = {};
            newFile.image = response.url,
            newFile.path  = response.file;
            newFile.title = '';

            try {
                this.addFileInfo(newFile);
            } catch (e) {
                alert(e);
            }
            this.uploader.removeFile(item.id);
        }.bind(this));

        this.container.setHasChanges();
    }
};