/**************************** EXTEND CONFIGURABLE PRODUCT **************************/
Product.Config = Class.create(Product.Config, {
    initMedia: function()
    {
        if (typeof mediaZoomer != 'undefined') {
            this.config.BigImgLink     = $$(mediaZoomer.config.tagBigImgLink)[0];
            this.config.BigImg         = $$(mediaZoomer.config.tagBigImg)[0];
            this.config.MoreViewsBlock = $$(mediaZoomer.config.tagMoreViewsBlock)[0];
            this.config.MoreViews      = $$(mediaZoomer.config.tagMoreViews)[0];
            this.config.MoreViewLinks  = $$(mediaZoomer.config.tagMoreViewLinks);
        } else {
            this.config.BigImgLink     = $$('.mediaZoomerMain, .product-image')[0];
            this.config.BigImg         = $$('.mediaZoomerMain-image, .product-image img')[0];
            this.config.MoreViewsBlock = $$('.mediaZoomerGallery-wrapper, .more-views')[0];
            this.config.MoreViews      = $$('.mediaZoomerGallery, .more-views ul')[0];
            this.config.MoreViewLinks  = $$('.mediaZoomerGallery a, .more-views ul a');
        }
    },

    initialize: function(config)
    {
        this.config           = config;
        this.flagImageChanged = false;
        this.taxConfig        = this.config.taxConfig;
        if (config.containerId) {
            this.settings     = $$('#' + config.containerId + ' ' + '.super-attribute-select');
        } else {
            this.settings     = $$('.super-attribute-select');
        }

        this.state            = new Hash();
        this.prices           = config.prices;
        this.priceTemplate    = new Template(config.template);

        if (config.dataSwitcher.stockEnabled) {
            this.stockTemplate = new Template(config.dataSwitcher.stockMessageTemplate, new RegExp('(^|.|\\r|\\n)({{\\s*(\\w+)\\s*}})', ""));
        }

        if (config.imageSwitcher.notAvailable) {
            document.observe("dom:loaded", function() {
                $$('.product-image')[0].insert(config.imageSwitcher.notAvailableTemplate);
            });
        }

        // Set default values from config
        if (config.defaultValues) {
            this.values = config.defaultValues;
        }

        // Overwrite defaults by url
        var separatorIndex = window.location.href.indexOf('#');
        if (separatorIndex != -1) {
            var paramsStr = window.location.href.substr(separatorIndex+1);
            var urlValues = paramsStr.toQueryParams();
            if (!this.values) {
                this.values = {};
            }

            for (var i in urlValues) {
                this.values[i] = urlValues[i];
            }
        }

        // Overwrite defaults by inputs values if needed
        if (config.inputsInitialized) {
            this.values = {};
            this.settings.each(function(element) {
                if (element.value) {
                    var attributeId = element.id.replace(/[a-z]*/, '');
                    this.values[attributeId] = element.value;
                }
            }.bind(this));
        }

        // Put events to check select reloads
        this.settings.each(function(element){
            Event.observe(element, 'change', this.configure.bind(this))
        }.bind(this));

        // fill state
        this.settings.each(function(element) {
            var attributeId = element.id.replace(/[a-z]*/, '');
            if (attributeId && this.config.attributes[attributeId]) {
                element.config = this.config.attributes[attributeId];
                element.attributeId = attributeId;
                this.state[attributeId] = false;
            }
        }.bind(this))

        // Init settings dropdown
        var childSettings = [];
        for (var i=this.settings.length-1; i>=0; i--) {
            var prevAttribute = this.settings[i-1] ? this.settings[i-1] : false;
            var nextAttribute = this.settings[i+1] ? this.settings[i+1] : false;

            /*if (i == 0){
                this.fillSelect(this.settings[i])
            } else {
                this.settings[i].disabled = true;
            }*/

            active = (i == 0) ? true : false;
            this.fillSelect(this.settings[i], active);

            $(this.settings[i]).childSettings = childSettings.clone();
            $(this.settings[i]).prevAttribute = prevAttribute;
            $(this.settings[i]).nextAttribute = nextAttribute;
            childSettings.push(this.settings[i]);
        }

        // Set values to inputs
        this.configureForValues();
        document.observe("dom:loaded", this.configureForValues.bind(this));
    },

    inArray: function(what, where)
    {
        for (var i=0; i<where.length; i++) {
            if (what == 'attribute' + where[i]) {
                return true;
            }
        }

        return false;
    },

    configureElement: function(element)
    {
        this.reloadOptionLabels(element);
        if (element.value) {
            this.state[element.config.id] = element.value;
            if (element.nextAttribute) {
                // one of the attributes has selected
                element.nextAttribute.disabled = false;
                this.fillSelect(element.nextAttribute);
                this.resetChildren(element.nextAttribute);
                /*if (this.inArray(element.id, this.config.attributeIdsThatChange)) {
                    if (this.config.imageSwitcher.enabled) {
                        this.changeImages(element);
                    }
                }*/
            } else {
                // all attributes have selected
                if (this.config.imageSwitcher.enabled) {
                    this.changeImages(element, true);
                }
            }

            if (this.config.imageSwitcher.enabled && this.inArray(element.id, this.config.attributeIdsThatChange)) {
                this.changeImages(element, false);
            }

            this.updateProductData(element);
        } else {
            this.resetChildren(element);
            if (element.nextAttribute) {
                this.fillSelect(element.nextAttribute, false);
            }

            if (!element.prevAttribute) {
                this.setDefaultImages();
            }
        }

        this.reloadPrice();

        if(typeof changeDiscountLabel === 'function') {
            changeDiscountLabel();
        }
    },

    fillSelect: function(element, active)
    {
        active = typeof active !== 'undefined' ? active : true;

        var attributeId    = element.id.replace(/[a-z]*/, '');
        var options        = this.getAttributeOptions(attributeId);

                //console.log(options);

        this.clearSelect(element);
        element.options[0] = new Option(this.config.chooseText, '');

        var prevConfig     = false;
        if (element.prevAttribute && element.prevAttribute.selectedIndex) {
            prevConfig     = element.prevAttribute.options[element.prevAttribute.selectedIndex];
        }

        if (options) {
            var index = 1;
            var ul_el = new Element('ul');
            var swatchesContainer = element.up().previous();
            var li_el, a_el, ico;

            for (var i=0; i<options.length; i++) {
                var allowedProducts = [];
                if (prevConfig) {
                    for (var j=0; j<options[i].products.length; j++) {
                        if (prevConfig.config.allowedProducts && prevConfig.config.allowedProducts.indexOf(options[i].products[j]) > -1){
                            allowedProducts.push(options[i].products[j]);
                        }
                    }
                } else {
                    allowedProducts = options[i].products.clone();
                }

                options[i].imageProducts = options[i].products.clone();

                if (allowedProducts.size() > 0) {
                    options[i].allowedProducts = allowedProducts;
                    element.options[index]     = new Option(this.getOptionLabel(options[i], options[i].price), options[i].id);
                    if (typeof options[i].price != 'undefined') {
                        element.options[index].setAttribute('price', options[i].price);
                    }

                    element.options[index].config = options[i];
                    index++;
                }

                if (this.config.attributeIdsToChange.indexOf(attributeId) != -1) {
                    if (options[i].swatch) {
                        if (allowedProducts.size() > 0 || (allowedProducts.size() == 0 && this.config.showNotAvailable)) {
                            ico  = options[i].swatch;
                            a_el = new Element('a', {
                                href: 'javascript:void(0);',
                                rel: options[i].id
                            }).insert({bottom: ico});

                            a_el.observe('click', function(event) {
                                var a_clicked = Event.findElement(event, 'a'), li = a_clicked.up();

                                if (li.hasClassName('disabled')) {
                                    return false;
                                }
                                

                                if (li.hasClassName('active')) {
                                    event.preventDefault();
                                   return false;
                                    li.removeClassName('active');
                                    element.value = "";                                    
                                } else {
                                    li.addClassName('active').siblings().each(function(el) {
                                        el.removeClassName('active');
                                    });
                                    element.value = a_clicked.readAttribute('rel');
                                }

                                this.configureElement(element);
                            }.bind(this));

                            li_class = (active && allowedProducts.size() > 0) ? '' : 'disabled';
                            li_el    = new Element('li').addClassName(li_class).insert({bottom: a_el});
                            ul_el.insert({bottom: li_el});
                        }
                    }
                }
            }

            if (typeof swatchesContainer != 'undefined') {
                if (swatchesContainer.select('ul').size()>0) {
                    swatchesContainer.select('ul').each(function(el) {
                        el.remove();
                    });
                }

                swatchesContainer.insert({top: ul_el});
            }
        }

        //if (!active) {
            if (element.nextAttribute) {
                this.fillSelect(element.nextAttribute, false);
            }
        //}
    },

    updateProductData: function(element)
    {
        var productIds = element.options[element.selectedIndex].config.allowedProducts;

        if (productIds.length == 1) {
            var data = this.config.imageSwitcher.collection;

            if (element && element.selectedIndex) {
                var productId = element.options[element.selectedIndex].config.allowedProducts[0];
            } else {
                var productId = $this.config.productId;
            }
            //ranai;
            if (typeof data[productId] != 'undefined') {
                // QTY show

                if (this.config.dataSwitcher.stockEnabled && data[productId].qty <= this.config.dataSwitcher.stockUpperLimit) {
                    if ($$('.stock-message').length) {
                        $$('.stock-message')[0].replace(this.stockTemplate.evaluate(data[productId]));
                    } else {
                        /*if ($$('.product-options-bottom').length) {
                            Element.insert($$('.product-options-bottom')[0], {
                                top : this.stockTemplate.evaluate(data[productId])
                            });
                        }*/
                        if ($$('.availability').length) {
                            Element.insert($$('.availability')[0], {
                                bottom : this.stockTemplate.evaluate(data[productId])
                            });
                        }
                    }
                } else {
                    $$('.stock-message').invoke('hide');
                }
            }
            if(typeof viewCampaign === 'function') {
                viewCampaign(productId);
            }
        } else {
            $$('.stock-message').invoke('hide');
        }
    },

    changeImages: function(element, finalChange)
    {
        this.initMedia();

        if (this.config.MoreViews && this.config.MoreViewsBlock && this.config.BigImg && this.config.BigImgLink) {
            var $this  = this;
            var images = $this.config.imageSwitcher.collection;

            if (element && element.selectedIndex) {
                if (finalChange) {
                    var allowedProducts = element.options[element.selectedIndex].config.allowedProducts; // if an individual picture in all Simple
                    var productIds      = [allowedProducts[0]];
                } else {
                    var allowedProducts = element.options[element.selectedIndex].config.allowedProducts; // if an individual picture in all Simple
                    var productIds      = element.options[element.selectedIndex].config.imageProducts;   // if the picture is in a simple
                    productIds.unshift(allowedProducts[0]);
                }
            } else {
                var productIds = [$this.config.productId];
            }

            if (this.config.attributeIdsThatChange.length == 0 || (this.config.attributeIdsThatChange.length == 1 && this.config.attributeIdsThatChange[0] == 0)) {
                this.flagImageChanged = false;
            }

            for (var i=0; i<productIds.length; i++) {
                if (typeof images[productIds[i]] != 'undefined') {
                    if (typeof images[productIds[i]].main != 'undefined') {
                        $this.config.BigImg.writeAttribute('src', images[productIds[i]].main.resized);
                        $this.config.BigImgLink.writeAttribute('href', images[productIds[i]].main.original);

                        if (images[productIds[i]].thumbs.length) {
                            $this.config.MoreViews.select('li').each(function(li) {
                                li.remove();
                            });

                            var i = 0;
                            images[productIds[i]].thumbs.each(function(item) {
                                var a_el, img_el = new Element('img', {
                                    src   : item.thumb,
                                    width : images.sizes.thumbWidth,
                                    height: images.sizes.thumbHeight
                                });

                                if ($this.config.BigImgLink) {
                                    a_el = new Element('a', {
                                        href : "javascript:void(0);",
                                        rel  : "{gallery: 'belvgcolorswatch', smallimage: '" + item.resized + "', largeimage: '" + item.original + "'}"
                                    });
                                    a_el.writeAttribute('class', (i++ == 0) ? 'zoomThumbActive' : '');
                                } else {
                                    a_el = new Element('a', {href: 'javascript:void(0);'});
                                    a_el.observe('click', function(event) {
                                        var thumb = event.element().up();
                                        if (!thumb.hasClassName('zoomThumbActive')) {
                                            if ($this.config.BigImg) {
                                                $this.config.BigImg.writeAttribute('src', item.resized);
                                            }

                                            $this.config.MoreViews.select('a.zoomThumbActive').each(function(a) {
                                                a.removeClassName('zoomThumbActive');
                                            });
                                            thumb.addClassName('zoomThumbActive');
                                        }
                                    }.bind(this));
                                }

                                a_el.insert({bottom: img_el});
                                $this.config.MoreViews.insert({
                                    bottom: new Element('li').insert({bottom: a_el})
                                });
                            }.bind(this));
                            if ($this.config.MoreViewsBlock) {
                                $this.config.MoreViewsBlock.show();
                            }
                        } else {
                            if ($this.config.MoreViewsBlock) {
                                $this.config.MoreViewsBlock.hide();
                            }
                        }

                        // found
                        this.flagImageChanged = true;
                        if (typeof mediaZoomer != 'undefined') {
                            mediaZoomer.refresh();
                        } else {
                            if (typeof ProductMediaManager != 'undefined') {
                                this.refreshDefaultZoomer();
                            }
                        }

                        return true;
                    }
                }
            }

            // dont find
            // show message
            if (!this.flagImageChanged || this.inArray(element.id, this.config.attributeIdsThatChange)) {
                //console.log('dont find');
                $$('.not-available-message').invoke('show');
                setTimeout(function(){
                    $$('.not-available-message').invoke('hide');
                }, 2000);
            }

            return false;
        }
    },

    // ------------------------------------
    // ---- Customize for your zoomer -----
    // ------------------------------------
    refreshDefaultZoomer: function()
    {
        var img       = this.config.BigImg.cloneNode();
        img.addClassName('visible');
        var container = this.config.BigImg.up(0);
        container.update(img);

        var i = 0;
        $$('.mediaZoomerGallery a, .more-views ul a').each(function(link) {
            var colorSwatchRel = eval('(' + link.readAttribute('rel') + ')');
            if (typeof colorSwatchRel != 'undefined') {
                var img = '<img id="image-' + i + '" class="gallery-image" src="' + colorSwatchRel.smallimage + '" data-zoom-image="' + colorSwatchRel.largeimage + '">';
                container.insert(img);

                link.addClassName('thumb-link').writeAttribute('data-image-index', i);
                i++;
            }
        });

        ProductMediaManager.init();
    },
    // ------------------------------------
    // ------------------------------------

    setDefaultImages: function()
    {
        this.flagImageChanged = false;
        this.changeImages(false, false);
        this.flagImageChanged = false;
    }
});
