/**
 * BelVG LLC.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 *
/**********************************************
 *        MAGENTO EDITION USAGE NOTICE        *
 **********************************************/
/* This package designed for Magento COMMUNITY edition
 * BelVG does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * BelVG does not provide extension support in case of
 * incorrect edition usage.
/**********************************************
 *        DISCLAIMER                          *
 **********************************************/
/* Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future.
 **********************************************
 * @category   Belvg
 * @package    Belvg_Colorswatch
 * @copyright  Copyright (c) 2010 - 2012 BelVG LLC. (http://www.belvg.com)
 * @license    http://store.belvg.com/BelVG-LICENSE-COMMUNITY.txt
 */

var ColorswatchList = Class.create();
ColorswatchList.prototype = {
    initialize: function(config)
    {
        this.config = Object.extend({
            itemTags : '.category-products .item',
            settings : new Object,
            products : new Array
        }, config);
    },

    init: function(products)
    {
        this.config.productIds = products.ids;
        this.config.swatches   = products.swatches;

        this.initList();
        this.initAction();
    },

    initList: function()
    {
        var $this = this;

        $$(this.config.itemTags).each(function(item, index) {
            var productId  = $this.config.productIds[index];
            var attributes = '';

            for (var attributeId in $this.config.swatches[productId]) {
                attributes += '<ul class="colorswatch_attribute colorswatch_attribute' + attributeId + '">';
                for (var optionId in $this.config.swatches[productId][attributeId]) {

                    var swatch = $this.config.swatches[productId][attributeId][optionId];
                    var pic    = (typeof swatch.img == 'undefined') ? '' : 'rel="' + swatch.img + '"';
                        attributes += '<li ' + pic + '><a href="javascript:;" onclick="colorswatchList.updateImg(this)">' + swatch.icon + '</a></li>';
                }

                attributes += '</ul>';
            }

            var attrBlock = '<div class="belvg-colorswatch product-list-options" rel="' + productId + '">' + attributes + '</div>';
            if ($this.config.settings.imageSwitcher.notAvailable) {
                attrBlock += $this.config.settings.imageSwitcher.notAvailableTemplate;
            }

            item.insert({
                bottom : attrBlock
            });
        });
    },

    initAction: function()
    {

    },

    updateImg: function(item)
    {
        var block = item.up('li.item');
        var img   = block.select('img')[0];
        var icon  = item.up('li');
        var pic   = icon.readAttribute('rel');
        
        icon.up().select('li').each(function(li){            
            li.removeClassName('active');
        });        
        icon.addClassName('active');
        
        block.select('img').each(function(n){                
            if($(n).src.indexOf('catalog/product') != -1) {
               img = n;               
            }
        });
        
        if (pic && typeof pic != 'undefined') {            
            img.writeAttribute('src', pic);
        } else if (this.config.settings.imageSwitcher.notAvailable) {
            block.select('.not-available-message').invoke('show');
            setTimeout(function(){
                block.select('.not-available-message').invoke('hide');
            }, 2000);
        }
    }
}
