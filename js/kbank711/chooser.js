var gridChooser = new Class.create();
gridChooser.prototype = {
    initialize : function(parent){
        this.parent = $(parent);
        this.updateElement = null;
        this.chooserSelectedItems = $H({});
        this.readOnly = false;

        var elems = this.parent.getElementsByClassName('rule-param');
        for (var i=0; i<elems.length; i++) {
            this.initParam(elems[i]);
        }
    },
    initParam: function (container) {
        container.rulesObject = this;
        var elem = Element.down(container, '.element');
        if (elem) {
            var trig = elem.down('.rule-chooser-trigger');
            var apply = elem.down('.rule-param-apply');
            if (trig) {
                Event.observe(trig, 'click', this.toggleChooser.bind(this, container));
            }
			if(apply) {
				Event.observe(apply, 'click', this.cleanChooser.bind(this,apply));
			}
        }
    },

    showChooserElement: function (chooser) {
		parent = chooser.up('li');
		this.updateElement = parent.down('.element-value-changer');
		if(!this.updateElement) {
			return;
		}
        this.chooserSelectedItems = $H({});
        if (chooser.hasClassName('no-split')) {
            this.chooserSelectedItems.set(this.updateElement.value, 1);
        } else {
            var values = this.updateElement.value.split(','), s = '';
            for (i=0; i<values.length; i++) {
                s = values[i].strip();
                if (s!='') {
                   this.chooserSelectedItems.set(s,1);
                }
            }
        }
        new Ajax.Request(chooser.getAttribute('url'), {
            evalScripts: true,
            parameters: {'form_key': FORM_KEY, 'selected[]':this.chooserSelectedItems.keys() },
            onSuccess: function(transport) {
                if (this._processSuccess(transport)) {
					parent.down('.rule-param-apply').show();
                    $(chooser).update(transport.responseText);
                    this.showChooserLoaded(chooser, transport);
                }
            }.bind(this),
            onFailure: this._processFailure.bind(this)
        });
    },

    showChooserLoaded: function(chooser, transport) {
        chooser.style.display = 'block';
    },

    showChooser: function (container, event) {
        var chooser = container.up('li');
        if (!chooser) {
            return;
        }
        chooser = chooser.down('.rule-chooser');
        if (!chooser) {
            return;
        }
        this.showChooserElement(chooser);
    },

    hideChooser: function (container, event) {
        var chooser = container.up('li');
        if (!chooser) {
            return;
        }
        chooser = chooser.down('.rule-chooser');
        if (!chooser) {
            return;
        }
        chooser.style.display = 'none';
    },

    toggleChooser: function (container, event) {
        if (this.readOnly || !container) {
            return false;
        }
		curr = container.up('li').down('.rule-chooser');
		var elems = this.parent.getElementsByClassName('rule-chooser');
        for (var i=0; i<elems.length; i++) {
            if(elems[i] !=curr) {				
				this.cleanChooser(elems[i], event);
			}
        }
        if (curr.style.display=='block') {
            this.cleanChooser(container, event);
        } else {			
            this.showChooserElement(curr);
        }
    },

    cleanChooser: function (container, event) {
        var chooser = container.up('li').down('.rule-chooser');
        if (!chooser) {
            return;
        }
		container.up('li').down('.rule-param-apply').hide();		
		chooser.style.display = 'none';
        chooser.innerHTML = '';
    },

    _processSuccess : function(transport) {
        if (transport.responseText.isJSON()) {
            var response = transport.responseText.evalJSON()
            if (response.error) {
                alert(response.message);
            }
            if(response.ajaxExpired && response.ajaxRedirect) {
                setLocation(response.ajaxRedirect);
            }
            return false;
        }
        return true;
    },

    _processFailure : function(transport) {
        location.href = BASE_URL;
    },
	chooserGridInit: function (grid) {
        //grid.reloadParams = {'selected[]':this.chooserSelectedItems.keys()};
    },

    chooserGridRowInit: function (grid, row) {
        if (!grid.reloadParams) {
            grid.reloadParams = {'selected[]':this.chooserSelectedItems.keys()};
        }
    },

    chooserGridRowClick: function (grid, event) {
        var trElement = Event.findElement(event, 'tr');
        var isInput = Event.element(event).tagName == 'INPUT';
        if (trElement) {
            var checkbox = Element.select(trElement, 'input');
            if (checkbox[0]) {
                var checked = isInput ? checkbox[0].checked : !checkbox[0].checked;
                grid.setCheckboxChecked(checkbox[0], checked);

            }
        }
    },

    chooserGridCheckboxCheck: function (grid, element, checked) {
        if (checked) {
            if (!element.up('th')) {
                this.chooserSelectedItems.set(element.value,1);
            }
        } else {
            this.chooserSelectedItems.unset(element.value);
        }
        grid.reloadParams = {'selected[]':this.chooserSelectedItems.keys()};
        this.updateElement.value = this.chooserSelectedItems.keys().join(', ');
    }
}
